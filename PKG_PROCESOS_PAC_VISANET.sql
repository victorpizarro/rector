create or replace PACKAGE PKG_PROCESOS_PAC_VISANET IS

  /******************************************************************************
     NAME:       PKG_PROCESOS_PAC_VISANET
     PURPOSE:
  
     REVISIONS:
     ----------------------------------------------------------------------------
     VER        DATE        AUTHOR           DESCRIPTION
     ----------------------------------------------------------------------------
     1.0        11-06-2008  LUIS MORA D.     CREATED THIS PACKAGE.
     ----------------------------------------------------------------------------
     1.1        01-09-2009  LUIS MORA D.     SE AGREGA FUNCION DE CUADRATURA
                                             DE RESPUESTA DE PROCESO PAC.
     ----------------------------------------------------------------------------
     --[ER-RECTOR-PROYMOROS-1]-19-07-2011 -SGALLEGOS-SE AGREGA CONTROL PARA QUE SE SUME LOS INTENTOS SOLO SI ES DIA PAC PARA EL MEDIO DE PAGO PROCESADO.
     ----------------------------------------------------------------------------
     V.1.1.1   04-06-2013   SERGIO GALLEGOS  EN PROC.CARGA_ARCH_RESP_CARGOS_PAC SE ASIGNA VALOR 000 YA QUE INSERTABA EN CART CARGOS CON V_CACA_CD_ORIGEN := 20;
               06-06-2013   SERGIO GALLEGOS  EN PROC.CARGA_ARCH_RESP_CARGOS_PAC,EN INSERCION EN CART_CARGOS, SE INSERTA EN FECHA_CMR LA FECHA ENVIO PAC. 
               10-03-2014   YGP SE MODIFICA  GENERA_ARCHIVO_INSCRIPCION PARA QUE FECHA ESTIMADA DE COBRO SIEMPRE SEA MAYOR O IGUAL A 48 HRS SEGUN VALIDACIN DE VISANET. 
               03-05-2017   RCV CAMBIA ORDEN DE COLUMNAS DE FECHAS EN LINEA 3731 
     ----------------------------------------------------------------------------
  
  ******************************************************************************/
  ---------------------------------------------------------------------------
  -- STATUS DE OPERACION, TABLA SCO_OP_DIARIAS --
  CDOP_ST_PENDIENTE CONSTANT NUMBER(2) := 0;

  CDOP_ST_TRANSF_CUADR_DIARIA CONSTANT NUMBER(2) := 1;

  ---------------------------------------------------------------------------
  -- MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
  CCTE_TRASP_ENV_CARGOS_SI CONSTANT VARCHAR2(1) := 'S';

  CCTE_TRASP_ENV_CARGOS_NO CONSTANT VARCHAR2(1) := 'N';

  ---------------------------------------------------------------------------
  -- MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
  CCTE_MOVTO_DEBE CONSTANT VARCHAR2(1) := 'D';

  CCTE_MOVTO_HABER CONSTANT VARCHAR2(1) := 'H';

  ---------------------------------------------------------------------------
  -- TIPO DE MOVIMIENTO, TABLA SCO_CUENTA_CORRIENTE --
  CCTE_TP_MOVTO_CARGO_NOR CONSTANT NUMBER(2) := 1;

  CCTE_TP_MOVTO_CARGO_DIR CONSTANT NUMBER(2) := 2;

  CCTE_TP_MOVTO_ABONO_DIR CONSTANT NUMBER(2) := 3;

  CCTE_TP_MOVTO_CARGO_DIR_SIN CONSTANT NUMBER(2) := 4;

  CCTE_TP_MOVTO_ABONO_DIR_SIN CONSTANT NUMBER(2) := 5;

  CCTE_TP_MOVTO_ABONO_DIR_CAJA CONSTANT NUMBER(2) := 6;

  CCTE_TP_MOVTO_ABONO_DIR_CHEQ CONSTANT NUMBER(2) := 7;

  CCTE_TP_MOVTO_ABONO_DIR_INT CONSTANT NUMBER(2) := 8;

  CCTE_TP_MOVTO_CARGO_WEBPAY CONSTANT NUMBER(2) := 13;

  ---------------------------------------------------------------------------
  -- STATUS DE CUENTA CORRIENTE, TABLA SCO_CUENTA_CORRIENTE --
  CCTE_ST_PENDIENTE CONSTANT NUMBER(2) := 0;

  CCTE_ST_ANULADO CONSTANT NUMBER(2) := 1;

  CCTE_ST_REZAGADO CONSTANT NUMBER(2) := 4;

  CCTE_ST_ACEPTADO CONSTANT NUMBER(2) := 5;

  CCTE_ST_CUOTA_GRATIS CONSTANT NUMBER(2) := 11;

  CCTE_ST_NO_REALIZADO CONSTANT NUMBER(2) := 15;

  CCTE_ST_CARGO_CAJA_CMR CONSTANT NUMBER(2) := 30;

  CCTE_ST_SIN_MEDIO_PAGO CONSTANT NUMBER(2) := 69;

  ---------------------------------------------------------------------------
  -- STATUS DE CUADRATURA, TABLA SCO_CUADRATURA_DIARIA --
  SCCD_ST_CERT_ACEP CONSTANT NUMBER := 0;

  SCCD_ST_CERT_RECH CONSTANT NUMBER := 1;

  SCCD_ST_INSC_RECH CONSTANT NUMBER := 2;

  SCCD_ST_INSC_ACEP_SIN_CTACTE CONSTANT NUMBER := 3;

  SCCD_ST_ERR_NO_EXIST_PPTA CONSTANT NUMBER := 4;

  SCCD_ST_SIN_ANULACION_CURSADA CONSTANT NUMBER := 5;

  SCCD_ST_CON_ANULACION_CURSADA CONSTANT NUMBER := 6;

  SCCD_ST_ERR_NO_EXIST_PRIM_MON CONSTANT NUMBER := 7;

  SCCD_ST_INICIAL_TRANSFERENCIA CONSTANT NUMBER := 8;

  SCCD_ST_ERR_NO_EXIST_PRODPAC CONSTANT NUMBER := 9;

  SCCD_ST_PRODUCTO_NO_PAC CONSTANT NUMBER := 10;

  SCCD_ST_ERR_GEN_INSC_PROD_PAC CONSTANT NUMBER := 11;

  SCCD_ST_ERR_GEN_INSC_DAT_CLI CONSTANT NUMBER := 12;

  SCCD_ST_ERR_GEN_INSC_PROD_REC CONSTANT NUMBER := 13;

  SCCD_ST_ERR_GEN_INSC_CTA_VN CONSTANT NUMBER := 14;

  SCCD_ST_ERR_GEN_INSC_INS_REG CONSTANT NUMBER := 15;

  SCCD_ST_INSC_ACEP_CON_CTACTE CONSTANT NUMBER := 16;

  SCCD_ST_INSC_CAMBIO_CTA_VN CONSTANT NUMBER := 17;

  SCCD_ST_INSC_PENDIENTE_ENV CONSTANT NUMBER := 18;

  SCCD_ST_INSC_RECEPCIONADA CONSTANT NUMBER := 19;

  SCCD_ST_ERR_GEN_INSC_SIN_CCTE CONSTANT NUMBER := 20;

  SCCD_ST_ERR_GEN_INSC_ID_BANCO CONSTANT NUMBER := 21;

  SCCD_ST_INSC_EN_PROCESO_RESP CONSTANT NUMBER := 22;

  SCCD_ST_CERT_PEND CONSTANT NUMBER := 23;

  SCCD_ST_VENCIDA_RECTOR CONSTANT NUMBER := 24;

  SCCD_ST_SUSPENDIDA_RECTOR CONSTANT NUMBER := 25;

  SCCD_ST_NO_RENOVADA_RECTOR CONSTANT NUMBER := 26;

  SCCD_ST_INSC_SIN_RESP_SIN_CCTE CONSTANT NUMBER := 27;

  SCCD_ST_INSC_SIN_RESP_CON_CCTE CONSTANT NUMBER := 28;

  SCCD_ST_INSC_CAMBIO_MEDIO_PAGO CONSTANT NUMBER := 29;

  SCCD_ST_INSC_DESEST_CARGOS CONSTANT NUMBER := 30;

  SCCD_ST_ERR_GEN_INSC_BOL_LIP CONSTANT NUMBER := 31;

  SCCD_ST_ERR_GEN_INSC_DFAC_LIP CONSTANT NUMBER := 32; -- DIA DE FACTURACION
  SCCD_ST_ERR_GEN_INSC_CTA_BCO  CONSTANT NUMBER := 33;

  -- MMA, 17-04-2012 --
  SCCD_ST_ERR_CSBF CONSTANT NUMBER := 34;

  SCCD_ST_ERR_GEN_CTA_CTE_RECIBO CONSTANT NUMBER := 35;

  SCCD_ST_ERR_PTA_NULA_EN_RECTOR CONSTANT NUMBER := 36;

  SCCD_ST_ERR_FRAGMENT_RECIBOS CONSTANT NUMBER := 37;

  SCCD_ST_ERR_RECIBO_OP_DIARIA CONSTANT NUMBER := 38;

  SCCD_ST_ERR_FECHA_PAC_CERT CONSTANT NUMBER := 39;

  SCCD_ST_ERR_PROPUESTA_NO_VIG CONSTANT NUMBER := 40;

  ---------------------------------------------------------------------------
  -- STATUS DE INSCRIPCION, TABLA SCO_INSCRIPCION --
  SCOI_ST_PENDIENTE_ENVIO CONSTANT NUMBER := 0;

  SCOI_ST_INSC_ACEP CONSTANT NUMBER := 1;

  SCOI_ST_INSC_RECH CONSTANT NUMBER := 2;

  SCOI_ST_CAMBIO_CUENTA_VN CONSTANT NUMBER := 4;

  SCOI_ST_INSC_ACEP_DIA_PAG_CERO CONSTANT NUMBER := 5;

  SCOI_ST_CAMBIO_MEDIO_PAGO CONSTANT NUMBER := 6;

  SCOI_ST_DESEST_CARGOS CONSTANT NUMBER := 7;

  SCOI_ST_INICIAL_SIN CONSTANT NUMBER := 10;

  SCOI_ST_INSC_ACEP_SIN CONSTANT NUMBER := 11;

  SCOI_ST_INSC_RECH_SIN CONSTANT NUMBER := 12;

  --SMB CONTROL DE CAMBIOS DE CUENTA SOE NUEVO STATUS PARA LOS REGISTROS SIN INSCRIPCION
  -- QUE NO SE TIENE QUE DAR DE BAJA ALA CUENTA PORQUE NO EXISTIA
  SCOI_ST_SIN_INSCRIPCION CONSTANT NUMBER := 13;

  ---------------------------------------------------------------------------
  -- TIPO DE INSCRIPCION, TABLA SCO_INSCRIPCION --
  SCOI_TP_INSC_GENERAL CONSTANT NUMBER := 0;

  SCOI_TP_INSC_NO_MASIVOS CONSTANT NUMBER := 1;

  SCOI_TP_INSC_MASIVOS CONSTANT NUMBER := 2;

  SCOI_TP_INSC_SIGLOXXI CONSTANT NUMBER := 4;

  SCOI_TP_INSC_HONDA CONSTANT NUMBER := 5;

  ---------------------------------------------------------------------------
  -- TIPO DE ARCHIVO, TABLA SCO_PLANO_ENVIO_CARGOS --
  TP_ARCH_GENERAL CONSTANT NUMBER := 0;

  TP_ARCH_NO_MASIVOS CONSTANT NUMBER := 1;

  TP_ARCH_MASIVOS CONSTANT NUMBER := 2;

  TP_ARCH_RENDICION CONSTANT NUMBER := 3;

  TP_ARCH_SIGLOXXI CONSTANT NUMBER := 4;

  TP_ARCH_HONDA CONSTANT NUMBER := 5;

  ---------------------------------------------------------------------------
  -- TIPO DE ARCHIVO TABLA SCOH_BITACORA_PROC_PAC_ARCH --
  SCHPPA_TP_ARCH_GENERAL CONSTANT NUMBER := 0;

  SCHPPA_TP_ARCH_NO_MASIVOS CONSTANT NUMBER := 1;

  SCHPPA_TP_ARCH_MASIVOS CONSTANT NUMBER := 2;

  SCHPPA_TP_ARCH_RENDICION CONSTANT NUMBER := 3;

  SCHPPA_TP_ARCH_SIGLOXXI CONSTANT NUMBER := 4;

  SCHPPA_TP_ARCH_HONDA CONSTANT NUMBER := 5;

  ---------------------------------------------------------------------------
  -- MOTIVOS DE ENDOSOS EN TABLA SCOH_INSCRIPCION --
  CAME_CD_MOTIVO_CAMB_CUENTA_VN CONSTANT NUMBER := 4;

  CAME_CD_MOTIVO_CAMB_MED_PAGO CONSTANT NUMBER := 520;

  ---------------------------------------------------------------------------
  -- CODIGO DE ALTAS Y BAJAS VISANET --
  CODIGO_ALTA_INSC_VN CONSTANT VARCHAR2(1) := 'A';

  CODIGO_BAJA_INSC_VN CONSTANT VARCHAR2(1) := 'B';

  -- SMB CONTROL DE CAMBIO SOE SE DEBE AGREGAR UN NUEVO TIPO DE MOVIEMITNO
  --MODIFICACION PARA LAS MODIFICACIONES DE CUENTA TRANSBANK
  CODIGO_MODIFICA_INSC_VN CONSTANT VARCHAR2(1) := 'M';

  ---------------------------------------------------------------------------
  -- TIPO DE TRANSACCION EN TABLA SCOH_INSCRIPCION --
  CAME_TP_TRANSAC_END_CUALIT CONSTANT VARCHAR2(1) := 'L';

  CAME_TP_TRANSAC_ANULACION CONSTANT VARCHAR2(1) := 'A';

  ---------------------------------------------------------------------------
  CODIGO_OPERACION_TRASP_CARTERA CONSTANT NUMBER(4) := 9999;

  CODIGO_OPERACION_VENTA_NUEVA CONSTANT NUMBER(4) := 1001;

  CODIGO_OPERACION_ANULACION CONSTANT NUMBER(4) := 1003;

  ---------------------------------------------------------------------------
  CODIGO_MEDIO_PAGO_SIN_MED_PAG CONSTANT NUMBER := 0;

  CODIGO_MEDIO_PAGO_CMR CONSTANT NUMBER := 5;

  CODIGO_MEDIO_PAGO_VN CONSTANT NUMBER := 8;

  ---------------------------------------------------------------------------
  CODIGO_MONEDA_PESOS CONSTANT VARCHAR2(2) := '01';

  CODIGO_MONEDA_UF CONSTANT VARCHAR2(2) := '02';

  CODIGO_MONEDA_DOLAR CONSTANT VARCHAR2(2) := '03';

  ---------------------------------------------------------------------------
  SCCMP_ID_SIC_CMR CONSTANT NUMBER := 1;

  SCCMP_ID_CMR_SIC CONSTANT NUMBER := 2;

  SCCMP_ID_MANDATO_BANC_CMR CONSTANT NUMBER := 3;

  SCCMP_ID_MANDATO_BANC_SIC CONSTANT NUMBER := 4;

  ---------------------------------------------------------------------------
  SCDAI_ID_PROCESO_ENV CONSTANT VARCHAR2(1) := 'E';

  SCDAI_ID_PROCESO_RESP CONSTANT VARCHAR2(1) := 'R';

  SCDAI_ID_PROCESO_ERR CONSTANT VARCHAR2(1) := 'X';

  ---------------------------------------------------------------------------
  SCDAC_ID_PROCESO_ENV CONSTANT VARCHAR2(1) := 'E';

  SCDAC_ID_PROCESO_RESP CONSTANT VARCHAR2(1) := 'R';

  --SCDAC_ID_PROCESO_REND          CONSTANT VARCHAR2(1) := 'C'; --RENDICION DE CARGOS
  SCDAC_ID_PROCESO_ERR CONSTANT VARCHAR2(1) := 'X';

  ---------------------------------------------------------------------------
  -- ESTADOS DE CART_CERTIFICADOS --
  CACE_ST_CERT_VIGENTE CONSTANT NUMBER := 1;

  CACE_ST_CERT_VENCIDA CONSTANT NUMBER := 7;

  CACE_ST_CERT_ANULADA CONSTANT NUMBER := 11;

  CACE_ST_CERT_NO_RENOVADA CONSTANT NUMBER := 14;

  CACE_ST_CERT_MIGRACION     CONSTANT NUMBER := 15; --- <JLG> SRF_74849 03122014
  CACE_ST_CERT_EN_RENOVACION CONSTANT NUMBER := 95;

  ---------------------------------------------------------------------------
  -- 
  SCDEI_TP_REGISTRO_ALTA CONSTANT CHAR(01) := 'A';

  SCDEI_TP_REGISTRO_BAJA CONSTANT CHAR(01) := 'B';

  SCDEI_TP_REGISTRO_MODIFICACION CONSTANT CHAR(01) := 'M';

  ---------------------------------------------------------------------------
  -- ESTADOS DE LOS RECIBOS DE RECTOR CART_RECIBOS
  CARE_ST_RECIBO_PENDIENTE CONSTANT NUMBER := 1;

  CARE_ST_RECIBO_REZAGADO CONSTANT NUMBER := 2;

  CARE_ST_RECIBO_ANULADO CONSTANT NUMBER := 3;

  CARE_ST_RECIBO_COBRADO CONSTANT NUMBER := 4;

  CARE_ST_RECIBO_DEVUELTO CONSTANT NUMBER := 5;

  CARE_ST_RECIBO_COBRO_DIF CONSTANT NUMBER := 6;

  CARE_ST_RECIBO_CUOTA_GRATIS CONSTANT NUMBER := 7;

  CARE_ST_RECIBO_NO_REALIZADO CONSTANT NUMBER := 8;

  CARE_ST_RECIBO_SIN_MED_PAGO CONSTANT NUMBER := 9;

  ---------------------------------------------------------------------------
  -- TIPO DE CARGO NSCRIPCION VISANET 
  TP_CARGO_FIJO CONSTANT CHAR(01) := 'F';

  TP_CARGO_VARIABLE CONSTANT CHAR(01) := 'V';

  ---------------------------------------------------------------------------
  -- PERIODICIDAD INSCRIPCION VISANET 
  TP_PERIODICIDAD_MENSUAL NUMBER(02) := 1;

  TP_PERIODICIDAD_BIMENSUAL NUMBER(02) := 2;

  TP_PERIODICIDAD_TRIMESTRAL NUMBER(02) := 3;

  TP_PERIODICIDAD_SEMESTRAL NUMBER(02) := 6;

  TP_PERIODICIDAD_ANUAL NUMBER(02) := 12;

  TP_PERIODICIDAD_SEMANAL NUMBER(02) := 13;

  TP_PERIODICIDAD_QUINCENAL NUMBER(02) := 14;

  ---------------------------------------------------------------------------
  -- MONEDA INSCRIPCION VISANET 
  TP_MONEDA_SOL NUMBER(03) := 604;

  TP_MONEDA_DOLAR NUMBER(03) := 840;

  ---------------------------------------------------------------------------
  -- MMA, 24-02-2012  --
  -- TIPO DE PROCESO DE RESPUESTA --
  SCDR_TIPO_PROCESO_CARGO CONSTANT VARCHAR2(01) := 'C';

  --DPKPE 291
  SCDR_TIPO_PROCESO_ABONO CONSTANT VARCHAR2(01) := 'A';

  --DPKPE 291
  SCDR_TIPO_PROCESO_INSCRIPCION CONSTANT VARCHAR2(01) := 'I';

  -- ESTADO DE RESPUESTA --
  SCDRD_ESTADO_ACTIVO CONSTANT VARCHAR2(01) := 'A';

  SCDRD_ESTADO_INACTIVO CONSTANT VARCHAR2(01) := 'I';

  -- TIPO DE RESPUESTA --
  SCDRD_TIPO_RESPUESTA_ACEPTADO CONSTANT VARCHAR2(01) := 'A';

  SCDRD_TIPO_RESPUESTA_RECHAZADO CONSTANT VARCHAR2(01) := 'R';

  -- CAMPO DE CONTROL DE REGISTROS ENVIADOS Y RESPONDIDOS EN LOS PROCESOS DE COBRANZA --
  CCTE_CAMPO7_PEND_ENV CONSTANT NUMBER(2) := 0;

  CCTE_CAMPO7_RESP CONSTANT NUMBER(2) := 1;

  CCTE_CAMPO7_ENV CONSTANT NUMBER(2) := 2;

  -- ESTADO DE LOS REZAGOS DE RECTOR BATB_CUOTAS_REZAGADAS
  BATB_ST_PAC_EN_PROCESO CONSTANT NUMBER := 1;

  BATB_ST_PAC_CARGADA CONSTANT NUMBER := 2;

  BATB_ST_PAC_RECHAZADA CONSTANT NUMBER := 3;

  BATB_ST_PAC_ESPERANDO_RESP CONSTANT NUMBER := 4;

  BATB_ST_PAC_NO_INFORMADA CONSTANT NUMBER := 5;

  BATB_ST_PAC_ANULADA CONSTANT NUMBER := 6;

  -- CODIGOS DE PAISES --
  CODIGO_PAIS_CHILE CONSTANT NUMBER := 1;

  CODIGO_PAIS_PERU CONSTANT NUMBER := 2;

  CODIGO_PAIS_ARGENTINA CONSTANT NUMBER := 3;

  CODIGO_PAIS_COLOMBIA CONSTANT NUMBER := 4;

  TIPO_ARCHIVO_EXTORNOS CONSTANT NUMBER := 1;

  ---------------------------------------------------------------------------
  TYPE TCURSOR IS REF CURSOR;

  ---------------------------------------------------------------------------------------------------
  -- TIPO DE REGISTRO RESPUESTA DE MOVIMIENTOS TRANSBANK --
  TYPE TRESP_NOMINA_MOVIMIENTOS IS RECORD(
    TIPO_REGISTRO      VARCHAR2(100),
    CODIGO_COMERCIO    VARCHAR2(100),
    ID_SERVICIO        VARCHAR2(100),
    MONTO_PESOS        VARCHAR2(100),
    RUT_TARJETA_HAB    VARCHAR2(100),
    NUMERO_TARJETA     VARCHAR2(100),
    FECHA_EXPIRACION   VARCHAR2(100),
    TIPO_INSTRUCCION   VARCHAR2(100),
    ORIGEN             VARCHAR2(100),
    ESTADO_INST_CARGO  VARCHAR2(100),
    FECHA_TBK_ING_ALTA VARCHAR2(100),
    HORA_TBK_ING_ALTA  VARCHAR2(100),
    FECHA_TBK_ING_BAJA VARCHAR2(100),
    HORA_TBK_ING_BAJA  VARCHAR2(100),
    FECHA_TBK_ING_MOD  VARCHAR2(100),
    HORA_TBK_ING_MOD   VARCHAR2(100),
    NOMBRE_CAMPA�A     VARCHAR2(100),
    DIAS_CORRIDOS_EXC  VARCHAR2(100),
    CODIGO_RESPUESTA   VARCHAR2(100),
    GLOSA_RESPUESTA    VARCHAR2(100),
    OBSERVACIONES      VARCHAR2(100),
    RESERVADO_USO_FUT  VARCHAR2(100));

  ---------------------------------------------------------------------------
  -- TIPO DE REGISTRO RESPUESTA DE MOVIMIENTOS TRANSBANK --
  TYPE TRESP_NOMINA_CARGOS IS RECORD(
    TIPO_TRANSACCION    VARCHAR2(100),
    MONTO_TRANSACCION   VARCHAR2(100),
    NUMERO_TARJETA      VARCHAR2(100),
    FECHA_EXPIRACION    VARCHAR2(100),
    FILLER1             VARCHAR2(100),
    NOMBRE_TARJETA_HAB  VARCHAR2(100),
    DESCRIPCION_SERV    VARCHAR2(100),
    TEL_TARJETA_HAB     VARCHAR2(100),
    ID_SERVICIO         VARCHAR2(100),
    RUT_TARJETA_HAB     VARCHAR2(100),
    REF_COBRO_EFECTUADO VARCHAR2(100),
    CODIGO_AUTORIZACION VARCHAR2(100),
    CODIGO_RESPUESTA    VARCHAR2(100),
    GLOSA_RESPUESTA     VARCHAR2(100),
    FILLER2             VARCHAR2(100),
    FILLER3             VARCHAR2(100),
    FECHA_PROCESO       VARCHAR2(100),
    OBSERVACIONES       VARCHAR2(100));

  ---------------------------------------------------------------------------
  FUNCTION FN_DEVOLVER_ID(PE_CADENA IN OUT VARCHAR2) RETURN BOOLEAN; --DPKPE-563
  ---------------------------------------------------------------------------
  PROCEDURE RECORRER_LINEA(PE_LINEA IN VARCHAR2,
                           PE_ID    IN OUT VARCHAR2); --DPKPE-563    
  ---------------------------------------------------------------------------
  FUNCTION FN_DIA_PAGO_VNET(PE_SODP_MEDIO_PAGO IN SCO_DIA_PAGO.SODP_MEDIO_PAGO%TYPE,
                            PE_SODP_DIA_PAC    IN SCO_DIA_PAGO.SODP_DIA_PAC%TYPE,
                            PS_SODP_DIA_PAGO   IN OUT SCO_DIA_PAGO.SODP_DIA_PAGO%TYPE,
                            PS_MENSAJE_ERROR   IN OUT VARCHAR2)
    RETURN BOOLEAN;

  ---------------------------------------------------------------------------  
  FUNCTION GENERA_REG_INSCRIPCION_X_PPTA(PE_NU_PROPUESTA  IN NUMBER,
                                         PS_MENSAJE_ERROR IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -- GENERACION DE REGISTROS DE INSCRIPCION --
  FUNCTION GENERA_REGISTRO_INSCRIPCION(PE_PROPUESTA            IN SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE,
                                       PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                       PS_REGISTROS_CREADOS    IN OUT NUMBER,
                                       PS_REGISTROS_ERRONEOS   IN OUT NUMBER,
                                       PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN;

  ---------------------------------------------------------------------------
  -- REINSCRIPCION DE PROPUESTAS --
  FUNCTION REINSCRIPCION(PS_MENSAJE_ERROR    IN OUT VARCHAR2,
                         PS_REGISTROS_REINSC IN OUT NUMBER) RETURN BOOLEAN;

  ---------------------------------------------------------------------------
  -- GENERACION DE ARCHIVO DE INSCRIPCION --
  FUNCTION GENERA_ARCHIVO_INSCRIPCION(PE_TP_INSCRIPCION       IN NUMBER,
                                      PE_FECHA_INSCRIPCION    IN DATE,
                                      PE_ID_USUARIO           IN VARCHAR2,
                                      PE_ID_TERMINAL          IN VARCHAR2,
                                      PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                      PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN;

  ---------------------------------------------------------------------------
  -- CARGA DE RESPUESTA DE INSCRIPCION TRANSBANK --
  FUNCTION CARGA_RESPUESTA_INSCRIPCION(PE_TP_INSCRIPCION       IN NUMBER,
                                       PE_FECHA_INSCRIPCION    IN DATE,
                                       PE_ID_USUARIO           IN VARCHAR2,
                                       PE_ID_TERMINAL          IN VARCHAR2,
                                       PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                       PS_REGISTROS_ACEPTADOS  IN OUT NUMBER,
                                       PS_REGISTROS_RECHAZADOS IN OUT NUMBER,
                                       PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN;

  ----------------------------------------------------------------------------
  -- OBTENER DATOS DE RESPUESTA DE MOVIMIENTOS, MEDIO DE PAGO TRANSBANK --
  --FUNCTION OBTENER_DATOS_RESP_MOV (
  --                                  PE_STR           IN  VARCHAR2,
  --                                 PS_REG_RESP      OUT TRESP_NOMINA_MOVIMIENTOS,
  --                                 PS_MENSAJE_ERROR OUT VARCHAR2
  --                                 ) RETURN BOOLEAN;
  ----------------------------------------------------------------------------
  -- OBTENER DATOS DE RESPUESTA DE CARGOS, MEDIO DE PAGO TRANSBANK --
  FUNCTION OBTENER_DATOS_RESP_CARGOS(PE_STR           IN VARCHAR2,
                                     PS_REG_RESP      OUT TRESP_NOMINA_CARGOS,
                                     PS_MENSAJE_ERROR OUT VARCHAR2)
    RETURN BOOLEAN;

  ----------------------------------------------------------------------------
  -- GENERACION DE CUENTAS CORRIENTES --
  --FUNCTION GENERA_CUENTAS_CORRIENTES (
  --                                        PS_REGISTROS_PROCESADOS IN OUT NUMBER,
  --                                        PS_CUENTAS_NUEVAS       IN OUT NUMBER,
  --                                         PS_CUENTAS_EXISTENTES   IN OUT NUMBER,
  --                                         PS_MENSAJE_ERROR        IN OUT VARCHAR2
  --                                       ) RETURN BOOLEAN;
  FUNCTION GENERA_CUENTAS_CORRIENTES(PE_PROPUESTA            IN SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE,
                                     PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                     PS_CUENTAS_NUEVAS       IN OUT NUMBER,
                                     PS_CUENTAS_EXISTENTES   IN OUT NUMBER,
                                     PS_CUENTAS_ERRONEAS     IN OUT NUMBER,
                                     PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN;

  ----------------------------------------------------------------------------
  -- TRASPASO DE MOVIMIENTOS A ENVIO DE CARGOS --
  /*FUNCTION TRASP_MOVTOS_ENVIO_CARGOS (
  PE_FECHA_PROCESO_PAC    IN     DATE,
  PE_TIPO_CARGA           IN     VARCHAR2,
  PS_REGISTROS_PROCESADOS IN OUT NUMBER,
  PS_REGISTROS_TRASP      IN OUT NUMBER,
  PS_REGISTROS_ERRONEOS   IN OUT NUMBER,
  PS_MENSAJE_ERROR        IN OUT VARCHAR2
  ) RETURN BOOLEAN;*/
  -------------------------------------------------------------------------------------------------
  -- GENERAR ARCHIVO DE ENVIO DE CARGOS Y ABONOS PAC, MEDIO DE PAGO TRANSBANK --
  FUNCTION GENERA_ARCH_ENV_CARGOS_PAC(PE_FECHA_PROCESO_PAC     IN DATE,
                                      PE_FECHA_ARCHIVO_PAC     IN DATE,
                                      PE_TP_ARCHIVO            IN NUMBER,
                                      PE_EJEC_ARCHIVO          IN VARCHAR2,
                                      PE_ID_USUARIO            IN VARCHAR2,
                                      PE_ID_TERMINAL           IN VARCHAR2,
                                      PS_REGISTROS_PROCESADOS  IN OUT NUMBER,
                                      PS_REGISTROS_ENVIADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_ENVIADOS IN OUT NUMBER,
                                      PS_MENSAJE_ERROR         IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  -- CARGA DE ARCHIVO DE RESPUESTA DE CARGOS PAC TRANSBANK --
  FUNCTION CARGA_ARCH_RESP_CARGOS_PAC(PE_FECHA_PROCESO_PAC       IN DATE,
                                      PE_FECHA_ARCHIVO_PAC       IN DATE,
                                      PE_TP_ARCHIVO              IN NUMBER,
                                      PE_ID_USUARIO              IN VARCHAR2,
                                      PE_ID_TERMINAL             IN VARCHAR2,
                                      PS_REGISTROS_LEIDOS        IN OUT NUMBER,
                                      PS_REGISTROS_PROCESADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_PROCESADOS IN OUT NUMBER,
                                      PS_REGISTROS_ERRONEOS      IN OUT NUMBER,
                                      PS_REGISTROS_ACEPTADOS     IN OUT NUMBER,
                                      PS_REGISTROS_RECHAZADOS    IN OUT NUMBER,
                                      PS_MENSAJE_ERROR           IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  -- CUADRATURA DE ARCHIVO DE RESPUESTA DE CARGOS --
  -- DPKPE-291
  FUNCTION CARGA_ARCH_RESP_ABONOS_PAC(PE_FECHA_PROCESO_PAC       IN DATE,
                                      PE_FECHA_ARCHIVO_PAC       IN DATE,
                                      PE_TP_ARCHIVO              IN NUMBER,
                                      PE_ID_USUARIO              IN VARCHAR2,
                                      PE_ID_TERMINAL             IN VARCHAR2,
                                      PS_REGISTROS_LEIDOS        IN OUT NUMBER,
                                      PS_REGISTROS_PROCESADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_PROCESADOS IN OUT NUMBER,
                                      PS_REGISTROS_ERRONEOS      IN OUT NUMBER,
                                      PS_REGISTROS_ACEPTADOS     IN OUT NUMBER,
                                      PS_REGISTROS_RECHAZADOS    IN OUT NUMBER,
                                      PS_MENSAJE_ERROR           IN OUT VARCHAR2)
    RETURN BOOLEAN;

  FUNCTION CUADRATURA_ARCHIVO_RESP_CARGOS(
                                          ---------------------------------------
                                          PE_FECHA_PROCESO_PAC IN DATE,
                                          PE_FECHA_ARCHIVO_PAC IN DATE,
                                          PE_MEDIO_PAGO        IN NUMBER,
                                          PE_TP_ARCHIVO        IN NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_ACEP IN OUT NUMBER,
                                          PS_CMONTO_CCTE_ACEP    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_RECH IN OUT NUMBER,
                                          PS_CMONTO_CCTE_RECH    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_ENV IN OUT NUMBER,
                                          PS_CMONTO_CCTE_ENV    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_RECEP_ACEP IN OUT NUMBER,
                                          PS_CMONTO_RECEP_ACEP    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGISTROS_RECEP_RECH IN OUT NUMBER,
                                          PS_CMONTO_RECEP_RECH     IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGISTROS_RECEP_ENV IN OUT NUMBER,
                                          PS_CMONTO_RECEP_ENV     IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_MENSAJE_ERROR IN OUT VARCHAR2
                                          ---------------------------------------
                                          ) RETURN BOOLEAN;

  /*
  FUNCTION CAMBIO_CUENTA (PE_NU_PROPUESTA  IN     NUMBER,
                          PE_NU_RUT        IN     VARCHAR2,
                          PE_NU_CUENTA     IN     VARCHAR2,
                          PE_NU_CUENTA_ANT IN     VARCHAR2,
                          PE_CD_MEDIO_PAGO IN        NUMBER,
                          PE_NU_ENDOSO     IN        NUMBER,
                          PS_MENSAJE_ERROR IN OUT VARCHAR2) RETURN BOOLEAN;
  */
  -------------------------------------------------------------------------------------------------
  -- TRASPASO DE MOVIMIENTO A ENVIO DE CARGOS POR ID--
  --FUNCTION TRASP_MOVTO_ENVIO_CARGOS_ID (
  --                                     PE_FECHA_PROCESO_PAC    IN     DATE,
  --                                     PE_ID                     IN     NUMBER,
  --                                     PS_MENSAJE_ERROR        IN OUT VARCHAR2
  --                                     ) RETURN BOOLEAN;
  -- ------------------------- --
  -- ELIMINA CARGOS DUPLICADOS --
  /*FUNCTION ELIMINA_DUP (
     PE_FECHA_PROCESO_PAC IN DATE
    ,PS_MENSAJE_ERROR     IN OUT VARCHAR2
  ) RETURN BOOLEAN;*/
  /*
  FUNCTION ELIM_DUPLICADOS_ENVIO_CARGOS (
                                         PE_FECHA_PROCESO_PAC     IN DATE,
                                         PS_CANT_CARG_NORM_PEND   OUT NUMBER,
                                         PS_CANT_CARG_NORM_REZAG  OUT NUMBER,
                                         PS_CANT_NORMALIZACIONES  OUT NUMBER,
                                         PS_MENSAJE_ERROR         OUT VARCHAR2
                                         )
  RETURN BOOLEAN;
  */
  -------------------------------------------------------------------------------------------------
  -- ELIMINA DUPLICADOS DE ENVIO CARGOS
  FUNCTION ELIM_DUPLICADOS_ENVIO_CARGOS(PE_FECHA_PROCESO_PAC    IN DATE,
                                        PS_CANT_CARG_NORM_PEND  OUT NUMBER,
                                        PS_CANT_CARG_NORM_REZAG OUT NUMBER,
                                        PS_CANT_NORMALIZACIONES OUT NUMBER,
                                        PS_CANT_ERRONEOS        OUT NUMBER,
                                        PS_MENSAJE_ERROR        OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  -- TRASPASO DE MOVIMIENTO A ENVIO DE CARGOS POR TIPO DE MOVIMIENTO Y ESTADO                    --
  FUNCTION TRASP_MOVTOS_ENVIO_CARGOS(PE_FECHA_PROCESO_PAC    IN DATE,
                                     PE_TIPO_MOVTO           IN SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE,
                                     PE_ESTADO               IN SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE,
                                     PS_REGISTROS_PROCESADOS OUT NUMBER,
                                     PS_REGISTROS_TRASP      OUT NUMBER,
                                     PS_REGISTROS_ERRONEOS   OUT NUMBER,
                                     PS_MENSAJE_ERROR        OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  FUNCTION FN_VALIDA_DATOS_CUENTA(PE_PROPUESTA  IN NUMBER,
                                  PE_MEDIO_PAGO IN NUMBER,
                                  PS_COD_ERROR  IN OUT NUMBER,
                                  PS_MSG_ERROR  IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  FUNCTION FN_END_CAMBIO_CUENTA(PE_NU_PROPUESTA   IN NUMBER,
                                PE_TP_DOCUMENTO   IN CART_CLIENTES.CACN_TP_DOCUMENTO%TYPE,
                                PE_NU_DOCUMENTO   IN CART_CLIENTES.CACN_NU_DOCUMENTO%TYPE,
                                PE_NU_ENDOSO      IN NUMBER,
                                PE_NU_CUENTA_ANT  IN VARCHAR2,
                                PE_NU_CUENTA_NEW  IN VARCHAR2,
                                PE_CD_PUNTO_VENTA IN NUMBER,
                                PS_MENSAJE_ERROR  IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  FUNCTION FN_REZAGAR_CUOTA_RECTOR(PE_CCTE_PROPUESTA       IN NUMBER,
                                   PE_CCTE_ID              IN NUMBER,
                                   PE_CCTE_RECIBO          IN NUMBER,
                                   PE_CCTE_TARJETA         IN VARCHAR2,
                                   PE_FECHA_PROCESO_PAC    IN DATE,
                                   PE_CCTE_MONEDA          IN VARCHAR2,
                                   PE_CCTE_MONTO_MONEDA    IN NUMBER,
                                   PE_CCTE_NU_ENDOSO       IN NUMBER,
                                   PE_CCTE_CUOTA           IN NUMBER,
                                   PE_CCTE_FECHA_ENVIO_PAC IN DATE,
                                   PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN;

  -------------------------------------------------------------------------------------------------
  FUNCTION GENERA_SOLICITUD_ANULACION(PE_NU_PROPUESTA IN CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                      PE_CD_MOTIVO    IN CART_OPERACIONES_DIARIAS.CAOD_CAME_CD_MOTIVO%TYPE,
                                      PE_MSJ          IN OUT VARCHAR2)
    RETURN BOOLEAN;

END PKG_PROCESOS_PAC_VISANET;
/
create or replace PACKAGE BODY PKG_PROCESOS_PAC_VISANET IS

  /******************************************************************************
     NAME:       PKG_PROCESOS_PAC_VISANET 
     PURPOSE:
  
     REVISIONS:
     ----------------------------------------------------------------------------
     VER        DATE        AUTHOR           DESCRIPTION
     ----------------------------------------------------------------------------
     1.0        11-06-2012  MARCOS MONTA�A.     CREATED THIS PACKAGE.
    ----------------------------------------------------------------------------
     1.1        01-09-2009  LUIS MORA D.     SE AGREGA FUNCION DE CUADRATURA
                                             DE RESPUESTA DE PROCESO PAC.
     ----------------------------------------------------------------------------
     --[ER-RECTOR-PROYMOROS-1]-19-07-2011 -SGALLEGOS-SE AGREGA CONTROL PARA QUE SE SUME LOS INTENTOS SOLO SI ES DIA PAC PARA EL MEDIO DE PAGO PROCESADO.
    ----------------------------------------------------------------------------
     V.1.1.1   04-06-2013   SERGIO GALLEGOS  EN PROC.CARGA_ARCH_RESP_CARGOS_PAC SE ASIGNA VALOR 000 YA QUE INSERTABA EN CART CARGOS CON V_CACA_CD_ORIGEN := 20;
               06-06-2013   SERGIO GALLEGOS  EN PROC.CARGA_ARCH_RESP_CARGOS_PAC,EN INSERCION EN CART_CARGOS, SE INSERTA EN FECHA_CMR LA FECHA ENVIO PAC. 
               10-03-2014   YGP SE MODIFICA  GENERA_ARCHIVO_INSCRIPCION PARA QUE FECHA ESTIMADA DE COBRO SIEMPRE SEA MAYOR O IGUAL A 48 HRS SEGUN VALIDACIN DE VISANET.
    ----------------------------------------------------------------------------
  
  ******************************************************************************/
  -- DPKPE-73 CAMBIO  DE BOLETA POR CODIGO DE TRANSACCION PARA CARGOS AUTORIZADOS JLCD  OCT-2017
  -- SMB CONTROL DE CAMBIO SOE 
  -- FUNCION PARA GENERAR REGISTRO DE INSCRIPCION DE UNA PROPUESTA EN PARTICULAR 
  --DPKPE-563
  FUNCTION FN_DEVOLVER_ID(PE_CADENA IN OUT VARCHAR2) RETURN BOOLEAN IS
    --Variables
    v_valida_id VARCHAR2(3) := NULL;
  BEGIN
    --
    dbms_output.put_line('Entra a buscar el ID');
    SELECT substr(PE_CADENA,
                  1,
                  3)
      INTO v_valida_id
      FROM dual;
    --
    IF v_valida_id = 'ID-'
    THEN
      dbms_output.put_line('SI ES ID');
      select ltrim(PE_CADENA,
                   'ID-')
        INTO PE_CADENA
        from dual;
      dbms_output.put_line('DEVUELVE ID ENCONTRADO ' || PE_CADENA);
      RETURN TRUE;
    END IF;
    --  
    dbms_output.put_line('NO ES ID');
    RETURN FALSE;
    --
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END FN_DEVOLVER_ID;

  --DPKPE-563
  PROCEDURE RECORRER_LINEA(PE_LINEA IN VARCHAR2,
                           PE_ID    IN OUT VARCHAR2) IS
    --
    v_seleccion  VARCHAR2(100);
    v_restante   VARCHAR2(500);
    v_pos_ini    NUMBER := 1;
    v_pos_fin    NUMBER;
    v_noconsigue BOOLEAN := TRUE;
    --
  BEGIN
    --
    dbms_output.put_line('1_prueba');
    SELECT INSTR(PE_LINEA,
                 ',')
      INTO v_pos_fin
      FROM DUAL;
    --
    v_restante := PE_LINEA;
    --
    WHILE v_noconsigue
    LOOP
      dbms_output.put_line('BUSCA NUEVA POSICION v_pos_ini ' || v_pos_ini ||
                           ' FINAL ' || v_pos_fin);
      v_seleccion := substr(v_restante,
                            v_pos_ini,
                            v_pos_fin - 1);
      v_restante  := substr(v_restante,
                            v_pos_fin + 1,
                            LENGTH(v_restante));
      --
      dbms_output.put_line('v_seleccion ' || v_seleccion);
      dbms_output.put_line('v_restante ' || v_restante);
      --
      IF FN_DEVOLVER_ID(v_seleccion)
      THEN
        dbms_output.put_line('Encontro ID ' || v_seleccion);
        PE_ID        := v_seleccion;
        v_noconsigue := FALSE;
        EXIT;
      ELSE
        --
        SELECT INSTR(v_restante,
                     ',')
          INTO v_pos_fin
          FROM DUAL;
        --
        v_seleccion := NULL;
        --
      END IF;
      --
    END LOOP;
    --
  END RECORRER_LINEA;

  --
  FUNCTION FN_DIA_PAGO_VNET(PE_SODP_MEDIO_PAGO IN SCO_DIA_PAGO.SODP_MEDIO_PAGO%TYPE,
                            PE_SODP_DIA_PAC    IN SCO_DIA_PAGO.SODP_DIA_PAC%TYPE,
                            PS_SODP_DIA_PAGO   IN OUT SCO_DIA_PAGO.SODP_DIA_PAGO%TYPE,
                            PS_MENSAJE_ERROR   IN OUT VARCHAR2)
    RETURN BOOLEAN IS
  BEGIN
    IF PE_SODP_MEDIO_PAGO = 8
    then
      -- VISANET 
      Begin
        SELECT --SODP_DIA_PAG_30
         DECODE(to_char(SYSDATE,
                        'MM'),
                '02',
                to_char(LAST_DAY(SYSDATE),
                        'DD'),
                SODP_DIA_PAG_30) --DPKPE-397
          INTO PS_SODP_DIA_PAGO
          FROM SCO_DIA_PAGO
         WHERE SODP_MEDIO_PAGO = PE_SODP_MEDIO_PAGO
           AND SODP_DIA_PAC = PE_SODP_DIA_PAC;
        RETURN TRUE;
      exception
        when no_data_found then
          PS_MENSAJE_ERROR := 'ERROR NO DATA FOUND DIA DE PAGO PARA VISANET. ' ||
                              SQLERRM;
          RETURN FALSE;
        when others then
          PS_MENSAJE_ERROR := 'ERROR AL OBTENER DIA DE PAGO PARA VISANET. ' ||
                              SQLERRM;
          RETURN FALSE;
      end;
    ELSE
      Begin
        SELECT SODP_DIA_PAGO
          INTO PS_SODP_DIA_PAGO
          FROM SCO_DIA_PAGO
         WHERE SODP_MEDIO_PAGO = PE_SODP_MEDIO_PAGO
           AND SODP_DIA_PAC = PE_SODP_DIA_PAC;
        RETURN TRUE;
      exception
        when no_data_found then
          PS_MENSAJE_ERROR := 'ERROR NO DATA FOUND DIA DE PAGO <> VISANET. ' ||
                              SQLERRM;
          RETURN FALSE;
        when others then
          PS_MENSAJE_ERROR := 'ERROR AL OBTENER DIA DE PAGO <> VISANET. ' ||
                              SQLERRM;
          RETURN FALSE;
      end;
    END IF;
    --
    -- RETURN TRUE;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'ERROR GENERAL AL OBTENER DIA DE PAGO. ' ||
                          SQLERRM;
      RETURN FALSE;
  END FN_DIA_PAGO_VNET;

  FUNCTION GENERA_REG_INSCRIPCION_X_PPTA(PE_NU_PROPUESTA  IN NUMBER,
                                         PS_MENSAJE_ERROR IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    ------------------------------------------------------------------------------------------
    SCOI_ID_AUX SCO_INSCRIPCION.SCOI_ID%TYPE;
    --V_SCCP_COD_COMERCIO         SCO_PRODUCTOS.SCCP_COD_COMERCIO%TYPE;
    V_SCCP_CD_PRODUCTO_CMR      SCO_PRODUCTOS.SCCP_CD_PRODUCTO_CMR%TYPE;
    V_SCCP_CD_COMPANIA_COMERCIO SCO_PRODUCTOS.SCCP_CD_COMPANIA_COMERCIO%TYPE := NULL;
    --V_SCCP_TP_INSCRIPCION         SCO_PRODUCTOS.SCCP_TP_INSCRIPCION%TYPE;
    V_SCDEI_TP_ARCHIVO           SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_ARCHIVO%TYPE;
    V_SCDEI_CD_COMPANIA_COMERCIO SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_COMPANIA_COMERCIO%TYPE;
    V_SCDEI_CD_PRODUCTO_CMR      SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_PRODUCTO_CMR%TYPE;
    CACN_NU_DOCUMENTO_AUX        VARCHAR2(9);
    CACN_CD_VERIFICADOR_AUX      VARCHAR2(1);
    CACN_NOMBRES_AUX             VARCHAR2(36);
    CACN_DI_COBRO_P_AUX          VARCHAR2(36);
    CAPB_NU_SEGURO_CMR_AUX       CART_PRODPLANES.CAPB_NU_SEGURO_CMR%TYPE;
    CACE_FE_SUSCRIPCION_AUX      CART_CERTIFICADOS.CACE_FE_SUSCRIPCION%TYPE;
    CADM_NU_CUENTA_AUX           CART_DOMICILIOS_BANCARIOS.CADM_NU_CUENTA%TYPE;
    REG_CUADRATURA               SCO_CUADRATURA_DIARIA%ROWTYPE;
    ------------------------------------------------------------------------------------------
    EXC_ERR_GRABAR_INSCRIPCION    EXCEPTION;
    EXC_ERR_OBT_DATOS_PROD_RECTOR EXCEPTION;
    EXC_ERR_OBT_DATOS_CLIENTE     EXCEPTION;
    EXC_ERR_OBT_CODIGO_COMERCIO   EXCEPTION;
    EXC_ERR_OBT_PRODUCTO_PAC      EXCEPTION;
    EXC_ERR_CERT_NO_ACEP          EXCEPTION;
    EXC_ERR_OBT_CUENTA_CMR        EXCEPTION;
    EXC_ERR_OBT_TP_INSCRIPCION    EXCEPTION;
    ------------------------------------------------------------------------------------------
  BEGIN
    ---------------------------------------------------------------------------------
    SELECT *
      INTO REG_CUADRATURA
      FROM SCO_CUADRATURA_DIARIA
     WHERE SCCD_OPERACION IN (1001,
                              9999)
       AND SCCD_SUB_OPERACION = 0
       AND SCCD_PROPUESTA = PE_NU_PROPUESTA;
    IF (REG_CUADRATURA.SCCD_STATUS != PKG_PROCESOS_PAC.SCCD_ST_CERT_ACEP)
    THEN
      RAISE EXC_ERR_CERT_NO_ACEP;
    END IF;
    ---------------------------------------------------------------------------------
    -- NUEVO ID PARA LA INSCRIPCION --
    SCOI_ID_AUX := PKG_PROCESOS_PAC.OBTENER_ID_INSCRIPCION;
    ---------------------------------------------------------------------------------
    BEGIN
      -- SE OBTIENE EL CODIGO DE COMERCIO DEL PRODUCTO, DESDE LA TABLA SCO_PRODUCTOS --
      SELECT SCCP_CD_PRODUCTO_CMR, --SCCP_COD_COMERCIO,
             SCCP_CD_COMPANIA_COMERCIO -- SCCP_TP_INSCRIPCION
        INTO V_SCCP_CD_PRODUCTO_CMR, --V_SCCP_COD_COMERCIO,
             V_SCCP_CD_COMPANIA_COMERCIO --V_SCCP_TP_INSCRIPCION
        FROM SCO_PRODUCTOS
       WHERE SCCP_CAPB_CD_PLAN = REG_CUADRATURA.SCCD_PLAN;
      IF (V_SCCP_CD_PRODUCTO_CMR IS NULL)
      THEN
        RAISE EXC_ERR_OBT_PRODUCTO_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_OBT_PRODUCTO_PAC;
    END;
    ---------------------------------------------------------------------------------
    -- MMA, 18-04-2012 --
    -- OBTIENE TIPO DE INSCRIPCION
    ---------------------------------------------------------------------------------
    BEGIN
      SELECT SCDEI_TP_ARCHIVO,
             SCDEI_CD_COMPANIA_COMERCIO,
             SCDEI_CD_PRODUCTO_CMR
        INTO V_SCDEI_TP_ARCHIVO,
             V_SCDEI_CD_COMPANIA_COMERCIO,
             V_SCDEI_CD_PRODUCTO_CMR
        FROM SCO_DEF_ENVIO_INSCRIPCION
       WHERE SCDEI_CD_PLAN = REG_CUADRATURA.SCCD_PLAN
         AND SCDEI_CD_MEDIO_PAGO = REG_CUADRATURA.SCCD_MEDIO_PAGO
         AND SCDEI_TP_REGISTRO = 'A'; -- ALTA 
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_OBT_TP_INSCRIPCION;
    END;
    ---------------------------------------------------------------------------------
    BEGIN
      -- SE OBTIENEN LOS DATOS DEL CLIENTE --
      SELECT LPAD(CACN_NU_DOCUMENTO,
                  9,
                  '0'),
             NVL(CACN_CD_VERIFICADOR,
                 ' '),
             NVL(SUBSTR(RTRIM(LTRIM(CACN_APELLIDO_PATERNO)) || ' ' ||
                        RTRIM(LTRIM(CACN_APELLIDO_MATERNO)) || ' ' ||
                        RTRIM(LTRIM(CACN_NOMBRES)),
                        1,
                        36),
                 SUBSTR(CACN_NM_APELLIDO_RAZON,
                        1,
                        36)),
             SUBSTR(CACN_DI_COBRO_P,
                    1,
                    36)
        INTO CACN_NU_DOCUMENTO_AUX,
             CACN_CD_VERIFICADOR_AUX,
             CACN_NOMBRES_AUX,
             CACN_DI_COBRO_P_AUX
        FROM CART_CERTIFICADOS,
             CART_CLIENTES
       WHERE CACE_NU_PROPUESTA = REG_CUADRATURA.SCCD_PROPUESTA
         AND CACE_CACN_CD_NAC_DEUDOR = CACN_CD_NACIONALIDAD
         AND CACE_CACN_NU_CED_DEUDOR = CACN_NU_CEDULA_RIF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_OBT_DATOS_CLIENTE;
    END;
    ---------------------------------------------------------------------------------
    BEGIN
      -- SE OBTIENE LA CUENTA CMR ASOCIADA A LA PROPUESTA --
      -- DESDE LA TABLA CART_DOMICILIOS_BANCARIOS --
      SELECT CADM_NU_CUENTA
        INTO CADM_NU_CUENTA_AUX
        FROM CART_CERTIFICADOS,
             CART_DOMICILIOS_BANCARIOS
       WHERE CACE_NU_PROPUESTA = REG_CUADRATURA.SCCD_PROPUESTA
         AND CACE_CACN_CD_NAC_DEUDOR = CADM_CACN_CD_NACIONALIDAD
         AND CACE_CACN_NU_CED_DEUDOR = CADM_CACN_NU_CEDULA_RIF
         AND CACE_CADM_NU_DOMICILIO_DB = CADM_NU_DOMICILIO;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_OBT_CUENTA_CMR;
    END;
    ---------------------------------------------------------------------------------
    BEGIN
      -- SE OBTIENEN LOS DATOS DE LA COMPA?IA, PRODUCTO Y MEDIO DE PAGO --
      SELECT CAPB_NU_SEGURO_CMR,
             CACE_FE_SUSCRIPCION
        INTO CAPB_NU_SEGURO_CMR_AUX,
             CACE_FE_SUSCRIPCION_AUX
        FROM CART_CERTIFICADOS,
             CART_PRODPLANES
       WHERE CACE_NU_PROPUESTA = REG_CUADRATURA.SCCD_PROPUESTA
         AND CACE_CARP_CD_RAMO = CAPB_CARP_CD_RAMO
         AND CACE_CAPU_CD_PRODUCTO = CAPB_CAPU_CD_PRODUCTO
         AND CACE_CAPB_CD_PLAN = CAPB_CD_PLAN;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_OBT_DATOS_PROD_RECTOR;
    END;
    ---------------------------------------------------------------------------------
    BEGIN
      -- SE GRABA EN LA TABLA SCO_INSCRIPCION --
      INSERT INTO SCO_INSCRIPCION
        (SCOI_ID,
         SCOI_FECHA_PROCESO,
         SCOI_STATUS,
         SCOI_NUMERO_COMERCIO,
         SCOI_RUT,
         SCOI_DV,
         SCOI_NOMBRE,
         SCOI_CUENTA_TARJETA,
         SCOI_TARJETA,
         SCOI_PRODUCTO,
         SCOI_MANDATO,
         SCOI_ORIGEN,
         SCOI_DIRECCION,
         SCOI_FECHA_INICIO,
         SCOI_FECHA_TERMINO,
         SCOI_MONTO,
         SCOI_MONEDA,
         SCOI_TIPO_REGISTRO,
         SCOI_CONTRATO_PROPUESTA,
         SCOI_DIA_PAGO,
         SCOI_RESPUESTA_CMR,
         SCOI_FECHA_NACIMIENTO,
         SCOI_SEXO,
         SCOI_NACIONALIDAD,
         SCOI_CALLE_NUMERO,
         SCOI_DEPARTAMENTO,
         SCOI_VILLA,
         SCOI_CIUDAD,
         SCOI_COMUNA,
         SCOI_TIPO_DIRECCION,
         SCOI_ZONA,
         SCOI_CENTRAL,
         SCOI_FONO,
         SCOI_CICLO,
         SCOI_CODIGO_ENTIDAD,
         SCOI_CODIGO_PRODUCTO,
         SCOI_FILLER,
         SCOI_STRING_ENVIADO,
         SCOI_STRING_DEVUELTO,
         SCOI_DIA_PAC,
         SCOI_DIA_FAC,
         SCOI_MEDIO_PAGO,
         SCOI_TP_INSCRIPCION)
      VALUES
        (SCOI_ID_AUX,
         SYSDATE,
         PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO,
         V_SCDEI_CD_PRODUCTO_CMR, --V_SCCP_CD_PRODUCTO_CMR,
         CACN_NU_DOCUMENTO_AUX,
         CACN_CD_VERIFICADOR_AUX,
         CACN_NOMBRES_AUX,
         CADM_NU_CUENTA_AUX,
         NULL,
         V_SCDEI_CD_COMPANIA_COMERCIO, --V_SCCP_CD_COMPANIA_COMERCIO,
         NULL,
         '02',
         CACN_DI_COBRO_P_AUX,
         TO_NUMBER(TO_CHAR(CACE_FE_SUSCRIPCION_AUX,
                           'YYYYMMDD')),
         0,
         0,
         '152',
         'A ',
         REG_CUADRATURA.SCCD_PROPUESTA,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         SCOI_ID_AUX,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         NULL,
         0, -- INICIALIZA DIA PAC HAY 2 ALGORITMOS CARTERA Y VENTA NUEVA --
         0, -- INICIALIZA DIA FACTURACION --
         REG_CUADRATURA.SCCD_MEDIO_PAGO,
         V_SCDEI_TP_ARCHIVO --V_SCDEI_TP_INSCRIPCION
         );
      -- SE ACTUALIZA LA CUADRATURA DIARIA --
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_GRABAR_INSCRIPCION;
    END;
    COMMIT;
    PS_MENSAJE_ERROR := NULL;
    RETURN TRUE;
  EXCEPTION
    WHEN EXC_ERR_OBT_TP_INSCRIPCION THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER TIPO DE INSCRIPCION';
      RETURN FALSE;
    WHEN EXC_ERR_CERT_NO_ACEP THEN
      PS_MENSAJE_ERROR := 'El estado de certificaci�n de la propuesta no es v�lido';
      RETURN FALSE;
    WHEN EXC_ERR_OBT_PRODUCTO_PAC THEN
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_PAC
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    WHEN EXC_ERR_OBT_DATOS_CLIENTE THEN
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_DAT_CLI
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    WHEN EXC_ERR_OBT_CUENTA_CMR THEN
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_CTA_VN
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    WHEN EXC_ERR_OBT_DATOS_PROD_RECTOR THEN
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_REC
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    WHEN EXC_ERR_GRABAR_INSCRIPCION THEN
      UPDATE SCO_CUADRATURA_DIARIA
         SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_INS_REG
       WHERE SCCD_ID = REG_CUADRATURA.SCCD_ID;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'Error al generar registro de inscripci�n ' || ', ' ||
                          SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END GENERA_REG_INSCRIPCION_X_PPTA;

  ----------------------------------------------------------------------------------------
  -- GENERACION DE REGISTROS DE INSCRIPCION, MEDIO DE PAGO TRANSBANK --
  FUNCTION GENERA_REGISTRO_INSCRIPCION(PE_PROPUESTA            IN SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE,
                                       PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                       PS_REGISTROS_CREADOS    IN OUT NUMBER,
                                       PS_REGISTROS_ERRONEOS   IN OUT NUMBER,
                                       PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    ------------------------------------------------------------------------------------------
    V_SCOI_ID             SCO_INSCRIPCION.SCOI_ID%TYPE;
    V_CAPB_NU_SEGURO_CMR  CART_PRODPLANES.CAPB_NU_SEGURO_CMR%TYPE;
    V_CACN_NU_DOCUMENTO   VARCHAR2(9);
    V_CACN_CD_VERIFICADOR VARCHAR2(1);
    V_CACN_NOMBRES        VARCHAR2(36);
    V_CACN_DI_COBRO_P     VARCHAR2(36);
    V_CACE_FE_SUSCRIPCION CART_CERTIFICADOS.CACE_FE_SUSCRIPCION%TYPE;
    V_CADM_NU_CUENTA      CART_DOMICILIOS_BANCARIOS.CADM_NU_CUENTA%TYPE;
    --V_SCCP_COD_COMERCIO           SCO_PRODUCTOS.SCCP_COD_COMERCIO%TYPE;
    V_SCCP_CD_PRODUCTO_CMR       SCO_PRODUCTOS.SCCP_CD_PRODUCTO_CMR%TYPE;
    V_SCCP_CD_COMPANIA_COMERCIO  SCO_PRODUCTOS.SCCP_CD_COMPANIA_COMERCIO%TYPE;
    V_SCDEI_TP_ARCHIVO           SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_ARCHIVO%TYPE;
    V_SCDEI_CD_COMPANIA_COMERCIO SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_COMPANIA_COMERCIO%TYPE;
    V_SCDEI_CD_PRODUCTO_CMR      SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_PRODUCTO_CMR%TYPE;
    --V_SCCP_TP_INSCRIPCION         SCO_PRODUCTOS.SCCP_TP_INSCRIPCION%TYPE;
    V_SCCD_PROPUESTA SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE;
    V_SCCD_PLAN      SCO_CUADRATURA_DIARIA.SCCD_PLAN%TYPE;
    V_COD_ERROR      NUMBER := NULL;
    V_SCOI_DIA_PAC   SCO_INSCRIPCION.SCOI_DIA_PAC%TYPE;
    --V_SCOI_DIA_FAC                SCO_INSCRIPCION.SCOI_DIA_FAC%TYPE;
    V_SCOI_DIA_PAGO SCO_INSCRIPCION.SCOI_DIA_PAGO%TYPE;
    V_SIG_FECHA_PAC DATE := NULL;
    V_SEM_FECHA_PAC DATE := NULL; -- -CVG - 17-02-2017 
    -- MMA 25-06-2012 --
    V_IMPLICITO            BOOLEAN := FALSE;
    V_ESTADO_INSCRIPCION   SCO_INSCRIPCION.SCOI_STATUS%TYPE := NULL;
    V_ESTADO_CUADRATURA    SCO_CUADRATURA_DIARIA.SCCD_STATUS%TYPE := NULL;
    V_CACE_FE_ULT_PAGOREAS CART_CERTIFICADOS.CACE_FE_ULT_PAGOREAS%TYPE := NULL;
    ------------------------------------------------------------------------------------------
    EXC_ERR_GRABAR_INSCRIPCION    EXCEPTION;
    EXC_ERR_OBT_NUM_SEGURO_CMR    EXCEPTION;
    EXC_ERR_OBT_DATOS_PROD_RECTOR EXCEPTION;
    EXC_ERR_OBT_DATOS_CLIENTE     EXCEPTION;
    EXC_ERR_OBT_CUENTA_CMR        EXCEPTION;
    EXC_ERR_OBT_CODIGO_COMERCIO   EXCEPTION;
    EXC_ERR_OBT_PRODUCTO_PAC      EXCEPTION;
    EXC_ERR_OBT_TP_INSCRIPCION    EXCEPTION;
    EXC_ERR_DATOS_CUENTA          EXCEPTION;
    EXC_ERR_DIA_PAGO              EXCEPTION;
    EXC_ERR_IMPLICITO             EXCEPTION;
    ------------------------------------------------------------------------------------------
    -- SE INSCRIBEN SOLO MEDIOS DE PAGO 8 (TRANSBANK) --
    CURSOR CUADRATURA_DIARIA IS
      SELECT SCCD_ID,
             SCCD_FECHA_OP,
             SCCD_PROPUESTA,
             SCCD_PERIODO,
             SCCD_OPERACION,
             SCCD_ORIGEN,
             SCCD_SUB_OPERACION,
             SCCD_ORIGEN2,
             SCCD_STATUS,
             SCCD_OBSERVACION,
             SCCD_SUCURSAL,
             SCCD_RAMO,
             SCCD_POLIZA,
             SCCD_CERTIFICADO,
             SCCD_MEDIO_PAGO,
             SCCD_PLAN,
             SCCD_PUNTO_VENTA,
             SCCD_PRODUCTOR,
             SCCD_USER,
             SCCD_CIERRE1,
             SCCD_CIERRE2,
             SCCD_CIERRE3,
             SCCD_CIERRE4,
             SCCD_CIERRE5,
             SCCD_CIERRE6,
             SCCD_CIERRE7,
             SCCD_CIERRE8,
             SCCD_ANO,
             SCCD_MES,
             SCCD_PRIMA_BRUTA,
             SCCD_PRIMA_IVA,
             SCCD_PRIMA_MONEDA,
             SCCD_COTIZACION,
             SCCD_CONSECUTIVO,
             SCCD_FECHA_DESDE,
             SCCD_FECHA_HASTA,
             SCCD_FRAGMENT,
             SCCD_CIERRE
        FROM SCO_CUADRATURA_DIARIA
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS IN
             (PKG_PROCESOS_PAC_VISANET.SCCD_ST_CERT_ACEP,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_PAC,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_DAT_CLI,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_REC,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_CTA_VN,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_INS_REG,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_SIN_CCTE,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_ID_BANCO,
              PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_CTA_BCO)
         AND SCCD_PROPUESTA = NVL(PE_PROPUESTA,
                                  SCCD_PROPUESTA);
  BEGIN
    ---------------------------------------------------------------------------------
    PS_REGISTROS_PROCESADOS := 0;
    PS_REGISTROS_CREADOS    := 0;
    PS_REGISTROS_ERRONEOS   := 0;
    ---------------------------------------------------------------------------------
    FOR CD IN CUADRATURA_DIARIA
    LOOP
      BEGIN
        PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
        ---------------------------------------------------------------------------------
        -- VARIABLE DE PASO PARA PROPUESTA FPRO --
        V_SCCD_PROPUESTA := CD.SCCD_PROPUESTA;
        V_SCCD_PLAN      := CD.SCCD_PLAN;
        ---------------------------------------------------------------------------------
        -- NUEVO ID PARA LA INSCRIPCION --
        V_SCOI_ID := PKG_PROCESOS_PAC.OBTENER_ID_INSCRIPCION;
        ---------------------------------------------------------------------------------
        BEGIN
          -- SE OBTIENE EL CODIGO DE COMERCIO DEL PRODUCTO, DESDE LA TABLA SCO_PRODUCTOS --
          SELECT SCCP_CD_PRODUCTO_CMR, --SCCP_COD_COMERCIO,
                 SCCP_CD_COMPANIA_COMERCIO --SCCP_TP_INSCRIPCION
            INTO V_SCCP_CD_PRODUCTO_CMR, --V_SCCP_COD_COMERCIO,
                 V_SCCP_CD_COMPANIA_COMERCIO --V_SCCP_TP_INSCRIPCION
            FROM SCO_PRODUCTOS
           WHERE SCCP_CAPB_CD_PLAN = CD.SCCD_PLAN;
          IF (V_SCCP_CD_PRODUCTO_CMR IS NULL)
          THEN
            RAISE EXC_ERR_OBT_PRODUCTO_PAC;
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
            RAISE EXC_ERR_OBT_PRODUCTO_PAC;
        END;
        ---------------------------------------------------------------------------------
        -- MMA, 18-04-2012 --
        -- OBTIENE TIPO DE INSCRIPCION
        ---------------------------------------------------------------------------------
        BEGIN
          --
          SELECT SCDEI_TP_ARCHIVO, --SCDEI_TP_INSCRIPCION
                 SCDEI_CD_COMPANIA_COMERCIO,
                 SCDEI_CD_PRODUCTO_CMR
            INTO V_SCDEI_TP_ARCHIVO, --V_SCDEI_TP_INSCRIPCION
                 V_SCDEI_CD_COMPANIA_COMERCIO,
                 V_SCDEI_CD_PRODUCTO_CMR
            FROM SCO_DEF_ENVIO_INSCRIPCION
           WHERE SCDEI_CD_PLAN = CD.SCCD_PLAN
             AND SCDEI_CD_MEDIO_PAGO = CD.SCCD_MEDIO_PAGO
             AND SCDEI_TP_REGISTRO = 'A' -- ALTA 
          ;
        EXCEPTION
          WHEN OTHERS THEN
            RAISE EXC_ERR_OBT_TP_INSCRIPCION;
        END;
        ---------------------------------------------------------------------------------
        BEGIN
          -- SE OBTIENEN LOS DATOS DEL CLIENTE --
          SELECT LPAD(CACN_NU_DOCUMENTO,
                      9,
                      '0'),
                 NVL(CACN_CD_VERIFICADOR,
                     ' '),
                 NVL(SUBSTR(RTRIM(LTRIM(CACN_APELLIDO_PATERNO)) || ' ' ||
                            RTRIM(LTRIM(CACN_APELLIDO_MATERNO)) || ' ' ||
                            RTRIM(LTRIM(CACN_NOMBRES)),
                            1,
                            36),
                     SUBSTR(CACN_NM_APELLIDO_RAZON,
                            1,
                            36)),
                 SUBSTR(CACN_DI_COBRO_P,
                        1,
                        36),
                 CACE_FE_ULT_PAGOREAS
          -- MMA 22-06-2012                         --
          -- AHORA SE OBITNE EL DIA PAC MAS CERCANO --
          --,TO_NUMBER(TO_CHAR(CACE_FE_ULT_PAGOREAS,'DD'))
            INTO V_CACN_NU_DOCUMENTO,
                 V_CACN_CD_VERIFICADOR,
                 V_CACN_NOMBRES,
                 V_CACN_DI_COBRO_P,
                 V_CACE_FE_ULT_PAGOREAS
          --,V_SCOI_DIA_PAC
            FROM CART_CERTIFICADOS,
                 CART_CLIENTES
           WHERE CACE_NU_PROPUESTA = CD.SCCD_PROPUESTA
             AND CACE_CACN_CD_NAC_DEUDOR = CACN_CD_NACIONALIDAD
             AND CACE_CACN_NU_CED_DEUDOR = CACN_NU_CEDULA_RIF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
            RAISE EXC_ERR_OBT_DATOS_CLIENTE;
        END;
        ---------------------------------------------------------------------------------
        --         IF NOT PKG_PROCESOS_PAC.FN_CALCULA_SIGUIENTE_FECHA_PAC (
        --                                                                 CODIGO_MEDIO_PAGO_VN,
        --                                                                 SYSDATE,
        --                                                                 V_SIG_FECHA_PAC,
        --                                                                 PS_MENSAJE_ERROR
        --                                                          ) THEN
        --            PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        --            RAISE EXC_ERR_GRABAR_INSCRIPCION;
        --         END IF;
        --         V_SCOI_DIA_PAC := TO_NUMBER(TO_CHAR(V_SIG_FECHA_PAC, 'DD'));
        --V_SCOI_DIA_PAC  := TO_NUMBER(TO_CHAR(V_CACE_FE_ULT_PAGOREAS,'DD'));
        V_SCOI_DIA_PAC  := TO_NUMBER(TO_CHAR(SYSDATE,
                                             'DD'));
        V_SEM_FECHA_PAC := trunc(SYSDATE); -- V_SIG_FECHA_PAC;  -- CVG - 17-02-2017 
        -- ****** CVG - 15-02-2017 ****** 
        FOR R IN (SELECT NVL(MIN(SODP_DIA_PAC),
                             2) sodp_dia_pac
                    FROM sco_dia_pago
                   WHERE sodp_medio_pago = CODIGO_MEDIO_PAGO_VN
                     AND sodp_dia_pac > V_SCOI_DIA_PAC)
        LOOP
          IF r.sodp_dia_pac >= TO_NUMBER(TO_CHAR(V_SEM_FECHA_PAC,
                                                 'DD'))
          THEN
            V_SEM_FECHA_PAC := to_date(lpad(r.sodp_dia_pac,
                                            2,
                                            '0') ||
                                       TO_CHAR(V_SEM_FECHA_PAC,
                                               'MMYYYY'),
                                       'dd/mm/yyyy');
          END IF;
          -- VALIDAR EL SEMAFORO  
          IF pkg_semaforo_pac.ESTADO_SEMAFORO(CODIGO_MEDIO_PAGO_VN,
                                              V_SEM_FECHA_PAC) = FALSE
          THEN
            V_SCOI_DIA_PAC := TO_NUMBER(TO_CHAR(V_SEM_FECHA_PAC,
                                                'DD'));
            GOTO Salir;
          ELSE
            --V_SCOI_DIA_PAC := TO_NUMBER(TO_CHAR(V_SIG_FECHA_PAC, 'DD'));                                    
            V_SIG_FECHA_PAC := NULL;
            IF NOT
                PKG_PROCESOS_PAC.FN_CALCULA_SIGUIENTE_FECHA_PAC(CODIGO_MEDIO_PAGO_VN,
                                                                V_SEM_FECHA_PAC, --SYSDATE,
                                                                V_SIG_FECHA_PAC,
                                                                PS_MENSAJE_ERROR)
            THEN
              PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
              RAISE EXC_ERR_GRABAR_INSCRIPCION;
            END IF;
            V_SCOI_DIA_PAC := TO_NUMBER(TO_CHAR(V_SIG_FECHA_PAC,
                                                'DD'));
          END IF;
        END LOOP;
        -- ** --------------- ** --------------
        <<SALIR>>
      ---------------------------------------------------------------------------------
        BEGIN
          -- SE OBTIENE LA CUENTA CMR ASOCIADA A LA PROPUESTA --
          -- DESDE LA TABLA CART_DOMICILIOS_BANCARIOS --
          SELECT CADM_NU_CUENTA
            INTO V_CADM_NU_CUENTA
            FROM CART_CERTIFICADOS,
                 CART_DOMICILIOS_BANCARIOS
           WHERE CACE_NU_PROPUESTA = CD.SCCD_PROPUESTA
             AND CACE_CACN_CD_NAC_DEUDOR = CADM_CACN_CD_NACIONALIDAD
             AND CACE_CACN_NU_CED_DEUDOR = CADM_CACN_NU_CEDULA_RIF
             AND CACE_CADM_NU_DOMICILIO_DB = CADM_NU_DOMICILIO;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE EXC_ERR_OBT_CUENTA_CMR;
        END;
        ---------------------------------------------------------------------------------
        BEGIN
          -- SE OBTIENEN LOS DATOS DE LA COMPA?IA, PRODUCTO Y MEDIO DE PAGO --
          SELECT CAPB_NU_SEGURO_CMR,
                 CACE_FE_SUSCRIPCION
            INTO V_CAPB_NU_SEGURO_CMR,
                 V_CACE_FE_SUSCRIPCION
            FROM CART_CERTIFICADOS,
                 CART_PRODPLANES
           WHERE CACE_NU_PROPUESTA = CD.SCCD_PROPUESTA
             AND CACE_CARP_CD_RAMO = CAPB_CARP_CD_RAMO
             AND CACE_CAPU_CD_PRODUCTO = CAPB_CAPU_CD_PRODUCTO
             AND CACE_CAPB_CD_PLAN = CAPB_CD_PLAN;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
            RAISE EXC_ERR_OBT_DATOS_PROD_RECTOR;
        END;
        ---------------------------------------------------------------------------------
        -- MMA, 24-04-2012        
        -- VALIDA DATOS DE CUENTA 
        IF NOT
            PKG_PROCESOS_PAC_VISANET.FN_VALIDA_DATOS_CUENTA(CD.SCCD_PROPUESTA,
                                                            CD.SCCD_MEDIO_PAGO,
                                                            V_COD_ERROR,
                                                            PS_MENSAJE_ERROR)
        THEN
          RAISE EXC_ERR_DATOS_CUENTA;
        END IF;
        --------------------------------------------------------------------------------
        -- MMA, 26-04-2012 
        -- OBTIENE DIA PAGO
        --IF NOT PKG_PROCESOS_PAC.FN_DIA_PAGO(CD.SCCD_MEDIO_PAGO  
        IF NOT FN_DIA_PAGO_VNET(CD.SCCD_MEDIO_PAGO,
                                V_SCOI_DIA_PAC,
                                V_SCOI_DIA_PAGO,
                                PS_MENSAJE_ERROR)
        THEN
          RAISE EXC_ERR_DIA_PAGO;
        END IF;
        --------------------------------------------------------------------------------         
        -- MMA 25-04-2012 --
        -- DETERMINA INSCRIPCION EXPLICITO O IMPLICITA --
        IF NOT PKG_PROCESOS_PAC.FN_IMPLICITO(CD.SCCD_PLAN, --V_REG_CERT.CACE_CAPB_CD_PLAN,
                                             CD.SCCD_MEDIO_PAGO, --V_REG_CERT.CACE_CAMD_CD_MEDIO_PAGO, 
                                             'A',
                                             V_IMPLICITO,
                                             PS_MENSAJE_ERROR)
        THEN
          RAISE EXC_ERR_IMPLICITO;
        END IF;
        --
        IF V_IMPLICITO
        THEN
          V_ESTADO_INSCRIPCION := PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP; --PKG_PROCESOS_PAC_VISANET.ACEPTADA
          V_ESTADO_CUADRATURA  := PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_SIN_CTACTE; --PKG_PROCESOS_PAC_VISANET.CREACION DE CTACTE PENDIENTE
        ELSE
          V_ESTADO_INSCRIPCION := PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO;
          V_ESTADO_CUADRATURA  := PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV;
        END IF;
        BEGIN
          -- SE GRABA EN LA TABLA SCO_INSCRIPCION --
          INSERT INTO SCO_INSCRIPCION
            (SCOI_ID,
             SCOI_FECHA_PROCESO,
             SCOI_STATUS,
             SCOI_NUMERO_COMERCIO,
             SCOI_RUT,
             SCOI_DV,
             SCOI_NOMBRE,
             SCOI_CUENTA_TARJETA,
             SCOI_TARJETA,
             SCOI_PRODUCTO,
             SCOI_MANDATO,
             SCOI_ORIGEN,
             SCOI_DIRECCION,
             SCOI_FECHA_INICIO,
             SCOI_FECHA_TERMINO,
             SCOI_MONTO,
             SCOI_MONEDA,
             SCOI_TIPO_REGISTRO,
             SCOI_CONTRATO_PROPUESTA,
             SCOI_DIA_PAGO,
             SCOI_RESPUESTA_CMR,
             SCOI_FECHA_NACIMIENTO,
             SCOI_SEXO,
             SCOI_NACIONALIDAD,
             SCOI_CALLE_NUMERO,
             SCOI_DEPARTAMENTO,
             SCOI_VILLA,
             SCOI_CIUDAD,
             SCOI_COMUNA,
             SCOI_TIPO_DIRECCION,
             SCOI_ZONA,
             SCOI_CENTRAL,
             SCOI_FONO,
             SCOI_CICLO,
             SCOI_CODIGO_ENTIDAD,
             SCOI_CODIGO_PRODUCTO,
             SCOI_FILLER,
             SCOI_STRING_ENVIADO,
             SCOI_STRING_DEVUELTO,
             SCOI_DIA_PAC,
             SCOI_DIA_FAC,
             SCOI_MEDIO_PAGO,
             SCOI_TP_INSCRIPCION)
          VALUES
            (V_SCOI_ID,
             SYSDATE,
             V_ESTADO_INSCRIPCION, --PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO,
             V_SCDEI_CD_PRODUCTO_CMR, --V_SCCP_CD_PRODUCTO_CMR, --V_SCCP_COD_COMERCIO,
             V_CACN_NU_DOCUMENTO,
             V_CACN_CD_VERIFICADOR,
             V_CACN_NOMBRES,
             V_CADM_NU_CUENTA,
             V_CADM_NU_CUENTA,
             V_SCDEI_CD_COMPANIA_COMERCIO, --V_SCCP_CD_COMPANIA_COMERCIO, --V_CAPB_NU_SEGURO_CMR,
             NULL, -- LO DEVUELVE CMR --
             '02', -- EMPRESA --
             V_CACN_DI_COBRO_P,
             TO_NUMBER(TO_CHAR(V_CACE_FE_SUSCRIPCION,
                               'YYYYMMDD')),
             0,
             0,
             '152',
             'A ',
             CD.SCCD_PROPUESTA,
             V_SCOI_DIA_PAGO, --NULL,-- LO DEVUELVE CMR --
             NULL, -- LO DEVUELVE CMR --
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             V_SCOI_ID,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             V_SCOI_DIA_PAC, --0,-- INICIALIZA DIA PAC HAY 2 ALGORITMOS CARTERA Y VENTA NUEVA --
             V_SCOI_DIA_PAGO, --0,-- INICIALIZA DIA FACTURACION --
             CD.SCCD_MEDIO_PAGO,
             V_SCDEI_TP_ARCHIVO --V_SCDEI_TP_INSCRIPCION
             );
          -- SE ACTUALIZA LA CUADRATURA DIARIA --
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = V_ESTADO_CUADRATURA --PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_CREADOS := (PS_REGISTROS_CREADOS + 1);
        EXCEPTION
          WHEN OTHERS THEN
            RAISE EXC_ERR_GRABAR_INSCRIPCION;
        END;
      EXCEPTION
        WHEN EXC_ERR_OBT_PRODUCTO_PAC THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_PAC
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_OBT_DATOS_CLIENTE THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_DAT_CLI
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_OBT_CUENTA_CMR THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_CTA_VN
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_OBT_DATOS_PROD_RECTOR THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_PROD_REC
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_GRABAR_INSCRIPCION THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_INS_REG
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_OBT_TP_INSCRIPCION THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_INS_REG
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_DATOS_CUENTA THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = V_COD_ERROR
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_DIA_PAGO THEN
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_INSC_INS_REG
           WHERE SCCD_ID = CD.SCCD_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
      END;
    END LOOP;
    COMMIT;
    PS_MENSAJE_ERROR := NULL;
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'ERROR AL PROCESAR PROPUESTA ' ||
                          V_SCCD_PROPUESTA || ', ' || SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END GENERA_REGISTRO_INSCRIPCION;

  ---------------------------------------------------------------------------------------------------------------------
  -- REINSCRIPCION DE PROPUESTAS --
  FUNCTION REINSCRIPCION(PS_MENSAJE_ERROR    IN OUT VARCHAR2,
                         PS_REGISTROS_REINSC IN OUT NUMBER) RETURN BOOLEAN IS
    -----------------------------------------------------------------------------------------
    V_REGISTROS_REINSC NUMBER := 0;
    V_SCCD_PROPUESTA   SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE;
    V_SCOI_ID          SCO_INSCRIPCION.SCOI_ID%TYPE;
    V_SCOI_STATUS      SCO_INSCRIPCION.SCOI_STATUS%TYPE;
    EXC_ERR_INSC_NO_EXISTE EXCEPTION;
    -----------------------------------------------------------------------------------------
    -- CURSOR DE PROPUESTAS A REINSCRIBIR --
    CURSOR REINSCRIPCION IS
      SELECT SCCD_ID,
             SCCD_PROPUESTA
        FROM SCO_CUADRATURA_DIARIA
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_RECH;
  BEGIN
    FOR R IN REINSCRIPCION
    LOOP
      V_SCCD_PROPUESTA := R.SCCD_PROPUESTA;
      BEGIN
        V_SCOI_ID     := NULL;
        V_SCOI_STATUS := NULL;
        SELECT SCOI_ID,
               SCOI_STATUS
          INTO V_SCOI_ID,
               V_SCOI_STATUS
          FROM SCO_INSCRIPCION
         WHERE SCOI_CONTRATO_PROPUESTA = R.SCCD_PROPUESTA;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          RAISE EXC_ERR_INSC_NO_EXISTE;
      END;
      IF (V_SCOI_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_RECH)
      THEN
        UPDATE SCO_INSCRIPCION
           SET SCOI_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         WHERE SCOI_ID = V_SCOI_ID;
        UPDATE SCO_CUADRATURA_DIARIA
           SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV
         WHERE SCCD_ID = R.SCCD_ID;
      ELSIF (V_SCOI_STATUS =
            PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_VN)
      THEN
        UPDATE SCOH_INSCRIPCION
           SET SCOIH_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         WHERE SCOIH_CONTRATO_PROPUESTA = R.SCCD_PROPUESTA
           AND SCOIH_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_RECH;
        UPDATE SCO_CUADRATURA_DIARIA
           SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_VN
         WHERE SCCD_ID = R.SCCD_ID;
      END IF;
      V_REGISTROS_REINSC := (V_REGISTROS_REINSC + 1);
    END LOOP;
    COMMIT;
    PS_MENSAJE_ERROR    := NULL;
    PS_REGISTROS_REINSC := V_REGISTROS_REINSC;
    RETURN TRUE;
  EXCEPTION
    WHEN EXC_ERR_INSC_NO_EXISTE THEN
      PS_MENSAJE_ERROR    := 'LA INSCRIPCI�N ASOCIADA A LA PROPUESTA ' ||
                             V_SCCD_PROPUESTA || ' NO EXISTE';
      PS_REGISTROS_REINSC := 0;
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR    := 'ERROR AL PROCESAR PROPUESTA ' ||
                             V_SCCD_PROPUESTA || ', ' || SQLERRM;
      PS_REGISTROS_REINSC := 0;
      ROLLBACK;
      RETURN FALSE;
  END REINSCRIPCION;

  -------------------------------------------------------------------------------------------------
  -- GENERACION DE ARCHIVO DE INSCRIPCION VISANET --
  -------------------------------------------------------------------------------------------------
  FUNCTION GENERA_ARCHIVO_INSCRIPCION(PE_TP_INSCRIPCION       IN NUMBER,
                                      PE_FECHA_INSCRIPCION    IN DATE,
                                      PE_ID_USUARIO           IN VARCHAR2,
                                      PE_ID_TERMINAL          IN VARCHAR2,
                                      PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                      PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    ---------------------------------------------------------------------------------
    V_ARCHIVO_ENV_INSCRIPCION UTL_FILE.FILE_TYPE;
    V_NOMBRE_DIRECTORIO       VARCHAR2(255) := NULL;
    V_NOMBRE_ARCHIVO_ENV_INSC VARCHAR2(255) := NULL;
    V_LINEA_BUFFER            VARCHAR2(500) := NULL;
    --V_CONTROLJ                  NUMBER := 0;
    V_SUMADOR_RUT NUMBER := 0;
    --V_POSICION                  NUMBER := 0;
    --V_SODP_DIA_PAGO             SCO_DIA_PAGO.SODP_DIA_PAGO%TYPE;
    REG_SCOH_BIT_PROC_INSC      SCOH_BITACORA_PROCESOS_INSC%ROWTYPE;
    REG_SCOH_BIT_PROC_INSC_ARCH SCOH_BITACORA_PROC_INSC_ARCH%ROWTYPE;
    ---------------------------------------------------------------------------------
    EXC_ERR_ABRIR_ARCH_ENV_INSC    EXCEPTION;
    EXC_ERR_OBT_NOMBRE_ARCH_INSC   EXCEPTION;
    EXC_ERR_PROC_INSC_NO_EXISTE    EXCEPTION;
    EXC_ERR_GRAB_BIT_PROC_INSC_ARC EXCEPTION;
    EXC_ERR_ST_PROC_INSC           EXCEPTION;
    EXC_ERR_ARCH_ENV_INSC_YA_EXIS  EXCEPTION;
    EXC_ERR_TP_INSCRIPCION         EXCEPTION;
    EXC_ERR_CONSULTA               EXCEPTION;
    W_MONEDA_INF VARCHAR2(10);
    W_MONEDA     VARCHAR2(10);
    ----------------------------------------------------------------------------------------
    V_FECHA_PROBABLE_COBRO DATE := NULL; --YGP 10-03-2014 
    ----------------------------------------------------------------------------------------
    -- CURSOR    CON TODOS LOA REGISTROS DE ALTA DE INSCRIPCION DE VENTA NUEVA Y OTRAS ALTAS --
    CURSOR C_ALTA IS
      SELECT SCOI_ID,
             SCOI_NUMERO_COMERCIO,
             NVL(SCOI_RUT,
                 0) AS SCOI_RUT,
             NVL(SCOI_DV,
                 0) AS SCOI_DV,
             SCOI_NOMBRE,
             SCOI_CUENTA_TARJETA,
             SCOI_PRODUCTO,
             SCOI_MANDATO,
             SCOI_ORIGEN,
             SCOI_DIRECCION,
             SCOI_FECHA_INICIO,
             SCOI_MONTO,
             SCOI_MONEDA,
             SCOI_TIPO_REGISTRO,
             SCOI_CONTRATO_PROPUESTA,
             SCOI_FILLER,
             SCOI_MEDIO_PAGO MEDIO_PAGO,
             SCCD_PRIMA_BRUTA PRIMA_BRUTA,
             SCOI_DIA_PAC DIA_PAC,
             SCCD_FECHA_DESDE,
             SCCD_ID
        FROM SCO_CUADRATURA_DIARIA,
             SCO_INSCRIPCION
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV
         AND SCCD_PROPUESTA = SCOI_CONTRATO_PROPUESTA
         AND SCOI_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         AND SCOI_TP_INSCRIPCION = PE_TP_INSCRIPCION
      UNION
      -- ALTAS DE CAMBIO DE MEDIO DE PAGO.
      SELECT SCOIH_ID,
             SCOIH_NUMERO_COMERCIO,
             NVL(SCOIH_RUT,
                 0) AS SCOIH_RUT,
             NVL(SCOIH_DV,
                 0) AS SCOIH_DV,
             SCOIH_NOMBRE,
             SCOIH_CUENTA_TARJETA,
             SCOIH_PRODUCTO,
             SCOIH_MANDATO,
             SCOIH_ORIGEN,
             SCOIH_DIRECCION,
             SCOIH_FECHA_INICIO,
             SCOIH_MONTO,
             SCOIH_MONEDA,
             SCOIH_TIPO_REGISTRO,
             SCOIH_CONTRATO_PROPUESTA,
             SCOIH_FILLER,
             SCOIH_MEDIO_PAGO MEDIO_PAGO,
             SCCD_PRIMA_BRUTA PRIMA_BRUTA,
             SCOI_DIA_PAC DIA_PAC,
             SCCD_FECHA_DESDE,
             SCCD_ID
        FROM SCO_CUADRATURA_DIARIA,
             SCO_INSCRIPCION,
             SCOH_INSCRIPCION
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_MEDIO_PAGO
         AND SCCD_PROPUESTA = SCOI_CONTRATO_PROPUESTA
         AND SCOI_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_MEDIO_PAGO
         AND SCOI_TP_INSCRIPCION = PE_TP_INSCRIPCION
         AND SCOI_CONTRATO_PROPUESTA = SCOIH_CONTRATO_PROPUESTA
         AND SCOIH_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         AND SCOIH_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCOIH_TIPO_REGISTRO =
             PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_ALTA --'A'
      UNION -- CAMBIO DE CUENTA --
      SELECT SCOIH_ID,
             SCOIH_NUMERO_COMERCIO,
             NVL(SCOIH_RUT,
                 0) AS SCOIH_RUT,
             NVL(SCOIH_DV,
                 0) AS SCOIH_DV,
             SCOIH_NOMBRE,
             SCOIH_CUENTA_TARJETA,
             SCOIH_PRODUCTO,
             SCOIH_MANDATO,
             SCOIH_ORIGEN,
             SCOIH_DIRECCION,
             SCOIH_FECHA_INICIO,
             SCOIH_MONTO,
             SCOIH_MONEDA,
             SCOIH_TIPO_REGISTRO,
             SCOIH_CONTRATO_PROPUESTA,
             SCOIH_FILLER,
             SCOIH_MEDIO_PAGO MEDIO_PAGO,
             SCCD_PRIMA_BRUTA PRIMA_BRUTA,
             SCOI_DIA_PAC DIA_PAC,
             SCCD_FECHA_DESDE,
             SCCD_ID
        FROM SCO_CUADRATURA_DIARIA,
             SCO_INSCRIPCION,
             SCOH_INSCRIPCION
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_VN
         AND SCCD_PROPUESTA = SCOI_CONTRATO_PROPUESTA
         AND SCOI_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_VN
         AND SCOI_TP_INSCRIPCION = PE_TP_INSCRIPCION
         AND SCOI_CONTRATO_PROPUESTA = SCOIH_CONTRATO_PROPUESTA
         AND SCOIH_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         AND SCOIH_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCOIH_TIPO_REGISTRO =
             PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_ALTA --'A'         
      ;
    ---------------------------------------------------------------------------------
    -- CURSOR DE BAJAS 
    CURSOR C_BAJA IS
    --BAJAS CAMBIO DE MEDIO DE PAGO
      SELECT SCOIH_ID,
             SCOIH_NUMERO_COMERCIO,
             NVL(SCOIH_RUT,
                 0) AS SCOIH_RUT,
             NVL(SCOIH_DV,
                 0) AS SCOIH_DV,
             SCOIH_NOMBRE,
             SCOIH_CUENTA_TARJETA,
             SCOIH_PRODUCTO,
             SCOIH_MANDATO,
             SCOIH_ORIGEN,
             SCOIH_DIRECCION,
             SCOIH_FECHA_INICIO,
             SCOIH_MONTO,
             SCOIH_MONEDA,
             SCOIH_TIPO_REGISTRO,
             SCOIH_CONTRATO_PROPUESTA,
             SCOIH_FILLER,
             SCOIH_MEDIO_PAGO - 1 SCCD_ID,
             -1 SCOI_ID
        FROM SCOH_INSCRIPCION
       WHERE SCOIH_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         AND SCOIH_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCOIH_TIPO_REGISTRO =
             PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_BAJA --'B'
         AND SCOIH_NOMBRE_ARCHIVO IS NULL;
    CURSOR C_MODIFICACION IS -- CAMBIO DE CUENTA
      SELECT SCOIH_ID,
             SCOIH_NUMERO_COMERCIO,
             NVL(SCOIH_RUT,
                 0) AS SCOIH_RUT,
             NVL(SCOIH_DV,
                 0) AS SCOIH_DV,
             SCOIH_NOMBRE,
             SCOIH_CUENTA_TARJETA,
             SCOIH_PRODUCTO,
             SCOIH_MANDATO,
             SCOIH_ORIGEN,
             SCOIH_DIRECCION,
             SCOIH_FECHA_INICIO,
             SCOIH_MONTO,
             SCOIH_MONEDA,
             SCOIH_TIPO_REGISTRO,
             SCOIH_CONTRATO_PROPUESTA,
             SCOIH_FILLER,
             SCOIH_MEDIO_PAGO,
             SCCD_ID,
             SCOI_ID
        FROM SCO_CUADRATURA_DIARIA,
             SCO_INSCRIPCION,
             SCOH_INSCRIPCION
       WHERE SCCD_OPERACION IN (1001,
                                9999)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_VN
         AND SCCD_PROPUESTA = SCOI_CONTRATO_PROPUESTA
         AND SCOI_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_VN
         AND SCOI_TP_INSCRIPCION = PE_TP_INSCRIPCION
         AND SCOI_CONTRATO_PROPUESTA = SCOIH_CONTRATO_PROPUESTA
         AND SCOIH_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
         AND SCOIH_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCOIH_TIPO_REGISTRO =
             PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_MODIFICACION --'M'
       ORDER BY SCOIH_TIPO_REGISTRO DESC;
    ---------------------------------------------------------------------------------
    CURSOR C_CERTIFICADO(PE_CACE_NU_PROPUESTA IN CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE) IS
      SELECT *
        FROM CART_CERTIFICADOS
       WHERE CACE_NU_PROPUESTA = PE_CACE_NU_PROPUESTA;
    ---------------------------------------------------------------------------------
    R_CERTIFICADO C_CERTIFICADO%ROWTYPE;
    ---------------------------------------------------------------------------------
    CURSOR C_CLIENTE(PE_CACN_CD_NACIONALIDAD IN CART_CLIENTES.CACN_CD_NACIONALIDAD%TYPE,
                     PE_CACN_NU_CEDULA_RIF   IN CART_CLIENTES.CACN_NU_CEDULA_RIF %TYPE) IS
      SELECT *
        FROM CART_CLIENTES
       WHERE CACN_CD_NACIONALIDAD = PE_CACN_CD_NACIONALIDAD
         AND CACN_NU_CEDULA_RIF = PE_CACN_NU_CEDULA_RIF;
    ---------------------------------------------------------------------------------
    R_CLIENTE_TITULAR   C_CLIENTE%ROWTYPE;
    R_CLIENTE_ASEGURADO C_CLIENTE%ROWTYPE;
    ---------------------------------------------------------------------------------
    CURSOR C_BANCO(PE_SOCB_PROPUESTA IN SCO_CUENTA_BANCO.SOCB_PROPUESTA%TYPE) IS
      SELECT *
        FROM SCO_CUENTA_BANCO
       WHERE SOCB_PROPUESTA = PE_SOCB_PROPUESTA;
    R_BANCO C_BANCO%ROWTYPE;
    --
    V_SCDEI_TP_REGISTRO SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_REGISTRO%TYPE;
  BEGIN
    ---------------------------------------------------------------------------------
    PS_REGISTROS_PROCESADOS := 0;
    ---------------------------------------------------------------------------------
    -- SE OBTIENE EL PROCESO DE INSCRIPCION EN CURSO --
    REG_SCOH_BIT_PROC_INSC := NULL;
    BEGIN
      SELECT *
        INTO REG_SCOH_BIT_PROC_INSC
        FROM SCOH_BITACORA_PROCESOS_INSC
       WHERE SCHPI_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPI_FECHA_PROCESO_INSC = PE_FECHA_INSCRIPCION;
      IF (REG_SCOH_BIT_PROC_INSC.SCHPI_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_INSC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE('ENTRO EN NO_DATA_FOUND ' ||
                             Pkg_Procesos_Pac_VISANET.CODIGO_MEDIO_PAGO_VN ||
                             ' fecha ' || PE_FECHA_INSCRIPCION);
        RAISE EXC_ERR_PROC_INSC_NO_EXISTE;
    END;
    ---------------------------------------------------------------------------------
    -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE INSCRIPCION PARA EL PROCESO DE INSCRIPCION --
    BEGIN
      SELECT *
        INTO REG_SCOH_BIT_PROC_INSC_ARCH
        FROM SCOH_BITACORA_PROC_INSC_ARCH
       WHERE SCHPIA_ID_PROCESO_INSC =
             REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         AND SCHPIA_TP_INSCRIPCION = PE_TP_INSCRIPCION;
      RAISE EXC_ERR_ARCH_ENV_INSC_YA_EXIS;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
        REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ID_PROCESO_INSC := REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC;
        REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_TP_INSCRIPCION  := PE_TP_INSCRIPCION;
    END;
    ---------------------------------------------------------------------------------
    -- SE OBTIENE EL NOMBRE DEL ARCHIVO DE INSCRIPCION CMR --
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_INSC(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                  PE_TP_INSCRIPCION,
                                                  PKG_PROCESOS_PAC_VISANET.SCDAI_ID_PROCESO_ENV,
                                                  PE_FECHA_INSCRIPCION,
                                                  V_NOMBRE_ARCHIVO_ENV_INSC,
                                                  PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_INSC;
    END IF;
    ---------------------------------------------------------------------------------
    -- SE OBTIENE LA RUTA DEL ARCHIVO DE INSCRIPCIONES --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    ---------------------------------------------------------------------------------
    -- SE ABRE EL ARCHIVO DE INSCRIPCIONES PAC EN MODO DE ESCRITURA --
    BEGIN
      V_ARCHIVO_ENV_INSCRIPCION := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                                  V_NOMBRE_ARCHIVO_ENV_INSC,
                                                  'W');
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_ENV_INSC;
    END;
    -- MMA, 23-04-2012 --
    -- DETERMINA SI ES ALTA, BAJO O MODIFICACION
    ---------------------------------------------------------------------------------
    BEGIN
      SELECT DISTINCT SCDEI_TP_REGISTRO
        INTO V_SCDEI_TP_REGISTRO
        FROM SCO_DEF_ENVIO_INSCRIPCION
       WHERE SCDEI_TP_ARCHIVO = PE_TP_INSCRIPCION;
    EXCEPTION
      --WHEN NO_DATA_FOUND THEN
      --WHEN TOO_MANY_ROWS
      WHEN OTHERS THEN
        RAISE EXC_ERR_TP_INSCRIPCION;
    END;
    IF V_SCDEI_TP_REGISTRO =
       PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_ALTA
    THEN
      -- ALTA --
      -- CABECERA --
      V_LINEA_BUFFER := 'CODIGO INTERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'NRO TARJETA,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'EXPIRACION-MES,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'EXPIRACION-A�O,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-NOMBRES,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-APELLIDO PATERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-APELLIDO MATERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-TELEFONO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-EMAIL,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-NRO IDENTIFICADOR,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-NOMBRES,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-APELLIDO PATERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-APELLIDO MATERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-MONTO MAXIMO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-TIPO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-MONTO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-DIA PAGO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-PERIOCIDAD,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'MONEDA,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-MES PAGO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'CARGO-ANO PAGO';
      --
      UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                        V_LINEA_BUFFER);
      ---------------------------------------------------------------------------------
      -- SE PROCESAN LAS INSCRIPCIONES NORMALES (VENTA NUEVA) --
      FOR R_ALTA IN C_ALTA
      LOOP
        -- OBTIENE DIA PAGO.
        --IF NOT PKG_PROCESOS_PAC.FN_DIA_PAGO(
        --                                  R_ALTA.MEDIO_PAGO,  
        --                                  R_ALTA.DIA_PAC,
        --                                  V_SODP_DIA_PAGO,
        --                                  PS_MENSAJE_ERROR
        --) THEN
        --   RAISE NO_DATA_FOUND;
        --END IF;
        ---------------------------------------------------------------------------------
        -- OBTIENE DATOS DE CUENTA.
        BEGIN
          SELECT *
            INTO R_BANCO
            FROM SCO_CUENTA_BANCO
           WHERE SOCB_PROPUESTA = R_ALTA.SCOI_CONTRATO_PROPUESTA;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CUENTA BANCO :' ||
                                SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO CERTIFICADO   --
        BEGIN
          SELECT *
            INTO R_CERTIFICADO
            FROM CART_CERTIFICADOS
           WHERE CACE_NU_PROPUESTA = R_ALTA.SCOI_CONTRATO_PROPUESTA;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CERTIFICADO :' || SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO TITULAR  --
        BEGIN
          SELECT *
            INTO R_CLIENTE_TITULAR
            FROM CART_CLIENTES
           WHERE CACN_CD_NACIONALIDAD =
                 R_CERTIFICADO.CACE_CACN_CD_NAC_DEUDOR
             AND CACN_NU_CEDULA_RIF = R_CERTIFICADO.CACE_CACN_NU_CED_DEUDOR;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CLIENTE TITULAR:' ||
                                SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO ASEGURADO  --
        --BEGIN
        --   SELECT *
        --     INTO R_CLIENTE_ASEGURADO
        --     FROM CART_CLIENTES
        --    WHERE CACN_CD_NACIONALIDAD = R_CERTIFICADO.CACE_CACN_CD_NACIONALIDAD
        --      AND CACN_NU_CEDULA_RIF   = R_CERTIFICADO.CACE_CACN_NU_CEDULA_RIF;
        --EXCEPTION
        --WHEN OTHERS THEN
        --   PS_MENSAJE_ERROR := 'ERROR AL OBTENER CLIENTE ASEGURADO:'||SQLERRM;
        --   RAISE EXC_ERR_CONSULTA;
        --END;
        ---------------------------------------------------------------------------------
        --INI YGP 10-03-2014 
        V_FECHA_PROBABLE_COBRO := TO_DATE(LPAD(TO_CHAR(R_ALTA.DIA_PAC),
                                               2,
                                               '0') ||
                                          TO_CHAR(SYSDATE,
                                                  'MMRRRR'),
                                          'DD-MM-RRRR');
        IF V_FECHA_PROBABLE_COBRO < TRUNC(SYSDATE) + 2
        THEN
          --LE INCREMENTO UN MES PORQUE VISANET VALIDA QUE SEA MAYOR O IGUAL A 48 HRS
          V_FECHA_PROBABLE_COBRO := ADD_MONTHS(V_FECHA_PROBABLE_COBRO,
                                               1);
        END IF;
        BEGIN
          SELECT UNIQUE CARE_CAMO_CD_MONEDA --CCTE_MONEDA 
            INTO W_MONEDA
            FROM CART_RECIBOS -- SCO_CUENTA_CORRIENTE
           WHERE CARE_CACE_NU_PROPUESTA = R_ALTA.SCOI_CONTRATO_PROPUESTA;
          --WHERE CCTE_PROPUESTA = R_ALTA.SCOI_CONTRATO_PROPUESTA;
        EXCEPTION
          WHEN OTHERS THEN
            W_MONEDA := '02';
        END;
        IF W_MONEDA = '02'
        THEN
          W_MONEDA_INF := PKG_PROCESOS_PAC_VISANET.TP_MONEDA_DOLAR;
        ELSE
          W_MONEDA_INF := PKG_PROCESOS_PAC_VISANET.TP_MONEDA_SOL;
        END IF;
        -- -----------------  ------------------
        --FIN YGP 10-03-2014
        ---------------------------------------------------------------------------------
        V_LINEA_BUFFER := NVL(R_ALTA.SCOI_NUMERO_COMERCIO,
                              '0') || ',' || CHR(39) ||
                          NVL(R_ALTA.SCOI_CUENTA_TARJETA,
                              '0') || ',' ||
                          TO_CHAR(R_BANCO.SOCB_MES_VENCIMIENTO) || ',' ||
                          LPAD(TO_CHAR(R_BANCO.SOCB_ANIO_VENCIMIENTO),
                               4,
                               '20') || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_NOMBRES,
                                 1,
                                 30) || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_APELLIDO_PATERNO,
                                 1,
                                 30) || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_APELLIDO_MATERNO,
                                 1,
                                 30) || ',' || NVL(R_CLIENTE_TITULAR.CACN_NU_TELEFONO_P,
                                                   0) || ',' || ' ' || ',' ||
                         --SUBSTR(R_CLIENTE_TITULAR.CACN_EMAIL,1,30)                  ||','||
                          CHR(39) ||
                          TO_CHAR(R_ALTA.SCOI_CONTRATO_PROPUESTA) || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_NOMBRES,
                                 1,
                                 30) || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_APELLIDO_PATERNO,
                                 1,
                                 20) || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_APELLIDO_MATERNO,
                                 1,
                                 20) || ',' ||
                         --CHR(39)||REPLACE(LTRIM(TO_CHAR((R_ALTA.PRIMA_BRUTA*3),'00000000000000000D00')),',','.')||','|| -- IMPORTE MAXIMO DE CARGO
                         -- CVG - 08-02-2017 , saca ceros a la izquierda en el monto 
                          CHR(39) ||
                         --REPLACE(LTRIM((R_ALTA.PRIMA_BRUTA * 3)),',','.') 
                          REPLACE(LTRIM(TO_CHAR((R_ALTA.PRIMA_BRUTA * 3),
                                                '99999999999999990D00')),
                                  ',',
                                  '.') --JS DESA-4810
                          || ',' || -- IMPORTE MAXIMO DE CARGO-------------
                          PKG_PROCESOS_PAC_VISANET.TP_CARGO_VARIABLE || ',' || -- VARIABLE
                          CHR(39) || /*0000000000000000*/
                          '0.00' || ',' || -- IMPORTE DEL CARGO, CERO!--JS DESA-4810
                         -- MMA, 28-06-2012 --
                         --TO_CHAR(V_SODP_DIA_PAGO)                                   ||','|| -- DIA PAGO
                          R_ALTA.DIA_PAC || ',' ||
                          PKG_PROCESOS_PAC_VISANET.TP_PERIODICIDAD_MENSUAL || ',' || -- PERIODICIDAD, MENSUAL!
                          W_MONEDA_INF || ',' ||
                         --PKG_PROCESOS_PAC_VISANET.TP_MONEDA_DOLAR                   ||','|| -- MONEDA, DOLAR!
                         --TO_CHAR(R_ALTA.SCCD_FECHA_DESDE,'MM')                      ||','|| -- MES DESDE
                         --TO_CHAR(R_ALTA.SCCD_FECHA_DESDE,'RRRR')                            -- MES DESDE
                         --TO_CHAR(SYSDATE, 'MM')                                     ||','|| -- MES DESDE
                         --TO_CHAR(SYSDATE, 'RRRR')                                           -- MES DESDE
                          TO_CHAR(V_FECHA_PROBABLE_COBRO,
                                  'MM') || ',' || -- MES DESDE YGP 10-03-2014
                          TO_CHAR(V_FECHA_PROBABLE_COBRO,
                                  'RRRR') -- MES DESDE YGP 10-03-2014
         ;
        --
        UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                          V_LINEA_BUFFER);
        PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
        V_SUMADOR_RUT           := (V_SUMADOR_RUT + R_ALTA.SCOI_RUT);
        -- SE MODIFICA EL ESTADO DE LA CUADRATURA DIARIA --
        -- A INSCRIPCION EN PROCESO DE RESPUESTA --
        UPDATE SCO_CUADRATURA_DIARIA
           SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_EN_PROCESO_RESP
         WHERE SCCD_ID = R_ALTA.SCCD_ID;
        -- SE MODIFICA EL ID DE INSCRIPCION EN LA TABLA SCO_INSCRIPCION --
        UPDATE SCO_INSCRIPCION
           SET SCOI_SCIN_ID_INSCRIPCION = REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         WHERE SCOI_ID = R_ALTA.SCOI_ID;
      END LOOP;
      ---------------------------------------------------------------------------------
    ELSIF V_SCDEI_TP_REGISTRO =
          PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_BAJA
    THEN
      -- BAJA --
      -- CABECERA --
      V_LINEA_BUFFER := 'CODIGO INTERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-NRO IDENTIFICADOR';
      --
      UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                        V_LINEA_BUFFER);
      FOR R_BAJA IN C_BAJA
      LOOP
        ---------------------------------------------------------------------------------
        V_LINEA_BUFFER := NVL(R_BAJA.SCOIH_NUMERO_COMERCIO,
                              '0') || ',' || CHR(39) ||
                          TO_CHAR(R_BAJA.SCOIH_CONTRATO_PROPUESTA);
        --
        UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                          V_LINEA_BUFFER);
        PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
        ---------------------------------------------------------------------------------           
        -- SE MODIFICA EL ESTADO DE LA CUADRATURA DIARIA --
        -- A INSCRIPCION EN PROCESO DE RESPUESTA --
        UPDATE SCO_CUADRATURA_DIARIA
           SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_EN_PROCESO_RESP
         WHERE SCCD_ID = R_BAJA.SCCD_ID;
        ---------------------------------------------------------------------------------
        -- SE MODIFICA EL ID DE INSCRIPCION EN LA TABLA SCO_INSCRIPCION --
        UPDATE SCO_INSCRIPCION
           SET SCOI_SCIN_ID_INSCRIPCION = REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         WHERE SCOI_ID = R_BAJA.SCOI_ID;
        ---------------------------------------------------------------------------------
        -- ACTUALIZA EL REGISTRO DE CAMBIO DE MEDIO DE PAGO EN SCOH_INCRIPCION --
        -- CON EL NOMBRE DEL ARCHIVO --
        UPDATE SCOH_INSCRIPCION
           SET SCOIH_NOMBRE_ARCHIVO      = V_NOMBRE_ARCHIVO_ENV_INSC,
               SCOIH_SCIN_ID_INSCRIPCION = REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         WHERE SCOIH_ID = R_BAJA.SCOIH_ID;
        ---------------------------------------------------------------------------------
      END LOOP;
    ELSIF V_SCDEI_TP_REGISTRO =
          PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_MODIFICACION
    THEN
      -- CAMBIO DE CUENTA --
      -- CABECERA --
      V_LINEA_BUFFER := 'CODIGO INTERNO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'NRO TARJETA,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'EXPIRACION-MES,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'EXPIRACION-A�O,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-TELEFONO,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'TITULAR-EMAIL,';
      V_LINEA_BUFFER := V_LINEA_BUFFER || 'USUARIO-NRO IDENTIFICADOR';
      --
      UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                        V_LINEA_BUFFER);
      FOR R_MODIFICACION IN C_MODIFICACION
      LOOP
        ---------------------------------------------------------------------------------
        -- OBTIENE DATOS DE CUENTA.
        BEGIN
          SELECT *
            INTO R_BANCO
            FROM SCO_CUENTA_BANCO
           WHERE SOCB_PROPUESTA = R_MODIFICACION.SCOIH_CONTRATO_PROPUESTA;
        EXCEPTION
          WHEN OTHERS THEN
            --CLOSE C_CERTIFICADO;
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CUENTA BANCO :' ||
                                SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO CERTIFICADO   --
        --OPEN C_CERTIFICADO(R_MODIFICACION.SCOIH_CONTRATO_PROPUESTA);
        --FETCH C_CERTIFICADO INTO R_CERTIFICADO;
        --IF C_CERTIFICADO%NOTFOUND THEN
        --   CLOSE C_CERTIFICADO;
        --   RAISE NO_DATA_FOUND;
        --END IF;
        --CLOSE C_CERTIFICADO;
        BEGIN
          SELECT *
            INTO R_CERTIFICADO
            FROM CART_CERTIFICADOS
           WHERE CACE_NU_PROPUESTA =
                 R_MODIFICACION.SCOIH_CONTRATO_PROPUESTA;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CERTIFICADO :' || SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO TITULAR  --
        --OPEN C_CLIENTE(R_CERTIFICADO.CACE_CACN_CD_NAC_DEUDOR, R_CERTIFICADO.CACE_CACN_NU_CED_DEUDOR);
        --FETCH C_CLIENTE INTO R_CLIENTE_TITULAR;
        --IF C_CLIENTE%NOTFOUND THEN
        --   CLOSE C_CLIENTE;
        --   RAISE NO_DATA_FOUND;
        --END IF;
        --CLOSE C_CLIENTE;
        BEGIN
          SELECT *
            INTO R_CLIENTE_TITULAR
            FROM CART_CLIENTES
           WHERE CACN_CD_NACIONALIDAD =
                 R_CERTIFICADO.CACE_CACN_CD_NAC_DEUDOR
             AND CACN_NU_CEDULA_RIF = R_CERTIFICADO.CACE_CACN_NU_CED_DEUDOR;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CLIENTE TITULAR:' ||
                                SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        -- OBTENGO ASEGURADO  --
        --OPEN C_CLIENTE(R_CERTIFICADO.CACE_CACN_CD_NACIONALIDAD, R_CERTIFICADO.CACE_CACN_NU_CEDULA_RIF);
        --FETCH C_CLIENTE INTO R_CLIENTE_ASEGURADO;
        --IF C_CLIENTE%NOTFOUND THEN
        --   CLOSE C_CLIENTE;
        --   RAISE NO_DATA_FOUND;
        --END IF;
        --CLOSE C_CLIENTE;
        BEGIN
          SELECT *
            INTO R_CLIENTE_ASEGURADO
            FROM CART_CLIENTES
           WHERE CACN_CD_NACIONALIDAD =
                 R_CERTIFICADO.CACE_CACN_CD_NACIONALIDAD
             AND CACN_NU_CEDULA_RIF = R_CERTIFICADO.CACE_CACN_NU_CEDULA_RIF;
        EXCEPTION
          WHEN OTHERS THEN
            PS_MENSAJE_ERROR := 'ERROR AL OBTENER CLIENTE ASEGURADO:' ||
                                SQLERRM;
            RAISE EXC_ERR_CONSULTA;
        END;
        ---------------------------------------------------------------------------------
        --
        V_LINEA_BUFFER := NVL(R_MODIFICACION.SCOIH_NUMERO_COMERCIO,
                              '0') || ',' || CHR(39) ||
                          NVL(R_MODIFICACION.SCOIH_CUENTA_TARJETA,
                              '0') || ',' ||
                          TO_CHAR(R_BANCO.SOCB_MES_VENCIMIENTO) || ',' ||
                          TO_CHAR(R_BANCO.SOCB_ANIO_VENCIMIENTO) || ',' ||
                          R_CLIENTE_TITULAR.CACN_NU_TELEFONO_P || ',' ||
                          SUBSTR(R_CLIENTE_TITULAR.CACN_EMAIL,
                                 1,
                                 30) || ',' || CHR(39) ||
                          TO_CHAR(R_MODIFICACION.SCOIH_CONTRATO_PROPUESTA);
        --
        UTL_FILE.PUT_LINE(V_ARCHIVO_ENV_INSCRIPCION,
                          V_LINEA_BUFFER);
        PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
        V_SUMADOR_RUT           := (V_SUMADOR_RUT +
                                   R_MODIFICACION.SCOIH_RUT);
        ---------------------------------------------------------------------------------           
        -- SE MODIFICA EL ESTADO DE LA CUADRATURA DIARIA --
        -- A INSCRIPCION EN PROCESO DE RESPUESTA --
        UPDATE SCO_CUADRATURA_DIARIA
           SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_EN_PROCESO_RESP
         WHERE SCCD_ID = R_MODIFICACION.SCCD_ID;
        ---------------------------------------------------------------------------------
        -- SE MODIFICA EL ID DE INSCRIPCION EN LA TABLA SCO_INSCRIPCION --
        UPDATE SCO_INSCRIPCION
           SET SCOI_SCIN_ID_INSCRIPCION = REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         WHERE SCOI_ID = R_MODIFICACION.SCOI_ID;
        ---------------------------------------------------------------------------------
        -- ACTUALIZA EL REGISTRO DE CAMBIO DE CUENTA EN SCOH_INCRIPCION --
        -- CON EL NOMBRE DEL ARCHIVO --
        UPDATE SCOH_INSCRIPCION
           SET SCOIH_NOMBRE_ARCHIVO      = V_NOMBRE_ARCHIVO_ENV_INSC,
               SCOIH_SCIN_ID_INSCRIPCION = REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         WHERE SCOIH_ID = R_MODIFICACION.SCOIH_ID;
        ---------------------------------------------------------------------------------
      END LOOP;
    END IF;
    -----------------------------------------------------------------------------------------
    -- CIERRA ARCHIVO 
    UTL_FILE.FCLOSE(V_ARCHIVO_ENV_INSCRIPCION);
    -----------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_NOMBRE_ARCHIVO_ENV   := V_NOMBRE_ARCHIVO_ENV_INSC;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_ENV        := PS_REGISTROS_PROCESADOS;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_NOMBRE_ARCHIVO_RESP  := NULL;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_RESP       := NULL;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_ACEPTADOS  := NULL;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_RECHAZADOS := NULL;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ESTADO               := 'PENDIENTE';
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_FECHA_ESTADO         := SYSDATE;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ID_USUARIO           := PE_ID_USUARIO;
    REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ID_TERMINAL          := PE_ID_TERMINAL;
    -- GRABA EL ARCHIVO DE INSCRIPCION EN LA BITACORA DE PROCESOS DE INSCRIPCION --
    IF (PKG_PROCESOS_PAC.GRABAR_SCOH_BIT_PROC_INSC_ARCH(REG_SCOH_BIT_PROC_INSC_ARCH,
                                                        PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_GRAB_BIT_PROC_INSC_ARC;
    END IF;
    -----------------------------------------------------------------------------------------
    --COMMIT;
    -----------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    -----------------------------------------------------------------------------------------
    RETURN TRUE;
    -----------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_CONSULTA THEN
      --CLOSE C_CERTIFICADO;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ST_PROC_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO DE INSCRIPCI�N NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_INSC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO DE INSCRIPCI�N NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_ENV_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL INTENTAR GENERAR EL ARCHIVO DE INSCRIPCION DE NEGOCIOS PAC';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ARCH_ENV_INSC_YA_EXIS THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE ENV�O DE INSCRIPCIONES YA EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_NOMBRE_ARCH_INSC THEN
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_GRAB_BIT_PROC_INSC_ARC THEN
      PS_MENSAJE_ERROR := 'ERROR AL GRABAR EN BITACORA DE PROCESOS DE INSCRIPCI�N, ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_TP_INSCRIPCION THEN
      PS_MENSAJE_ERROR := 'ERROR AL RECUPERAR TIPO DE INSCRIPCION ' ||
                          TO_CHAR(PE_TP_INSCRIPCION);
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      IF PS_MENSAJE_ERROR IS NULL
      THEN
        PS_MENSAJE_ERROR := SQLERRM;
      END IF;
      UTL_FILE.FCLOSE(V_ARCHIVO_ENV_INSCRIPCION);
      ROLLBACK;
      RETURN FALSE;
  END GENERA_ARCHIVO_INSCRIPCION;

  ---------------------------------------------------------------------------------------------------------------------
  -- CARGA DE RESPUESTA DE INSCRIPCION, MEDIO DE PAGO TRANSBANK --
  --------------------------------------------------------------------------------------------------------------------- 
  FUNCTION CARGA_RESPUESTA_INSCRIPCION(PE_TP_INSCRIPCION       IN NUMBER,
                                       PE_FECHA_INSCRIPCION    IN DATE,
                                       PE_ID_USUARIO           IN VARCHAR2,
                                       PE_ID_TERMINAL          IN VARCHAR2,
                                       PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                       PS_REGISTROS_ACEPTADOS  IN OUT NUMBER,
                                       PS_REGISTROS_RECHAZADOS IN OUT NUMBER,
                                       PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    -----------------------------------------------------------------------------------------
    V_ARCHIVO_RESP_INSCRIPCION UTL_FILE.FILE_TYPE;
    V_NOMBRE_DIRECTORIO        VARCHAR2(255) := NULL;
    V_NOMBRE_ARCHIVO_RESP_INSC VARCHAR2(255) := NULL;
    V_LINEA_ARCHIVO            VARCHAR2(500) := NULL;
    --V_REG_RESP_MOV               PKG_PROCESOS_PAC_VISANET.TRESP_NOMINA_MOVIMIENTOS;
    V_MENSAJE_ERROR VARCHAR2(200) := NULL;
    -----------------------------------------------------------------------------------------
    V_SCOI_CONTRATO_PROPUESTA SCO_INSCRIPCION.SCOI_CONTRATO_PROPUESTA%TYPE;
    --V_SCOI_TARJETA                SCO_INSCRIPCION.SCOI_TARJETA%TYPE;
    --V_SCOI_MANDATO                SCO_INSCRIPCION.SCOI_MANDATO%TYPE;
    --V_SCOI_RESPUESTA_CMR          SCO_INSCRIPCION.SCOI_RESPUESTA_CMR%TYPE;
    --V_SCOI_STATUS                 SCO_INSCRIPCION.SCOI_STATUS%TYPE;
    --V_SCOI_DIA_PAGO               SCO_INSCRIPCION.SCOI_DIA_PAGO%TYPE;
    --V_SCOI_DIA_PAC                SCO_INSCRIPCION.SCOI_DIA_PAC%TYPE;
    --V_SCOI_DIA_FAC                SCO_INSCRIPCION.SCOI_DIA_FAC%TYPE;
    --V_SCOI_STRING_DEVUELTO        SCO_INSCRIPCION.SCOI_STRING_DEVUELTO%TYPE;
    --V_SCOI_TIPO_REGISTRO          SCO_INSCRIPCION.SCOI_TIPO_REGISTRO%TYPE;
    V_SCOI_NUMERO_COMERCIO        SCO_INSCRIPCION.SCOI_NUMERO_COMERCIO%TYPE;
    V_SCOIH_ID                    SCOH_INSCRIPCION.SCOIH_ID%TYPE;
    V_SCCD_PLAN                   SCO_CUADRATURA_DIARIA.SCCD_PLAN%TYPE;
    V_SCDEI_TP_ARCHIVO            SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_ARCHIVO%TYPE;
    V_SCDEI_CD_COMPANIA_COMERCIO  SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_COMPANIA_COMERCIO%TYPE;
    V_SCDEI_CD_PRODUCTO_CMR       SCO_DEF_ENVIO_INSCRIPCION.SCDEI_CD_PRODUCTO_CMR%TYPE;
    V_REG_SCOH_BIT_PROC_INSC      SCOH_BITACORA_PROCESOS_INSC%ROWTYPE;
    V_REG_SCOH_BIT_PROC_INSC_ARCH SCOH_BITACORA_PROC_INSC_ARCH%ROWTYPE;
    V_REG_SCO_INSCRIPCION         SCO_INSCRIPCION%ROWTYPE;
    --V_REG_DEF_RESP_PROC           SCO_DEF_RESPUESTAS_PROCESOS%ROWTYPE;
    -----------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------
    -- GIO INICIO - 09-07-2009 PRO CAMBIO MEDIO V2 DECLARACION DE VARIABLES NUEVA
    --V_REG_SCOH_INSCRIPCION SCOH_INSCRIPCION%ROWTYPE;
    --V_DIA_PAC              NUMBER;
    EXC_ERR_PARAMETROS_MULTIPLES EXCEPTION;
    --
    V_INICIO NUMBER;
    V_FIN    NUMBER;
    V_TEXTO  VARCHAR2(200) := NULL;
    -- MMA, 04-07-2012 --
    V_SCDEI_TP_REGISTRO    SCO_DEF_ENVIO_INSCRIPCION.SCDEI_TP_REGISTRO%TYPE;
    V_REGISTROS_PROCESADOS NUMBER := 0;
    V_CUENTAS_NUEVAS       NUMBER := 0;
    V_CUENTAS_EXISTENTES   NUMBER := 0;
    V_CUENTAS_ERRONEAS     NUMBER := 0;
    --
    -- GIO FIN -------------------------------------------------------------------------------
    EXC_ERR_OBT_NOMBRE_ARCH_INSC   EXCEPTION;
    EXC_ERR_ABRIR_ARCH_RESP_INSC   EXCEPTION;
    EXC_ERR_LEER_ARCH_RESP_INSC    EXCEPTION;
    EXC_ERR_ST_PROC_INSC           EXCEPTION;
    EXC_ERR_PROC_INSC_NO_EXISTE    EXCEPTION;
    EXC_ERR_ARCH_RESP_INSC_NO_EXIS EXCEPTION;
    EXC_ERR_ST_ARCH_RESP_INSC      EXCEPTION;
    EXC_ERR_MOD_BIT_PROC_INSC_ARCH EXCEPTION;
    EXC_ERR_NO_EXIST_INSC          EXCEPTION;
    EXC_ERR_NO_EXIST_INSC_HIST     EXCEPTION;
    EXC_ERR_OBT_DAT_RESP_MOV_TRAN  EXCEPTION;
    EXC_ERR_FECHA_CAB_NO_VALIDA    EXCEPTION;
    EXC_ERR_CANT_REG_RESP_NO_VALID EXCEPTION;
    EXC_ERR_ST_MOV_NO_PEND_ENV     EXCEPTION;
    EXC_ERR_TP_MOVTO_NO_VALIDO     EXCEPTION;
    EXC_ERR_TP_INSCRIPCION         EXCEPTION;
    EXC_ERR_CTACTE                 EXCEPTION;
    EXC_ERR_COD_PROD               EXCEPTION;
    EXC_ERR_NO_EXIST_REG_CUADR     EXCEPTION;
    EXC_ERR_OBT_TP_INSCRIPCION     EXCEPTION;
    -----------------------------------------------------------------------------------------
  BEGIN
    -----------------------------------------------------------------------------------------
    PS_REGISTROS_PROCESADOS := 0;
    PS_REGISTROS_ACEPTADOS  := 0;
    PS_REGISTROS_RECHAZADOS := 0;
    -----------------------------------------------------------------------------------------
    -- SE OBTIENE EL PROCESO DE INSCRIPCION EN CURSO --
    V_REG_SCOH_BIT_PROC_INSC := NULL;
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_INSC
        FROM SCOH_BITACORA_PROCESOS_INSC
       WHERE SCHPI_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPI_FECHA_PROCESO_INSC = PE_FECHA_INSCRIPCION;
      IF (V_REG_SCOH_BIT_PROC_INSC.SCHPI_ESTADO != 'PENDIENTE' AND
         V_REG_SCOH_BIT_PROC_INSC.SCHPI_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_INSC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_PROC_INSC_NO_EXISTE;
    END;
    -----------------------------------------------------------------------------------------
    -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE INSCRIPCION PARA EL PROCESO DE INSCRIPCION --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_INSC_ARCH
        FROM SCOH_BITACORA_PROC_INSC_ARCH
       WHERE SCHPIA_ID_PROCESO_INSC =
             V_REG_SCOH_BIT_PROC_INSC.SCHPI_ID_PROCESO_INSC
         AND SCHPIA_TP_INSCRIPCION = PE_TP_INSCRIPCION;
      IF (V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ESTADO != 'PENDIENTE')
      THEN
        RAISE EXC_ERR_ST_ARCH_RESP_INSC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_ARCH_RESP_INSC_NO_EXIS;
    END;
    -----------------------------------------------------------------------------------------
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_INSC(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                  PE_TP_INSCRIPCION,
                                                  PKG_PROCESOS_PAC_VISANET.SCDAI_ID_PROCESO_RESP,
                                                  PE_FECHA_INSCRIPCION,
                                                  V_NOMBRE_ARCHIVO_RESP_INSC,
                                                  PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_INSC;
    END IF;
    -----------------------------------------------------------------------------------------
    -- SE OBTIENE EL DIRECTORIO DE DESTINO DE LOS ARCHIVOS --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    -----------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_RESP_INSCRIPCION := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                                   V_NOMBRE_ARCHIVO_RESP_INSC,
                                                   'R');
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_RESP_INSC;
    END;
    -----------------------------------------------------------------------------------------
    BEGIN
      SELECT DISTINCT SCDEI_TP_REGISTRO
        INTO V_SCDEI_TP_REGISTRO
        FROM SCO_DEF_ENVIO_INSCRIPCION
       WHERE SCDEI_TP_ARCHIVO = PE_TP_INSCRIPCION;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_TP_INSCRIPCION;
    END;
    LOOP
      BEGIN
        -----------------------------------------------------------------------------------------
        BEGIN
          -- LEER LINEA DE REGISTRO DEL ARCHIVO --
          UTL_FILE.GET_LINE(V_ARCHIVO_RESP_INSCRIPCION,
                            V_LINEA_ARCHIVO);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            EXIT;
          WHEN OTHERS THEN
            RAISE EXC_ERR_LEER_ARCH_RESP_INSC;
        END;
        --------------------------------------------------------------------------------------------
        IF V_SCDEI_TP_REGISTRO =
           PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_ALTA
        THEN
          IF SUBSTR(V_LINEA_ARCHIVO,
                    1,
                    6) = 'CODIGO'
          THEN
            NULL;
            -- SE OBTIENEN LOS DATOS DE LA CABECERA --
          ELSE
            BEGIN
              V_INICIO                  := INSTR(V_LINEA_ARCHIVO,
                                                 ',',
                                                 1,
                                                 9);
              V_FIN                     := INSTR(V_LINEA_ARCHIVO,
                                                 ',',
                                                 1,
                                                 10);
              V_TEXTO                   := SUBSTR(V_LINEA_ARCHIVO,
                                                  V_INICIO + 2,
                                                  V_FIN - V_INICIO - 2);
              V_SCOI_CONTRATO_PROPUESTA := TO_NUMBER(V_TEXTO); -- PROPUESTA.                                            
              V_FIN                     := INSTR(V_LINEA_ARCHIVO,
                                                 ',',
                                                 1,
                                                 1);
              V_TEXTO                   := SUBSTR(V_LINEA_ARCHIVO,
                                                  1,
                                                  V_FIN - 1);
              V_SCOI_NUMERO_COMERCIO    := TO_NUMBER(V_TEXTO); --CODIGO CMR 
            EXCEPTION
              WHEN OTHERS THEN
                PS_MENSAJE_ERROR := 'ERROR AL OBTENER PROPUESTA EN LINEA DE ARCHIVO';
                RAISE NO_DATA_FOUND;
            END;
            -- SE OBTIENEN LOS DATOS DEL CUERPO DEL ARCHIVO --
            -----------------------------------------------------------------------------------------
            -- CANTIDAD DE REGISTROS PROCESADOS --
            PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
            -- SE OBTIENEN LOS DATOS DE LA INSCRIPCION --
            BEGIN
              SELECT *
                INTO V_REG_SCO_INSCRIPCION
                FROM SCO_INSCRIPCION
               WHERE SCOI_CONTRATO_PROPUESTA = V_SCOI_CONTRATO_PROPUESTA;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RAISE EXC_ERR_NO_EXIST_INSC;
            END;
            BEGIN
              SELECT SCCD_PLAN
                INTO V_SCCD_PLAN
                FROM SCO_CUADRATURA_DIARIA
               WHERE SCCD_PROPUESTA = V_SCOI_CONTRATO_PROPUESTA;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RAISE EXC_ERR_NO_EXIST_REG_CUADR;
            END;
            -----------------------------------------------------------------------------------------
            -- SE VALIDA QUE REGISTRO DE INSCRIPCION ESTE PENDIENTE DE RESPUESTA --
            IF (V_REG_SCO_INSCRIPCION.SCOI_STATUS IN
               (PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO,
                 PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_MEDIO_PAGO,
                 PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_VN))
            THEN
              V_SCDEI_TP_ARCHIVO           := NULL;
              V_SCDEI_CD_COMPANIA_COMERCIO := NULL;
              V_SCDEI_CD_PRODUCTO_CMR      := NULL;
              IF (V_REG_SCO_INSCRIPCION.SCOI_STATUS IN
                 (PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_MEDIO_PAGO,
                   PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_VN))
              THEN
                BEGIN
                  SELECT SCOIH_ID
                    INTO V_SCOIH_ID
                    FROM SCOH_INSCRIPCION
                   WHERE SCOIH_CONTRATO_PROPUESTA =
                         V_SCOI_CONTRATO_PROPUESTA
                     AND SCOIH_MEDIO_PAGO =
                         PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                     AND SCOIH_STATUS =
                         PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
                     AND SCOIH_TIPO_REGISTRO =
                         PKG_PROCESOS_PAC.CODIGO_ALTA_INSC_CMR;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    RAISE EXC_ERR_NO_EXIST_INSC_HIST;
                END;
                BEGIN
                  SELECT SCDEI_TP_ARCHIVO,
                         SCDEI_CD_COMPANIA_COMERCIO,
                         SCDEI_CD_PRODUCTO_CMR
                    INTO V_SCDEI_TP_ARCHIVO,
                         V_SCDEI_CD_COMPANIA_COMERCIO,
                         V_SCDEI_CD_PRODUCTO_CMR
                    FROM SCO_DEF_ENVIO_INSCRIPCION
                   WHERE SCDEI_CD_PLAN = V_SCCD_PLAN
                     AND SCDEI_CD_MEDIO_PAGO =
                         PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                     AND SCDEI_TP_REGISTRO = 'A';
                EXCEPTION
                  WHEN OTHERS THEN
                    RAISE EXC_ERR_OBT_TP_INSCRIPCION;
                END;
                UPDATE SCOH_INSCRIPCION
                   SET SCOIH_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP
                 WHERE SCOIH_ID = V_SCOIH_ID;
              END IF;
              -- SE MODIFICA EL REGISTRO DE INSCRIPCION EN LA TABLA SCO_INSCRIPCION --
              UPDATE SCO_INSCRIPCION
                 SET SCOI_STATUS          = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP,
                     SCOI_NUMERO_COMERCIO = NVL(V_SCDEI_CD_PRODUCTO_CMR,
                                                SCOI_NUMERO_COMERCIO),
                     SCOI_PRODUCTO        = NVL(V_SCDEI_CD_COMPANIA_COMERCIO,
                                                SCOI_PRODUCTO),
                     SCOI_TP_INSCRIPCION  = NVL(V_SCDEI_TP_ARCHIVO,
                                                SCOI_TP_INSCRIPCION)
               WHERE SCOI_ID = V_REG_SCO_INSCRIPCION.SCOI_ID;
              -- SE ACTUALIZA EL REGISTRO DE CUADRATURA DIARIA COMO ACEPTADO --
              UPDATE SCO_CUADRATURA_DIARIA
                 SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_SIN_CTACTE
               WHERE SCCD_OPERACION IN (1001,
                                        9999)
                 AND SCCD_SUB_OPERACION = 0
                 AND SCCD_PROPUESTA = V_SCOI_CONTRATO_PROPUESTA;
            ELSE
              RAISE EXC_ERR_ST_MOV_NO_PEND_ENV;
            END IF; -- FIN IF DE VALIDACION DE REGISTROS PENDIENTES DE ENVIO --   
            --LLAMAR AL GENERA CUENTAS CORRIENTES 
            IF NOT
                PKG_PROCESOS_PAC_VISANET.GENERA_CUENTAS_CORRIENTES(V_SCOI_CONTRATO_PROPUESTA,
                                                                   V_REGISTROS_PROCESADOS,
                                                                   V_CUENTAS_NUEVAS,
                                                                   V_CUENTAS_EXISTENTES,
                                                                   V_CUENTAS_ERRONEAS,
                                                                   PS_MENSAJE_ERROR)
            THEN
              RAISE EXC_ERR_CTACTE;
            END IF;
            PS_REGISTROS_ACEPTADOS := PS_REGISTROS_ACEPTADOS + 1;
            --
          END IF; -- FIN IF DE REGISTROS PROCESADOS DISTINTOS DE 0 --
        ELSIF V_SCDEI_TP_REGISTRO =
              PKG_PROCESOS_PAC_VISANET.SCDEI_TP_REGISTRO_BAJA
        THEN
          -- BAJA DE ARCHIVO 
          IF SUBSTR(V_LINEA_ARCHIVO,
                    1,
                    6) = 'CODIGO'
          THEN
            NULL; -- SE OBTIENEN LOS DATOS DE LA CABECERA --
          ELSE
            BEGIN
              V_INICIO                  := INSTR(V_LINEA_ARCHIVO,
                                                 ',',
                                                 1,
                                                 1);
              V_TEXTO                   := SUBSTR(V_LINEA_ARCHIVO,
                                                  V_INICIO + 2);
              V_SCOI_CONTRATO_PROPUESTA := TO_NUMBER(V_TEXTO);
            EXCEPTION
              WHEN OTHERS THEN
                PS_MENSAJE_ERROR := 'ERROR AL OBTENER PROPUESTA EN LINEA DE ARCHIVO';
                RAISE NO_DATA_FOUND;
            END;
            -- SE OBTIENEN LOS DATOS DEL CUERPO DEL ARCHIVO --
            -----------------------------------------------------------------------------------------
            -- CANTIDAD DE REGISTROS PROCESADOS --
            PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
            BEGIN
              SELECT SCOIH_ID
                INTO V_SCOIH_ID
                FROM SCOH_INSCRIPCION
               WHERE SCOIH_CONTRATO_PROPUESTA = V_SCOI_CONTRATO_PROPUESTA
                 AND SCOIH_MEDIO_PAGO =
                     PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                 AND SCOIH_STATUS =
                     PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO
                 AND SCOIH_TIPO_REGISTRO =
                     PKG_PROCESOS_PAC.CODIGO_BAJA_INSC_CMR;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                RAISE EXC_ERR_NO_EXIST_INSC_HIST;
            END;
            UPDATE SCOH_INSCRIPCION
               SET SCOIH_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP
             WHERE SCOIH_ID = V_SCOIH_ID;
            PS_REGISTROS_ACEPTADOS := PS_REGISTROS_ACEPTADOS + 1;
            --
          END IF;
        END IF;
      EXCEPTION
        WHEN EXC_ERR_CTACTE THEN
          PS_REGISTROS_RECHAZADOS := PS_REGISTROS_RECHAZADOS + 1;
      END;
    END LOOP; -- FIN DEL RECORRIDO DEL ARCHIVO --
    -----------------------------------------------------------------------------------------
    UTL_FILE.FCLOSE(V_ARCHIVO_RESP_INSCRIPCION);
    -----------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ESTADO               := 'FINALIZADO';
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_FECHA_ESTADO         := SYSDATE;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ID_USUARIO           := PE_ID_USUARIO;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_ID_TERMINAL          := PE_ID_TERMINAL;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_NOMBRE_ARCHIVO_RESP  := V_NOMBRE_ARCHIVO_RESP_INSC;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_RESP       := PS_REGISTROS_PROCESADOS;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_ACEPTADOS  := PS_REGISTROS_ACEPTADOS;
    V_REG_SCOH_BIT_PROC_INSC_ARCH.SCHPIA_REGISTROS_RECHAZADOS := PS_REGISTROS_RECHAZADOS;
    -- MODIFICA EL ARCHIVO DE INSCRIPCION EN LA BITACORA DE PROCESOS DE INSCRIPCION --
    IF (PKG_PROCESOS_PAC.MODIFICAR_SCOH_BIT_PROC_INSC_A(V_REG_SCOH_BIT_PROC_INSC_ARCH,
                                                        PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_MOD_BIT_PROC_INSC_ARCH;
    END IF;
    -----------------------------------------------------------------------------------------
    COMMIT;
    -----------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    -----------------------------------------------------------------------------------------
    RETURN TRUE;
    -----------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_ST_PROC_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO DE INSCRIPCI�N NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_INSC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO DE INSCRIPCI�N NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ST_ARCH_RESP_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL ARCHIVO DE RESPUESTA DE INSCRIPCIONES NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ARCH_RESP_INSC_NO_EXIS THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE RESPUESTA DE INSCRIPCIONES NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_NOMBRE_ARCH_INSC THEN
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_RESP_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL INTENTAR ABRIR EL ARCHIVO DE RESPUESTA DE INSCRIPCION DE NEGOCIOS PAC';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_LEER_ARCH_RESP_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL LEER EL ARCHIVO DE RESPUESTA DE INSCRIPCI�N';
      UTL_FILE.FCLOSE(V_ARCHIVO_RESP_INSCRIPCION);
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_MOD_BIT_PROC_INSC_ARCH THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR BITACORA DE PROCESOS DE INSCRIPCI�N' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_NO_EXIST_REG_CUADR THEN
      PS_MENSAJE_ERROR := 'ERROR AL BUSCAR REGISTRO EN CUADRATURA DIARIA ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_NO_EXIST_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL BUSCAR REGISTRO DE INSCRIPCION ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_NO_EXIST_INSC_HIST THEN
      PS_MENSAJE_ERROR := 'ERROR AL BUSCAR REGISTRO DE CAMBIO MEDIO DE PAGO/CAMBIO DE CUENTA ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_FECHA_CAB_NO_VALIDA THEN
      PS_MENSAJE_ERROR := 'LA FECHA DE INSCRIPCI�N DEL ARCHIVO DE RESPUESTA NO ES V�LIDA';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_CANT_REG_RESP_NO_VALID THEN
      PS_MENSAJE_ERROR := 'LA CANTIDAD DE REGISTROS DEL ARCHIVO DE RESPUESTA NO ES V�LIDA';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ST_MOV_NO_PEND_ENV THEN
      PS_MENSAJE_ERROR := 'EL ESTADO DE INSCRIPCI�N DE LA PROPUESTA ' ||
                          V_SCOI_CONTRATO_PROPUESTA || ', NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_TP_MOVTO_NO_VALIDO THEN
      PS_MENSAJE_ERROR := 'EL TIPO DE MOVIMIENTO DE RESPUESTA DE LA PROPUESTA ' ||
                          V_SCOI_CONTRATO_PROPUESTA || ', NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PARAMETROS_MULTIPLES THEN
      PS_MENSAJE_ERROR := 'Problemas al buscar parametros multiples ' ||
                          SQLERRM;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_DAT_RESP_MOV_TRAN THEN
      PS_MENSAJE_ERROR := 'Error: Al obtener respuesta de inscripci�n ' ||
                          V_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_TP_INSCRIPCION THEN
      PS_MENSAJE_ERROR := 'Error: Al obtener Tipo de Inscripci�n '; --||V_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_COD_PROD THEN
      PS_MENSAJE_ERROR := 'Error: codigo de producto incorrecto '; --||V_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := SQLERRM;
      UTL_FILE.FCLOSE(V_ARCHIVO_RESP_INSCRIPCION);
      ROLLBACK;
      RETURN FALSE;
  END CARGA_RESPUESTA_INSCRIPCION;

  ---------------------------------------------------------------------------------------------------
  ---------------------------------------------------------------------------------------------------
  -- SE OBTIENEN LOS DATOS DE LA RESPUESTA DE CARGOS TRANSBANK --
  FUNCTION OBTENER_DATOS_RESP_CARGOS(PE_STR           IN VARCHAR2,
                                     PS_REG_RESP      OUT TRESP_NOMINA_CARGOS,
                                     PS_MENSAJE_ERROR OUT VARCHAR2)
    RETURN BOOLEAN IS
    V_POS_MARCA_SIG NUMBER := 1;
    V_POS_MARCA_INI NUMBER := 1;
    V_I             NUMBER := 1;
    V_VALOR         VARCHAR2(100) := NULL;
  BEGIN
    IF SUBSTR(PE_STR,
              1,
              1) IN ('H',
                     'F')
    THEN
      RETURN FALSE;
    END IF;
    -- SE OBTIENEN LOS CAMPOS DE LA RESPUESTA --
    LOOP
      IF (INSTR(PE_STR,
                ';',
                1,
                V_I) = 0)
      THEN
        EXIT;
      ELSE
        V_POS_MARCA_SIG := INSTR(PE_STR,
                                 ';',
                                 1,
                                 V_I);
        --V_VALOR         := NULL;
        V_VALOR := SUBSTR(PE_STR,
                          V_POS_MARCA_INI,
                          V_POS_MARCA_SIG - V_POS_MARCA_INI);
        IF (V_I = 1)
        THEN
          PS_REG_RESP.TIPO_TRANSACCION := V_VALOR;
        ELSIF (V_I = 2)
        THEN
          PS_REG_RESP.MONTO_TRANSACCION := V_VALOR;
        ELSIF (V_I = 3)
        THEN
          PS_REG_RESP.NUMERO_TARJETA := V_VALOR;
        ELSIF (V_I = 4)
        THEN
          PS_REG_RESP.FECHA_EXPIRACION := V_VALOR;
        ELSIF (V_I = 5)
        THEN
          PS_REG_RESP.FILLER1 := V_VALOR;
        ELSIF (V_I = 6)
        THEN
          PS_REG_RESP.NOMBRE_TARJETA_HAB := V_VALOR;
        ELSIF (V_I = 7)
        THEN
          PS_REG_RESP.DESCRIPCION_SERV := V_VALOR;
        ELSIF (V_I = 8)
        THEN
          PS_REG_RESP.TEL_TARJETA_HAB := V_VALOR;
        ELSIF (V_I = 9)
        THEN
          PS_REG_RESP.ID_SERVICIO := V_VALOR;
        ELSIF (V_I = 10)
        THEN
          PS_REG_RESP.RUT_TARJETA_HAB := V_VALOR;
        ELSIF (V_I = 11)
        THEN
          PS_REG_RESP.REF_COBRO_EFECTUADO := V_VALOR;
        ELSIF (V_I = 12)
        THEN
          PS_REG_RESP.CODIGO_AUTORIZACION := V_VALOR;
        ELSIF (V_I = 13)
        THEN
          PS_REG_RESP.CODIGO_RESPUESTA := V_VALOR;
        ELSIF (V_I = 14)
        THEN
          PS_REG_RESP.GLOSA_RESPUESTA := V_VALOR;
        ELSIF (V_I = 15)
        THEN
          PS_REG_RESP.FILLER2 := V_VALOR;
        ELSIF (V_I = 16)
        THEN
          PS_REG_RESP.FILLER3 := V_VALOR;
        ELSIF (V_I = 17)
        THEN
          PS_REG_RESP.FECHA_PROCESO := V_VALOR;
        ELSIF (V_I = 18)
        THEN
          PS_REG_RESP.OBSERVACIONES := V_VALOR;
        END IF;
        V_POS_MARCA_INI := (V_POS_MARCA_SIG + 1);
        V_I             := (V_I + 1);
      END IF;
    END LOOP;
    ---------------------------------------------------------------------------------------------------
    -- SE OBTIENE EL VALOR DEL ULTIMO CAMPO --
    --V_VALOR := NULL;
    V_VALOR := SUBSTR(PE_STR,
                      V_POS_MARCA_SIG + 1);
    IF (V_I = 1)
    THEN
      PS_REG_RESP.TIPO_TRANSACCION := V_VALOR;
    ELSIF (V_I = 2)
    THEN
      PS_REG_RESP.MONTO_TRANSACCION := V_VALOR;
    ELSIF (V_I = 3)
    THEN
      PS_REG_RESP.NUMERO_TARJETA := V_VALOR;
    ELSIF (V_I = 4)
    THEN
      PS_REG_RESP.FECHA_EXPIRACION := V_VALOR;
    ELSIF (V_I = 5)
    THEN
      PS_REG_RESP.FILLER1 := V_VALOR;
    ELSIF (V_I = 6)
    THEN
      PS_REG_RESP.NOMBRE_TARJETA_HAB := V_VALOR;
    ELSIF (V_I = 7)
    THEN
      PS_REG_RESP.DESCRIPCION_SERV := V_VALOR;
    ELSIF (V_I = 8)
    THEN
      PS_REG_RESP.TEL_TARJETA_HAB := V_VALOR;
    ELSIF (V_I = 9)
    THEN
      PS_REG_RESP.ID_SERVICIO := V_VALOR;
    ELSIF (V_I = 10)
    THEN
      PS_REG_RESP.RUT_TARJETA_HAB := V_VALOR;
    ELSIF (V_I = 11)
    THEN
      PS_REG_RESP.REF_COBRO_EFECTUADO := V_VALOR;
    ELSIF (V_I = 12)
    THEN
      PS_REG_RESP.CODIGO_AUTORIZACION := V_VALOR;
    ELSIF (V_I = 13)
    THEN
      PS_REG_RESP.CODIGO_RESPUESTA := V_VALOR;
    ELSIF (V_I = 14)
    THEN
      PS_REG_RESP.GLOSA_RESPUESTA := V_VALOR;
    ELSIF (V_I = 15)
    THEN
      PS_REG_RESP.FILLER2 := V_VALOR;
    ELSIF (V_I = 16)
    THEN
      PS_REG_RESP.FILLER3 := V_VALOR;
    ELSIF (V_I = 17)
    THEN
      PS_REG_RESP.FECHA_PROCESO := V_VALOR;
    ELSIF (V_I = 18)
    THEN
      PS_REG_RESP.OBSERVACIONES := V_VALOR;
    END IF;
    ---------------------------------------------------------------------------------------------------
    DBMS_OUTPUT.PUT_LINE('TIPO_TRANSACCION    : ' ||
                         PS_REG_RESP.TIPO_TRANSACCION);
    DBMS_OUTPUT.PUT_LINE('MONTO_TRANSACCION   : ' ||
                         PS_REG_RESP.MONTO_TRANSACCION);
    DBMS_OUTPUT.PUT_LINE('NUMERO_TARJETA      : ' ||
                         PS_REG_RESP.NUMERO_TARJETA);
    DBMS_OUTPUT.PUT_LINE('FECHA_EXPIRACION    : ' ||
                         PS_REG_RESP.FECHA_EXPIRACION);
    DBMS_OUTPUT.PUT_LINE('FILLER1             : ' || PS_REG_RESP.FILLER1);
    DBMS_OUTPUT.PUT_LINE('NOMBRE_TARJETA_HAB  : ' ||
                         PS_REG_RESP.NOMBRE_TARJETA_HAB);
    DBMS_OUTPUT.PUT_LINE('DESCRIPCION_SERV    : ' ||
                         PS_REG_RESP.DESCRIPCION_SERV);
    DBMS_OUTPUT.PUT_LINE('TEL_TARJETA_HAB     : ' ||
                         PS_REG_RESP.TEL_TARJETA_HAB);
    DBMS_OUTPUT.PUT_LINE('ID_SERVICIO         : ' ||
                         PS_REG_RESP.ID_SERVICIO);
    DBMS_OUTPUT.PUT_LINE('RUT_TARJETA_HAB     : ' ||
                         PS_REG_RESP.RUT_TARJETA_HAB);
    DBMS_OUTPUT.PUT_LINE('REF_COBRO_EFECTUADO : ' ||
                         PS_REG_RESP.REF_COBRO_EFECTUADO);
    DBMS_OUTPUT.PUT_LINE('CODIGO_AUTORIZACION : ' ||
                         PS_REG_RESP.CODIGO_AUTORIZACION);
    DBMS_OUTPUT.PUT_LINE('CODIGO_RESPUESTA    : ' ||
                         PS_REG_RESP.CODIGO_RESPUESTA);
    DBMS_OUTPUT.PUT_LINE('GLOSA_RESPUESTA     : ' ||
                         PS_REG_RESP.GLOSA_RESPUESTA);
    DBMS_OUTPUT.PUT_LINE('FILLER2             : ' || PS_REG_RESP.FILLER2);
    DBMS_OUTPUT.PUT_LINE('FILLER3             : ' || PS_REG_RESP.FILLER3);
    DBMS_OUTPUT.PUT_LINE('FECHA_PROCESO       : ' ||
                         PS_REG_RESP.FECHA_PROCESO);
    DBMS_OUTPUT.PUT_LINE('OBSERVACIONES       : ' ||
                         PS_REG_RESP.OBSERVACIONES);
    ---------------------------------------------------------------------------------------------------
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := SQLERRM;
      RETURN FALSE;
  END OBTENER_DATOS_RESP_CARGOS;

  ---------------------------------------------------------------------------------------------------
  -- GENERACION DE CUENTAS CORRIENTES --
  /*
  FUNCTION GENERA_CUENTAS_CORRIENTES (
                                      PE_PROPUESTA            IN     SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE,
                                      PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                      PS_CUENTAS_NUEVAS       IN OUT NUMBER,
                                      PS_CUENTAS_EXISTENTES   IN OUT NUMBER,
                                      PS_CUENTAS_ERRONEAS     IN OUT NUMBER,
                                      PS_MENSAJE_ERROR        IN OUT VARCHAR2
                                      ) RETURN BOOLEAN
  IS
    ------------------------------------------------------------------------------------
    V_MES                         NUMBER;
    V_MES_DESFASE                 NUMBER;
    V_REGISTROS_CTACTE            NUMBER;
    V_FECHA_CALCULO_PAC           DATE;
    V_CCTE_PROPUESTA              SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_CCTE_ID                     SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_STATUS                 SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE;
    V_CCTE_CUOTA                  SCO_CUENTA_CORRIENTE.CCTE_CUOTA%TYPE;
    V_CCTE_FINICIAL               SCO_CUENTA_CORRIENTE.CCTE_FINICIAL%TYPE;
    V_CCTE_FFINAL                 SCO_CUENTA_CORRIENTE.CCTE_FFINAL%TYPE;
    V_CCTE_FECHA_ENVIO_PAC        SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
    V_CCTE_PERIODO                SCO_CUENTA_CORRIENTE.CCTE_PERIODO%TYPE;
    V_CCTE_MONEDA                 SCO_CUENTA_CORRIENTE.CCTE_MONEDA%TYPE;
    V_CCTE_MONTO_MONEDA           SCO_CUENTA_CORRIENTE.CCTE_MONTO_MONEDA%TYPE;
    V_CCTE_MONTO_PESOS            SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    V_CCTE_TP_MOVTO               SCO_CUENTA_CORRIENTE. CCTE_TP_MOVTO %TYPE := PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR;
    V_SCCP_STRING_VENTA           SCO_PRODUCTOS.SCCP_STRING_VENTA%TYPE;
    V_SCCP_IND_FECHA_COBRANZA     SCO_PRODUCTOS.SCCP_IND_FECHA_COBRANZA%TYPE;
    V_CAPB_NU_SEGURO_CMR          CART_PRODPLANES.CAPB_NU_SEGURO_CMR%TYPE;
    V_CAPB_CACM_CD_COMPANIA       CART_PRODPLANES.CAPB_CACM_CD_COMPANIA%TYPE;
    V_CACE_CAPU_CD_PRODUCTO       CART_CERTIFICADOS.CACE_CAPU_CD_PRODUCTO%TYPE;
    V_CACW_NU_POLIZA_LIDER        CART_CERTIFICADOS_ENDOSOS.CACW_NU_POLIZA_LIDER%TYPE;
    V_CACW_NU_CERTIFICADO_LIDER   CART_CERTIFICADOS_ENDOSOS.CACW_NU_CERTIFICADO_LIDER%TYPE;
    V_CACE_NU_ENDOSO              CART_CERTIFICADOS.CACE_NU_ENDOSO%TYPE;
    V_CACE_ST_CERTIFICADO         CART_CERTIFICADOS.CACE_ST_CERTIFICADO%TYPE;
    V_REG_RECIBO                  CART_RECIBOS%ROWTYPE;
    V_DIA_PAC                     NUMBER     := NULL;
    V_FEC_PAC_PRIMERA             DATE       := NULL;
    V_ERROR_35                    BOOLEAN    := FALSE;
    V_MODIFICO_FECHA_PAC_RECTOR   BOOLEAN          := FALSE;
    V_MENSAJE_ERROR               VARCHAR2( 2000 ) := NULL;
    ------------------------------------------------------------------------------------
    EXC_ERROR_NO_EXIST_PROD_PAC   EXCEPTION;
    EXC_ERROR_CONF_CUOTAGRATIS    EXCEPTION;
    EXC_ERROR_CALCULO_FECHA_PAC   EXCEPTION;
    EXC_ERROR_OBT_CONV_ST_RECIBOS EXCEPTION;
    ------------------------------------------------------------------------------------
    CURSOR INSC_ACEP_SIN_CTA_CTE IS
        SELECT SCCD_ID,
               SCCD_FECHA_OP,
               SCCD_PROPUESTA,
               SCCD_PERIODO,
               SCCD_OPERACION,
               SCCD_ORIGEN,
               SCCD_SUB_OPERACION,
               SCCD_ORIGEN2,
               SCCD_STATUS,
               SCCD_OBSERVACION,
               SCCD_SUCURSAL,
               SCCD_RAMO,
               SCCD_POLIZA,
               SCCD_CERTIFICADO,
               SCCD_MEDIO_PAGO,
               SCCD_PLAN,
               SCCD_PUNTO_VENTA,
               SCCD_PRODUCTOR,
               SCCD_USER,
               SCCD_CIERRE1,
               SCCD_CIERRE2,
               SCCD_CIERRE3,
               SCCD_CIERRE4,
               SCCD_CIERRE5,
               SCCD_CIERRE6,
               SCCD_CIERRE7,
               SCCD_CIERRE8,
               SCCD_ANO,
               SCCD_MES,
               SCCD_PRIMA_BRUTA,
               SCCD_PRIMA_IVA,
               SCCD_PRIMA_MONEDA,
               SCCD_COTIZACION,
               SCCD_CONSECUTIVO,
               SCCD_FECHA_DESDE,
               SCCD_FECHA_HASTA,
               SCCD_FRAGMENT,
               SCOI_ID,
               SCOI_DIA_PAC,
               0 AS SCOIH_ID
          FROM SCO_CUADRATURA_DIARIA,
               SCO_INSCRIPCION
         WHERE SCCD_OPERACION          IN (PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_VENTA_NUEVA,PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_TRASP_CARTERA)
           AND SCCD_SUB_OPERACION      = 0
           AND SCCD_MEDIO_PAGO         = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
           AND SCCD_STATUS             = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_SIN_CTACTE
           AND SCCD_PROPUESTA          = NVL(PE_PROPUESTA,SCCD_PROPUESTA)
           AND SCOI_CONTRATO_PROPUESTA = SCCD_PROPUESTA
           AND SCOI_STATUS             = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP;
    ------------------------------------------------------------------------------------
    CURSOR RECIBOS_PENDIENTES_CTACTE (PE_PROPUESTA IN NUMBER) IS
        SELECT CCTE_ID
          FROM SCO_CUENTA_CORRIENTE
         WHERE CCTE_PROPUESTA        = PE_PROPUESTA
           AND CCTE_TP_MOVTO         = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
           AND CCTE_MOVTO            = PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE
           AND CCTE_STATUS           = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
           AND CCTE_TRASP_ENV_CARGOS = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO
         ORDER BY CCTE_PERIODO,CCTE_CUOTA;
    ------------------------------------------------------------------------------------
    CURSOR C_RECIBO IS
    SELECT CART_RECIBOS.*
      FROM CART_RECIBOS,
           CART_CERTIFICADOS
     WHERE CACE_NU_PROPUESTA     = V_CCTE_PROPUESTA
       AND CACE_CASU_CD_SUCURSAL = CARE_CASU_CD_SUCURSAL
       AND CACE_CARP_CD_RAMO     = CARE_CARP_CD_RAMO
       AND CACE_CAPO_NU_POLIZA   = CARE_CAPO_NU_POLIZA
       AND CACE_NU_CERTIFICADO   = CARE_CACE_NU_CERTIFICADO
       AND (
            (CARE_ST_RECIBO        = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_PENDIENTE AND
             V_CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VIGENTE)
            OR
            (CARE_NU_ENDOSO        = 0 AND
             V_CACE_ST_CERTIFICADO IN (
                                       PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA,
                                       PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA,
                                       PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA,
                                       PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_EN_RENOVACION
                                       ,PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_MIGRACION   ---JLG <SRF 74849> 03122014
                                       ))
            )
     ORDER BY CARE_NU_CONSECUTIVO_CUOTA;
    ------------------------------------------------------------------------------------
  BEGIN
       ------------------------------------------------------------------------------------
       -- SE LIMPIAN LAS VARIABLES DE SALIDA --
       PS_REGISTROS_PROCESADOS := 0;
       PS_CUENTAS_NUEVAS       := 0;
       PS_CUENTAS_EXISTENTES   := 0;
       PS_CUENTAS_ERRONEAS     := 0;
       ------------------------------------------------------------------------------------
       FOR INSC IN INSC_ACEP_SIN_CTA_CTE LOOP
       BEGIN
           ----------------------------------------------------------------------
           PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
           ----------------------------------------------------------------------
           -- SE OBTIENE LA CANTIDAD DE REGISTROS EN LA CUENTA CORRIENTE POR PROPUESTA --
           SELECT COUNT(*)
             INTO V_REGISTROS_CTACTE
             FROM SCO_CUENTA_CORRIENTE
            WHERE CCTE_PROPUESTA = INSC.SCCD_PROPUESTA
              AND CCTE_TP_MOVTO  = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
              AND CCTE_MOVTO     = PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE
              AND CCTE_STATUS    IN (
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_ANULADO,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_CUOTA_GRATIS,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_NO_REALIZADO,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_CARGO_CAJA_CMR,
                                     PKG_PROCESOS_PAC_VISANET.CCTE_ST_SIN_MEDIO_PAGO
                                     );
           IF (V_REGISTROS_CTACTE = 0) THEN
              ----------------------------------------------------------------
              -- SE TRASPASA EL VALOR DE LA PROPUESTA A LA VARIABLE DE PASO --
              V_CCTE_PROPUESTA := INSC.SCCD_PROPUESTA;
              ----------------------------------------------------------------
              -- SE OBTIENE STRING DE VENTA DEL PRODUCTO Y EL INDICADOR DE FECHA DE CALCULO --
              V_SCCP_STRING_VENTA       := NULL;
              V_SCCP_IND_FECHA_COBRANZA := NULL;
              ----------------------------------------------------------------
              BEGIN
                   SELECT SCCP_STRING_VENTA,
                          SCCP_IND_FECHA_COBRANZA
                     INTO V_SCCP_STRING_VENTA,
                          V_SCCP_IND_FECHA_COBRANZA
                     FROM SCO_PRODUCTOS
                    WHERE SCCP_CAPB_CD_PLAN = INSC.SCCD_PLAN;
              EXCEPTION
                       WHEN NO_DATA_FOUND THEN
                            -- SE MODIFICA ESTADO EN CUADRATURA PARA QUE SEA RE-CERTIFICADO --
                            UPDATE SCO_CUADRATURA_DIARIA
                               SET SCCD_STATUS    = PKG_PROCESOS_PAC.SCCD_ST_ERR_NO_EXIST_PRODPAC
                             WHERE SCCD_PROPUESTA = INSC.SCCD_PROPUESTA;
                            -------------------------------------------------------------------
                            PS_MENSAJE_ERROR := 'ERROR AL PROCESAR PROPUESTA '||INSC.SCCD_PROPUESTA||', NO EXISTE PRODUCTO EN SCO_PRODUCTOS';
                            RAISE EXC_ERROR_NO_EXIST_PROD_PAC;
                            -------------------------------------------------------------------
              END;
              ----------------------------------------------------------------
              -- SE DEFINE PERIODO DE INICIO 1 PARA LA VENTA NUEVA --
              V_CCTE_PERIODO := 1;
              ----------------------------------------------------------------
              -- MMA, 27-02-2012                                            --
              -- SE OBTIENE LA FECHA DE CALCULO PAC --
              V_FECHA_CALCULO_PAC  := NULL;
              --
              IF NOT PKG_PROCESOS_PAC.FN_CALCULA_DIA_PAC(V_CCTE_PROPUESTA, V_FECHA_CALCULO_PAC, V_MENSAJE_ERROR) THEN
              -- ERROR..
                 RAISE EXC_ERROR_CALCULO_FECHA_PAC;
              END IF;
              -- FECHA PAC OK..DETERMINO DIA PAC!
              V_DIA_PAC := TO_NUMBER( TO_CHAR( V_FECHA_CALCULO_PAC, 'DD' ) );
              ----------------------------------------------------------------
              V_CACE_CAPU_CD_PRODUCTO := NULL;
              SELECT CACE_CAPU_CD_PRODUCTO,
                     CACE_NU_ENDOSO,
                     CACE_ST_CERTIFICADO
                INTO V_CACE_CAPU_CD_PRODUCTO,
                     V_CACE_NU_ENDOSO,
                     V_CACE_ST_CERTIFICADO
                FROM CART_CERTIFICADOS
               WHERE CACE_NU_PROPUESTA = INSC.SCCD_PROPUESTA;
              ------------------------------------------------------------------
              -- SE MARCA CUOTA EN 0 PARA VALIDAR EXISTENCIA DE RECIBOS RECTOR -
              ------------------------------------------------------------------
              V_CCTE_CUOTA := 0;
              ------------------------------------------------------------------
              FOR V_REG_RECIBO IN C_RECIBO LOOP
                  ----------------------------------------------------------------
                  V_CCTE_CUOTA := (V_REG_RECIBO.CARE_NU_CONSECUTIVO_CUOTA - 1);
                  -- DIA PAC SE DETERMINO ANTES DEL LOOP, 03-11-2011.
                  ----------------------------------------------------------------
                  -- MMA 06-02-2012                                             --
                  ----------------------------------------------------------------
                  V_CCTE_FECHA_ENVIO_PAC := ADD_MONTHS( V_FECHA_CALCULO_PAC, ( V_CCTE_CUOTA ) );
                  ----------------------------------------------------------------
                  V_CCTE_FINICIAL    := V_CCTE_FECHA_ENVIO_PAC;
                  V_CCTE_FFINAL      := ADD_MONTHS(V_CCTE_FECHA_ENVIO_PAC, 1);
                  ----------------------------------------------------------------
                  V_CCTE_CUOTA       := (V_CCTE_CUOTA + 1);
                  V_CCTE_MONTO_PESOS := 0;
                  ----------------------------------------------------------------
                  -- NUEVO ID PARA SCO_CUENTA_CORRIENTE --
                  V_CCTE_ID := NULL;
                  V_CCTE_ID := PKG_PROCESOS_PAC.OBTENER_ID_REGISTRO_CTA_CTE;
                  ----------------------------------------------------------------
                  V_CAPB_NU_SEGURO_CMR    := NULL;
                  V_CAPB_CACM_CD_COMPANIA := NULL;
                  ----------------------------------------------------------------
                  SELECT CAPB_NU_SEGURO_CMR,
                         CAPB_CACM_CD_COMPANIA
                    INTO V_CAPB_NU_SEGURO_CMR,
                         V_CAPB_CACM_CD_COMPANIA
                    FROM CART_PRODPLANES
                   WHERE CAPB_CARP_CD_RAMO     = INSC.SCCD_RAMO
                     AND CAPB_CAPU_CD_PRODUCTO = V_CACE_CAPU_CD_PRODUCTO
                     AND CAPB_CD_PLAN          = INSC.SCCD_PLAN;
                  ----------------------------------------------------------------------
                  -- SI PROPUESTA NO ESTA ANULADA SE DETERMIAN ESTADO DE LA CUOTA PAC --
                  ----------------------------------------------------------------------
                  IF (V_CACE_ST_CERTIFICADO IN (
                                                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA,
                                                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA,
                                                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA,
                                                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_EN_RENOVACION                                             
                                                )) THEN
                     ----------------------------------------------------------------------
                     IF (PKG_PROCESOS_PAC.FN_HOMOLOGA_ST_CART_RECIBOS (
                                                                       V_REG_RECIBO.CARE_ST_RECIBO,
                                                                       V_CCTE_STATUS,
                                                                       PS_MENSAJE_ERROR
                                                                       ) = FALSE) THEN
                        RAISE EXC_ERROR_OBT_CONV_ST_RECIBOS;
                     END IF;                                                    
                     ----------------------------------------------------------------------
                  ELSE
                     ----------------------------------------------------------------------
                     -- SE DEFINE EL STATUS INICIAL DEL RECIBO EN PENDIENTE --
                     V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE;
                     ----------------------------------------------------------------
                     -- SE VALIDA LA CONFIGURACION DE LA CUOTA EN LA TABLA DE PRODUCTOS --
                     IF (SUBSTR(V_SCCP_STRING_VENTA,V_CCTE_CUOTA,1) = '0') THEN
                        V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_CARGO_CAJA_CMR;
                     ELSIF (SUBSTR(V_SCCP_STRING_VENTA,V_CCTE_CUOTA,1) = '1') THEN
                        V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE;
                     ELSIF (SUBSTR(V_SCCP_STRING_VENTA,V_CCTE_CUOTA,1) = '2') THEN
                         V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_CUOTA_GRATIS;
                        IF (V_REG_RECIBO.CARE_CD_MOTIVO != 'C') THEN
                           V_ERROR_35 := TRUE;
                        END IF;
                     END IF;
  
                  END IF;
                  ----------------------------------------------------------------
                  -- SE AGREGA CALCULO DE POLIZA LIDER
                  BEGIN
                       SELECT CACW_NU_POLIZA_LIDER,
                              CACW_NU_CERTIFICADO_LIDER
                         INTO V_CACW_NU_POLIZA_LIDER,
                              V_CACW_NU_CERTIFICADO_LIDER
                         FROM CART_CERTIFICADOS_ENDOSOS
                        WHERE CACW_CASU_CD_SUCURSAL    = INSC.SCCD_SUCURSAL
                          AND CACW_CARP_CD_RAMO        = INSC.SCCD_RAMO
                          AND CACW_CAPO_NU_POLIZA      = INSC.SCCD_POLIZA
                          AND CACW_CACE_NU_CERTIFICADO = INSC.SCCD_CERTIFICADO
                          AND CACW_NU_ENDOSO           = 0;
                  EXCEPTION
                           WHEN NO_DATA_FOUND THEN
                                V_CACW_NU_POLIZA_LIDER      := NULL;
                                V_CACW_NU_CERTIFICADO_LIDER := NULL;
                  END;
                  ---------------------------------------------------------------
                  -- SE INSERTA REGISTRO EN TABLA SCO_CUENTA_CORRIENTE --
                  ---------------------------------------------------------------
                  INSERT INTO SCO_CUENTA_CORRIENTE (
                                                    CCTE_ID,
                                                    CCTE_PROPUESTA,
                                                    CCTE_MOVTO,
                                                    CCTE_CUOTA,
                                                    CCTE_CUOTA_TOTAL,
                                                    CCTE_MONEDA,
                                                    CCTE_STATUS,
                                                    CCTE_FINICIAL,
                                                    CCTE_FFINAL,
                                                    CCTE_DIA_PAC,
                                                    CCTE_MONTO_PESOS,
                                                    CCTE_MONTO_MONEDA,
                                                    CCTE_FTRANSACCION,
                                                    CCTE_RECIBO,
                                                    CCTE_CDOP_ID,
                                                    CCTE_PERIODO,
                                                    CCTE_INTENTOS,
                                                    CCTE_MEDIO_PAGO,
                                                    CCTE_DIA_PAC_PROCESADO,
                                                    CCTE_CUOTA_GRATIS,
                                                    CCTE_CAMPO1,
                                                    CCTE_CAMPO2,
                                                    CCTE_CAMPO3,
                                                    CCTE_CAMPO4,
                                                    CCTE_CAMPO5,
                                                    CCTE_CAMPO6,
                                                    CCTE_CODIGO_MOVTO,
                                                    CCTE_DESCRIPCION_MOVTO,
                                                    CCTE_ID_REFERENCIA,
                                                    CCTE_PRIMA_AFECTA,
                                                    CCTE_PRIMA_EXENTA,
                                                    CCTE_IVA,
                                                    CCTE_LIQUIDA_CIA,
                                                    CCTE_POLIZA_LIDER,
                                                    CCTE_CERTIFICADO_LIDER,
                                                    CCTE_CODIGO_COMPANIA,
                                                    CCTE_TIPO,
                                                    CCTE_USER,
                                                    CCTE_NOMBREPC,
                                                    CCTE_COD_OBSERVACION,
                                                    CCTE_OBSERVACION,
                                                    CCTE_FECHA_ENVIO_PAC,
                                                    CCTE_PLAN,
                                                    CCTE_MOVTO_CONTABLE,
                                                    CCTE_RAMO,
                                                    CCTE_CODIGO_CMR,
                                                    CCTE_SCOI_ID,
                                                    CCTE_SALDO_ABONO,
                                                    CCTE_CAMPO7,
                                                    CCTE_REZAGO,
                                                    CCTE_TIPO_CAMBIO,
                                                    CCTE_TP_MOVTO,
                                                    CCTE_TRASP_ENV_CARGOS,
                                                    CCTE_CUENTA,
                                                    CCTE_BOLETA,
                                                    CCTE_NU_ENDOSO
                                                    )
                                            VALUES (
                                                    V_CCTE_ID,               -- ID DEL MOVIMIENTO EN LA CUENTA CORRIENTE --
                                                    INSC.SCCD_PROPUESTA,     -- PROPUESTA ASOCIADA A LA CUENTA CORRIENTE --
                                                    PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE, -- TIPO DE MOVIMIENTO --
                                                    V_CCTE_CUOTA,            -- NUMERO DE LA CUOTA --
                                                    INSC.SCCD_FRAGMENT,      -- TOTAL DE CUOTAS PLAN DE PAGO --
                                                    --V_CCTE_MONEDA,           --INSC.SCCD_PRIMA_MONEDA, -- MONEDA DE COBRO --
                                                    V_REG_RECIBO.CARE_CAMO_CD_MONEDA, -- CODIGO MONEDA
                                                    V_CCTE_STATUS,           -- STATUS DEL MOVIMIENTO EN LA CUENTA CORRIENTE --
                                                    V_CCTE_FINICIAL,         -- FECHA INICIAL DE COBERTURA DE CUOTA --
                                                    V_CCTE_FFINAL,           -- FECHA FINAL DE COBERTURA DE CUOTA --
                                                    V_DIA_PAC,               --INSC.SCOI_DIA_PAC,  --EL DIA PAC SE ACTUALIZA EN LA RESPUESTA INSCRIPCION CMR --
                                                    V_CCTE_MONTO_PESOS,      --0, -- MONTO EN PESOS --
                                                    --V_CCTE_MONTO_MONEDA,     --INSC.SCCD_PRIMA_BRUTA, -- MONTO MONEDA --
                                                    V_REG_RECIBO.CARE_MT_PRIMA,  -- MONTO MONEDA
                                                    SYSDATE,                 -- FECHA DE TRANSACCION --
                                                    V_REG_RECIBO.CARE_NU_RECIBO, -- SE INICIALIZA EN NULL(RECIBO) PORQUE NO ESTA ACTUALIZADO RECTOR --
                                                    INSC.SCCD_ID,            -- ID DE LA SCO_OP_DIARIAS --
                                                    V_CCTE_PERIODO,          -- PERIODO DE COBRANZA --
                                                    0,                       -- PRIMER INTENTO DE COBRO DEBE SER CERO PARA COBRANZA REGIONAL --
                                                    INSC.SCCD_MEDIO_PAGO,    -- MEDIO DE PAGO --
                                                    NULL,                    -- DIA PAC PROCESADO --
                                                    0,                       -- CUOTA GRATIS --
                                                    NULL,                    -- CAMPO1 --
                                                    NULL,                    -- CAMPO2 --
                                                    NULL,                    -- CAMPO3 --
                                                    NULL,                    -- CAMPO4 --
                                                    NULL,                    -- CAMPO5 --
                                                    NULL,                    -- CAMPO6 --
                                                    PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_VENTA_NUEVA, -- OPERACION --
                                                    'VENTA',                 -- DESCRIPCION DE LA OPERACION --
                                                    V_CCTE_ID,               -- ID DE REFERENCIA DEL MOVIMIENTO --
                                                    V_REG_RECIBO.CARE_MT_PRIMA,           -- PRIMA AFECTA --
                                                    V_REG_RECIBO.CARE_MT_PRIMA_COASEGURO_CEDIDO,    -- PRIMA EXENTA --
                                                    (V_REG_RECIBO.CARE_MT_PRIMA-V_REG_RECIBO.CARE_MT_PRIMA_COASEGURO_CEDIDO), -- IVA --
                                                    'N',                 -- LIQUIDA LA COMPA�IA --
                                                    V_CACW_NU_POLIZA_LIDER,                -- POLIZA LIDER --
                                                    V_CACW_NU_CERTIFICADO_LIDER,                -- CERTUFICADO LIDER --
                                                    V_CAPB_CACM_CD_COMPANIA, -- COMPA�IA --
                                                    1,                   -- TIPO = 1 SEGUROS --
                                                    NULL,                -- USUARIO --
                                                    NULL,                -- PC --
                                                    NULL,                -- CODIGO DE OBSERVACION --
                                                    NULL,                -- OBSERVACION --
                                                    V_CCTE_FECHA_ENVIO_PAC, -- FECHA DE ENVIO PAC --
                                                    INSC.SCCD_PLAN,      -- PLAN --
                                                    'N',                 -- MOVIMIENTO CONTABLE --
                                                    INSC.SCCD_RAMO,      -- RAMO --
                                                    V_CAPB_NU_SEGURO_CMR,-- CODIGO CMR --
                                                    INSC.SCOI_ID,        -- ID DE LA INSCRIPCION --
                                                    0,                   -- SALDO ABONO --
                                                    0,                   -- CAMPO7 --
                                                    'N',                 -- INDICADOR DE REZAGO --
                                                    0,                   -- TIPO DE CAMBIO --
                                                    V_CCTE_TP_MOVTO,     -- TIPO DE MOVIMIENTO --
                                                    PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO, -- TRASPASADO A REPOSITORIO DE ENVIO DE CARGOS --
                                                    NULL,
                                                    NULL,
                                                    V_REG_RECIBO.CARE_NU_ENDOSO
                                                    );
                   -------------------------------------------------------------------
                     -- SE VALIDA QUE FECHA ENVIO PAC SEA IGUAL AL DEL RECIBO RECTOR --
                   IF V_CCTE_FECHA_ENVIO_PAC != V_REG_RECIBO.CARE_FE_EMISION THEN
                      -- SI HAY DIFERENCIA SE ACTUALIZA FECHA EN RECIBOS RECTOR
                      IF PKG_PROCESOS_PAC.FN_ACTUALIZA_FECHAS_RECIBO(V_REG_RECIBO.CARE_NU_RECIBO,
                                                                     V_CCTE_FECHA_ENVIO_PAC) = FALSE THEN
                         -------------------------------------------------------------------
                         -- SE MARCA VARIABLE QUE ESPECIFICA DIFERENCIA ENTRE CUENTA CORRIENTE Y RECIBOS  --
                         V_ERROR_35:= TRUE;
                      END IF;
                   END IF;
                      -------------------------------------------------------------------
              END LOOP;
              ----------------------------------------------------------------------------------
              -- SE ACTUALIZA EL ESTADO DE LA CUADRATURA DIARIA --
              IF (V_CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA) THEN
                 ----------------------------------------------------------------------------------
                   UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_CON_ANULACION_CURSADA
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                 PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
                 ----------------------------------------------------------------------------------
              ELSIF (V_CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA) THEN
                 ----------------------------------------------------------------------------------
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_VENCIDA_RECTOR
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                 PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);               
                 ----------------------------------------------------------------------------------
              ELSIF (V_CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA) THEN
                 ----------------------------------------------------------------------------------
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_NO_RENOVADA_RECTOR
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                 PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);               
                 ----------------------------------------------------------------------------------
              ELSIF (V_ERROR_35) THEN
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_CTA_CTE_RECIBO
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                  PS_CUENTAS_ERRONEAS := (PS_CUENTAS_ERRONEAS + 1);
                  ----------------------------------------------------------------------------------
              ELSIF V_CCTE_CUOTA != 0 THEN
                 ----------------------------------------------------------------------------------
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                 PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
                 ----------------------------------------------------------------------------------
              END IF;
              ----------------------------------------------------------------------
              --COMMIT;
              ----------------------------------------------------------------------
           ELSE
              ----------------------------------------------------------------------
              -- EXISTE CUENTA CORRIENTE --
              ----------------------------------------------------------------------
              V_CCTE_FECHA_ENVIO_PAC := TO_DATE(LPAD(TO_CHAR(INSC.SCOI_DIA_PAC),2,'0')||TO_CHAR(SYSDATE,'MMRRRR'),'DD/MM/RRRR');
  
              -- validar que sea mayor al sysdate y que no este bloqueada por el sem�foro
                 FOR I IN 0..12 LOOP
                   V_CCTE_FECHA_ENVIO_PAC := ADD_MONTHS(V_CCTE_FECHA_ENVIO_PAC,I);
                   IF V_CCTE_FECHA_ENVIO_PAC < TRUNC(SYSDATE) THEN
                                NULL;
                   ELSE
                      --IF PKG_PROCESOS_PAC.ESTADO_SEMAFORO(INSC.SCOI_MEDIO_PAGO, V_CCTE_FECHA_ENVIO_PAC) THEN
                      IF PKG_SEMAFORO_PAC.ESTADO_SEMAFORO(INSC.SCCD_MEDIO_PAGO, V_CCTE_FECHA_ENVIO_PAC) THEN
                                NULL;
                      ELSE
                             EXIT;
                      END IF; --SEMAFORO
                   END IF; --SYSDATE
                 END LOOP;
  
                  FOR RPCC IN RECIBOS_PENDIENTES_CTACTE (INSC.SCCD_PROPUESTA) LOOP
                     -- JORTEGA 04-02-2011 --
                     -- SE AGREGA A LA ACTUALIZACION QUE LOS REGISTROS NO SE ENCUENTREN TRASPASADOS A COBRANZA --
                     UPDATE SCO_CUENTA_CORRIENTE
                     SET       CCTE_FECHA_ENVIO_PAC  = V_CCTE_FECHA_ENVIO_PAC,
                               CCTE_DIA_PAC           = INSC.SCOI_DIA_PAC,
                               CCTE_MEDIO_PAGO         = INSC.SCCD_MEDIO_PAGO,
                               CCTE_STATUS             = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
                     WHERE  CCTE_ID                = RPCC.CCTE_ID
                       AND  CCTE_TRASP_ENV_CARGOS = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO;
                     --
                     -------------------------------------------------------------------
                     -- SE VALIDA QUE FECHA ENVIO PAC SEA IGUAL AL DEL RECIBO RECTOR --
                     IF V_CCTE_FECHA_ENVIO_PAC != V_REG_RECIBO.CARE_FE_EMISION THEN
                      -- SI HAY DIFERENCIA SE ACTUALIZA FECHA EN RECIBOS RECTOR
                        IF PKG_PROCESOS_PAC.FN_ACTUALIZA_FECHAS_RECIBO(V_REG_RECIBO.CARE_NU_RECIBO,
                                                                     V_CCTE_FECHA_ENVIO_PAC) = FALSE THEN
                         -------------------------------------------------------------------
                         -- SE MARCA VARIABLE QUE ESPECIFICA DIFERENCIA ENTRE CUENTA CORRIENTE Y RECIBOS  --
                           V_ERROR_35:= TRUE;
                        END IF;
                     END IF;  
  
                        V_CCTE_FECHA_ENVIO_PAC := ADD_MONTHS(V_CCTE_FECHA_ENVIO_PAC, 1);
              END LOOP;
              ----------------------------------------------------------------------
              -- SE ACTUALIZA EL ESTADO DE LA CUADRATURA DIARIA --
              IF (V_ERROR_35) THEN
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_CTA_CTE_RECIBO
                  WHERE SCCD_ID     = INSC.SCCD_ID;
                 ----------------------------------------------------------------------------------
                  PS_CUENTAS_ERRONEAS := (PS_CUENTAS_ERRONEAS + 1);
                  ----------------------------------------------------------------------------------
              ELSE
                 UPDATE SCO_CUADRATURA_DIARIA
                    SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE
                  WHERE SCCD_ID     = INSC.SCCD_ID;
              END IF;
              ----------------------------------------------------------------------
              --COMMIT;
              ----------------------------------------------------------------------
              PS_CUENTAS_EXISTENTES := (PS_CUENTAS_EXISTENTES + 1);
              ----------------------------------------------------------------------
           END IF;
           ----------------------------------------------------------------------
       EXCEPTION
       WHEN EXC_ERROR_CALCULO_FECHA_PAC THEN
           PS_CUENTAS_ERRONEAS := (PS_CUENTAS_ERRONEAS + 1);
       END;         
       END LOOP;
       ---------------------------------------------------------------------
       COMMIT;
       ---------------------------------------------------------------------
       PS_MENSAJE_ERROR := NULL;
       ---------------------------------------------------------------------
       RETURN TRUE;
       ---------------------------------------------------------------------
  EXCEPTION
           WHEN EXC_ERROR_NO_EXIST_PROD_PAC THEN
                --ROLLBACK;
                RETURN FALSE;
           WHEN OTHERS THEN
                --ROLLBACK;
                PS_MENSAJE_ERROR := SQLERRM;
                RETURN FALSE;
  END GENERA_CUENTAS_CORRIENTES;
  */
  FUNCTION GENERA_CUENTAS_CORRIENTES(PE_PROPUESTA            IN SCO_CUADRATURA_DIARIA.SCCD_PROPUESTA%TYPE,
                                     PS_REGISTROS_PROCESADOS IN OUT NUMBER,
                                     PS_CUENTAS_NUEVAS       IN OUT NUMBER,
                                     PS_CUENTAS_EXISTENTES   IN OUT NUMBER,
                                     PS_CUENTAS_ERRONEAS     IN OUT NUMBER,
                                     PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    ------------------------------------------------------------------------------------
    --V_MES                       NUMBER;
    --V_MES_DESFASE               NUMBER;
    V_REGISTROS_CTACTE     NUMBER;
    V_FECHA_CALCULO_PAC    DATE;
    V_CCTE_PROPUESTA       SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_CCTE_ID              SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_STATUS          SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE;
    V_CCTE_CUOTA           SCO_CUENTA_CORRIENTE.CCTE_CUOTA%TYPE;
    V_CCTE_FINICIAL        SCO_CUENTA_CORRIENTE.CCTE_FINICIAL%TYPE;
    V_CCTE_FFINAL          SCO_CUENTA_CORRIENTE.CCTE_FFINAL%TYPE;
    V_CCTE_FECHA_ENVIO_PAC SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
    V_CCTE_PERIODO         SCO_CUENTA_CORRIENTE.CCTE_PERIODO%TYPE;
    --V_CCTE_MONEDA               SCO_CUENTA_CORRIENTE.CCTE_MONEDA%TYPE;
    --V_CCTE_MONTO_MONEDA         SCO_CUENTA_CORRIENTE.CCTE_MONTO_MONEDA%TYPE;
    V_CCTE_MONTO_PESOS          SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    V_CCTE_TP_MOVTO             SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE := PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR;
    V_SCCP_STRING_VENTA         SCO_PRODUCTOS.SCCP_STRING_VENTA%TYPE;
    V_SCCP_IND_FECHA_COBRANZA   SCO_PRODUCTOS.SCCP_IND_FECHA_COBRANZA%TYPE;
    V_CAPB_NU_SEGURO_CMR        CART_PRODPLANES.CAPB_NU_SEGURO_CMR%TYPE;
    V_CAPB_CACM_CD_COMPANIA     CART_PRODPLANES.CAPB_CACM_CD_COMPANIA%TYPE;
    V_CACE_CAPU_CD_PRODUCTO     CART_CERTIFICADOS.CACE_CAPU_CD_PRODUCTO%TYPE;
    V_CACW_NU_POLIZA_LIDER      CART_CERTIFICADOS_ENDOSOS.CACW_NU_POLIZA_LIDER%TYPE;
    V_CACW_NU_CERTIFICADO_LIDER CART_CERTIFICADOS_ENDOSOS.CACW_NU_CERTIFICADO_LIDER%TYPE;
    V_CACE_NU_ENDOSO            CART_CERTIFICADOS.CACE_NU_ENDOSO%TYPE;
    V_CACE_ST_CERTIFICADO       CART_CERTIFICADOS.CACE_ST_CERTIFICADO%TYPE;
    V_REG_RECIBO                CART_RECIBOS%ROWTYPE;
    V_DIA_PAC                   NUMBER := NULL;
    V_DIA_PAGO                  NUMBER := NULL;
    --V_FEC_PAC_PRIMERA           DATE := NULL;
    V_ERROR_35 BOOLEAN := FALSE;
    --V_MODIFICO_FECHA_PAC_RECTOR BOOLEAN := FALSE;
    V_SEM_FECHA_PAC DATE := NULL; -- -CVG - 21-02-2017 
    V_DIA_NUEVO     VARCHAR2(2); --JS COP 3369
    ------------------------------------------------------------------------------------
    EXC_ERROR_NO_EXIST_PROD_PAC   EXCEPTION;
    EXC_ERROR_OBT_CONV_ST_RECIBOS EXCEPTION;
    EXC_ERROR_CALCULO_FECHA_PAC   EXCEPTION;
    EXC_ERROR_OBT_DIA_PAGO        EXCEPTION;
    ------------------------------------------------------------------------------------
    CURSOR INSC_ACEP_SIN_CTA_CTE IS
      SELECT SCCD_ID,
             SCCD_FECHA_OP,
             SCCD_PROPUESTA,
             SCCD_PERIODO,
             SCCD_OPERACION,
             SCCD_ORIGEN,
             SCCD_SUB_OPERACION,
             SCCD_ORIGEN2,
             SCCD_STATUS,
             SCCD_OBSERVACION,
             SCCD_SUCURSAL,
             SCCD_RAMO,
             SCCD_POLIZA,
             SCCD_CERTIFICADO,
             SCCD_MEDIO_PAGO,
             SCCD_PLAN,
             SCCD_PUNTO_VENTA,
             SCCD_PRODUCTOR,
             SCCD_USER,
             SCCD_CIERRE1,
             SCCD_CIERRE2,
             SCCD_CIERRE3,
             SCCD_CIERRE4,
             SCCD_CIERRE5,
             SCCD_CIERRE6,
             SCCD_CIERRE7,
             SCCD_CIERRE8,
             SCCD_ANO,
             SCCD_MES,
             SCCD_PRIMA_BRUTA,
             SCCD_PRIMA_IVA,
             SCCD_PRIMA_MONEDA,
             SCCD_COTIZACION,
             SCCD_CONSECUTIVO,
             SCCD_FECHA_DESDE,
             SCCD_FECHA_HASTA,
             SCCD_FRAGMENT,
             SCOI_ID,
             SCOI_DIA_PAC,
             0 AS SCOIH_ID
        FROM SCO_CUADRATURA_DIARIA,
             SCO_INSCRIPCION
       WHERE SCCD_OPERACION IN
             (PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_VENTA_NUEVA,
              PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_TRASP_CARTERA)
         AND SCCD_SUB_OPERACION = 0
         AND SCCD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCCD_STATUS =
             PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_SIN_CTACTE
         AND SCCD_PROPUESTA = NVL(PE_PROPUESTA,
                                  SCCD_PROPUESTA)
         AND SCOI_CONTRATO_PROPUESTA = SCCD_PROPUESTA
         AND SCOI_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP;
    ------------------------------------------------------------------------------------
    CURSOR RECIBOS_PENDIENTES_CTACTE(PE_PROPUESTA IN NUMBER) IS
      SELECT CCTE_ID
        FROM SCO_CUENTA_CORRIENTE
       WHERE CCTE_PROPUESTA = PE_PROPUESTA
         AND CCTE_TP_MOVTO =
             PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
         AND CCTE_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE
         AND CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
         AND CCTE_TRASP_ENV_CARGOS =
             PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO
       ORDER BY CCTE_PERIODO,
                CCTE_CUOTA;
    ------------------------------------------------------------------------------------
    CURSOR C_RECIBO IS
      SELECT CART_RECIBOS.*
        FROM CART_RECIBOS,
             CART_CERTIFICADOS
       WHERE CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
         AND CACE_CASU_CD_SUCURSAL = CARE_CASU_CD_SUCURSAL
         AND CACE_CARP_CD_RAMO = CARE_CARP_CD_RAMO
         AND CACE_CAPO_NU_POLIZA = CARE_CAPO_NU_POLIZA
         AND CACE_NU_CERTIFICADO = CARE_CACE_NU_CERTIFICADO
         AND ((CARE_ST_RECIBO IN
             (PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_PENDIENTE,
                PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_COBRADO) AND
             V_CACE_ST_CERTIFICADO =
             PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VIGENTE) OR
             (CARE_NU_ENDOSO = 0 AND
             V_CACE_ST_CERTIFICADO IN (PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA,
                                         PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA,
                                         PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA,
                                         PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_EN_RENOVACION,
                                         PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_MIGRACION -- <JLG> SRF 74849  03122014 
                                         )))
       ORDER BY CARE_NU_CONSECUTIVO_CUOTA;
    ------------------------------------------------------------------------------------
  BEGIN
    ------------------------------------------------------------------------------------
    -- SE LIMPIAN LAS VARIABLES DE SALIDA --
    PS_REGISTROS_PROCESADOS := 0;
    PS_CUENTAS_NUEVAS       := 0;
    PS_CUENTAS_EXISTENTES   := 0;
    PS_CUENTAS_ERRONEAS     := 0;
    ------------------------------------------------------------------------------------
    FOR INSC IN INSC_ACEP_SIN_CTA_CTE
    LOOP
      BEGIN
        ----------------------------------------------------------------------
        PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
        ----------------------------------------------------------------------
        -- SE OBTIENE LA CANTIDAD DE REGISTROS EN LA CUENTA CORRIENTE POR PROPUESTA --
        SELECT COUNT(*)
          INTO V_REGISTROS_CTACTE
          FROM SCO_CUENTA_CORRIENTE
         WHERE CCTE_PROPUESTA = INSC.SCCD_PROPUESTA
           AND CCTE_TP_MOVTO =
               PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
           AND CCTE_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE
           AND CCTE_STATUS IN
               (PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_ANULADO,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_CUOTA_GRATIS,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_NO_REALIZADO,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_CARGO_CAJA_CMR,
                PKG_PROCESOS_PAC_VISANET.CCTE_ST_SIN_MEDIO_PAGO);
        IF (V_REGISTROS_CTACTE = 0)
        THEN
          ----------------------------------------------------------------
          -- SE TRASPASA EL VALOR DE LA PROPUESTA A LA VARIABLE DE PASO --
          V_CCTE_PROPUESTA := INSC.SCCD_PROPUESTA;
          ----------------------------------------------------------------
          -- SE OBTIENE STRING DE VENTA DEL PRODUCTO Y EL INDICADOR DE FECHA DE CALCULO --
          V_SCCP_STRING_VENTA       := NULL;
          V_SCCP_IND_FECHA_COBRANZA := NULL;
          ----------------------------------------------------------------
          BEGIN
            SELECT SCCP_STRING_VENTA,
                   SCCP_IND_FECHA_COBRANZA
              INTO V_SCCP_STRING_VENTA,
                   V_SCCP_IND_FECHA_COBRANZA
              FROM SCO_PRODUCTOS
             WHERE SCCP_CAPB_CD_PLAN = INSC.SCCD_PLAN;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              -- SE MODIFICA ESTADO EN CUADRATURA PARA QUE SEA RE-CERTIFICADO --
              UPDATE SCO_CUADRATURA_DIARIA
                 SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_NO_EXIST_PRODPAC
               WHERE SCCD_PROPUESTA = INSC.SCCD_PROPUESTA;
              -------------------------------------------------------------------
              PS_MENSAJE_ERROR := 'ERROR AL PROCESAR PROPUESTA ' ||
                                  INSC.SCCD_PROPUESTA ||
                                  ', NO EXISTE PRODUCTO EN SCO_PRODUCTOS';
              RAISE EXC_ERROR_NO_EXIST_PROD_PAC;
              -------------------------------------------------------------------
          END;
          ----------------------------------------------------------------
          -- SE DEFINE PERIODO DE INICIO 1 PARA LA VENTA NUEVA --
          V_CCTE_PERIODO := 1;
          ----------------------------------------------------------------
          -- SE OBTIENE LA FECHA DE CALCULO PAC --
          V_FECHA_CALCULO_PAC := NULL;
          ----------------------------------------------------------------
          --            IF NOT PKG_PROCESOS_PAC.FN_CALCULA_DIA_PAC (V_CCTE_PROPUESTA,V_FECHA_CALCULO_PAC,PS_MENSAJE_ERROR) THEN
          --               -- ERROR..
          --               RAISE EXC_ERROR_CALCULO_FECHA_PAC;
          --            END IF; 
          --            
          --            ----------------------------------------------------------------
          --            -- FECHA PAC OK..DETERMINO DIA PAC!
          --            V_DIA_PAC := TO_NUMBER(TO_CHAR(V_FECHA_CALCULO_PAC,'DD')); 
          --            ----------------------------------------------------------------
          -----   CVG - 21-02-2017  -------------------------------------
          --V_FECHA_CALCULO_PAC := SYSDATE;          -- CVG - 21-02-2017 
          V_DIA_PAC       := TO_NUMBER(TO_CHAR(SYSDATE,
                                               'DD'));
          V_SEM_FECHA_PAC := trunc(SYSDATE); -- V_FECHA_CALCULO_PAC;  -- CVG - 21-02-2017 
          FOR R IN (SELECT NVL(MIN(SODP_DIA_PAC),
                               2) sodp_dia_pac
                      FROM sco_dia_pago
                     WHERE sodp_medio_pago =
                           PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                       AND sodp_dia_pac > V_DIA_PAC)
          LOOP
            IF r.sodp_dia_pac >= TO_NUMBER(TO_CHAR(V_SEM_FECHA_PAC,
                                                   'DD'))
            THEN
              V_SEM_FECHA_PAC := to_date(lpad(r.sodp_dia_pac,
                                              2,
                                              '0') ||
                                         TO_CHAR(V_SEM_FECHA_PAC,
                                                 'MMYYYY'),
                                         'dd/mm/yyyy');
            ELSE
              ------JS COP 3369
              BEGIN
                SELECT TO_CHAR(NVL(MIN(SODP_DIA_PAC),
                                   2))
                  INTO V_DIA_NUEVO
                  FROM sco_dia_pago
                 WHERE sodp_medio_pago =
                       PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                   AND sodp_dia_pac > V_DIA_PAC;
              END;
              --
              V_SEM_FECHA_PAC := add_months(to_date('0' || V_DIA_NUEVO ||
                                                    TO_CHAR(V_SEM_FECHA_PAC,
                                                            'MMYYYY'),
                                                    'dd/mm/yyyy'),
                                            1);
            END IF;
            -- VALIDAR EL SEMAFORO  
            IF pkg_semaforo_pac.ESTADO_SEMAFORO(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                V_SEM_FECHA_PAC) = FALSE
            THEN
              V_DIA_PAC           := TO_NUMBER(TO_CHAR(V_SEM_FECHA_PAC,
                                                       'DD'));
              V_FECHA_CALCULO_PAC := V_SEM_FECHA_PAC;
              GOTO Salir;
            ELSE
              --V_DIA_PAC := TO_NUMBER(TO_CHAR(V_SIG_FECHA_PAC, 'DD')); 
              V_FECHA_CALCULO_PAC := sysdate;
              IF NOT
                  PKG_PROCESOS_PAC.FN_CALCULA_DIA_PAC(V_CCTE_PROPUESTA,
                                                      V_FECHA_CALCULO_PAC,
                                                      PS_MENSAJE_ERROR)
              THEN
                RAISE EXC_ERROR_CALCULO_FECHA_PAC;
              END IF;
              ----------------------------------------------------------------
              -- FECHA PAC OK..DETERMINO DIA PAC!
              V_DIA_PAC := TO_NUMBER(TO_CHAR(V_FECHA_CALCULO_PAC,
                                             'DD'));
              ---------------------------------------------------------------- 
            END IF;
          END LOOP;
          -- ** --------------- ** --------------
          <<SALIR>>
        ----------------------------------------------------------------
          IF (V_DIA_PAC <> INSC.SCOI_DIA_PAC)
          THEN
            ----------------------------------------------------------------
            -- DIA PAC DISTINTO ENTRE CTA.CTE. Y RECTOR..ACTUALIZO RECTOR! 
            ----------------------------------------------------------------
            --  ACTUALIZO FECHA PAC EN RECTOR
            UPDATE CART_CERTIFICADOS
               SET CACE_FE_ULT_PAGOREAS = TO_DATE(LPAD(V_DIA_PAC,
                                                       2,
                                                       '0') || '011900',
                                                  'DDMMRRRR')
             WHERE CACE_NU_PROPUESTA = PE_PROPUESTA;
            ----------------------------------------------------------------
            --IF (PKG_PROCESOS_PAC.FN_DIA_PAGO (
            IF (FN_DIA_PAGO_VNET(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                 V_DIA_PAC,
                                 V_DIA_PAGO,
                                 PS_MENSAJE_ERROR) = FALSE)
            THEN
              RAISE EXC_ERROR_OBT_DIA_PAGO;
            END IF;
            ----------------------------------------------------------------
            --  ACTUALIZO FECHA PAC EN INSCRIPCION
            UPDATE SCO_INSCRIPCION
               SET SCOI_DIA_PAC  = V_DIA_PAC,
                   SCOI_DIA_FAC  = V_DIA_PAGO,
                   SCOI_DIA_PAGO = V_DIA_PAGO
             WHERE SCOI_CONTRATO_PROPUESTA = PE_PROPUESTA;
            ----------------------------------------------------------------
          END IF;
          ----------------------------------------------------------------
          -- INICIALIZO..ASUMO NO ERROR 35! --
          V_ERROR_35 := FALSE;
          ----------------------------------------------------------------
          V_CACE_CAPU_CD_PRODUCTO := NULL;
          SELECT CACE_CAPU_CD_PRODUCTO,
                 CACE_NU_ENDOSO,
                 CACE_ST_CERTIFICADO
            INTO V_CACE_CAPU_CD_PRODUCTO,
                 V_CACE_NU_ENDOSO,
                 V_CACE_ST_CERTIFICADO
            FROM CART_CERTIFICADOS
           WHERE CACE_NU_PROPUESTA = INSC.SCCD_PROPUESTA;
          ------------------------------------------------------------------
          -- SE MARCA CUOTA EN 0 PARA VALIDAR EXISTENCIA DE RECIBOS RECTOR -
          ------------------------------------------------------------------
          V_CCTE_CUOTA := 0;
          ------------------------------------------------------------------
          FOR V_REG_RECIBO IN C_RECIBO
          LOOP
            ----------------------------------------------------------------
            V_CCTE_CUOTA := (V_REG_RECIBO.CARE_NU_CONSECUTIVO_CUOTA - 1);
            ----------------------------------------------------------------
            V_CCTE_FECHA_ENVIO_PAC := ADD_MONTHS(V_FECHA_CALCULO_PAC,
                                                 V_CCTE_CUOTA);
            V_CCTE_FINICIAL        := ADD_MONTHS(INSC.SCCD_FECHA_DESDE,
                                                 V_CCTE_CUOTA);
            V_CCTE_FFINAL          := ADD_MONTHS(INSC.SCCD_FECHA_DESDE,
                                                 V_CCTE_CUOTA + 1);
            ----------------------------------------------------------------
            V_CCTE_CUOTA       := (V_CCTE_CUOTA + 1);
            V_CCTE_MONTO_PESOS := 0;
            ----------------------------------------------------------------
            -- NUEVO ID PARA SCO_CUENTA_CORRIENTE --
            V_CCTE_ID := NULL;
            V_CCTE_ID := PKG_PROCESOS_PAC.OBTENER_ID_REGISTRO_CTA_CTE;
            ----------------------------------------------------------------
            V_CAPB_NU_SEGURO_CMR    := NULL;
            V_CAPB_CACM_CD_COMPANIA := NULL;
            ----------------------------------------------------------------
            SELECT CAPB_NU_SEGURO_CMR,
                   CAPB_CACM_CD_COMPANIA
              INTO V_CAPB_NU_SEGURO_CMR,
                   V_CAPB_CACM_CD_COMPANIA
              FROM CART_PRODPLANES
             WHERE CAPB_CARP_CD_RAMO = INSC.SCCD_RAMO
               AND CAPB_CAPU_CD_PRODUCTO = V_CACE_CAPU_CD_PRODUCTO
               AND CAPB_CD_PLAN = INSC.SCCD_PLAN;
            ----------------------------------------------------------------------
            -- SI PROPUESTA NO ESTA ANULADA SE DETERMIAN ESTADO DE LA CUOTA PAC --
            ----------------------------------------------------------------------
            IF (V_CACE_ST_CERTIFICADO IN (PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA,
                                          PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA,
                                          PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA,
                                          PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_EN_RENOVACION,
                                          PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_MIGRACION ---<JLG> SRF 74849  03122014                                               
                                          ))
            THEN
              ----------------------------------------------------------------------
              IF (PKG_PROCESOS_PAC.FN_HOMOLOGA_ST_CART_RECIBOS(V_REG_RECIBO.CARE_ST_RECIBO,
                                                               V_CCTE_STATUS,
                                                               PS_MENSAJE_ERROR) =
                 FALSE)
              THEN
                RAISE EXC_ERROR_OBT_CONV_ST_RECIBOS;
              END IF;
              ----------------------------------------------------------------------
            ELSE
              ----------------------------------------------------------------------
              -- SE DEFINE EL STATUS INICIAL DEL RECIBO EN PENDIENTE --
              V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE;
              ----------------------------------------------------------------
              -- SE VALIDA LA CONFIGURACION DE LA CUOTA EN LA TABLA DE PRODUCTOS --
              IF (SUBSTR(V_SCCP_STRING_VENTA,
                         V_CCTE_CUOTA,
                         1) = '0')
              THEN
                V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_CARGO_CAJA_CMR;
              ELSIF (SUBSTR(V_SCCP_STRING_VENTA,
                            V_CCTE_CUOTA,
                            1) = '1')
              THEN
                V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE;
              ELSIF (SUBSTR(V_SCCP_STRING_VENTA,
                            V_CCTE_CUOTA,
                            1) = '2')
              THEN
                V_CCTE_STATUS := PKG_PROCESOS_PAC_VISANET.CCTE_ST_CUOTA_GRATIS;
                IF (V_REG_RECIBO.CARE_CD_MOTIVO != 'C')
                THEN
                  V_ERROR_35 := TRUE;
                END IF;
              END IF;
              ------------------------------------------------------------------------------
            END IF;
            ----------------------------------------------------------------
            -- SE AGREGA CALCULO DE POLIZA LIDER --
            BEGIN
              SELECT CACW_NU_POLIZA_LIDER,
                     CACW_NU_CERTIFICADO_LIDER
                INTO V_CACW_NU_POLIZA_LIDER,
                     V_CACW_NU_CERTIFICADO_LIDER
                FROM CART_CERTIFICADOS_ENDOSOS
               WHERE CACW_CASU_CD_SUCURSAL = INSC.SCCD_SUCURSAL
                 AND CACW_CARP_CD_RAMO = INSC.SCCD_RAMO
                 AND CACW_CAPO_NU_POLIZA = INSC.SCCD_POLIZA
                 AND CACW_CACE_NU_CERTIFICADO = INSC.SCCD_CERTIFICADO
                 AND CACW_NU_ENDOSO = 0;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                V_CACW_NU_POLIZA_LIDER      := NULL;
                V_CACW_NU_CERTIFICADO_LIDER := NULL;
            END;
            ---------------------------------------------------------------
            -- SE INSERTA REGISTRO EN TABLA SCO_CUENTA_CORRIENTE --
            ---------------------------------------------------------------
            INSERT INTO SCO_CUENTA_CORRIENTE
              (CCTE_ID,
               CCTE_PROPUESTA,
               CCTE_MOVTO,
               CCTE_CUOTA,
               CCTE_CUOTA_TOTAL,
               CCTE_MONEDA,
               CCTE_STATUS,
               CCTE_FINICIAL,
               CCTE_FFINAL,
               CCTE_DIA_PAC,
               CCTE_MONTO_PESOS,
               CCTE_MONTO_MONEDA,
               CCTE_FTRANSACCION,
               CCTE_RECIBO,
               CCTE_CDOP_ID,
               CCTE_PERIODO,
               CCTE_INTENTOS,
               CCTE_MEDIO_PAGO,
               CCTE_DIA_PAC_PROCESADO,
               CCTE_CUOTA_GRATIS,
               CCTE_CAMPO1,
               CCTE_CAMPO2,
               CCTE_CAMPO3,
               CCTE_CAMPO4,
               CCTE_CAMPO5,
               CCTE_CAMPO6,
               CCTE_CODIGO_MOVTO,
               CCTE_DESCRIPCION_MOVTO,
               CCTE_ID_REFERENCIA,
               CCTE_PRIMA_AFECTA,
               CCTE_PRIMA_EXENTA,
               CCTE_IVA,
               CCTE_LIQUIDA_CIA,
               CCTE_POLIZA_LIDER,
               CCTE_CERTIFICADO_LIDER,
               CCTE_CODIGO_COMPANIA,
               CCTE_TIPO,
               CCTE_USER,
               CCTE_NOMBREPC,
               CCTE_COD_OBSERVACION,
               CCTE_OBSERVACION,
               CCTE_FECHA_ENVIO_PAC,
               CCTE_PLAN,
               CCTE_MOVTO_CONTABLE,
               CCTE_RAMO,
               CCTE_CODIGO_CMR,
               CCTE_SCOI_ID,
               CCTE_SALDO_ABONO,
               CCTE_CAMPO7,
               CCTE_REZAGO,
               CCTE_TIPO_CAMBIO,
               CCTE_TP_MOVTO,
               CCTE_TRASP_ENV_CARGOS,
               CCTE_CUENTA,
               CCTE_BOLETA,
               CCTE_NU_ENDOSO)
            VALUES
              (V_CCTE_ID, -- ID DEL MOVIMIENTO EN LA CUENTA CORRIENTE --
               INSC.SCCD_PROPUESTA, -- PROPUESTA ASOCIADA A LA CUENTA CORRIENTE --
               PKG_PROCESOS_PAC_VISANET.CCTE_MOVTO_DEBE, -- TIPO DE MOVIMIENTO --
               V_CCTE_CUOTA, -- NUMERO DE LA CUOTA --
               INSC.SCCD_FRAGMENT, -- TOTAL DE CUOTAS PLAN DE PAGO --
               --V_CCTE_MONEDA,           --INSC.SCCD_PRIMA_MONEDA, -- MONEDA DE COBRO --
               V_REG_RECIBO.CARE_CAMO_CD_MONEDA, -- CODIGO MONEDA
               V_CCTE_STATUS, -- STATUS DEL MOVIMIENTO EN LA CUENTA CORRIENTE --
               V_CCTE_FINICIAL, -- FECHA INICIAL DE COBERTURA DE CUOTA --
               V_CCTE_FFINAL, -- FECHA FINAL DE COBERTURA DE CUOTA --
               V_DIA_PAC, --INSC.SCOI_DIA_PAC,  --EL DIA PAC SE ACTUALIZA EN LA RESPUESTA INSCRIPCION CMR --
               V_CCTE_MONTO_PESOS, --0, -- MONTO EN PESOS --
               --V_CCTE_MONTO_MONEDA,     --INSC.SCCD_PRIMA_BRUTA, -- MONTO MONEDA --
               V_REG_RECIBO.CARE_MT_PRIMA, -- MONTO MONEDA
               SYSDATE, -- FECHA DE TRANSACCION --
               V_REG_RECIBO.CARE_NU_RECIBO, -- SE INICIALIZA EN NULL(RECIBO) PORQUE NO ESTA ACTUALIZADO RECTOR --
               INSC.SCCD_ID, -- ID DE LA SCO_OP_DIARIAS --
               V_CCTE_PERIODO, -- PERIODO DE COBRANZA --
               0, -- PRIMER INTENTO DE COBRO DEBE SER CERO PARA COBRANZA REGIONAL --
               INSC.SCCD_MEDIO_PAGO, -- MEDIO DE PAGO --
               NULL, -- DIA PAC PROCESADO --
               0, -- CUOTA GRATIS --
               NULL, -- CAMPO1 --
               NULL, -- CAMPO2 --
               NULL, -- CAMPO3 --
               NULL, -- CAMPO4 --
               NULL, -- CAMPO5 --
               NULL, -- CAMPO6 --
               PKG_PROCESOS_PAC_VISANET.CODIGO_OPERACION_VENTA_NUEVA, -- OPERACION --
               'VENTA', -- DESCRIPCION DE LA OPERACION --
               V_CCTE_ID, -- ID DE REFERENCIA DEL MOVIMIENTO --
               V_REG_RECIBO.CARE_MT_PRIMA, -- PRIMA AFECTA --
               V_REG_RECIBO.CARE_MT_PRIMA_COASEGURO_CEDIDO, -- PRIMA EXENTA --
               (V_REG_RECIBO.CARE_MT_PRIMA -
               V_REG_RECIBO.CARE_MT_PRIMA_COASEGURO_CEDIDO), -- IVA --
               'N', -- LIQUIDA LA COMPA�IA --
               V_CACW_NU_POLIZA_LIDER, -- POLIZA LIDER --
               V_CACW_NU_CERTIFICADO_LIDER, -- CERTUFICADO LIDER --
               V_CAPB_CACM_CD_COMPANIA, -- COMPA�IA --
               1, -- TIPO = 1 SEGUROS --
               NULL, -- USUARIO --
               NULL, -- PC --
               NULL, -- CODIGO DE OBSERVACION --
               NULL, -- OBSERVACION --
               V_CCTE_FECHA_ENVIO_PAC, -- FECHA DE ENVIO PAC --
               INSC.SCCD_PLAN, -- PLAN --
               'N', -- MOVIMIENTO CONTABLE --
               INSC.SCCD_RAMO, -- RAMO --
               V_CAPB_NU_SEGURO_CMR, -- CODIGO CMR --
               INSC.SCOI_ID, -- ID DE LA INSCRIPCION --
               0, -- SALDO ABONO --
               0, -- CAMPO7 --
               'N', -- INDICADOR DE REZAGO --
               0, -- TIPO DE CAMBIO --
               V_CCTE_TP_MOVTO, -- TIPO DE MOVIMIENTO --
               PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO, -- TRASPASADO A REPOSITORIO DE ENVIO DE CARGOS --
               NULL,
               NULL, --BOLETA 
               V_REG_RECIBO.CARE_NU_ENDOSO);
            -------------------------------------------------------------------
            -- SE VALIDA QUE FECHA ENVIO PAC SEA IGUAL AL DEL RECIBO RECTOR --
            IF V_CCTE_FECHA_ENVIO_PAC != V_REG_RECIBO.CARE_FE_EMISION
            THEN
              -------------------------------------------------------------------
              -- SI HAY DIFERENCIA SE ACTUALIZA FECHA EN RECIBOS RECTOR
              IF PKG_PROCESOS_PAC.FN_ACTUALIZA_FECHAS_RECIBO(V_REG_RECIBO.CARE_NU_RECIBO,
                                                             V_CCTE_FECHA_ENVIO_PAC) =
                 FALSE
              THEN
                -------------------------------------------------------------------
                -- SE MARCA VARIABLE QUE ESPECIFICA DIFERENCIA ENTRE CUENTA CORRIENTE Y RECIBOS  --
                V_ERROR_35 := TRUE;
              END IF;
            END IF;
            -------------------------------------------------------------------
          END LOOP;
          ----------------------------------------------------------------------------------
          -- SE ACTUALIZA EL ESTADO DE LA CUADRATURA DIARIA --
          IF (V_CACE_ST_CERTIFICADO =
             PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA)
          THEN
            ----------------------------------------------------------------------------------
            UPDATE SCO_CUADRATURA_DIARIA
               SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_CON_ANULACION_CURSADA
             WHERE SCCD_ID = INSC.SCCD_ID;
            ----------------------------------------------------------------------------------
            PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
            ----------------------------------------------------------------------------------
          ELSIF (V_CACE_ST_CERTIFICADO =
                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VENCIDA)
          THEN
            ----------------------------------------------------------------------------------
            UPDATE SCO_CUADRATURA_DIARIA
               SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_VENCIDA_RECTOR
             WHERE SCCD_ID = INSC.SCCD_ID;
            ----------------------------------------------------------------------------------
            PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
            ----------------------------------------------------------------------------------
          ELSIF (V_CACE_ST_CERTIFICADO =
                PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_NO_RENOVADA)
          THEN
            ----------------------------------------------------------------------------------
            UPDATE SCO_CUADRATURA_DIARIA
               SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_NO_RENOVADA_RECTOR
             WHERE SCCD_ID = INSC.SCCD_ID;
            ----------------------------------------------------------------------------------
            PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
            ----------------------------------------------------------------------------------
          ELSIF (V_ERROR_35)
          THEN
            ----------------------------------------------------------------------------------
            -- SE ELIMINA EL DELETE A LA CUENTA CORRIENTE PARA REFLEJAR DIFERENCIAS --
            -- DELETE SCO_CUENTA_CORRIENTE WHERE CCTE_PROPUESTA = INSC.SCCD_PROPUESTA;
            ----------------------------------------------------------------------------------
            UPDATE SCO_CUADRATURA_DIARIA
               SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_ERR_GEN_CTA_CTE_RECIBO
             WHERE SCCD_ID = INSC.SCCD_ID;
            ----------------------------------------------------------------------------------
            PS_CUENTAS_ERRONEAS := (PS_CUENTAS_ERRONEAS + 1);
            ----------------------------------------------------------------------------------
            -- SCIFUENTES  15-09-2011 --
            -- SI CUOTA DISTINTO DE 0 SE AGREGARON REGISTROS A LA CUENTA CORRIENTE --
          ELSIF (V_CCTE_CUOTA != 0)
          THEN
            ----------------------------------------------------------------------------------
            UPDATE SCO_CUADRATURA_DIARIA
               SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE
             WHERE SCCD_ID = INSC.SCCD_ID;
            ----------------------------------------------------------------------------------
            PS_CUENTAS_NUEVAS := (PS_CUENTAS_NUEVAS + 1);
            ----------------------------------------------------------------------------------
          END IF;
          ---------------------------------------------------------------------------------- 
        ELSE
          ----------------------------------------------------------------------
          -- EXISTE CUENTA CORRIENTE --
          ----------------------------------------------------------------------
          V_CCTE_FECHA_ENVIO_PAC := NULL;
          IF NOT PKG_PROCESOS_PAC.FN_CALCULA_DIA_PAC(V_CCTE_PROPUESTA,
                                                     V_FECHA_CALCULO_PAC,
                                                     PS_MENSAJE_ERROR)
          THEN
            -- ERROR..
            RAISE EXC_ERROR_CALCULO_FECHA_PAC;
          END IF;
          ----------------------------------------------------------------------
          -- FECHA PAC OK..DETERMINO DIA PAC!
          V_DIA_PAC              := TO_NUMBER(TO_CHAR(V_FECHA_CALCULO_PAC,
                                                      'DD'));
          V_CCTE_FECHA_ENVIO_PAC := V_FECHA_CALCULO_PAC;
          ----------------------------------------------------------------------
          IF (V_DIA_PAC <> INSC.SCOI_DIA_PAC)
          THEN
            ----------------------------------------------------------------
            -- DIA PAC DISTINTO ENTRE CTA.CTE. Y RECTOR..ACTUALIZO RECTOR!
            ----------------------------------------------------------------
            --  ACTUALIZO FECHA PAC EN RECTOR --
            UPDATE CART_CERTIFICADOS
               SET CACE_FE_ULT_PAGOREAS = TO_DATE(LPAD(V_DIA_PAC,
                                                       2,
                                                       '0') || '011900',
                                                  'DDMMRRRR')
             WHERE CACE_NU_PROPUESTA = PE_PROPUESTA;
            ----------------------------------------------------------------
            --IF (PKG_PROCESOS_PAC.FN_DIA_PAGO (
            IF (FN_DIA_PAGO_VNET(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                 V_DIA_PAC,
                                 V_DIA_PAGO,
                                 PS_MENSAJE_ERROR) = FALSE)
            THEN
              RAISE EXC_ERROR_OBT_DIA_PAGO;
            END IF;
            ----------------------------------------------------------------               
            --  ACTUALIZO FECHA PAC EN INSCRIPCION --
            UPDATE SCO_INSCRIPCION
               SET SCOI_DIA_PAC  = V_DIA_PAC,
                   SCOI_DIA_FAC  = V_DIA_PAGO,
                   SCOI_DIA_PAGO = V_DIA_PAGO
             WHERE SCOI_CONTRATO_PROPUESTA = PE_PROPUESTA;
            ----------------------------------------------------------------
          END IF;
          ----------------------------------------------------------------------
          -- SE OBTIENEN LOS RECIBOS PENDIENTES NO ENVIADOS EN LA CUENTA CORRIENTE --
          FOR RPCC IN RECIBOS_PENDIENTES_CTACTE(INSC.SCCD_PROPUESTA)
          LOOP
            ----------------------------------------------------------------------
            UPDATE SCO_CUENTA_CORRIENTE
               SET CCTE_FECHA_ENVIO_PAC = V_CCTE_FECHA_ENVIO_PAC,
                   CCTE_DIA_PAC         = V_DIA_PAC
             WHERE CCTE_ID = RPCC.CCTE_ID;
            ----------------------------------------------------------------------  
            V_CCTE_FECHA_ENVIO_PAC := ADD_MONTHS(V_CCTE_FECHA_ENVIO_PAC,
                                                 1);
            ----------------------------------------------------------------------
          END LOOP;
          ----------------------------------------------------------------------
          -- SE ACTUALIZA EL ESTADO DE LA CUADRATURA DIARIA --
          UPDATE SCO_CUADRATURA_DIARIA
             SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE
           WHERE SCCD_ID = INSC.SCCD_ID;
          ----------------------------------------------------------------------
          PS_CUENTAS_EXISTENTES := (PS_CUENTAS_EXISTENTES + 1);
          ----------------------------------------------------------------------
        END IF;
        ----------------------------------------------------------------------
      EXCEPTION
        WHEN EXC_ERROR_CALCULO_FECHA_PAC THEN
          PS_CUENTAS_ERRONEAS := (PS_CUENTAS_ERRONEAS + 1);
      END;
    END LOOP;
    ---------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    ---------------------------------------------------------------------
    RETURN TRUE;
    ---------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERROR_OBT_DIA_PAGO THEN
      RETURN FALSE;
    WHEN EXC_ERROR_OBT_CONV_ST_RECIBOS THEN
      RETURN FALSE;
    WHEN EXC_ERROR_NO_EXIST_PROD_PAC THEN
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := SQLERRM;
      RETURN FALSE;
  END GENERA_CUENTAS_CORRIENTES;

  /*---------------------------------------------------------------------------------------------------
  NOMBRE     : GENERA_ARCH_ENV_CARGOS_PAC
  PROPOSITO  : GENERAR ARCHIVO DE ENVIO DE CARGOS Y ABONOS PAC, MEDIO DE PAGO VISANET 
  PARAMETROS : 
      PE_FECHA_PROCESO_PAC     : Fecha en la cual se hace el proceso
      PE_FECHA_ARCHIVO_PAC     :
      PE_TP_ARCHIVO            : Tipo de datos que se generaran (Extornos, etc..)
      PE_EJEC_ARCHIVO          : 
      PE_ID_USUARIO            :
      PE_ID_TERMINAL           :
      PS_REGISTROS_PROCESADOS  : (Salida) Cantidad de Registros que se procesaron
      PS_REGISTROS_ENVIADOS    : (Salida) Cantidad de Registros que se generaron en el archivo
      PS_REGISTROS_NO_ENVIADOS : (Salida) Cantidad de Registros que no se generaron
      PS_MENSAJE_ERROR         : (Salida) Mensaje de Error si lo hay.
  
  MODIFICACIONES
  FECHA      COD. JIRA  AUTOR                OBSERVACION
  ---------- ---------- -------------------- ------------------------
  13-12-2017 DPKPE-239  VLADIMIR GUTIERRREZ  .- Creaci�n de especificaci�n del c�digo
                                             .- Optimizaci�n de Codigo (sacar variables que no se utilizan, 
                                                agregar comentarios, identar)
                                             .- Se agrega funcionalidar para generar archivos de extornos 
  ---------------------------------------------------------------------------------------------------*/
  FUNCTION GENERA_ARCH_ENV_CARGOS_PAC(PE_FECHA_PROCESO_PAC     IN DATE,
                                      PE_FECHA_ARCHIVO_PAC     IN DATE,
                                      PE_TP_ARCHIVO            IN NUMBER,
                                      PE_EJEC_ARCHIVO          IN VARCHAR2,
                                      PE_ID_USUARIO            IN VARCHAR2,
                                      PE_ID_TERMINAL           IN VARCHAR2,
                                      PS_REGISTROS_PROCESADOS  IN OUT NUMBER,
                                      PS_REGISTROS_ENVIADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_ENVIADOS IN OUT NUMBER,
                                      PS_MENSAJE_ERROR         IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    V_ARCHIVO_ENVIO_CARGOS        UTL_FILE.FILE_TYPE;
    V_ARCHIVO_NO_ENVIADOS         UTL_FILE.FILE_TYPE;
    V_NOMBRE_DIRECTORIO           VARCHAR2(255);
    V_NOMBRE_ARCHIVO_ENVIO_CARGOS VARCHAR2(255);
    V_NOMBRE_ARCHIVO_NO_ENVIADOS  VARCHAR2(255);
    V_LINEA_ARCHIVO               VARCHAR2(500);
    V_MONTO_TOTAL_ENV             NUMBER := 0;
    V_CACE_ST_CERTIFICADO         NUMBER := 0;
    V_CACE_DE_ST_CERTIFICADO      VARCHAR2(50) := NULL;
    V_EXISTE_ARCH_PAC_PRELIMINAR  VARCHAR2(1) := 'N';
    V_CANTIDAD_REGISTROS          NUMBER := 0;
    V_SUMA_CARGOS                 NUMBER := 0;
    V_SUMA_ABONOS                 NUMBER := 0;
    V_FECHA_CARGOS                DATE := NULL;
    V_SODP_DIA_PAGO               SCO_DIA_PAGO.SODP_DIA_PAGO%TYPE;
    V_SOIC_CCTE_ID_NUEVO          NUMBER; -- CVG - 30/03/2017 
    V_MAX_DIA_PAC                 NUMBER; -- CVG - 04-04-2017 
    V_FECHA_ULD_PAGO              DATE; -- JS DESA-4810 
    V_CCTE_BOLETA                 SCO_CUENTA_CORRIENTE.CCTE_BOLETA%TYPE;
    V_VALOR_AGREG                 VARCHAR2(500); --DPKPE-553
    -------------------------------------------------------------------------------------------------
    V_REG_SCOH_BIT_PROC_PAC      SCOH_BITACORA_PROCESOS_PAC%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE;
    -------------------------------------------------------------------------------------------------
    EXC_ERR_ST_PROC_PAC            EXCEPTION;
    EXC_ERR_PROC_PAC_NO_EXISTE     EXCEPTION;
    EXC_ERR_ARCH_ENV_PAC_YA_EXIS   EXCEPTION;
    EXC_ERR_OBT_NOMBRE_ARCH_CARGOS EXCEPTION;
    EXC_ERR_ABRIR_ARCH_ENV_CARGOS  EXCEPTION;
    EXC_ERR_ST_PROC_PAC_ARCH       EXCEPTION;
    EXC_ERR_GRAB_BIT_PROC_PAC_ARCH EXCEPTION;
    EXC_ERR_MOD_BIT_PROC_PAC_ARCH  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_NO_ENV      EXCEPTION;
    EXC_ERR_DIA_PAGO               EXCEPTION;
    EXC_ERR_OBTENER_BOLETA         EXCEPTION;
    CURSOR ENVIO_CARGOS IS
      SELECT *
        FROM SCO_PLANO_ENVIO_CARGOS
       WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
         AND TP_ARCHIVO = PE_TP_ARCHIVO
         AND TP_MOVTO IN (1,
                          2,
                          3);
  BEGIN
    --SE OBTIENE EL PROCESO PAC EN CURSO --
    V_REG_SCOH_BIT_PROC_PAC := NULL;
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC
        FROM SCOH_BITACORA_PROCESOS_PAC
       WHERE SCHPP_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
      --SI NO ESTA INICIADO ES ERROR, SE TERMINA EL PROCESO
      IF (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        --ERROR SI NO SE ENCUETRA EL PROCESO
        RAISE EXC_ERR_PROC_PAC_NO_EXISTE;
    END;
    -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE CARGOS PARA EL PROCESO PAC --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC_ARCH
        FROM SCOH_BITACORA_PROC_PAC_ARCH
       WHERE SCHPPA_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
         AND SCHPPA_TP_ARCHIVO = PE_TP_ARCHIVO;
      IF (V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO != 'INICIADO')
         AND (V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO != 'PRELIMINAR')
      THEN
        RAISE EXC_ERR_ST_PROC_PAC_ARCH;
      END IF;
      V_EXISTE_ARCH_PAC_PRELIMINAR := 'S';
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
        V_EXISTE_ARCH_PAC_PRELIMINAR := 'N';
    END;
    --------------------------------------------------------------------------------------
    IF PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                   PE_TP_ARCHIVO,
                                                   PKG_PROCESOS_PAC_VISANET.SCDAC_ID_PROCESO_ENV,
                                                   PE_FECHA_ARCHIVO_PAC,
                                                   V_NOMBRE_ARCHIVO_ENVIO_CARGOS,
                                                   PS_MENSAJE_ERROR) =
       FALSE
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
    END IF;
    --------------------------------------------------------------------------------------
    -- SE OBTIENE EL DIRECTORIO DE DESTINO 
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    --------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_ENVIO_CARGOS := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                               V_NOMBRE_ARCHIVO_ENVIO_CARGOS,
                                               'W');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_ENV_CARGOS;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_ENV_CARGOS;
    END;
    --------------------------------------------------------------------------------------
    V_NOMBRE_ARCHIVO_NO_ENVIADOS := 'NO_ENVIADOS_VISANET' ||
                                    TO_CHAR(PE_FECHA_PROCESO_PAC,
                                            'DDMMYYYY') || PE_TP_ARCHIVO ||
                                    '.TXT';
    BEGIN
      V_ARCHIVO_NO_ENVIADOS := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                              V_NOMBRE_ARCHIVO_NO_ENVIADOS,
                                              'W');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_NO_ENV;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_NO_ENV;
    END;
    --------------------------------------------------------------------------------------
    -- SETEO DE VARIABLES DE PASO --
    V_CANTIDAD_REGISTROS := 0;
    V_SUMA_CARGOS        := 0;
    V_SUMA_ABONOS        := 0;
    --------------------------------------------------------------------------------------
    -- SE OBTIENE LA CANTIDAD DE REGISTROS ENVIADOS --
    SELECT COUNT(1)
      INTO V_CANTIDAD_REGISTROS
      FROM SCO_PLANO_ENVIO_CARGOS,
           CART_CERTIFICADOS
     WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND PROPUESTA = CACE_NU_PROPUESTA
       AND ((TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR AND
           CACE_ST_CERTIFICADO <>
           PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA)
           --CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VIGENTE) --SE COMENTA DEBIDO A QUE NO COINCIDIAN LAS CANTIDADES TOTALES
           OR (TP_MOVTO IN
           (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR,
                 PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_ABONO_DIR)));
    --------------------------------------------------------------------------------------
    -- SE OBTIENE LA SUMA DE LOS DEBITOS A ENVIAR --
    SELECT NVL(SUM(VALOR_CUOTA / 100),
               0)
      INTO V_SUMA_CARGOS
      FROM SCO_PLANO_ENVIO_CARGOS,
           CART_CERTIFICADOS
     WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND PROPUESTA = CACE_NU_PROPUESTA
       AND ((TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR AND
           CACE_ST_CERTIFICADO <>
           PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA) --SE COMENTA DEBIDO A QUE NO COINCIDIAN LAS CANTIDADES TOTALES
           --CACE_ST_CERTIFICADO = PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_VIGENTE)
           OR
           (TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR));
    --------------------------------------------------------------------------------------
    -- SE OBTIENE LA SUMA DE LOS CREDITOS ENVIADOS --
    SELECT NVL(SUM(VALOR_CUOTA / 100),
               0)
      INTO V_SUMA_ABONOS
      FROM SCO_PLANO_ENVIO_CARGOS
     WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_ABONO_DIR;
    --------------------------------------------------------------------------------------
    --IF NOT PKG_PROCESOS_PAC.FN_DIA_PAGO(PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,  
    IF PE_TP_ARCHIVO <> 1
    THEN
      IF NOT FN_DIA_PAGO_VNET(PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                              TO_CHAR(PE_FECHA_PROCESO_PAC,
                                      'DD'),
                              V_SODP_DIA_PAGO,
                              PS_MENSAJE_ERROR)
      THEN
        RAISE EXC_ERR_DIA_PAGO;
      ELSE
        V_FECHA_CARGOS := TO_DATE(TO_CHAR(V_SODP_DIA_PAGO) || '/' ||
                                  TO_CHAR(PE_FECHA_PROCESO_PAC,
                                          'MM/RRRR'),
                                  'DD/MM/RRRR');
      END IF;
    END IF;
    -- SE DEFINE LA CABECERA DEL ARCHIVO DE ENVIO DE CARGOS PAC --
    V_LINEA_ARCHIVO := NULL;
    --V_LINEA_ARCHIVO := TO_CHAR(PE_FECHA_PROCESO_PAC,'DDMMRRRR')||';'||(V_SUMA_CARGOS + V_SUMA_ABONOS)||';'||V_CANTIDAD_REGISTROS||';';
    --------------------------------------------------------------------------------------
    -- SE ESCRIBE CABECERA EN ARCHIVO DE ENVIO DE CARGOS --
    --UTL_FILE.PUT_LINE (V_ARCHIVO_ENVIO_CARGOS,V_LINEA_ARCHIVO);
    --------------------------------------------------------------------------------------
    V_MONTO_TOTAL_ENV := 0;
    --DPKPE-239-VGM-18122017-SE AGREGA CONDICION PARA QUE COLOQUE EL TITULO CORRESPONDEINETS 
    --A LAS COLUMNAS DE EXTORNOS, SI NO ES EXTORNOS COLOCA LAS NORMALES
    --------------------------------------------------------------------------------------
    IF PE_TP_ARCHIVO = TIPO_ARCHIVO_EXTORNOS
    THEN
      V_LINEA_ARCHIVO := 'ID TRANSACCION ORIGINAL, MONTO DEVOLUCION, REFERENCIA, OBSERVACIONES, COMPANIA, RAMO, SUB PRODUCTO, MOTIVO'; --DPKPE-553
    ELSE
      V_LINEA_ARCHIVO := 'NRO IDENTIFICACION,';
      V_LINEA_ARCHIVO := V_LINEA_ARCHIVO || 'CODIGO INTERNO,';
      V_LINEA_ARCHIVO := V_LINEA_ARCHIVO || 'FECHA ESTIMADA PAGO,';
      V_LINEA_ARCHIVO := V_LINEA_ARCHIVO || 'FECHA PROCESO PAGO,';
      V_LINEA_ARCHIVO := V_LINEA_ARCHIVO || 'IMPORTE,';
      V_LINEA_ARCHIVO := V_LINEA_ARCHIVO || 'NRO RECIBO';
    END IF;
    --------------------------------------------------------------------------------------
    UTL_FILE.PUT_LINE(V_ARCHIVO_ENVIO_CARGOS,
                      V_LINEA_ARCHIVO);
    --------------------------------------------------------------------------------------
    FOR E IN ENVIO_CARGOS
    LOOP
      V_CACE_ST_CERTIFICADO := NULL;
      SELECT CACE_ST_CERTIFICADO
        INTO V_CACE_ST_CERTIFICADO
        FROM CART_CERTIFICADOS
       WHERE CACE_NU_PROPUESTA = E.PROPUESTA;
      --------------------------------------------------------------------------------------
      IF (E.TP_MOVTO IN (2,
                         3))
         OR ((V_CACE_ST_CERTIFICADO <> 11) AND (E.TP_MOVTO = 1))
      THEN
        SELECT NVL(MAX(SODP_DIA_PAC),
                   2)
          INTO V_MAX_DIA_PAC
          FROM sco_dia_pago
         WHERE sodp_medio_pago =
               PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN;
        -- CVG -- 30/03/2017 --------------------------------
        --            IF TO_CHAR(PE_FECHA_PROCESO_PAC,'DD') = V_MAX_DIA_PAC THEN
        -- OBTENGO nueva SECUENCIA ccte_id archivo..---------
        SELECT SEQ_ID_CTA_CTE_TMP.NEXTVAL
          INTO V_SOIC_CCTE_ID_NUEVO
          FROM DUAL;
        BEGIN
          INSERT INTO SCO_ID_CTA_TMP
            (SOIC_CTE_ID,
             SOIC_ID_SEC)
          VALUES
            (E.NUMERO_DOCUMENTO,
             V_SOIC_CCTE_ID_NUEVO);
        EXCEPTION
          WHEN others THEN
            PS_MENSAJE_ERROR := 'ERROR Al insertar en table temporal secuencias ' ||
                                V_SOIC_CCTE_ID_NUEVO || ' para el ccte id ' ||
                                E.NUMERO_DOCUMENTO;
            UTL_FILE.PUT_LINE(V_ARCHIVO_NO_ENVIADOS,
                              PS_MENSAJE_ERROR);
            --GOTO Siguiente;
        END;
        --            END IF;
        --JS DESA-4810
        --DPKPE-239-VGM-18122017-OPTIMIZACION DE CODIGO 
        --SE HACE COMENTARIO YA QUE CON ESTA CONSULTA SE PUEDE HACER LO QUE ESTA COMENTADO, 
        --TAMBIEN POR QUE SE MAL UTILIZA LA VARIABLE DE TIPO FECHA AL RECIBIR DATA DE TIPO CHAR
        select decode(to_char(last_day(e.fecha_carga),
                              'dd'),
                      '31',
                      last_day(e.fecha_carga) - 1,
                      last_day(e.fecha_carga)) --DPKPE 407
          into V_FECHA_ULD_PAGO
          from dual;
        --V_FECHA_ULD_PAGO := to_char(LAST_DAY(e.fecha_carga), 'DD/MM/RRRR');
        --IF TO_CHAR(V_FECHA_ULD_PAGO, 'DD') = '31'  THEN
        --   V_FECHA_ULD_PAGO := TO_DATE('30' || TO_CHAR(V_FECHA_ULD_PAGO, 'MMRRRR'));
        --END IF;
        --DPKPE-239-VGM-18122017-OPTIMIZACION DE CODIGO
        --DPKPE-239-VGM-18122017-SE AGREGA CONDICION PARA QUE GENERE LA LINEA QUE CORRESPONDE PARA EXTORNOS
        IF PE_TP_ARCHIVO = TIPO_ARCHIVO_EXTORNOS
        THEN
          BEGIN
            SELECT CCTE_BOLETA
              INTO V_CCTE_BOLETA
              FROM SCO_CUENTA_CORRIENTE
             WHERE CCTE_ID = E.NUMERO_DOCUMENTO;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              -- SE GENERA UNA LINEA CON EL REGISTRO NO ENVIADO EN ARCHIVO DE ENVIO PAC --
              PS_MENSAJE_ERROR := 'NO SE ENCONTRO BOLETA DE LA PROPUESTA ' ||
                                  E.PROPUESTA;
              RAISE EXC_ERR_OBTENER_BOLETA;
            WHEN OTHERS THEN
              PS_MENSAJE_ERROR := 'ERROR AL CONSULTAR BOLETA DE LA PROPUESTA ' ||
                                  E.PROPUESTA || ' ERROR SQL ' || SQLERRM;
              RAISE EXC_ERR_OBTENER_BOLETA;
          END;
          --VALOR AGREGADO--  DPKPE-553         
          BEGIN
            SELECT ',' || b.cacm_de_compania || ',' || c.caro_de_ramo || ',' ||
                   ccte_plan || ',' || ccte_observacion
              INTO v_valor_agreg
              FROM sco_cuenta_corriente a,
                   cart_companias       b,
                   cart_ramos_oficiales c
             WHERE a.CCTE_ID = E.NUMERO_DOCUMENTO
               AND a.ccte_codigo_compania = b.cacm_cd_compania
               AND a.CCTE_RAMO = c.caro_cd_ramo;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              -- SE GENERA UNA LINEA CON EL REGISTRO NO ENVIADO EN ARCHIVO DE ENVIO PAC --
              v_valor_agreg := ',,,,';
              RAISE EXC_ERR_OBTENER_BOLETA;
            WHEN OTHERS THEN
              PS_MENSAJE_ERROR := 'ERROR AL CONSULTAR BOLETA DE LA PROPUESTA ' ||
                                  E.PROPUESTA || ' ERROR SQL ' || SQLERRM;
              RAISE EXC_ERR_OBTENER_BOLETA;
          END;
          V_LINEA_ARCHIVO := CHR(39) || SUBSTR(TO_CHAR(V_CCTE_BOLETA),
                                               1,
                                               20) || ',' || --E.NUMERO_DOCUMENTO
                             REPLACE(LTRIM(TO_CHAR(E.VALOR_CUOTA / 100,
                                                   '99999999999999990D00')),
                                     ',',
                                     '.') || ',' || 'Prop-' ||
                             SUBSTR(TO_CHAR(E.PROPUESTA),
                                    1,
                                    15) || ',' || 'ID-' ||
                             E.NUMERO_DOCUMENTO || v_valor_agreg; --DPKPE-553
        ELSE
          V_LINEA_ARCHIVO := CHR(39) || SUBSTR(TO_CHAR(E.PROPUESTA),
                                               1,
                                               24) || ',' ||
                             SUBSTR(TO_CHAR(E.CODIGO_PRODUCTO),
                                    1,
                                    15) || ',' ||
                            --TO_CHAR(V_FECHA_CARGOS,'DD/MM/RRRR') 
                            /*to_char(LAST_DAY(e.fecha_carga),                                                                                                                                                                    'DD/MM/RRRR')*/
                             V_FECHA_ULD_PAGO || ',' ||
                             TO_CHAR(E.FECHA_CARGA_ARCHIVO,
                                     'DD/MM/RRRR') || ',' || CHR(39) ||
                             REPLACE(LTRIM(TO_CHAR(E.VALOR_CUOTA / 100,
                                                   '99999999999999990D00')),
                                     ',',
                                     '.') || ',' ||
                             SUBSTR(TO_CHAR(V_SOIC_CCTE_ID_NUEVO),
                                    1,
                                    20);
        END IF;
        --------------------------------------------------------------------------------------
        -- SI EL MOVIMIENTO ESTA COMO NO ENVIADO SE MODIFICA INDICADOR A ENVIADO EN PLANO ENVIO CARGOS --
        IF (E.ENV_ARCH_PAC = 'N')
        THEN
          UPDATE SCO_CUENTA_CORRIENTE
             SET CCTE_CAMPO7 = 2
           WHERE CCTE_ID = E.NUMERO_DOCUMENTO;
          UPDATE SCO_PLANO_ENVIO_CARGOS
             SET ENV_ARCH_PAC = 'S'
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND NUMERO_DOCUMENTO = E.NUMERO_DOCUMENTO;
          COMMIT;
        END IF;
        IF (PE_EJEC_ARCHIVO = 'F')
        THEN
          UPDATE SCO_CUENTA_CORRIENTE
             SET CCTE_CAMPO7 = 2
           WHERE CCTE_ID = E.NUMERO_DOCUMENTO;
          UPDATE SCO_PLANO_ENVIO_CARGOS
             SET ENV_ARCH_PAC = 'S'
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND NUMERO_DOCUMENTO = E.NUMERO_DOCUMENTO;
          IF (MOD(PS_REGISTROS_ENVIADOS,
                  1000) = 0)
          THEN
            COMMIT;
          END IF;
        END IF;
        UTL_FILE.PUT_LINE(V_ARCHIVO_ENVIO_CARGOS,
                          V_LINEA_ARCHIVO);
        PS_REGISTROS_ENVIADOS := (PS_REGISTROS_ENVIADOS + 1);
      ELSE
        IF (PE_EJEC_ARCHIVO = 'F')
        THEN
          UPDATE SCO_CUENTA_CORRIENTE
             SET CCTE_CAMPO7 = NULL
           WHERE CCTE_ID = E.NUMERO_DOCUMENTO;
          UPDATE SCO_PLANO_ENVIO_CARGOS
             SET ENV_ARCH_PAC = 'N'
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND NUMERO_DOCUMENTO = E.NUMERO_DOCUMENTO;
          COMMIT;
        END IF;
        -- SE ACTUALIZA REGISTRO COMO NO ENVIADO EN ENVIO DE CARGOS --
        IF (E.ENV_ARCH_PAC = 'S')
        THEN
          UPDATE SCO_CUENTA_CORRIENTE
             SET CCTE_CAMPO7 = NULL
           WHERE CCTE_ID = E.NUMERO_DOCUMENTO;
          UPDATE SCO_PLANO_ENVIO_CARGOS
             SET ENV_ARCH_PAC = 'N'
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND NUMERO_DOCUMENTO = E.NUMERO_DOCUMENTO;
          COMMIT;
        END IF;
        --------------------------------------------------------------------------------------
        PS_REGISTROS_NO_ENVIADOS := (PS_REGISTROS_NO_ENVIADOS + 1);
        --------------------------------------------------------------------------------------
        -- SE OBTIENE LA DESCRIPCION DEL ESTADO DE LA PROPUESTA --
        SELECT RV_MEANING
          INTO V_CACE_DE_ST_CERTIFICADO
          FROM CG_REF_CODES
         WHERE RV_DOMAIN = 'CART_CERTIFICADOS.CACE_ST_CERTIFICADO'
           AND RV_LOW_VALUE = V_CACE_ST_CERTIFICADO;
        --------------------------------------------------------------------------------------
        -- SE GENERA UNA LINEA CON EL REGISTRO NO ENVIADO EN ARCHIVO DE ENVIO PAC --
        PS_MENSAJE_ERROR := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                            E.NUMERO_DOCUMENTO ||
                            ', EL ESTADO DE LA PROPUESTA ' || E.PROPUESTA ||
                            ' NO ES V�LIDO (' || V_CACE_DE_ST_CERTIFICADO || ')';
        UTL_FILE.PUT_LINE(V_ARCHIVO_NO_ENVIADOS,
                          PS_MENSAJE_ERROR);
        --------------------------------------------------------------------------------------
      END IF;
      PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
      -- ** -- CVG -- 03/04/2017 ** ---------
    --<< SIGUIENTE >>   
    ---------------------------------------
    END LOOP;
    -- SE CIERRAN LOS ARCHIVO DE ENVIO DE CARGOS PAC Y ARCHIVO DE RESUMEN DE MOVIMIENTOS NO ENVIADOS --
    UTL_FILE.FCLOSE(V_ARCHIVO_ENVIO_CARGOS);
    UTL_FILE.FCLOSE(V_ARCHIVO_NO_ENVIADOS);
    IF (V_EXISTE_ARCH_PAC_PRELIMINAR = 'N')
    THEN
      -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE CARGOS PAC --
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_PROCESO_PAC       := V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_TP_ARCHIVO           := PE_TP_ARCHIVO;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_ENV   := V_NOMBRE_ARCHIVO_ENVIO_CARGOS;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ARCHIVO        := PE_FECHA_ARCHIVO_PAC;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ENV        := PS_REGISTROS_ENVIADOS;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_RESP  := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP       := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS  := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS := NULL;
      -- DEPENDIENDO DE LA TIPO DE EJECUCION DE LOS ARCHIVOS SE DEFINE ESTADO DE LOS ARCHIVOS --
      IF (PE_EJEC_ARCHIVO = 'F')
      THEN
        V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO := 'PENDIENTE';
      ELSE
        V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO := 'PRELIMINAR';
      END IF;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ESTADO := SYSDATE;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_USUARIO   := PE_ID_USUARIO;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_TERMINAL  := PE_ID_TERMINAL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_MONTO_ENV    := V_MONTO_TOTAL_ENV; --YGP VALIDACIONES PAC 10-11-2008
      -- GRABA EL ARCHIVO DE CARGOS EN LA BITACORA DE PROCESOS DE PAC --
      IF (PKG_PROCESOS_PAC.GRABAR_SCOH_BIT_PROC_PAC_ARCH(V_REG_SCOH_BIT_PROC_PAC_ARCH,
                                                         PS_MENSAJE_ERROR) =
         FALSE)
      THEN
        RAISE EXC_ERR_GRAB_BIT_PROC_PAC_ARCH;
      END IF;
    ELSE
      -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE CARGOS PAC --
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ARCHIVO        := PE_FECHA_ARCHIVO_PAC;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_ENV   := V_NOMBRE_ARCHIVO_ENVIO_CARGOS;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ENV        := PS_REGISTROS_ENVIADOS;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_RESP  := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP       := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS  := NULL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS := NULL;
      -- DEPENDIENDO DE LA TIPO DE EJECUCION DE LOS ARCHIVOS SE DEFINE ESTADO DE LOS ARCHIVOS --
      IF (PE_EJEC_ARCHIVO = 'F')
      THEN
        V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO := 'PENDIENTE';
      ELSE
        V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO := 'PRELIMINAR';
      END IF;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ESTADO := SYSDATE;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_USUARIO   := PE_ID_USUARIO;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_TERMINAL  := PE_ID_TERMINAL;
      V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_MONTO_ENV    := V_MONTO_TOTAL_ENV; --YGP VALIDACIONES PAC 10-11-2008
      -- MODIFICAR EL ARCHIVO DE CARGOS EN LA BITACORA DE PROCESOS DE PAC --
      IF (PKG_PROCESOS_PAC.MODIFICAR_SCOH_BIT_PROC_PAC_AR(V_REG_SCOH_BIT_PROC_PAC_ARCH,
                                                          PS_MENSAJE_ERROR) =
         FALSE)
      THEN
        RAISE EXC_ERR_GRAB_BIT_PROC_PAC_ARCH;
      END IF;
    END IF;
    ---------------------------------------------------------------------------------------------------
    COMMIT;
    PS_MENSAJE_ERROR := NULL;
    RETURN TRUE;
  EXCEPTION
    WHEN EXC_ERR_ST_PROC_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO PAC NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_PAC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_ENV_CARGOS THEN
      PS_MENSAJE_ERROR := 'ERROR AL INTENTAR GENERAR EL ARCHIVO DE ENV�O DE CARGOS PAC';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_NO_ENV THEN
      PS_MENSAJE_ERROR := 'ERROR AL INTENTAR GENERAR EL ARCHIVO DE NO ENVIADOS';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ARCH_ENV_PAC_YA_EXIS THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE ENV�O DE CARGOS PAC YA EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_NOMBRE_ARCH_CARGOS THEN
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_DIA_PAGO THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER DIA PAGO. ' || PS_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_GRAB_BIT_PROC_PAC_ARCH THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER EL N� DE BOLETA, ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_MOD_BIT_PROC_PAC_ARCH THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR EN BITACORA DE PROCESOS PAC, ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBTENER_BOLETA THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR EN BITACORA DE PROCESOS PAC, ' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'ERROR AL GENERAR ARCHIVO DE CARGOS PAC DEL ' ||
                          PE_FECHA_PROCESO_PAC || ', ' || SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END GENERA_ARCH_ENV_CARGOS_PAC;

  ---------------------------------------------------------------------------------------------------
  -- CARGA DE ARCHIVO DE RESPUESTA DE CARGOS PAC TRANSBANK --
  /*FUNCTION CARGA_ARCH_RESP_CARGOS_PAC (
                                       PE_FECHA_PROCESO_PAC       IN     DATE,
                                       PE_FECHA_ARCHIVO_PAC       IN     DATE,
                                       PE_TP_ARCHIVO              IN     NUMBER,
                                       PE_ID_USUARIO              IN     VARCHAR2,
                                       PE_ID_TERMINAL             IN     VARCHAR2,
                                       PS_REGISTROS_LEIDOS        IN OUT NUMBER,
                                       PS_REGISTROS_PROCESADOS    IN OUT NUMBER,
                                       PS_REGISTROS_NO_PROCESADOS IN OUT NUMBER,
                                       PS_REGISTROS_ERRONEOS      IN OUT NUMBER,
                                       PS_REGISTROS_ACEPTADOS     IN OUT NUMBER,
                                       PS_REGISTROS_RECHAZADOS    IN OUT NUMBER,
                                       PS_MENSAJE_ERROR           IN OUT VARCHAR2
                                       ) RETURN BOOLEAN
  IS
    --------------------------------------------------------------------------------------
    V_ARCHIVO_RESPUESTA_CARGOS   UTL_FILE.FILE_TYPE;
    V_ARCHIVO_ERRORES            UTL_FILE.FILE_TYPE;
    V_LINEA_ARCHIVO              VARCHAR2(500);
    V_NOMBRE_DIRECTORIO          VARCHAR2(255);
    V_NOMBRE_ARCHIVO_RESP_CARGOS VARCHAR2(255);
    V_NOMBRE_ARCHIVO_ERRORES     VARCHAR2(255);
    V_CODIGO_TX                  NUMBER := 0;
    V_FECHA_PAC_SIGUIENTE        DATE;
    V_STATUS_RESPUESTA           NUMBER;
    V_REG_RESP_CARGOS            TRESP_NOMINA_CARGOS;
    V_MENSAJE_ERROR              VARCHAR2(200) := NULL;
    --------------------------------------------------------------------------------------
    V_CACA_CD_ORIGEN             CART_CARGOS.CACA_CD_ORIGEN%TYPE;
    V_DIA_PAC_SIGUIENTE          SCO_DIAPAC.SDP_DIA_SIGUIENTE%TYPE;
    V_DIA_PAC_ACTUAL             SCO_DIAPAC.SDP_DIA_PAC%TYPE;
    V_CCTE_ID                    SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_PROPUESTA             SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_CCTE_CUOTA                 SCO_CUENTA_CORRIENTE.CCTE_CUOTA%TYPE;
    V_CCTE_CUOTA_TOTAL           SCO_CUENTA_CORRIENTE.CCTE_CUOTA_TOTAL%TYPE;
    V_CCTE_STATUS                SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE;
    V_CCTE_DIA_PAC               SCO_CUENTA_CORRIENTE.CCTE_DIA_PAC%TYPE;
    V_CCTE_MONTO_PESOS           SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    V_CCTE_MONTO_MONEDA          SCO_CUENTA_CORRIENTE.CCTE_MONTO_MONEDA%TYPE;
    V_CCTE_CAMPO4                SCO_CUENTA_CORRIENTE.CCTE_CAMPO4%TYPE;
    V_CCTE_TIPO_CAMBIO           SCO_CUENTA_CORRIENTE.CCTE_TIPO_CAMBIO%TYPE;
    V_CCTE_ANOMESCONTABLE        SCO_CUENTA_CORRIENTE.CCTE_ANOMESCONTABLE%TYPE;
    V_CCTE_FTRANSACCION          SCO_CUENTA_CORRIENTE.CCTE_FTRANSACCION%TYPE;
    V_CCTE_PLAN                  SCO_CUENTA_CORRIENTE.CCTE_PLAN%TYPE;
    V_CCTE_SALDO_ABONO           SCO_CUENTA_CORRIENTE.CCTE_SALDO_ABONO%TYPE;
    V_CCTE_REZAGO                SCO_CUENTA_CORRIENTE.CCTE_REZAGO%TYPE;
    V_CCTE_TP_MOVTO              SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE;
    V_CCTE_CODIGO_CMR            SCO_CUENTA_CORRIENTE.CCTE_CODIGO_CMR%TYPE;
    V_RESP_ARCH_PAC              SCO_PLANO_ENVIO_CARGOS.RESP_ARCH_PAC%TYPE;
    V_CCTE_RESPUESTA_CMR         SCO_CUENTA_CORRIENTE.CCTE_CAMPO3%TYPE;
    V_SCOI_CUENTA_TARJETA        SCO_INSCRIPCION.SCOI_CUENTA_TARJETA%TYPE;
    V_REG_SCOH_BIT_PROC_PAC      SCOH_BITACORA_PROCESOS_PAC%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE;
    V_REG_DEF_RESP_PROC              SCO_DEF_RESPUESTAS_PROCESOS%ROWTYPE;
    --------------------------------------------------------------------------------------
    EXC_ERR_ST_PROC_PAC            EXCEPTION;
    EXC_ERR_PROC_PAC_NO_EXISTE     EXCEPTION;
    EXC_ERR_ST_ARCH_RESP_PAC       EXCEPTION;
    EXC_ERR_ARCH_RESP_PAC_NO_EXIS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_RESP_CARG   EXCEPTION;
    EXC_ERR_LEER_ARCH_RESP_CARGOS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_ERRORES     EXCEPTION;
    EXC_ERR_OBT_NOMBRE_ARCH_CARGOS EXCEPTION;
    EXC_ERR_MOD_BIT_PROC_PAC_ARCH  EXCEPTION;
    EXC_ERR_OBT_DAT_RESP_CARG_TRAN EXCEPTION;
    --
    V_ES_DIA_PAC                    NUMBER:=0;
    --------------------------------------------------------------------------------------
  BEGIN
       --------------------------------------------------------------------------------------
       PS_REGISTROS_LEIDOS        := 0;
       PS_REGISTROS_PROCESADOS    := 0;
       PS_REGISTROS_NO_PROCESADOS := 0;
       PS_REGISTROS_ERRONEOS      := 0;
       PS_REGISTROS_ACEPTADOS     := 0;
       PS_REGISTROS_RECHAZADOS    := 0;
       --------------------------------------------------------------------------------------
       -- SE OBTIENE EL PROCESO DE INSCRIPCION EN CURSO --
       V_REG_SCOH_BIT_PROC_PAC := NULL;
       BEGIN
             SELECT *
              INTO V_REG_SCOH_BIT_PROC_PAC
              FROM SCOH_BITACORA_PROCESOS_PAC
             WHERE SCHPP_CD_MEDIO_PAGO     = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
               AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
  
            IF (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ESTADO != 'INICIADO') THEN
                 RAISE EXC_ERR_ST_PROC_PAC;
            END IF;
       EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                       RAISE EXC_ERR_PROC_PAC_NO_EXISTE;
       END;
       -----------------------------------------------------------------------------------------
       -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE INSCRIPCION PARA EL PROCESO DE INSCRIPCION --
       BEGIN
             SELECT *
              INTO V_REG_SCOH_BIT_PROC_PAC_ARCH
              FROM SCOH_BITACORA_PROC_PAC_ARCH
             WHERE SCHPPA_ID_PROCESO_PAC = V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
               AND SCHPPA_TP_ARCHIVO     = PE_TP_ARCHIVO;
  
            IF (V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO != 'PENDIENTE') THEN
                 RAISE EXC_ERR_ST_ARCH_RESP_PAC;
            END IF;
       EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                          RAISE EXC_ERR_ARCH_RESP_PAC_NO_EXIS;
       END;
       --------------------------------------------------------------------------------------
       -- SE OBTIENE EL DIRECTORIO DE DESTINO --
       SELECT CACH_DIR_INTERFACE
         INTO V_NOMBRE_DIRECTORIO
         FROM CART_CONFIG_BATCH;
       --------------------------------------------------------------------------------------
       IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS (
                                                        PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                        PE_TP_ARCHIVO,
                                                        PKG_PROCESOS_PAC_VISANET.SCDAC_ID_PROCESO_RESP,
                                                        PE_FECHA_ARCHIVO_PAC,
                                                        V_NOMBRE_ARCHIVO_RESP_CARGOS,
                                                        PS_MENSAJE_ERROR
                                                        ) = FALSE) THEN
          RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
       END IF;
       --------------------------------------------------------------------------------------
       BEGIN
            V_ARCHIVO_RESPUESTA_CARGOS := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,V_NOMBRE_ARCHIVO_RESP_CARGOS,'R');
       EXCEPTION
                WHEN UTL_FILE.INVALID_OPERATION THEN
                        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
                  WHEN OTHERS THEN
                        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
       END;
       --------------------------------------------------------------------------------------
       IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS (
                                                        PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                        PE_TP_ARCHIVO,
                                                        PKG_PROCESOS_PAC_VISANET.SCDAC_ID_PROCESO_ERR,
                                                        PE_FECHA_ARCHIVO_PAC,
                                                        V_NOMBRE_ARCHIVO_ERRORES,
                                                        PS_MENSAJE_ERROR
                                                        ) = FALSE) THEN
          RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
       END IF;
       --------------------------------------------------------------------------------------
       BEGIN
            V_ARCHIVO_ERRORES := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,V_NOMBRE_ARCHIVO_ERRORES,'W');
       EXCEPTION
                WHEN UTL_FILE.INVALID_OPERATION THEN
                     RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
                WHEN OTHERS THEN
                     RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
       END;
  
       --BUSCA SI ES DIA PAC:
       --------------------------------------------------------------------------------------
       --IC-[ER-RECTOR-PROYMOROS-1]-SE BUSCA SI ES DIA PAC
        BEGIN
          SELECT 1 INTO V_ES_DIA_PAC
            FROM SCO_DIA_PAGO O
           WHERE O.SODP_MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND O.SODP_DIA_PAC    = TO_CHAR(PE_FECHA_PROCESO_PAC, 'DD');
        EXCEPTION WHEN OTHERS THEN
           V_ES_DIA_PAC:=0;
        END;
       --FC-[ER-RECTOR-PROYMOROS-1]
  
  
       --------------------------------------------------------------------------------------
       LOOP -- LOOP PRINCIPAL DEL ARCHIVO PLANO
           BEGIN -- CONTROL DE CARGA DE TODOS LOS REGISTROS DEL ARCHIVO DE CARGOS --
                --------------------------------------------------------------------------------------
                BEGIN
                       -- LEER LINEA DE REGISTRO DEL ARCHIVO --
                     UTL_FILE.GET_LINE(V_ARCHIVO_RESPUESTA_CARGOS,V_LINEA_ARCHIVO);
                EXCEPTION
                         WHEN NO_DATA_FOUND THEN
                              EXIT;
                         WHEN OTHERS THEN
                              RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
                END;
                --------------------------------------------------------------------------------------
                -- SE OBTIENEN LOS DATOS DE LA RESPUESTA DE INSCRIPCION DE TRANSBANK --
                IF (PKG_PROCESOS_PAC_VISANET.OBTENER_DATOS_RESP_CARGOS (
                                                                                  V_LINEA_ARCHIVO,
                                                                                  V_REG_RESP_CARGOS,
                                                                                  V_MENSAJE_ERROR
                                                                                  ) = FALSE) THEN
                   RAISE EXC_ERR_OBT_DAT_RESP_CARG_TRAN;
                END IF;
                -----------------------------------------------------------------------------------------
                -- SE OBTIENEN LOS REGISTROS LEIDOS --
                PS_REGISTROS_LEIDOS := (PS_REGISTROS_LEIDOS + 1);
                -----------------------------------------------------------------------------------------
                -- SE OBTIENE EL CODIGO DE RESPUESTA DEL PROCESO DE CARGOS --
                -- PARA EL MEDIO DE PAGO TRANSBANK --
                SELECT *
                  INTO V_REG_DEF_RESP_PROC
                  FROM SCO_DEF_RESPUESTAS_PROCESOS
                 WHERE SCO_DEF_RESPUESTAS_PROCESOS.SCDCP_CD_MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                   AND SCO_DEF_RESPUESTAS_PROCESOS.SCDCP_CD_RESPUESTA  = V_REG_RESP_CARGOS.CODIGO_RESPUESTA
                   AND SCO_DEF_RESPUESTAS_PROCESOS.SCDCP_ESTADO         = 'A'
                   AND SCO_DEF_RESPUESTAS_PROCESOS.SCDCP_TP_PROCESO    = 'C';
                -----------------------------------------------------------------------------------------
                -- SE OBTIENEN LOS DATOS DE LA RESPUESTA DE TRANSBANK --
                V_CCTE_ANOMESCONTABLE := TO_CHAR(PE_FECHA_PROCESO_PAC,'RRRRMM');
                V_CCTE_FTRANSACCION   := TO_DATE(SYSDATE,'DD-MM-RRRR');
                   V_CCTE_PROPUESTA      := V_REG_RESP_CARGOS.ID_SERVICIO;
                V_CCTE_ID             := V_REG_RESP_CARGOS.REF_COBRO_EFECTUADO;
                V_CCTE_RESPUESTA_CMR  := V_REG_DEF_RESP_PROC.SCDCP_CD_RESPUESTA_FPRO;
                V_CCTE_MONTO_PESOS    := V_REG_RESP_CARGOS.MONTO_TRANSACCION;
                V_CODIGO_TX            := 200;
                --------------------------------------------------------------------------------------
                IF (V_CCTE_RESPUESTA_CMR = 0) THEN
                      V_STATUS_RESPUESTA := 1; -- CARGO O ABONO ACEPTADO --
                ELSE
                   V_STATUS_RESPUESTA := 2; -- CARGO O ABONO RECHAZADO --
                END IF;
                --------------------------------------------------------------------------------------
                SELECT RESP_ARCH_PAC
                  INTO V_RESP_ARCH_PAC
                  FROM SCO_PLANO_ENVIO_CARGOS
                 WHERE MEDIO_PAGO          = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                   AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                   AND TP_ARCHIVO          = PE_TP_ARCHIVO
                   AND NUMERO_DOCUMENTO    = V_CCTE_ID;
                --------------------------------------------------------------------------------------
  
  
                --------------------------------------------------------------------------------------
  
  
  
                IF (V_RESP_ARCH_PAC = 'N') THEN
                     -- BUSCO REGISTRO EN CUENTA CORRIENTE PARA GENERA REGISTRO INVERSO SI ESTA ACEPTADO --
                   SELECT CCTE_PROPUESTA,
                           CCTE_CUOTA,
                          CCTE_CUOTA_TOTAL,
                          CCTE_STATUS,
                          CCTE_DIA_PAC,
                          CCTE_MONTO_MONEDA,
                          CCTE_CAMPO4,
                          CCTE_TIPO_CAMBIO,
                          CCTE_PLAN,
                          CCTE_CODIGO_CMR,
                          CCTE_TP_MOVTO
                     INTO V_CCTE_PROPUESTA,
                             V_CCTE_CUOTA,
                          V_CCTE_CUOTA_TOTAL,
                          V_CCTE_STATUS,
                          V_CCTE_DIA_PAC,
                          V_CCTE_MONTO_MONEDA,
                          V_CCTE_CAMPO4,
                          V_CCTE_TIPO_CAMBIO,
                          V_CCTE_PLAN,
                          V_CCTE_CODIGO_CMR,
                          V_CCTE_TP_MOVTO
                     FROM SCO_CUENTA_CORRIENTE
                    WHERE CCTE_ID = V_CCTE_ID;
  
                   --CONTROL DE RECHAZOS --
                   IF (V_STATUS_RESPUESTA = 1) THEN
  
                      SELECT SCOI_CUENTA_TARJETA
                        INTO V_SCOI_CUENTA_TARJETA
                        FROM SCO_INSCRIPCION
                       WHERE SCOI_CONTRATO_PROPUESTA = V_CCTE_PROPUESTA;
  
  
                      -- MOVIMIENTOS CURSADOS POR CMR --
                      UPDATE SCO_CUENTA_CORRIENTE
                         SET CCTE_STATUS              = PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO,
                             CCTE_CAMPO3              = V_CCTE_RESPUESTA_CMR,
                             CCTE_MONTO_PESOS         = V_CCTE_MONTO_PESOS,
                             CCTE_FTRANSACCION        = V_CCTE_FTRANSACCION,
                             CCTE_ANOMESCONTABLE      = V_CCTE_ANOMESCONTABLE,
                             CCTE_DIA_PAC_PROCESADO   = PE_FECHA_PROCESO_PAC,
                             CCTE_MOVTO_CONTABLE        = 'S',
                             CCTE_SALDO_ABONO            = V_CCTE_MONTO_MONEDA,
                             CCTE_CAMPO7              = 1,
                             CCTE_CUENTA              = V_SCOI_CUENTA_TARJETA
                       WHERE CCTE_ID                  = V_CCTE_ID;
  
                      V_CACA_CD_ORIGEN := 0;
                      IF (V_CODIGO_TX = 200) THEN
                         V_CACA_CD_ORIGEN := 0;
                      ELSE
                         V_CACA_CD_ORIGEN := 20;
                      END IF;
  
  
                      INSERT INTO CART_CARGOS (
                                               CACA_CD_ORIGEN,
                                               CACA_CACE_NU_PROPUESTA,
                                               CACA_CADM_NU_CUENTA,
                                               CACA_CAPB_NU_SEGURO_CMR,
                                               CACA_FECHA_CMR,
                                                  CACA_FECHA_RECTOR,
                                                  CACA_ESTADO_CMR,
                                               CACA_ESTADO_RECTOR,
                                                   CACA_MOTIVO,
                                                  CACA_DIAS_MORA,
                                                  CACA_NUMERO_CUOTA,
                                                  CACA_TOTAL_CUOTA,
                                                 CACA_PRIMA_PESOS,
                                                 CACA_PRIMA_SEGURO,
                                                CACA_VALOR_UF,
                                                    CACA_RECIBO
                                               )
                                       VALUES (
                                               V_CACA_CD_ORIGEN,
                                                  V_CCTE_PROPUESTA,
                                                  V_SCOI_CUENTA_TARJETA,
                                                  V_CCTE_CODIGO_CMR,
                                                  V_CCTE_FTRANSACCION,
                                               PE_FECHA_PROCESO_PAC,
                                                 'PR',
                                                     'PR',
                                               1,
                                               0,
                                                  V_CCTE_CUOTA,
                                                  V_CCTE_CUOTA_TOTAL,
                                               V_CCTE_MONTO_PESOS,
                                                  V_CCTE_MONTO_MONEDA,
                                               V_CCTE_TIPO_CAMBIO,
                                                  0 --CCTE_ID_AUX
                                               );
  
                      -- CONTADOR DE REGISTROS ACEPTADOS --
                      PS_REGISTROS_ACEPTADOS := (PS_REGISTROS_ACEPTADOS + 1);
  
                   ELSE -- MOVIMIENTOS RECHAZADOS POR CMR --
  
                      IF (V_CCTE_TP_MOVTO IN (
                                              PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR,
                                              PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_ABONO_DIR)) THEN
  
                         UPDATE SCO_CUENTA_CORRIENTE
                            SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                                CCTE_TRASP_ENV_CARGOS  = 'N',
                                CCTE_FTRANSACCION         = V_CCTE_FTRANSACCION,
                                CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                                CCTE_CAMPO3            = V_CCTE_RESPUESTA_CMR,
                                CCTE_CAMPO7            = 1,
                                CCTE_CUENTA            = V_SCOI_CUENTA_TARJETA,
                                CCTE_INTENTOS          = (NVL(CCTE_INTENTOS,0)+1),
                               --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                               CCTE_INTENTOS_ESTATUS  = DECODE(V_ES_DIA_PAC,1,(NVL(CCTE_INTENTOS_ESTATUS,0)+1) , CCTE_INTENTOS_ESTATUS )
                               --FC-[ER-RECTOR-PROYMOROS-1]
                            WHERE CCTE_ID                = V_CCTE_ID;
  
                      ELSIF (V_CCTE_TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR) THEN
  
                         SELECT SCCP_IND_REZAGO
                           INTO V_CCTE_REZAGO
                           FROM SCO_PRODUCTOS
                          WHERE SCCP_CAPB_CD_PLAN = V_CCTE_PLAN;
  
                         IF (V_CCTE_REZAGO = 'N') THEN
  
                            UPDATE SCO_CUENTA_CORRIENTE
                               SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_NO_REALIZADO,
                                    CCTE_FTRANSACCION      = V_CCTE_FTRANSACCION,
                                   CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                                   CCTE_MONTO_PESOS       = V_CCTE_MONTO_PESOS,
                                   CCTE_CAMPO3            = V_CCTE_RESPUESTA_CMR,
                                   CCTE_CUENTA            = V_SCOI_CUENTA_TARJETA,
                                   CCTE_INTENTOS          = (NVL(CCTE_INTENTOS,0)+1),
                                   --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                                   CCTE_INTENTOS_ESTATUS  = DECODE(V_ES_DIA_PAC,1,(NVL(CCTE_INTENTOS_ESTATUS,0)+1) , CCTE_INTENTOS_ESTATUS )
                                   --FC-[ER-RECTOR-PROYMOROS-1]
                               WHERE CCTE_ID                = V_CCTE_ID;
  
                         ELSIF (V_CCTE_REZAGO = 'S') THEN
  
                            -- SI ES RECHAZADO BUSCO EL PRIMER PROXIMO ENVIO A SCO_DIAPAC --
                            IF (V_CCTE_CAMPO4 IS NULL) THEN
                               SELECT SDP_DIA_SIGUIENTE,
                                       SDP_DIA_PAC
                                 INTO V_DIA_PAC_SIGUIENTE,
                                      V_DIA_PAC_ACTUAL
                                 FROM SCO_DIAPAC
                                WHERE SDP_DIA_PAC = V_CCTE_DIA_PAC;
                            ELSE -- BUSCA SIGUIENTE DIA PAC DE ACUERDO A DIA-PAC DE CAMPO4
                               SELECT SDP_DIA_SIGUIENTE,
                                      SDP_DIA_PAC
                                 INTO V_DIA_PAC_SIGUIENTE,
                                      V_DIA_PAC_ACTUAL
                                 FROM SCO_DIAPAC
                                WHERE SDP_DIA_PAC = TO_NUMBER(TO_CHAR(V_CCTE_CAMPO4,'DD'));
                            END IF;
  
                            IF (V_CCTE_CAMPO4 IS NOT NULL) THEN
                               IF (V_DIA_PAC_SIGUIENTE < V_DIA_PAC_ACTUAL) THEN
                                  V_FECHA_PAC_SIGUIENTE := ADD_MONTHS(TO_DATE(LPAD(V_DIA_PAC_SIGUIENTE,2,'0')||TO_CHAR(V_CCTE_CAMPO4,'MMYYYY'),'DDMMYYYY'),1);
                               ELSE
                                  V_FECHA_PAC_SIGUIENTE := TO_DATE(LPAD(V_DIA_PAC_SIGUIENTE,2,'0')||TO_CHAR(V_CCTE_CAMPO4,'MMYYYY'),'DDMMYYYY');
                               END IF;
                            ELSE
                               IF (V_DIA_PAC_SIGUIENTE < V_DIA_PAC_ACTUAL) THEN
                                  V_FECHA_PAC_SIGUIENTE := ADD_MONTHS(TO_DATE(LPAD(V_DIA_PAC_SIGUIENTE,2,'0')||TO_CHAR(SYSDATE,'MMYYYY'),'DDMMYYYY'),1);
                               ELSE
                                  V_FECHA_PAC_SIGUIENTE := TO_DATE(LPAD(V_DIA_PAC_SIGUIENTE,2,'0')||TO_CHAR(SYSDATE,'MMYYYY'),'DDMMYYYY');
                               END IF;
                            END IF;
  
                            UPDATE SCO_CUENTA_CORRIENTE
                               SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                                   CCTE_FTRANSACCION      = V_CCTE_FTRANSACCION,
                                   CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                                   CCTE_MONTO_PESOS       = V_CCTE_MONTO_PESOS,
                                   CCTE_CAMPO3            = V_CCTE_RESPUESTA_CMR,
                                   CCTE_CAMPO4            = V_FECHA_PAC_SIGUIENTE,
                                   CCTE_TRASP_ENV_CARGOS  = 'N',
                                   CCTE_CAMPO7            = 1,
                                   CCTE_INTENTOS          = (NVL(CCTE_INTENTOS,0)+1),
                                   --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                                   CCTE_INTENTOS_ESTATUS  = DECODE(V_ES_DIA_PAC,1,(NVL(CCTE_INTENTOS_ESTATUS,0)+1) , CCTE_INTENTOS_ESTATUS )
                                   --FC-[ER-RECTOR-PROYMOROS-1]
                             WHERE CCTE_ID                = V_CCTE_ID;
                         END IF;
                      END IF;
  
                      -- CONTADOR DE REGISTROS RECHAZADOS --
                      PS_REGISTROS_RECHAZADOS := (PS_REGISTROS_RECHAZADOS + 1);
  
                    END IF;
  
                   -- FUE DESCARGADO DEL PLANO --
                   UPDATE SCO_PLANO_ENVIO_CARGOS
                      SET COD_RESPUESTA_CMR   = V_CCTE_RESPUESTA_CMR,
                          NOMBRE_ARCHIVO      = V_NOMBRE_ARCHIVO_RESP_CARGOS,
                          RESP_ARCH_PAC       = 'S'
                      WHERE MEDIO_PAGO          = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                      AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                      AND TP_ARCHIVO          = PE_TP_ARCHIVO
                      AND NUMERO_DOCUMENTO    = V_CCTE_ID;
  
                   -- CONTADOR DE REGISTROS PROCESADOS --
                   PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
  
                ELSE
  
                   -- CONTADOR DE REGISTROS NO PROCESADOS --
                   PS_REGISTROS_NO_PROCESADOS := (PS_REGISTROS_NO_PROCESADOS + 1);
  
                END IF;
  
                IF (MOD(PS_REGISTROS_PROCESADOS,1000) = 0) THEN
                   UPDATE SCOH_BITACORA_PROC_PAC_ARCH
                      SET SCHPPA_REGISTROS_RESP       = (PS_REGISTROS_PROCESADOS + PS_REGISTROS_NO_PROCESADOS),
                          SCHPPA_REGISTROS_ACEPTADOS  = PS_REGISTROS_ACEPTADOS,
                          SCHPPA_REGISTROS_RECHAZADOS = PS_REGISTROS_RECHAZADOS
                    WHERE SCHPPA_ID_PROCESO_PAC       = V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
                      AND SCHPPA_TP_ARCHIVO            = PE_TP_ARCHIVO;
                   COMMIT;
                END IF;
  
           EXCEPTION
                    WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
                         RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
                    WHEN OTHERS THEN
                         UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,V_LINEA_ARCHIVO||'|'||SQLERRM);
                         PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
           END;
     END LOOP; -- LOOP DEL ARCHIVO PLANO --
     ---------------------------------------------------------------------------------------------------
     UTL_FILE.FCLOSE (V_ARCHIVO_RESPUESTA_CARGOS);
     UTL_FILE.FCLOSE (V_ARCHIVO_ERRORES);
     ---------------------------------------------------------------------------------------------------
     -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO               := 'FINALIZADO';
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ESTADO         := SYSDATE;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_USUARIO           := PE_ID_USUARIO;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_TERMINAL            := PE_ID_TERMINAL;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_RESP  := V_NOMBRE_ARCHIVO_RESP_CARGOS;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP        := PS_REGISTROS_PROCESADOS;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS  := PS_REGISTROS_ACEPTADOS;
     V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS := PS_REGISTROS_RECHAZADOS;
     -- MODIFICA EL ARCHIVO DE INSCRIPCION EN LA BITACORA DE PROCESOS DE INSCRIPCION --
     IF (PKG_PROCESOS_PAC.MODIFICAR_SCOH_BIT_PROC_PAC_AR (V_REG_SCOH_BIT_PROC_PAC_ARCH,PS_MENSAJE_ERROR) = FALSE) THEN
        RAISE EXC_ERR_MOD_BIT_PROC_PAC_ARCH;
     END IF;
     ---------------------------------------------------------------------------------------------------
     COMMIT;
     ---------------------------------------------------------------------------------------------------
     PS_MENSAJE_ERROR := NULL;
     ---------------------------------------------------------------------------------------------------
     RETURN TRUE;
     ---------------------------------------------------------------------------------------------------
  EXCEPTION
           WHEN EXC_ERR_ST_PROC_PAC THEN
                 PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO PAC NO ES V�LIDO';
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_PROC_PAC_NO_EXISTE THEN
                 PS_MENSAJE_ERROR := 'ERROR, EL PROCESO PAC NO EXISTE';
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_ST_ARCH_RESP_PAC THEN
                 PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL ARCHIVO DE RESPUESTA DE CARGOS PAC NO ES V�LIDO';
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_ARCH_RESP_PAC_NO_EXIS THEN
                 PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE RESPUESTA DE CARGOS PAC NO EXISTE';
                ROLLBACK;
                 RETURN FALSE;
           WHEN EXC_ERR_OBT_NOMBRE_ARCH_CARGOS THEN
                 ROLLBACK;
                 RETURN FALSE;
           WHEN EXC_ERR_MOD_BIT_PROC_PAC_ARCH THEN
                 PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR BITACORA DE PROCESOS PAC'||PS_MENSAJE_ERROR;
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_ABRIR_ARCH_RESP_CARG THEN
                PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE RESPUESTA DE CARGOS PAC '||V_NOMBRE_ARCHIVO_RESP_CARGOS;
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_ABRIR_ARCH_ERRORES THEN
                 UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
                PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE ERRORES DE CARGOS PAC';
                ROLLBACK;
                RETURN FALSE;
           WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
                PS_MENSAJE_ERROR := 'ERROR AL LEER ARCHIVO DE RESPUESTA DE CARGOS PAC';
                UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
                UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
                ROLLBACK;
                RETURN FALSE;
           WHEN OTHERS THEN
                UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
                UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
                PS_MENSAJE_ERROR := SQLERRM;
                ROLLBACK;
                RETURN FALSE;
  END CARGA_ARCH_RESP_CARGOS_PAC;
  */
  FUNCTION CARGA_ARCH_RESP_CARGOS_PAC(PE_FECHA_PROCESO_PAC       IN DATE,
                                      PE_FECHA_ARCHIVO_PAC       IN DATE,
                                      PE_TP_ARCHIVO              IN NUMBER,
                                      PE_ID_USUARIO              IN VARCHAR2,
                                      PE_ID_TERMINAL             IN VARCHAR2,
                                      PS_REGISTROS_LEIDOS        IN OUT NUMBER,
                                      PS_REGISTROS_PROCESADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_PROCESADOS IN OUT NUMBER,
                                      PS_REGISTROS_ERRONEOS      IN OUT NUMBER,
                                      PS_REGISTROS_ACEPTADOS     IN OUT NUMBER,
                                      PS_REGISTROS_RECHAZADOS    IN OUT NUMBER,
                                      PS_MENSAJE_ERROR           IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    --------------------------------------------------------------------------------------
    V_ARCHIVO_RESPUESTA_CARGOS   UTL_FILE.FILE_TYPE;
    V_ARCHIVO_ERRORES            UTL_FILE.FILE_TYPE;
    V_LINEA_ARCHIVO              VARCHAR2(500);
    V_NOMBRE_DIRECTORIO          VARCHAR2(255);
    V_NOMBRE_ARCHIVO_RESP_CARGOS VARCHAR2(255);
    V_NOMBRE_ARCHIVO_ERRORES     VARCHAR2(255);
    V_CODIGO_TX                  VARCHAR2(3) := NULL; -- SOLO PARA PERU EL CODIGO DE TRANSACCION DE CARGO NORMAL "000" --
    --------------------------------------------------------------------------------------
    V_CACA_CD_ORIGEN       CART_CARGOS.CACA_CD_ORIGEN%TYPE;
    V_SCCP_IND_REZAGO      SCO_PRODUCTOS.SCCP_IND_REZAGO%TYPE;
    V_RESP_ARCH_PAC        SCO_PLANO_ENVIO_CARGOS.RESP_ARCH_PAC%TYPE;
    V_TARJETA_CMR          SCO_PLANO_ENVIO_CARGOS.TARJETA_CMR%TYPE;
    V_CCTE_ID              SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_PROPUESTA       SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_CCTE_BOLETA          SCO_CUENTA_CORRIENTE.CCTE_BOLETA%TYPE;
    V_CCTE_BOLETA_TRANSAC  SCO_CUENTA_CORRIENTE.CCTE_BOLETA%TYPE;
    V_CCTE_NU_ENDOSO       SCO_CUENTA_CORRIENTE.CCTE_NU_ENDOSO%TYPE;
    V_CCTE_FECHA_ENVIO_PAC SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
    V_CCTE_MONTO_MONEDA    SCO_CUENTA_CORRIENTE.CCTE_MONTO_MONEDA%TYPE;
    V_CCTE_TIPO_CAMBIO     SCO_CUENTA_CORRIENTE.CCTE_TIPO_CAMBIO%TYPE;
    V_CCTE_MONTO_PESOS     SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    V_CCTE_CUOTA_TOTAL     SCO_CUENTA_CORRIENTE.CCTE_CUOTA_TOTAL%TYPE;
    V_CCTE_CUOTA           SCO_CUENTA_CORRIENTE.CCTE_CUOTA%TYPE;
    V_CCTE_CODIGO_CMR      SCO_CUENTA_CORRIENTE.CCTE_CODIGO_CMR%TYPE;
    V_CCTE_TP_MOVTO        SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE;
    V_CCTE_MONEDA          SCO_CUENTA_CORRIENTE.CCTE_MONEDA%TYPE;
    V_CCTE_PLAN            SCO_CUENTA_CORRIENTE.CCTE_PLAN%TYPE;
    V_CCTE_RECIBO          SCO_CUENTA_CORRIENTE.CCTE_RECIBO%TYPE;
    V_CCTE_STATUS          SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE;
    V_CCTE_CAMPO3          VARCHAR2(3);
    V_CACE_ST_CERTIFICADO  CART_CERTIFICADOS.CACE_ST_CERTIFICADO%TYPE;
    V_CACE_CAPB_CD_PLAN    CART_CERTIFICADOS.CACE_CAPB_CD_PLAN%TYPE;
    V_CAME_CD_MOTIVO       CART_MOTIVOS_ENDOSOS.CAME_CD_MOTIVO%TYPE;
    --V_REG_DEF_RESP_PROC            SCO_DEF_RESPUESTAS_PROCESOS%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC      SCOH_BITACORA_PROCESOS_PAC%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE;
    V_ES_DIA_PAC                 NUMBER := 0;
    V_SITUACION                  NUMBER(2) := 0;
    V_DIAS_MORA                  NUMBER(4) := 0;
    V_EXISTE_EN_REZAGADAS        NUMBER := 0;
    V_ANULAR_PROPUESTA           NUMBER := 0;
    --------------------------------------------------------------------------------------
    --V_INICIO      NUMBER := 0;
    --V_FIN         NUMBER := 0;
    V_TEXTO       VARCHAR2(100) := NULL;
    V_CCTE_ID_sec SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE; -- CVG - 03/04/2017 
    --------------------------------------------------------------------------------------
    V_SCDR_SCDRG_ID_GRUPO         SCO_DEF_RESPUESTA.SCDR_SCDRG_ID_GRUPO%TYPE;
    V_SCDRD_COD_RESPUESTA_INTERNO SCO_DEF_RESPUESTA_DETALLE.SCDRD_COD_RESPUESTA_INTERNO%TYPE;
    V_SCDRD_TIPO_RESPUESTA        SCO_DEF_RESPUESTA_DETALLE.SCDRD_TIPO_RESPUESTA%TYPE;
    --------------------------------------------------------------------------------------
    EXC_ERR_ST_PROC_PAC            EXCEPTION;
    EXC_ERR_PROC_PAC_NO_EXISTE     EXCEPTION;
    EXC_ERR_ST_ARCH_RESP_PAC       EXCEPTION;
    EXC_ERR_ARCH_RESP_PAC_NO_EXIS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_RESP_CARG   EXCEPTION;
    EXC_ERR_LEER_ARCH_RESP_CARGOS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_ERRORES     EXCEPTION;
    EXC_ERR_OBT_NOMBRE_ARCH_CARGOS EXCEPTION;
    EXC_ERR_MOD_BIT_PROC_PAC_ARCH  EXCEPTION;
    EXC_ERR_INS_REZAGO             EXCEPTION;
    EXC_ERR_UPD_REZAGO             EXCEPTION;
    EXC_ERR_INS_SOL_ANULACION      EXCEPTION;
    EXC_ERR_COD_RESP_NO_EXISTE     EXCEPTION;
    EXC_ERR_UPD_ST_RECIBO_RECTOR   EXCEPTION;
    EXC_ERR_REZ_CUOTA_RECTOR       EXCEPTION;
    EXC_ERR_CCTE_ST_INV            EXCEPTION;
    -- MMA 24-02-2012. --
    EXC_ERR_GRUPO_NO_EXISTE EXCEPTION;
    EXC_ERR_COD_RESP_INV    EXCEPTION;
    EXC_ERR_PROPUESTA       EXCEPTION;
    EXC_ERR_ID              EXCEPTION;
    --DPKPE-73
    v_sw_boleta number;
    --------------------------------------------------------------------------------------
  BEGIN
    --------------------------------------------------------------------------------------
    PS_REGISTROS_LEIDOS        := 0;
    PS_REGISTROS_PROCESADOS    := 0;
    PS_REGISTROS_NO_PROCESADOS := 0;
    PS_REGISTROS_ERRONEOS      := 0;
    PS_REGISTROS_ACEPTADOS     := 0;
    PS_REGISTROS_RECHAZADOS    := 0;
    -- inicio dpkepe-73
    v_sw_boleta := 0;
    -- fin dpkepe-73
    --------------------------------------------------------------------------------------
    -- SE OBTIENE EL PROCESO DE CARGOS EN CURSO --
    V_REG_SCOH_BIT_PROC_PAC := NULL;
    -- VALIDACIONES ANTES DEL PROCESO --
    -- SE VALIDA PROCESO PAC --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC
        FROM SCOH_BITACORA_PROCESOS_PAC
       WHERE SCHPP_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
      IF (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_PROC_PAC_NO_EXISTE;
    END;
    -----------------------------------------------------------------------------------------
    -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE CARGOS PARA EL PROCESO DE CARGOS --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC_ARCH
        FROM SCOH_BITACORA_PROC_PAC_ARCH
       WHERE SCHPPA_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
         AND SCHPPA_TP_ARCHIVO = PE_TP_ARCHIVO;
      IF (V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO != 'PENDIENTE')
      THEN
        RAISE EXC_ERR_ST_ARCH_RESP_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_ARCH_RESP_PAC_NO_EXIS;
    END;
    --------------------------------------------------------------------------------------
    -- SE OBTIENE EL DIRECTORIO DE DESTINO --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    --------------------------------------------------------------------------------------
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                    PE_TP_ARCHIVO,
                                                    PKG_PROCESOS_PAC_VISANET.SCDAC_ID_PROCESO_RESP,
                                                    PE_FECHA_ARCHIVO_PAC,
                                                    V_NOMBRE_ARCHIVO_RESP_CARGOS,
                                                    PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
    END IF;
    --------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_RESPUESTA_CARGOS := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                                   V_NOMBRE_ARCHIVO_RESP_CARGOS,
                                                   'R');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
    END;
    --------------------------------------------------------------------------------------
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                    PE_TP_ARCHIVO,
                                                    PKG_PROCESOS_PAC_VISANET.SCDAC_ID_PROCESO_ERR,
                                                    PE_FECHA_ARCHIVO_PAC,
                                                    V_NOMBRE_ARCHIVO_ERRORES,
                                                    PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
    END IF;
    --------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_ERRORES := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                          V_NOMBRE_ARCHIVO_ERRORES,
                                          'W');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
    END;
    --------------------------------------------------------------------------------------
    --IC-[ER-RECTOR-PROYMOROS-1]-SE BUSCA SI ES DIA PAC --
    BEGIN
      SELECT 1
        INTO V_ES_DIA_PAC
        FROM SCO_DIA_PAGO O
       WHERE O.SODP_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN --8
         AND O.SODP_DIA_PAC = TO_CHAR(PE_FECHA_PROCESO_PAC,
                                      'DD');
    EXCEPTION
      WHEN OTHERS THEN
        V_ES_DIA_PAC := 0;
    END;
    -- FC-[ER-RECTOR-PROYMOROS-1] --
    --------------------------------------------------------------------------------------
    -- MMA, 24-02-2012 --
    -- BUSCA SET DE RESPUESTAS POSIBLES PARA CARGOS --
    BEGIN
      SELECT SCDR_SCDRG_ID_GRUPO
        INTO V_SCDR_SCDRG_ID_GRUPO
        FROM SCO_DEF_RESPUESTA
       WHERE SCDR_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN --8
         AND SCDR_TIPO_ARCHIVO = PE_TP_ARCHIVO
         AND SCDR_TIPO_PROCESO =
             PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_CARGO; --C
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_GRUPO_NO_EXISTE;
    END;
    --------------------------------------------------------------------------------------
    -- SE LEE Y PROCESO ARCHIVO DE RESPUESTA DE CARGOS --
    LOOP
      -- LOOP PRINCIPAL DEL ARCHIVO PLANO
      BEGIN
        -- CONTROL DE CARGA DE TODOS LOS REGISTROS DEL ARCHIVO DE CARGOS --
        --------------------------------------------------------------------------------------
        BEGIN
          -- LEER LINEA DE REGISTRO DEL ARCHIVO --
          UTL_FILE.GET_LINE(V_ARCHIVO_RESPUESTA_CARGOS,
                            V_LINEA_ARCHIVO);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            EXIT;
          WHEN OTHERS THEN
            RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
        END;
        --------------------------------------------------------------------------------------
        IF SUBSTR(V_LINEA_ARCHIVO,
                  1,
                  1) = 'D'
        THEN
          -- DETALLE --
          PS_REGISTROS_LEIDOS := (PS_REGISTROS_LEIDOS + 1);
          -------------------------------------------------------------------------------------
          -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE RESPUESTA --
          --V_CODIGO_TX   := SUBSTR(V_LINEA_ARCHIVO,179,3);                           -- ORIGEN 000 (CARGO NORMAL) --
          --V_CCTE_BOLETA := TO_NUMBER(NVL(LTRIM(SUBSTR(V_LINEA_ARCHIVO,117,10)),0)); -- BOLETA CMR, DEJAR EN CCTE_BOLETA --
          -------------------------------------------------------------------------------------
          -- IDENTIFICAR DE RECIBO --
          BEGIN
            V_TEXTO := SUBSTR(V_LINEA_ARCHIVO,
                              127,
                              20);
            --- CVG - 03-04-2017 ------------
            --V_CCTE_ID     := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO, 127, 20)));       --TO_NUMBER(NVL(V_TEXTO,'0'));
            V_CCTE_ID_sec := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                   127,
                                                   20))); --TO_NUMBER(NVL(V_TEXTO,'0'));  
            ---------------------------------
            V_CCTE_CAMPO3 := SUBSTR(V_LINEA_ARCHIVO,
                                    118,
                                    3);
            --INICIO DPKPE-73 
            -- Se pregunta si esta activo el SW para sacar la boleta o el cod transaccion                              
            BEGIN
              select SFO_LOW_VALUE
                into v_sw_boleta
                from SFO_CONFIG_DATOS
               where SFO_DOMAIN = 'PKG_PROCESOS_PAC_VISANET.BOLETA';
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                v_sw_boleta := 0;
              WHEN OTHERS THEN
                v_sw_boleta := 0;
            END;
            if v_sw_boleta = 1
            then
              V_CCTE_BOLETA_TRANSAC := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                             166,
                                                             35)));
            else
              V_CCTE_BOLETA_TRANSAC := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                             147,
                                                             10)));
            end if;
            --FIN DPKPE73
            V_CCTE_BOLETA := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                   147,
                                                   10)));
            --V.1.1.1, SE ASIGNA VALOR 000 POR DEFECTO
            V_CODIGO_TX := '000';
          EXCEPTION
            WHEN OTHERS THEN
              RAISE EXC_ERR_PROPUESTA;
          END;
          -------------------------------------------------------------------------------------
          -- CODIGO DE RESPUESTA --
          /*BEGIN
             V_TEXTO := SUBSTR(V_LINEA_ARCHIVO, 118, 9);
             V_CCTE_CAMPO3 := SUBSTR(V_TEXTO,1,3);      -- CODIGO RESPUESTA --
          EXCEPTION
          WHEN OTHERS THEN
             RAISE EXC_ERR_ID;
          END;*/
          --- CVG - 03-04-2017 ---------------
          BEGIN
            SELECT SOIC_CTE_ID
              INTO V_CCTE_ID
              FROM SCO_ID_CTA_TMP
             WHERE SOIC_ID_SEC = V_CCTE_ID_sec;
          EXCEPTION
            WHEN no_data_found then
              V_CCTE_ID := TO_NUMBER(TRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                 127,
                                                 20)));
              --PS_MENSAJE_ERROR := 'ERROR, no pudo rescatar ccte_id de tabla temporal con sec : '||V_CCTE_ID_sec;
            --R-AISE EXC_ERR_ABRIR_ARCH_ERRORES;
          END;
          ------------------------------------ 
          -- OBTIENE CUOTA DESDE SCO_PLANO_ENVIO_CARGOS --
          SELECT RESP_ARCH_PAC,
                 TARJETA_CMR
            INTO V_RESP_ARCH_PAC,
                 V_TARJETA_CMR
            FROM SCO_PLANO_ENVIO_CARGOS
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND ENV_ARCH_PAC = 'S'
             AND NUMERO_DOCUMENTO = V_CCTE_ID;
          --------------------------------------------------------------------------------------
          IF (V_RESP_ARCH_PAC = 'N')
          THEN
            --------------------------------------------------------------------------------------
            -- BUSCO REGISTRO EN CUENTA CORRIENTE PARA GENERA REGISTRO INVERSO SI ESTA ACEPTADO --
            SELECT CCTE_PROPUESTA,
                   CCTE_CUOTA,
                   CCTE_CUOTA_TOTAL,
                   CCTE_MONEDA,
                   CCTE_MONTO_MONEDA,
                   CCTE_MONTO_PESOS,
                   CCTE_TIPO_CAMBIO,
                   CCTE_PLAN,
                   CCTE_CODIGO_CMR,
                   CCTE_TP_MOVTO,
                   CCTE_FECHA_ENVIO_PAC,
                   CCTE_RECIBO,
                   CCTE_NU_ENDOSO,
                   CCTE_STATUS
              INTO V_CCTE_PROPUESTA,
                   V_CCTE_CUOTA,
                   V_CCTE_CUOTA_TOTAL,
                   V_CCTE_MONEDA,
                   V_CCTE_MONTO_MONEDA,
                   V_CCTE_MONTO_PESOS,
                   V_CCTE_TIPO_CAMBIO,
                   V_CCTE_PLAN,
                   V_CCTE_CODIGO_CMR,
                   V_CCTE_TP_MOVTO,
                   V_CCTE_FECHA_ENVIO_PAC,
                   V_CCTE_RECIBO,
                   V_CCTE_NU_ENDOSO,
                   V_CCTE_STATUS
              FROM SCO_CUENTA_CORRIENTE
             WHERE CCTE_ID = V_CCTE_ID;
            --------------------------------------------------------------------------------------
            BEGIN
              SELECT SCDRD_COD_RESPUESTA_INTERNO,
                     SCDRD_TIPO_RESPUESTA
                INTO V_SCDRD_COD_RESPUESTA_INTERNO,
                     V_SCDRD_TIPO_RESPUESTA
                FROM SCO_DEF_RESPUESTA_DETALLE
               WHERE SCDRD_SCDRG_ID_GRUPO = V_SCDR_SCDRG_ID_GRUPO
                 AND SCDRD_COD_RESPUESTA = V_CCTE_CAMPO3 --V_MENSAJE_RESPUESTA_JOURNAL
                 AND SCDRD_ESTADO = 'A';
              --
              V_CCTE_CAMPO3 := V_SCDRD_COD_RESPUESTA_INTERNO;
              --  
            EXCEPTION
              WHEN OTHERS THEN
                PS_MENSAJE_ERROR := 'ERROR, C�DIGO DE RESPUESTA ' ||
                                    V_CCTE_CAMPO3 ||
                                    ' ENVIADO NO VALIDO PARA CARGA PAC.';
                RAISE EXC_ERR_COD_RESP_INV;
            END;
            --------------------------------------------------------------------------------------
            -- SE VALIDA SI EL MOVIMIENTO FUE ACEPTADO O RECHAZADO ("A" O "R") --
            -- MMA, 24-02-2012 --
            IF V_SCDRD_TIPO_RESPUESTA = 'A'
            THEN
              -- ACEPTADA
              --------------------------------------------------------------------------------------
              -- SE ACTUALIZA CUENTA CORRIENTE --
              -- MOVIMIENTOS CURSADOS POR CMR --
              UPDATE SCO_CUENTA_CORRIENTE
                 SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO,
                     CCTE_CAMPO3            = V_CCTE_CAMPO3,
                     CCTE_FTRANSACCION      = SYSDATE, --V_CCTE_FTRANSACCION,
                     CCTE_ANOMESCONTABLE    = TO_CHAR(PE_FECHA_PROCESO_PAC,
                                                      'RRRRMM'),
                     CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                     CCTE_MOVTO_CONTABLE    = 'S',
                     CCTE_SALDO_ABONO       = V_CCTE_MONTO_MONEDA,
                     CCTE_CAMPO7            = PKG_PROCESOS_PAC_VISANET.CCTE_CAMPO7_RESP,
                     CCTE_CUENTA            = V_TARJETA_CMR, -- VIENE DE SCO_PLANO_ENVIO_CARGOS --
                     -- INICIO DPKPE-73 
                     CCTE_BOLETA = V_CCTE_BOLETA_TRANSAC,
                     --CCTE_BOLETA            = V_CCTE_BOLETA,
                     --FIN DPKPE-73 
                     CCTE_INTENTOS         = NVL(CCTE_INTENTOS,
                                                 0) + 1,
                     CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                    1,
                                                    (NVL(CCTE_INTENTOS_ESTATUS,
                                                         0) + 1),
                                                    CCTE_INTENTOS_ESTATUS)
               WHERE CCTE_ID = V_CCTE_ID;
              --------------------------------------------------------------------------------------
              -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
              UPDATE CART_RECIBOS
                 SET CARE_ST_RECIBO           = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_COBRADO, --PAGADO RECTOR --
                     CARE_NU_AVISO            = V_CCTE_BOLETA, -- NO SE PUEDE CAMBIAR CAMPO ES DE 10 DIGITOS DPKPE-73
                     CARE_NU_CESION_COASEGURO = V_SITUACION,
                     CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                     CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                     CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
               WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                 AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
              --------------------------------------------------------------------------------------
              --  LUIS MORA DIAZ --
              -- 17/05/2011 --
              --  SE OBTIENE EL CODIGO DE ORIGEN PARA CART_CARGOS --
              -- EN PERU EL CODIGO DE TRANSACCION CARGOS ES "000" NO 200 COMO EN TODOS LOS PAISES --
              -- ORIGEN "0" ES CARGO, DISTINTO DE "0" ES ABONO O DEVOLUCION O EXTORNO --
              V_CACA_CD_ORIGEN := 0;
              --
              IF (V_CODIGO_TX = '000')
              THEN
                V_CACA_CD_ORIGEN := 0;
              ELSE
                V_CACA_CD_ORIGEN := 20;
              END IF;
              --------------------------------------------------------------------------------------
              -- LUIS MORA DIAZ --
              -- 24-05-2010 --
              -- SE INSERTA REGISTRO DE CARGO EN TABLA CART_CARGOS --
              INSERT INTO CART_CARGOS
                (CACA_CD_ORIGEN,
                 CACA_CACE_NU_PROPUESTA,
                 CACA_CADM_NU_CUENTA,
                 CACA_CAPB_NU_SEGURO_CMR,
                 CACA_FECHA_CMR, -- CCTE_DIA_PAC_PROCESADO --
                 CACA_FECHA_RECTOR, -- CCTE_FTRANSACCION --
                 CACA_FE_PAC, -- CCTE-FECHA_ENVIO_PAC --
                 CACA_FE_PAGO_CMR, -- CCTE_DIA_PAC_PROCESADO --
                 CACA_ESTADO_CMR,
                 CACA_ESTADO_RECTOR,
                 CACA_MOTIVO,
                 CACA_DIAS_MORA,
                 CACA_NUMERO_CUOTA,
                 CACA_TOTAL_CUOTA,
                 CACA_PRIMA_PESOS,
                 CACA_PRIMA_SEGURO,
                 CACA_VALOR_UF,
                 CACA_RECIBO)
              VALUES
                (V_CACA_CD_ORIGEN,
                 V_CCTE_PROPUESTA,
                 V_TARJETA_CMR,
                 0,
                 PE_FECHA_PROCESO_PAC, -- FECHA DEL PROCESO PAC --
                 TO_DATE(SYSDATE,
                         'DD-MM-RRRR'), -- FECHA DE CARGA DEL PROCESO DE RESPUESTA --
                 V_CCTE_FECHA_ENVIO_PAC, -- FECHA DE LA CUOTA --
                 PE_FECHA_PROCESO_PAC, -- FECHA DEL PROCESO -- 
                 'PR',
                 'PE',
                 1,
                 0,
                 V_CCTE_CUOTA,
                 V_CCTE_CUOTA_TOTAL,
                 V_CCTE_MONTO_PESOS,
                 V_CCTE_MONTO_MONEDA,
                 V_CCTE_TIPO_CAMBIO,
                 V_CCTE_RECIBO);
              --------------------------------------------------------------------------------------
              -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_REZAGADAS --
              UPDATE BATB_CUOTAS_REZAGADAS
                 SET BATB_ST_PAC          = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_CARGADA,
                     BATB_FE_CARGO_CMR    = PE_FECHA_PROCESO_PAC,
                     BATB_FE_ULTIMO_ENVIO = PE_FECHA_PROCESO_PAC,
                     BATB_NU_ENVIOS      =
                     (NVL(BATB_NU_ENVIOS,
                          0) + 1)
               WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                 AND BATB_TP_REGISTRO = 'C'
                 AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                 AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                 AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                 AND BATB_CARE_SUB_CUOTA = 0;
              --------------------------------------------------------------------------------------
              -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_FACTURADAS --
              UPDATE BATB_CUOTAS_FACTURADAS
                 SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_CARGADA,
                     BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
               WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                 AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                 AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                 AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                 AND BATB_CARE_SUB_CUOTA = 0;
              --------------------------------------------------------------------------------------
              -- CONTADOR DE REGISTROS ACEPTADOS --
              PS_REGISTROS_ACEPTADOS := (PS_REGISTROS_ACEPTADOS + 1);
              --------------------------------------------------------------------------------------
            ELSIF (V_SCDRD_TIPO_RESPUESTA = 'R')
            THEN
              -- MOVIMIENTOS RECHAZADOS POR CMR --
              --------------------------------------------------------------------------------------
              -- SE MARCA COMO CLIENTE NUEVO CUANDO EXISTE RECHAZO DE INSCRIPCION
              IF V_CCTE_CAMPO3 = 8
              THEN
                UPDATE SCO_INSCRIPCION
                   SET SCOI_CLIENTE_NUEVO = NULL
                 WHERE SCOI_CONTRATO_PROPUESTA = V_CCTE_PROPUESTA;
              END IF;
              --------------------------------------------------------------------------------------
              -- SE OBTIENE EL ESTADO Y PRODUCTO DE CART_CERTIFICADOS --
              SELECT CACE_ST_CERTIFICADO,
                     CACE_CAPB_CD_PLAN
                INTO V_CACE_ST_CERTIFICADO,
                     V_CACE_CAPB_CD_PLAN
                FROM CART_CERTIFICADOS
               WHERE CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
              --------------------------------------------------------------------------------------
              -- SE PROCESA RECHAZO DEPENDIENDO EL TIPO DE MOVIMIENTO
              IF (V_CCTE_TP_MOVTO IN
                 (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR))
              THEN
                --------------------------------------------------------------------------------------
                -- SE REZAGA LA CUOTA RECHAZADA --
                SELECT COUNT(1)
                  INTO V_EXISTE_EN_REZAGADAS
                  FROM BATB_CUOTAS_REZAGADAS
                 WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                   AND BATB_TP_REGISTRO = 'C'
                   AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                   AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                   AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                   AND BATB_CARE_SUB_CUOTA = 0
                   AND BATB_ST_PAC IN (1,
                                       3,
                                       6);
                --------------------------------------------------------------------------------------
                IF (V_EXISTE_EN_REZAGADAS = 0)
                THEN
                  --------------------------------------------------------------------------------------
                  IF (PKG_PROCESOS_PAC_VISANET.FN_REZAGAR_CUOTA_RECTOR(V_CCTE_PROPUESTA,
                                                                       V_CCTE_ID,
                                                                       V_CCTE_RECIBO,
                                                                       V_TARJETA_CMR,
                                                                       PE_FECHA_PROCESO_PAC,
                                                                       V_CCTE_MONEDA,
                                                                       V_CCTE_MONTO_MONEDA,
                                                                       V_CCTE_NU_ENDOSO,
                                                                       V_CCTE_CUOTA,
                                                                       V_CCTE_FECHA_ENVIO_PAC,
                                                                       PS_MENSAJE_ERROR) =
                     FALSE)
                  THEN
                    RAISE EXC_ERR_REZ_CUOTA_RECTOR;
                  END IF;
                  --------------------------------------------------------------------------------------
                ELSE
                  --------------------------------------------------------------------------------------
                  UPDATE BATB_CUOTAS_REZAGADAS
                     SET BATB_ST_PAC          = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_EN_PROCESO,
                         BATB_CD_ERROR        = TO_NUMBER(V_CCTE_CAMPO3),
                         BATB_FE_ULTIMO_ENVIO = PE_FECHA_PROCESO_PAC,
                         BATB_NU_ENVIOS      =
                         (NVL(BATB_NU_ENVIOS,
                              0) + 1)
                   WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                     AND BATB_TP_REGISTRO = 'C'
                     AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                     AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                     AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                     AND BATB_CARE_SUB_CUOTA = 0;
                  --------------------------------------------------------------------------------------
                  UPDATE BATB_CUOTAS_FACTURADAS
                     SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_EN_PROCESO,
                         BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
                   WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                     AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                     AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                     AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                     AND BATB_CARE_SUB_CUOTA = 0;
                  --------------------------------------------------------------------------------------
                END IF;
                --------------------------------------------------------------------------------------
                -- ACTUALIZACION DE TABLA SCO_CUENTA_CORRIENTE --
                UPDATE SCO_CUENTA_CORRIENTE
                   SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                       CCTE_FTRANSACCION      = SYSDATE,
                       CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                       CCTE_CAMPO3            = V_CCTE_CAMPO3,
                       CCTE_TRASP_ENV_CARGOS  = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO,
                       CCTE_CAMPO7            = PKG_PROCESOS_PAC_VISANET.CCTE_CAMPO7_RESP,
                       CCTE_INTENTOS         =
                       (NVL(CCTE_INTENTOS,
                            0) + 1),
                       --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                       CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                      1,
                                                      (NVL(CCTE_INTENTOS_ESTATUS,
                                                           0) + 1),
                                                      CCTE_INTENTOS_ESTATUS)
                --FC-[ER-RECTOR-PROYMOROS-1]
                 WHERE CCTE_ID = V_CCTE_ID;
                --------------------------------------------------------------------------------------
                -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
                UPDATE CART_RECIBOS
                   SET CARE_ST_RECIBO           = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_REZAGADO, -- REZAGADO RECTOR --
                       CARE_NU_CESION_COASEGURO = V_SITUACION,
                       CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                       CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                       CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
                 WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                   AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
                --------------------------------------------------------------------------------------
              ELSIF (V_CCTE_TP_MOVTO =
                    PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR)
              THEN
                --------------------------------------------------------------------------------------
                IF (V_CACE_ST_CERTIFICADO !=
                   PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA)
                THEN
                  --------------------------------------------------------------------------------------
                  -- SE VALIDA QUE EL PRODUCTO MANEJA REZAGOS DE COBRANZA --
                  SELECT SCCP_IND_REZAGO
                    INTO V_SCCP_IND_REZAGO
                    FROM SCO_PRODUCTOS
                   WHERE SCCP_CAPB_CD_PLAN = V_CCTE_PLAN;
                  --------------------------------------------------------------------------------------
                  IF (V_SCCP_IND_REZAGO = 'N')
                  THEN
                    --------------------------------------------------------------------------------------
                    -- PRODUCTO NO TIENE REZAGO --
                    -- SE ACTUALIZA ESTADO COMO NO REALIZADO EN CUENTA CORRIENTE --
                    UPDATE SCO_CUENTA_CORRIENTE
                       SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_NO_REALIZADO,
                           CCTE_FTRANSACCION      = SYSDATE,
                           CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                           CCTE_CAMPO3            = V_CCTE_CAMPO3,
                           CCTE_INTENTOS         =
                           (NVL(CCTE_INTENTOS,
                                0) + 1),
                           --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                           CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                          1,
                                                          (NVL(CCTE_INTENTOS_ESTATUS,
                                                               0) + 1),
                                                          CCTE_INTENTOS_ESTATUS)
                    --FC-[ER-RECTOR-PROYMOROS-1]
                     WHERE CCTE_ID = V_CCTE_ID;
                    --------------------------------------------------------------------------------------
                    -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
                    UPDATE CART_RECIBOS
                       SET CARE_ST_RECIBO           = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_NO_REALIZADO, -- NO REALIZADO RECTOR --
                           CARE_NU_CESION_COASEGURO = V_SITUACION,
                           CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                           CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                           CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
                     WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                       AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
                    --------------------------------------------------------------------------------------
                    -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_REZAGADAS --
                    UPDATE BATB_CUOTAS_REZAGADAS
                       SET BATB_ST_PAC          = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_RECHAZADA,
                           BATB_FE_CARGO_CMR    = PE_FECHA_PROCESO_PAC,
                           BATB_FE_ULTIMO_ENVIO = PE_FECHA_PROCESO_PAC,
                           BATB_NU_ENVIOS      =
                           (NVL(BATB_NU_ENVIOS,
                                0) + 1)
                     WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                       AND BATB_TP_REGISTRO = 'C'
                       AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                       AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                       AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                       AND BATB_CARE_SUB_CUOTA = 0;
                    --------------------------------------------------------------------------------------
                    -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_FACTURADAS --
                    UPDATE BATB_CUOTAS_FACTURADAS
                       SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_RECHAZADA,
                           BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
                     WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                       AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                       AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                       AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                       AND BATB_CARE_SUB_CUOTA = 0;
                    --------------------------------------------------------------------------------------
                  ELSIF (V_SCCP_IND_REZAGO = 'S')
                  THEN
                    --------------------------------------------------------------------------------------
                    -- SOLO PARA PAC DE PER� MIENTRAS SE HABILITA MOROSIDAD REGIONAL --
                    -- SE INSERTA MOROSIDAD EN RECTOR --
                    IF (V_CCTE_STATUS IN
                       (PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE,
                         PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO))
                    THEN
                      --------------------------------------------------------------------------------------
                      SELECT COUNT(1)
                        INTO V_EXISTE_EN_REZAGADAS
                        FROM BATB_CUOTAS_REZAGADAS
                       WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                         AND BATB_TP_REGISTRO = 'C'
                         AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                         AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                         AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                         AND BATB_CARE_SUB_CUOTA = 0
                         AND BATB_ST_PAC IN (1,
                                             3,
                                             6);
                      --------------------------------------------------------------------------------------
                      IF (V_EXISTE_EN_REZAGADAS = 0)
                      THEN
                        --------------------------------------------------------------------------------------
                        IF (PKG_PROCESOS_PAC_VISANET.FN_REZAGAR_CUOTA_RECTOR(V_CCTE_PROPUESTA,
                                                                             V_CCTE_ID,
                                                                             V_CCTE_RECIBO,
                                                                             V_TARJETA_CMR,
                                                                             PE_FECHA_PROCESO_PAC,
                                                                             V_CCTE_MONEDA,
                                                                             V_CCTE_MONTO_MONEDA,
                                                                             V_CCTE_NU_ENDOSO,
                                                                             V_CCTE_CUOTA,
                                                                             V_CCTE_FECHA_ENVIO_PAC,
                                                                             PS_MENSAJE_ERROR) =
                           FALSE)
                        THEN
                          RAISE EXC_ERR_REZ_CUOTA_RECTOR;
                        END IF;
                        --------------------------------------------------------------------------------------
                      ELSE
                        --------------------------------------------------------------------------------------
                        UPDATE BATB_CUOTAS_REZAGADAS
                           SET BATB_ST_PAC          = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_EN_PROCESO,
                               BATB_CD_ERROR        = TO_NUMBER(V_CCTE_CAMPO3),
                               BATB_FE_ULTIMO_ENVIO = PE_FECHA_PROCESO_PAC,
                               BATB_NU_ENVIOS      =
                               (NVL(BATB_NU_ENVIOS,
                                    0) + 1)
                         WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                           AND BATB_TP_REGISTRO = 'C'
                           AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                           AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                           AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                           AND BATB_CARE_SUB_CUOTA = 0;
                        --------------------------------------------------------------------------------------
                        UPDATE BATB_CUOTAS_FACTURADAS
                           SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_EN_PROCESO,
                               BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
                         WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                           AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                           AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                           AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                           AND BATB_CARE_SUB_CUOTA = 0;
                        --------------------------------------------------------------------------------------
                      END IF;
                      --------------------------------------------------------------------------------------
                      -- ACTUALIZACION DE TABLA SCO_CUENTA_CORRIENTE --
                      UPDATE SCO_CUENTA_CORRIENTE
                         SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                             CCTE_FTRANSACCION      = SYSDATE,
                             CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                             CCTE_CAMPO3            = V_CCTE_CAMPO3,
                             CCTE_TRASP_ENV_CARGOS  = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO,
                             CCTE_CAMPO7            = PKG_PROCESOS_PAC_VISANET.CCTE_CAMPO7_RESP,
                             CCTE_INTENTOS         =
                             (NVL(CCTE_INTENTOS,
                                  0) + 1),
                             --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                             CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                            1,
                                                            (NVL(CCTE_INTENTOS_ESTATUS,
                                                                 0) + 1),
                                                            CCTE_INTENTOS_ESTATUS)
                      --FC-[ER-RECTOR-PROYMOROS-1]
                       WHERE CCTE_ID = V_CCTE_ID;
                      --------------------------------------------------------------------------------------
                      -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
                      UPDATE CART_RECIBOS
                         SET CARE_ST_RECIBO           = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_REZAGADO, -- REZAGADO RECTOR --
                             CARE_NU_CESION_COASEGURO = V_SITUACION,
                             CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                             CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                             CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
                       WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                         AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
                      --------------------------------------------------------------------------------------
                    ELSE
                      --------------------------------------------------------------------------------------
                      PS_MENSAJE_ERROR := 'ERROR, ESTADO DE CUOTA ID ' ||
                                          V_CCTE_ID ||
                                          ' NO VALIDO PARA CARGA PAC.';
                      RAISE EXC_ERR_CCTE_ST_INV;
                      --------------------------------------------------------------------------------------
                    END IF;
                  END IF;
                ELSIF (V_CACE_ST_CERTIFICADO =
                      PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA)
                THEN
                  --------------------------------------------------------------------------------------
                  -- PRODUCTO NO TIENE REZAGO --
                  -- SE ACTUALIZA ESTADO COMO NO REALIZADO EN CUENTA CORRIENTE --
                  UPDATE SCO_CUENTA_CORRIENTE
                     SET CCTE_STATUS            = PKG_PROCESOS_PAC_VISANET.CCTE_ST_ANULADO,
                         CCTE_FTRANSACCION      = SYSDATE,
                         CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                         CCTE_CAMPO3            = V_CCTE_CAMPO3,
                         CCTE_INTENTOS         =
                         (NVL(CCTE_INTENTOS,
                              0) + 1),
                         --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                         CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                        1,
                                                        (NVL(CCTE_INTENTOS_ESTATUS,
                                                             0) + 1),
                                                        CCTE_INTENTOS_ESTATUS)
                  --FC-[ER-RECTOR-PROYMOROS-1]
                   WHERE CCTE_ID = V_CCTE_ID;
                  --------------------------------------------------------------------------------------
                  -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
                  UPDATE CART_RECIBOS
                     SET CARE_ST_RECIBO           = PKG_PROCESOS_PAC_VISANET.CARE_ST_RECIBO_ANULADO, -- NO REALIZADO RECTOR --
                         CARE_NU_CESION_COASEGURO = V_SITUACION,
                         CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                         CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                         CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
                   WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                     AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
                  --------------------------------------------------------------------------------------
                  -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_REZAGADAS --
                  UPDATE BATB_CUOTAS_REZAGADAS
                     SET BATB_ST_PAC          = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_RECHAZADA,
                         BATB_FE_CARGO_CMR    = PE_FECHA_PROCESO_PAC,
                         BATB_FE_ULTIMO_ENVIO = PE_FECHA_PROCESO_PAC,
                         BATB_NU_ENVIOS      =
                         (NVL(BATB_NU_ENVIOS,
                              0) + 1)
                   WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                     AND BATB_TP_REGISTRO = 'C'
                     AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                     AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                     AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                     AND BATB_CARE_SUB_CUOTA = 0;
                  --------------------------------------------------------------------------------------
                  -- SE ACTUALIZA POR MOROSIDAD EN RECTOR BATB_CUOTAS_FACTURADAS --
                  UPDATE BATB_CUOTAS_FACTURADAS
                     SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_RECHAZADA,
                         BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
                   WHERE BATB_FE_PAC >= V_CCTE_FECHA_ENVIO_PAC
                     AND BATB_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA
                     AND BATB_NU_ENDOSO = V_CCTE_NU_ENDOSO
                     AND BATB_CARE_NU_CUOTA = V_CCTE_CUOTA
                     AND BATB_CARE_SUB_CUOTA = 0;
                  --------------------------------------------------------------------------------------
                END IF;
                --------------------------------------------------------------------------------------
              END IF;
              --------------------------------------------------------------------------------------
              -- SOLO PARA COBRANZA PAC REGIONAL PERU --
              -- SE GENERA SOLICITUD DE ANULACION PARA RECHAZOS CODIGO 001 Y 011, ADEM�S DE LA SITUACI�N 3 Y 7 --
              -- SE VALIDA EXITENCIA DE RESPUESTA O SITUACI�N PARA GENERAR SOLICITUD DE ANULACI�N --
              SELECT COUNT(1)
                INTO V_ANULAR_PROPUESTA
                FROM (SELECT *
                        FROM CG_REF_CODES
                       WHERE RV_DOMAIN = 'ESTADOS_RETORNO_PAC'
                         AND RV_LOW_VALUE = V_CCTE_CAMPO3
                         AND RV_TYPE = 'S'
                         AND V_CACE_CAPB_CD_PLAN NOT IN
                             (SELECT RV_LOW_VALUE
                                FROM CG_REF_CODES
                               WHERE RV_DOMAIN = 'PAC.CUOTA_TERMINO_CORTO'
                                 AND RV_MEANING = 'S'
                                 AND RV_LOW_VALUE = V_CACE_CAPB_CD_PLAN)
                      UNION
                      SELECT *
                        FROM CG_REF_CODES
                       WHERE RV_DOMAIN = 'SITUACION_CUENTA_PAC'
                         AND RV_LOW_VALUE = V_SITUACION
                         AND RV_TYPE = 'S');
              --------------------------------------------------------------------------------------
              IF ((V_ANULAR_PROPUESTA > 0) AND
                 V_CACE_ST_CERTIFICADO !=
                 PKG_PROCESOS_PAC_VISANET.CACE_ST_CERT_ANULADA)
              THEN
                --------------------------------------------------------------------------------------
                -- SE DETERMINA EL MOTIVO DE ANULACION SEGUN CODIGO DE RESPUESTA INFORMADO EN ARCHIVO..
                BEGIN
                  SELECT RV_MEANING
                    INTO V_CAME_CD_MOTIVO
                    FROM CG_REF_CODES
                   WHERE RV_DOMAIN = 'DESAFILIACION.RETORNO_PAC'
                     AND RV_LOW_VALUE = 'MOTIVO' || V_CCTE_CAMPO3;
                EXCEPTION
                  WHEN OTHERS THEN
                    V_CAME_CD_MOTIVO := 0;
                END;
                --------------------------------------------------------------------------------------
                -- SOLO PARA PER�  --
                -- GENERAR SOLICITUD DE ANULACION --
                -- SE GENERA SOLICITUD DE ANULACION --
                IF (PKG_PROCESOS_PAC_VISANET.GENERA_SOLICITUD_ANULACION(V_CCTE_PROPUESTA,
                                                                        V_CAME_CD_MOTIVO,
                                                                        PS_MENSAJE_ERROR) =
                   FALSE)
                THEN
                  RAISE EXC_ERR_INS_SOL_ANULACION;
                END IF;
                --------------------------------------------------------------------------------------
              END IF;
              --------------------------------------------------------------------------------------
              -- CONTADOR DE REGISTROS RECHAZADOS --
              PS_REGISTROS_RECHAZADOS := (PS_REGISTROS_RECHAZADOS + 1);
              --------------------------------------------------------------------------------------
            END IF;
            --------------------------------------------------------------------------------------
            -- FUE DESCARGADO DEL PLANO --
            UPDATE SCO_PLANO_ENVIO_CARGOS
               SET COD_RESPUESTA_CMR = V_CCTE_CAMPO3,
                   NOMBRE_ARCHIVO    = V_NOMBRE_ARCHIVO_RESP_CARGOS,
                   RESP_ARCH_PAC     = 'S',
                   BOLETA            = V_CCTE_BOLETA -- campo es hasta 15 caracteres no se actualiza para DPKPE-73
             WHERE MEDIO_PAGO =
                   PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
               AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
               AND TP_ARCHIVO = PE_TP_ARCHIVO
               AND NUMERO_DOCUMENTO = V_CCTE_ID;
            --------------------------------------------------------------------------------------
            -- CONTADOR DE REGISTROS PROCESADOS --
            PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
            --------------------------------------------------------------------------------------
          ELSE
            --------------------------------------------------------------------------------------
            -- CONTADOR DE REGISTROS NO PROCESADOS --
            PS_REGISTROS_NO_PROCESADOS := (PS_REGISTROS_NO_PROCESADOS + 1);
            --------------------------------------------------------------------------------------
          END IF;
        END IF;
        --------------------------------------------------------------------------------------
        IF (MOD(PS_REGISTROS_PROCESADOS,
                1000) = 0)
        THEN
          UPDATE SCOH_BITACORA_PROC_PAC_ARCH
             SET SCHPPA_REGISTROS_RESP      =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP,
                      0) + PS_REGISTROS_PROCESADOS +
                 PS_REGISTROS_NO_PROCESADOS),
                 SCHPPA_REGISTROS_ACEPTADOS =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS,
                      0) + PS_REGISTROS_ACEPTADOS),
                 SCHPPA_REGISTROS_RECHAZADOS =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS,
                      0) + PS_REGISTROS_RECHAZADOS)
           WHERE SCHPPA_ID_PROCESO_PAC =
                 V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
             AND SCHPPA_TP_ARCHIVO = PE_TP_ARCHIVO;
          COMMIT;
        END IF;
        --------------------------------------------------------------------------------------
      EXCEPTION
        WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
          RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
        WHEN EXC_ERR_COD_RESP_INV THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_INS_SOL_ANULACION THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR GENERANDO SOLICITUD ANULACION :' ||
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_UPD_REZAGO THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR ACTUALIZANDO REZAGO :' ||
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_INS_REZAGO THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR CREANDO REZAGO :' || PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_UPD_ST_RECIBO_RECTOR THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_COD_RESP_NO_EXISTE THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_REZ_CUOTA_RECTOR THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_CCTE_ST_INV THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_PROPUESTA THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR AL OBTENER PROPUESTA DESDE LINEA DE ARCHIVO');
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_ID THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR AL OBTENER ID DE RECIBO DESDE LINEA DE ARCHIVO');
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN OTHERS THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            V_LINEA_ARCHIVO || ' :' || SQLERRM);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
      END;
      --------------------------------------------------------------------------------------
    END LOOP; -- LOOP DEL ARCHIVO PLANO --
    ---------------------------------------------------------------------------------------------------
    UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
    UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
    ---------------------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO               := 'FINALIZADO';
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ESTADO         := SYSDATE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_USUARIO           := PE_ID_USUARIO;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_TERMINAL          := PE_ID_TERMINAL;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_RESP  := V_NOMBRE_ARCHIVO_RESP_CARGOS;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP       := PS_REGISTROS_PROCESADOS +
                                                                PS_REGISTROS_NO_PROCESADOS;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS  := (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS,
                                                                     0) +
                                                                PS_REGISTROS_ACEPTADOS);
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS := (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS,
                                                                     0) +
                                                                PS_REGISTROS_RECHAZADOS);
    ---------------------------------------------------------------------------------------------------
    -- MODIFICA EL ARCHIVO DE INSCRIPCION EN LA BITACORA DE PROCESOS DE INSCRIPCION --
    IF (PKG_PROCESOS_PAC.MODIFICAR_SCOH_BIT_PROC_PAC_AR(V_REG_SCOH_BIT_PROC_PAC_ARCH,
                                                        PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_MOD_BIT_PROC_PAC_ARCH;
    END IF;
    ---------------------------------------------------------------------------------------------------
    COMMIT;
    ---------------------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    ---------------------------------------------------------------------------------------------------
    RETURN TRUE;
    ---------------------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_ST_PROC_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO PAC NO ES VALIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_PAC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ST_ARCH_RESP_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL ARCHIVO DE RESPUESTA DE CARGOS PAC NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ARCH_RESP_PAC_NO_EXIS THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE RESPUESTA DE CARGOS PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_NOMBRE_ARCH_CARGOS THEN
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_MOD_BIT_PROC_PAC_ARCH THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR BITACORA DE PROCESOS PAC' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_RESP_CARG THEN
      PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE RESPUESTA DE CARGOS PAC ' ||
                          V_NOMBRE_ARCHIVO_RESP_CARGOS;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_ERRORES THEN
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE ERRORES DE CARGOS PAC';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
      PS_MENSAJE_ERROR := 'ERROR AL LEER ARCHIVO DE RESPUESTA DE CARGOS PAC';
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_GRUPO_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR AL RECUPERAR GRUPO DE RESPUESTAS DE CARGOS PAC';
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      PS_MENSAJE_ERROR := SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END CARGA_ARCH_RESP_CARGOS_PAC;

  --DPKPE-291
  FUNCTION CARGA_ARCH_RESP_ABONOS_PAC(PE_FECHA_PROCESO_PAC       IN DATE,
                                      PE_FECHA_ARCHIVO_PAC       IN DATE,
                                      PE_TP_ARCHIVO              IN NUMBER,
                                      PE_ID_USUARIO              IN VARCHAR2,
                                      PE_ID_TERMINAL             IN VARCHAR2,
                                      PS_REGISTROS_LEIDOS        IN OUT NUMBER,
                                      PS_REGISTROS_PROCESADOS    IN OUT NUMBER,
                                      PS_REGISTROS_NO_PROCESADOS IN OUT NUMBER,
                                      PS_REGISTROS_ERRONEOS      IN OUT NUMBER,
                                      PS_REGISTROS_ACEPTADOS     IN OUT NUMBER,
                                      PS_REGISTROS_RECHAZADOS    IN OUT NUMBER,
                                      PS_MENSAJE_ERROR           IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    --------------------------------------------------------------------------------------
    V_ARCHIVO_RESPUESTA_CARGOS   UTL_FILE.FILE_TYPE;
    V_ARCHIVO_ERRORES            UTL_FILE.FILE_TYPE;
    V_LINEA_ARCHIVO              VARCHAR2(500);
    V_NOMBRE_DIRECTORIO          VARCHAR2(255);
    V_NOMBRE_ARCHIVO_RESP_CARGOS VARCHAR2(255);
    V_NOMBRE_ARCHIVO_ERRORES     VARCHAR2(255);
    V_CODIGO_TX                  VARCHAR2(3) := NULL; -- SOLO PARA PERU EL CODIGO DE TRANSACCION DE CARGO NORMAL "000" --
    --------------------------------------------------------------------------------------
    V_CACA_CD_ORIGEN       CART_CARGOS.CACA_CD_ORIGEN%TYPE;
    V_SCCP_IND_REZAGO      SCO_PRODUCTOS.SCCP_IND_REZAGO%TYPE;
    V_RESP_ARCH_PAC        SCO_PLANO_ENVIO_CARGOS.RESP_ARCH_PAC%TYPE;
    V_TARJETA_CMR          SCO_PLANO_ENVIO_CARGOS.TARJETA_CMR%TYPE;
    V_CODIGO_PRODUCTO      SCO_PLANO_ENVIO_CARGOS.CODIGO_PRODUCTO%TYPE;
    V_CCTE_ID              SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_PROPUESTA       SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_CCTE_BOLETA          SCO_CUENTA_CORRIENTE.CCTE_BOLETA%TYPE;
    V_CCTE_NU_ENDOSO       SCO_CUENTA_CORRIENTE.CCTE_NU_ENDOSO%TYPE;
    V_CCTE_FECHA_ENVIO_PAC SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
    V_CCTE_MONTO_MONEDA    SCO_CUENTA_CORRIENTE.CCTE_MONTO_MONEDA%TYPE;
    V_CCTE_TIPO_CAMBIO     SCO_CUENTA_CORRIENTE.CCTE_TIPO_CAMBIO%TYPE;
    V_CCTE_MONTO_PESOS     SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    V_CCTE_CUOTA_TOTAL     SCO_CUENTA_CORRIENTE.CCTE_CUOTA_TOTAL%TYPE;
    V_CCTE_CUOTA           SCO_CUENTA_CORRIENTE.CCTE_CUOTA%TYPE;
    V_CCTE_CODIGO_CMR      SCO_CUENTA_CORRIENTE.CCTE_CODIGO_CMR%TYPE;
    V_CCTE_TP_MOVTO        SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE;
    V_CCTE_MONEDA          SCO_CUENTA_CORRIENTE.CCTE_MONEDA%TYPE;
    V_CCTE_PLAN            SCO_CUENTA_CORRIENTE.CCTE_PLAN%TYPE;
    V_CCTE_RECIBO          SCO_CUENTA_CORRIENTE.CCTE_RECIBO%TYPE;
    V_CCTE_STATUS          SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE;
    V_CCTE_COD_OBSERVACION SCO_CUENTA_CORRIENTE.CCTE_COD_OBSERVACION%TYPE;
    V_CCTE_CAMPO3          VARCHAR2(3);
    V_CACE_ST_CERTIFICADO  CART_CERTIFICADOS.CACE_ST_CERTIFICADO%TYPE;
    V_CACE_CAPB_CD_PLAN    CART_CERTIFICADOS.CACE_CAPB_CD_PLAN%TYPE;
    V_CAME_CD_MOTIVO       CART_MOTIVOS_ENDOSOS.CAME_CD_MOTIVO%TYPE;
    --V_REG_DEF_RESP_PROC            SCO_DEF_RESPUESTAS_PROCESOS%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC      SCOH_BITACORA_PROCESOS_PAC%ROWTYPE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH SCOH_BITACORA_PROC_PAC_ARCH%ROWTYPE;
    V_ES_DIA_PAC                 NUMBER := 0;
    V_SITUACION                  NUMBER(2) := 0;
    V_DIAS_MORA                  NUMBER(4) := 0;
    V_EXISTE_EN_REZAGADAS        NUMBER := 0;
    V_ANULAR_PROPUESTA           NUMBER := 0;
    --------------------------------------------------------------------------------------
    V_SCDR_SCDRG_ID_GRUPO         SCO_DEF_RESPUESTA.SCDR_SCDRG_ID_GRUPO%TYPE;
    V_SCDRD_COD_RESPUESTA_INTERNO SCO_DEF_RESPUESTA_DETALLE.SCDRD_COD_RESPUESTA_INTERNO%TYPE;
    V_SCDRD_TIPO_RESPUESTA        SCO_DEF_RESPUESTA_DETALLE.SCDRD_TIPO_RESPUESTA%TYPE;
    --------------------------------------------------------------------------------------
    EXC_ERR_ST_PROC_PAC            EXCEPTION;
    EXC_ERR_PROC_PAC_NO_EXISTE     EXCEPTION;
    EXC_ERR_ST_ARCH_RESP_PAC       EXCEPTION;
    EXC_ERR_ARCH_RESP_PAC_NO_EXIS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_RESP_CARG   EXCEPTION;
    EXC_ERR_LEER_ARCH_RESP_CARGOS  EXCEPTION;
    EXC_ERR_ABRIR_ARCH_ERRORES     EXCEPTION;
    EXC_ERR_OBT_NOMBRE_ARCH_CARGOS EXCEPTION;
    EXC_ERR_MOD_BIT_PROC_PAC_ARCH  EXCEPTION;
    EXC_ERR_INS_REZAGO             EXCEPTION;
    EXC_ERR_UPD_REZAGO             EXCEPTION;
    EXC_ERR_INS_SOL_ANULACION      EXCEPTION;
    EXC_ERR_COD_RESP_NO_EXISTE     EXCEPTION;
    EXC_ERR_UPD_ST_RECIBO_RECTOR   EXCEPTION;
    EXC_ERR_REZ_CUOTA_RECTOR       EXCEPTION;
    EXC_ERR_CCTE_ST_INV            EXCEPTION;
    -- MMA 24-02-2012. --
    EXC_ERR_GRUPO_NO_EXISTE EXCEPTION;
    EXC_ERR_COD_RESP_INV    EXCEPTION;
    --------------------------------------------------------------------------------------
  BEGIN
    --------------------------------------------------------------------------------------
    PS_REGISTROS_LEIDOS        := 0;
    PS_REGISTROS_PROCESADOS    := 0;
    PS_REGISTROS_NO_PROCESADOS := 0;
    PS_REGISTROS_ERRONEOS      := 0;
    PS_REGISTROS_ACEPTADOS     := 0;
    PS_REGISTROS_RECHAZADOS    := 0;
    --------------------------------------------------------------------------------------
    -- SE OBTIENE EL PROCESO DE CARGOS EN CURSO --
    V_REG_SCOH_BIT_PROC_PAC := NULL;
    -- VALIDACIONES ANTES DEL PROCESO --
    -- SE VALIDA PROCESO PAC --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC
        FROM SCOH_BITACORA_PROCESOS_PAC
       WHERE SCHPP_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
      IF (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_PROC_PAC_NO_EXISTE;
    END;
    -----------------------------------------------------------------------------------------
    -- SE VALIDA SI EXISTE ARCHIVO DE ENVIO DE CARGOS PARA EL PROCESO DE CARGOS --
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC_ARCH
        FROM SCOH_BITACORA_PROC_PAC_ARCH
       WHERE SCHPPA_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
         AND SCHPPA_TP_ARCHIVO = PE_TP_ARCHIVO;
      IF (V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO != 'PENDIENTE')
      THEN
        RAISE EXC_ERR_ST_ARCH_RESP_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_ARCH_RESP_PAC_NO_EXIS;
    END;
    --------------------------------------------------------------------------------------
    -- SE OBTIENE EL DIRECTORIO DE DESTINO --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    --------------------------------------------------------------------------------------
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                    PE_TP_ARCHIVO,
                                                    PKG_PROCESOS_PAC_CMR.SCDAC_ID_PROCESO_RESP,
                                                    PE_FECHA_ARCHIVO_PAC,
                                                    V_NOMBRE_ARCHIVO_RESP_CARGOS,
                                                    PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
    END IF;
    --------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_RESPUESTA_CARGOS := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                                   V_NOMBRE_ARCHIVO_RESP_CARGOS,
                                                   'R');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_RESP_CARG;
    END;
    --------------------------------------------------------------------------------------
    IF (PKG_PROCESOS_PAC.OBTENER_NOMBRE_ARCH_CARGOS(PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN,
                                                    PE_TP_ARCHIVO,
                                                    PKG_PROCESOS_PAC_CMR.SCDAC_ID_PROCESO_ERR,
                                                    PE_FECHA_ARCHIVO_PAC,
                                                    V_NOMBRE_ARCHIVO_ERRORES,
                                                    PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_OBT_NOMBRE_ARCH_CARGOS;
    END IF;
    --------------------------------------------------------------------------------------
    BEGIN
      V_ARCHIVO_ERRORES := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                          V_NOMBRE_ARCHIVO_ERRORES,
                                          'W');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_ERRORES;
    END;
    --------------------------------------------------------------------------------------
    --IC-[ER-RECTOR-PROYMOROS-1]-SE BUSCA SI ES DIA PAC --
    --BEGIN
    --     SELECT 1
    --       INTO V_ES_DIA_PAC
    --       FROM SCO_DIA_PAGO O
    --      WHERE O.SODP_MEDIO_PAGO = PKG_PROCESOS_PAC_CMR.CODIGO_MEDIO_PAGO_CMR
    --        AND O.SODP_DIA_PAC    = TO_CHAR(PE_FECHA_PROCESO_PAC, 'DD');
    --EXCEPTION
    --          WHEN OTHERS THEN
    --               V_ES_DIA_PAC := 0;
    --END;
    -- FC-[ER-RECTOR-PROYMOROS-1] --
    --------------------------------------------------------------------------------------
    -- MMA, 24-02-2012 --
    -- BUSCA SET DE RESPUESTAS POSIBLES PARA CARGOS --
    BEGIN
      SELECT SCDR_SCDRG_ID_GRUPO
        INTO V_SCDR_SCDRG_ID_GRUPO
        FROM SCO_DEF_RESPUESTA
       WHERE SCDR_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCDR_TIPO_ARCHIVO = PE_TP_ARCHIVO
            --AND SCDR_TIPO_PROCESO  = PKG_PROCESOS_PAC_CMR.SCDR_TIPO_PROCESO_CARGO;
         AND SCDR_TIPO_PROCESO =
             PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_ABONO;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_GRUPO_NO_EXISTE;
    END;
    --------------------------------------------------------------------------------------
    -- SE LEE Y PROCESO ARCHIVO DE RESPUESTA DE CARGOS --
    LOOP
      -- LOOP PRINCIPAL DEL ARCHIVO PLANO
      BEGIN
        -- CONTROL DE CARGA DE TODOS LOS REGISTROS DEL ARCHIVO DE CARGOS --
        --------------------------------------------------------------------------------------
        BEGIN
          -- LEER LINEA DE REGISTRO DEL ARCHIVO --
          UTL_FILE.GET_LINE(V_ARCHIVO_RESPUESTA_CARGOS,
                            V_LINEA_ARCHIVO);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            EXIT;
          WHEN OTHERS THEN
            RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
        END;
        --------------------------------------------------------------------------------------
        PS_REGISTROS_LEIDOS := (PS_REGISTROS_LEIDOS + 1);
        -------------------------------------------------------------------------------------
        -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE RESPUESTA --
        --V_CODIGO_TX   := SUBSTR(V_LINEA_ARCHIVO,179,3); -- ORIGEN 400 (EXTORNOS) --
        V_CODIGO_TX   := '400'; -- ORIGEN 400 (EXTORNOS) --
        V_CCTE_BOLETA := TO_NUMBER(NVL(LTRIM(SUBSTR(V_LINEA_ARCHIVO,
                                                    2,
                                                    15)),
                                       0));
        --V_CCTE_ID := TO_NUMBER(NVL(SUBSTR(V_LINEA_ARCHIVO,
        --                                  1,
        --                                  15),
        --                           '0')); -- CCTE_ID --
        --DPKPE-563
        RECORRER_LINEA(V_LINEA_ARCHIVO,
                       V_CCTE_ID);
        --[V.1.3.3  --->
        -- ELIMINA SIMULACION Y LEE EL DATO DESDE EL ARCHIVO DE ENTRADA --
        -- SE SIMULA QUE ES ACEPTADA:CON NVL 000 --
        --V_CCTE_CAMPO3 := NVL( SUBSTR(V_LINEA_ARCHIVO,79,3),'000' ); -- CODIGO RESPUESTA --
        V_CCTE_CAMPO3 := NVL(SUBSTR(V_LINEA_ARCHIVO,
                                    length(V_LINEA_ARCHIVO) - 2,
                                    3),
                             '000'); -- CODIGO RESPUESTA --
        --<---  V.1.3.3]
        --V_SITUACION := TRIM(SUBSTR(V_LINEA_ARCHIVO,
        --                           162,
        --                           2));
        --V_DIAS_MORA := TRIM(SUBSTR(V_LINEA_ARCHIVO,
        --                           164,
        --                           4));
        --------------------------------------------------------------------------------------
        -- OBTIENE CUOTA DESDE SCO_PLANO_ENVIO_CARGOS --
        SELECT RESP_ARCH_PAC,
               TARJETA_CMR,
               CODIGO_PRODUCTO
          INTO V_RESP_ARCH_PAC,
               V_TARJETA_CMR,
               V_CODIGO_PRODUCTO
          FROM SCO_PLANO_ENVIO_CARGOS
         WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
           AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
           AND TP_ARCHIVO = PE_TP_ARCHIVO
           AND ENV_ARCH_PAC = 'S'
           AND NUMERO_DOCUMENTO = V_CCTE_ID;
        --------------------------------------------------------------------------------------
        IF (V_RESP_ARCH_PAC = 'N')
        THEN
          --------------------------------------------------------------------------------------
          -- BUSCO REGISTRO EN CUENTA CORRIENTE PARA GENERAR REGISTRO INVERSO SI ESTA ACEPTADO --
          SELECT CCTE_PROPUESTA,
                 CCTE_CUOTA,
                 CCTE_CUOTA_TOTAL,
                 CCTE_MONEDA,
                 CCTE_MONTO_MONEDA,
                 CCTE_MONTO_PESOS,
                 CCTE_TIPO_CAMBIO,
                 CCTE_PLAN,
                 CCTE_CODIGO_CMR,
                 CCTE_TP_MOVTO,
                 CCTE_FECHA_ENVIO_PAC,
                 CCTE_RECIBO,
                 CCTE_NU_ENDOSO,
                 CCTE_STATUS,
                 CCTE_COD_OBSERVACION
            INTO V_CCTE_PROPUESTA,
                 V_CCTE_CUOTA,
                 V_CCTE_CUOTA_TOTAL,
                 V_CCTE_MONEDA,
                 V_CCTE_MONTO_MONEDA,
                 V_CCTE_MONTO_PESOS,
                 V_CCTE_TIPO_CAMBIO,
                 V_CCTE_PLAN,
                 V_CCTE_CODIGO_CMR,
                 V_CCTE_TP_MOVTO,
                 V_CCTE_FECHA_ENVIO_PAC,
                 V_CCTE_RECIBO,
                 V_CCTE_NU_ENDOSO,
                 V_CCTE_STATUS,
                 V_CCTE_COD_OBSERVACION
            FROM SCO_CUENTA_CORRIENTE
           WHERE CCTE_ID = V_CCTE_ID;
          --------------------------------------------------------------------------------------
          BEGIN
            SELECT SCDRD_COD_RESPUESTA_INTERNO,
                   SCDRD_TIPO_RESPUESTA
              INTO V_SCDRD_COD_RESPUESTA_INTERNO,
                   V_SCDRD_TIPO_RESPUESTA
              FROM SCO_DEF_RESPUESTA_DETALLE
             WHERE SCDRD_SCDRG_ID_GRUPO = V_SCDR_SCDRG_ID_GRUPO
               AND SCDRD_COD_RESPUESTA = V_CCTE_CAMPO3 --V_MENSAJE_RESPUESTA_JOURNAL
                  --AND SCDRD_TIPO_RESPUESTA = SUBSTR(V_COD_RESPUESTA_JOURNAL, 1, 1) -- SE OBTIENE PRIMER CARACTER (A/R).
               AND SCDRD_ESTADO = 'A';
            --
            V_CCTE_CAMPO3 := V_SCDRD_COD_RESPUESTA_INTERNO;
            -- 
          EXCEPTION
            WHEN OTHERS THEN
              PS_MENSAJE_ERROR := 'ERROR, C�DIGO DE RESPUESTA ' ||
                                  V_CCTE_CAMPO3 ||
                                  ' ENVIADO NO VALIDO PARA CARGA PAC.';
              RAISE EXC_ERR_COD_RESP_INV;
          END;
          --------------------------------------------------------------------------------------
          -- SE VALIDA SI EL MOVIMIENTO FUE ACEPTADO O RECHAZADO ("A" O "R") --
          --IF (V_REG_DEF_RESP_PROC.SCDCP_TP_RESPUESTA = 'A') THEN
          -- MMA, 24-02-2012 --
          IF (V_SCDRD_TIPO_RESPUESTA = 'A')
          THEN
            -- ACEPTADA --
            --------------------------------------------------------------------------------------
            -- SE ACTUALIZA CUENTA CORRIENTE --
            -- MOVIMIENTOS CURSADOS POR CMR --
            UPDATE SCO_CUENTA_CORRIENTE
               SET CCTE_STATUS = PKG_PROCESOS_PAC_CMR.CCTE_ST_ACEPTADO,
                   CCTE_CAMPO3 = V_CCTE_CAMPO3,
                   --CCTE_MONTO_PESOS         = V_CCTE_MONTO_PESOS,
                   CCTE_FTRANSACCION      = SYSDATE, --V_CCTE_FTRANSACCION,
                   CCTE_ANOMESCONTABLE    = TO_CHAR(PE_FECHA_PROCESO_PAC,
                                                    'RRRRMM'),
                   CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                   CCTE_MOVTO_CONTABLE    = 'S',
                   --V.1.3.1 se asigna monto pesos..
                   CCTE_SALDO_ABONO      = V_CCTE_MONTO_PESOS,
                   CCTE_CAMPO7           = PKG_PROCESOS_PAC_CMR.CCTE_CAMPO7_RESP,
                   CCTE_CUENTA           = V_TARJETA_CMR, -- VIENE DE SCO_PLANO_ENVIO_CARGOS --
                   CCTE_BOLETA           = V_CCTE_BOLETA,
                   CCTE_INTENTOS         = NVL(CCTE_INTENTOS,
                                               0) + 1,
                   CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                  1,
                                                  (NVL(CCTE_INTENTOS_ESTATUS,
                                                       0) + 1),
                                                  CCTE_INTENTOS_ESTATUS)
             WHERE CCTE_ID = V_CCTE_ID;
            --------------------------------------------------------------------------------------
            -- LUIS MORA DIAZ --
            -- 17/05/2011 --
            --  SE OBTIENE EL CODIGO DE ORIGEN PARA CART_CARGOS --
            -- EN PERU EL CODIGO DE TRANSACCION CARGOS ES "000" NO 200 COMO EN TODOS LOS PAISES --
            -- ORIGEN "0" ES CARGO, DISTINTO DE "0" ES ABONO O DEVOLUCION O EXTORNO --
            V_CACA_CD_ORIGEN := 20;
            --IF (V_CODIGO_TX = '000') THEN
            --   V_CACA_CD_ORIGEN := 0;
            --ELSE
            --   V_CACA_CD_ORIGEN := 20;
            --END IF;
            --------------------------------------------------------------------------------------
            -- LUIS MORA DIAZ --
            -- 24-05-2010 --
            -- SE INSERTA REGISTRO DE CARGO EN TABLA CART_CARGOS --
            INSERT INTO CART_CARGOS
              (CACA_CD_ORIGEN,
               CACA_CACE_NU_PROPUESTA,
               CACA_CADM_NU_CUENTA,
               CACA_CAPB_NU_SEGURO_CMR,
               CACA_FECHA_CMR, -- CCTE_DIA_PAC_PROCESADO --
               CACA_FECHA_RECTOR, -- CCTE_FTRANSACCION --
               CACA_FE_PAC, -- CCTE-FECHA_ENVIO_PAC --
               CACA_FE_PAGO_CMR, -- CCTE_DIA_PAC_PROCESADO --
               CACA_ESTADO_CMR,
               CACA_ESTADO_RECTOR,
               CACA_MOTIVO,
               CACA_DIAS_MORA,
               CACA_NUMERO_CUOTA,
               CACA_TOTAL_CUOTA,
               CACA_PRIMA_PESOS,
               CACA_PRIMA_SEGURO,
               CACA_VALOR_UF,
               CACA_RECIBO)
            VALUES
              (V_CACA_CD_ORIGEN, -- ORIGEN EXTORNO (20) --
               V_CCTE_PROPUESTA, -- PROPUESTA --
               V_TARJETA_CMR, -- CUENTA CMR --
               V_CODIGO_PRODUCTO, -- CODIGO DE CONVENIO --
               PE_FECHA_PROCESO_PAC, -- FECHA DEL PROCESO PAC --
               TO_DATE(SYSDATE,
                       'DD-MM-RRRR'), -- FECHA DE CARGA DEL PROCESO DE RESPUESTA --
               V_CCTE_FECHA_ENVIO_PAC, -- FECHA DE LA CUOTA --
               PE_FECHA_PROCESO_PAC, -- FECHA DEL PROCESO --
               'PR',
               'PE',
               V_CCTE_COD_OBSERVACION, -- MOTIVO EXTORNO --
               0, -- DIAS DE MORA --
               V_CCTE_CUOTA,
               V_CCTE_CUOTA_TOTAL,
               V_CCTE_MONTO_PESOS,
               V_CCTE_MONTO_MONEDA,
               V_CCTE_TIPO_CAMBIO,
               V_CCTE_RECIBO);
            --------------------------------------------------------------------------------------
            -- CONTADOR DE REGISTROS ACEPTADOS --
            PS_REGISTROS_ACEPTADOS := (PS_REGISTROS_ACEPTADOS + 1);
            --------------------------------------------------------------------------------------
            --ELSIF (V_REG_DEF_RESP_PROC.SCDCP_TP_RESPUESTA = 'R') THEN -- MOVIMIENTOS RECHAZADOS POR CMR --
          ELSIF (V_SCDRD_TIPO_RESPUESTA = 'R')
          THEN
            -- MOVIMIENTOS RECHAZADOS POR CMR --
            --------------------------------------------------------------------------------------
            -- SE OBTIENE EL ESTADO Y PRODUCTO DE CART_CERTIFICADOS --
            SELECT CACE_ST_CERTIFICADO,
                   CACE_CAPB_CD_PLAN
              INTO V_CACE_ST_CERTIFICADO,
                   V_CACE_CAPB_CD_PLAN
              FROM CART_CERTIFICADOS
             WHERE CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
            --------------------------------------------------------------------------------------
            -- SE PROCESA RECHAZO DEPENDIENDO EL TIPO DE MOVIMIENTO
            IF (V_CCTE_TP_MOVTO IN
               (PKG_PROCESOS_PAC_CMR.CCTE_TP_MOVTO_CARGO_DIR,
                 PKG_PROCESOS_PAC_CMR.CCTE_TP_MOVTO_ABONO_DIR))
            THEN
              --------------------------------------------------------------------------------------
              -- ACTUALIZACION DE TABLA SCO_CUENTA_CORRIENTE --
              UPDATE SCO_CUENTA_CORRIENTE
                 SET CCTE_STATUS            = PKG_PROCESOS_PAC_CMR.CCTE_ST_NO_REALIZADO,
                     CCTE_FTRANSACCION      = SYSDATE,
                     CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
                     CCTE_CAMPO3            = V_CCTE_CAMPO3,
                     CCTE_TRASP_ENV_CARGOS  = PKG_PROCESOS_PAC_CMR.CCTE_TRASP_ENV_CARGOS_NO,
                     CCTE_CAMPO7            = PKG_PROCESOS_PAC_CMR.CCTE_CAMPO7_RESP,
                     CCTE_INTENTOS         =
                     (NVL(CCTE_INTENTOS,
                          0) + 1),
                     --IC-[ER-RECTOR-PROYMOROS-1]-SE AGREGA DECODE : SI ES DIA PAC SUMA 1 SINO NO MODIFICA EL VALOR.
                     CCTE_INTENTOS_ESTATUS = DECODE(V_ES_DIA_PAC,
                                                    1,
                                                    (NVL(CCTE_INTENTOS_ESTATUS,
                                                         0) + 1),
                                                    CCTE_INTENTOS_ESTATUS)
              --FC-[ER-RECTOR-PROYMOROS-1]
               WHERE CCTE_ID = V_CCTE_ID;
              --------------------------------------------------------------------------------------
              -- ACTUALIZACION DE TABLA CART_RECIBOS RECTOR --
              UPDATE CART_RECIBOS
                 SET CARE_ST_RECIBO = PKG_PROCESOS_PAC_CMR.CARE_ST_RECIBO_REZAGADO, -- REZAGADO RECTOR --
                     --CARE_NU_AVISO            = V_CCTE_BOLETA,
                     CARE_NU_CESION_COASEGURO = V_SITUACION,
                     CARE_NU_CESION_REASEGURO = V_DIAS_MORA,
                     CARE_FE_NOTIFICACION     = PE_FECHA_PROCESO_PAC,
                     CARE_CAUS_CD_USUARIO     = V_CCTE_CAMPO3
               WHERE CARE_NU_RECIBO = V_CCTE_RECIBO
                 AND CARE_CACE_NU_PROPUESTA = V_CCTE_PROPUESTA;
              --------------------------------------------------------------------------------------
            END IF;
            --------------------------------------------------------------------------------------
            -- CONTADOR DE REGISTROS RECHAZADOS --
            PS_REGISTROS_RECHAZADOS := (PS_REGISTROS_RECHAZADOS + 1);
            --------------------------------------------------------------------------------------
          END IF;
          --------------------------------------------------------------------------------------
          -- FUE DESCARGADO DEL PLANO
          UPDATE SCO_PLANO_ENVIO_CARGOS
             SET COD_RESPUESTA_CMR = V_CCTE_CAMPO3,
                 NOMBRE_ARCHIVO    = V_NOMBRE_ARCHIVO_RESP_CARGOS,
                 RESP_ARCH_PAC     = 'S'
           WHERE MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
             AND TP_ARCHIVO = PE_TP_ARCHIVO
             AND NUMERO_DOCUMENTO = V_CCTE_ID;
          --------------------------------------------------------------------------------------
          -- CONTADOR DE REGISTROS PROCESADOS --
          PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
          --------------------------------------------------------------------------------------
        ELSE
          --------------------------------------------------------------------------------------
          -- CONTADOR DE REGISTROS NO PROCESADOS --
          PS_REGISTROS_NO_PROCESADOS := (PS_REGISTROS_NO_PROCESADOS + 1);
          --------------------------------------------------------------------------------------
        END IF;
        --------------------------------------------------------------------------------------
        IF (MOD(PS_REGISTROS_PROCESADOS,
                1000) = 0)
        THEN
          UPDATE SCOH_BITACORA_PROC_PAC_ARCH
             SET SCHPPA_REGISTROS_RESP      =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP,
                      0) + PS_REGISTROS_PROCESADOS +
                 PS_REGISTROS_NO_PROCESADOS),
                 SCHPPA_REGISTROS_ACEPTADOS =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS,
                      0) + PS_REGISTROS_ACEPTADOS),
                 SCHPPA_REGISTROS_RECHAZADOS =
                 (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS,
                      0) + PS_REGISTROS_RECHAZADOS)
           WHERE SCHPPA_ID_PROCESO_PAC =
                 V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC
             AND SCHPPA_TP_ARCHIVO = PE_TP_ARCHIVO;
          COMMIT;
        END IF;
        --------------------------------------------------------------------------------------
      EXCEPTION
        WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
          RAISE EXC_ERR_LEER_ARCH_RESP_CARGOS;
        WHEN EXC_ERR_COD_RESP_INV THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_INS_SOL_ANULACION THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR GENERANDO SOLICITUD ANULACION :' ||
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_UPD_REZAGO THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR ACTUALIZANDO REZAGO :' ||
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_INS_REZAGO THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            'ERROR CREANDO REZAGO :' || PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_UPD_ST_RECIBO_RECTOR THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_COD_RESP_NO_EXISTE THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_REZ_CUOTA_RECTOR THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN EXC_ERR_CCTE_ST_INV THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            PS_MENSAJE_ERROR);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
        WHEN OTHERS THEN
          UTL_FILE.PUT_LINE(V_ARCHIVO_ERRORES,
                            V_LINEA_ARCHIVO || ' :' || SQLERRM);
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
      END;
      --------------------------------------------------------------------------------------
    END LOOP; -- LOOP DEL ARCHIVO PLANO --
    ---------------------------------------------------------------------------------------------------
    UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
    UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
    ---------------------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL ARCHIVO DE ENVIO DE INSCRIPCION --
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ESTADO               := 'FINALIZADO';
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_FECHA_ESTADO         := SYSDATE;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_USUARIO           := PE_ID_USUARIO;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_ID_TERMINAL          := PE_ID_TERMINAL;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_NOMBRE_ARCHIVO_RESP  := V_NOMBRE_ARCHIVO_RESP_CARGOS;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RESP       := PS_REGISTROS_PROCESADOS +
                                                                PS_REGISTROS_NO_PROCESADOS;
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS  := (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_ACEPTADOS,
                                                                     0) +
                                                                PS_REGISTROS_ACEPTADOS);
    V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS := (NVL(V_REG_SCOH_BIT_PROC_PAC_ARCH.SCHPPA_REGISTROS_RECHAZADOS,
                                                                     0) +
                                                                PS_REGISTROS_RECHAZADOS);
    ---------------------------------------------------------------------------------------------------
    -- MODIFICA EL ARCHIVO DE INSCRIPCION EN LA BITACORA DE PROCESOS DE INSCRIPCION --
    IF (PKG_PROCESOS_PAC.MODIFICAR_SCOH_BIT_PROC_PAC_AR(V_REG_SCOH_BIT_PROC_PAC_ARCH,
                                                        PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_MOD_BIT_PROC_PAC_ARCH;
    END IF;
    ---------------------------------------------------------------------------------------------------
    COMMIT;
    ---------------------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    ---------------------------------------------------------------------------------------------------
    RETURN TRUE;
    ---------------------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_ST_PROC_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO PAC NO ES VALIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_PAC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ST_ARCH_RESP_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL ARCHIVO DE RESPUESTA DE CARGOS PAC NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ARCH_RESP_PAC_NO_EXIS THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ARCHIVO DE RESPUESTA DE CARGOS PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_OBT_NOMBRE_ARCH_CARGOS THEN
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_MOD_BIT_PROC_PAC_ARCH THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR BITACORA DE PROCESOS PAC' ||
                          PS_MENSAJE_ERROR;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_RESP_CARG THEN
      PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE RESPUESTA DE CARGOS PAC ' ||
                          V_NOMBRE_ARCHIVO_RESP_CARGOS;
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_ABRIR_ARCH_ERRORES THEN
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      PS_MENSAJE_ERROR := 'ERROR AL ABRIR ARCHIVO DE ERRORES DE CARGOS PAC';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_LEER_ARCH_RESP_CARGOS THEN
      PS_MENSAJE_ERROR := 'ERROR AL LEER ARCHIVO DE RESPUESTA DE CARGOS PAC';
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_GRUPO_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR AL RECUPERAR GRUPO DE RESPUESTAS DE CARGOS PAC';
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      UTL_FILE.FCLOSE(V_ARCHIVO_RESPUESTA_CARGOS);
      UTL_FILE.FCLOSE(V_ARCHIVO_ERRORES);
      PS_MENSAJE_ERROR := SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END CARGA_ARCH_RESP_ABONOS_PAC;

  ---------------------------------------------------------------------------------------------------
  -- CUADRATURA DE ARCHIVO DE RESPUESTA DE CARGOS --
  FUNCTION CUADRATURA_ARCHIVO_RESP_CARGOS(
                                          ---------------------------------------
                                          PE_FECHA_PROCESO_PAC IN DATE,
                                          PE_FECHA_ARCHIVO_PAC IN DATE,
                                          PE_MEDIO_PAGO        IN NUMBER,
                                          PE_TP_ARCHIVO        IN NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_ACEP IN OUT NUMBER,
                                          PS_CMONTO_CCTE_ACEP    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_RECH IN OUT NUMBER,
                                          PS_CMONTO_CCTE_RECH    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_CCTE_ENV IN OUT NUMBER,
                                          PS_CMONTO_CCTE_ENV    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGITROS_RECEP_ACEP IN OUT NUMBER,
                                          PS_CMONTO_RECEP_ACEP    IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGISTROS_RECEP_RECH IN OUT NUMBER,
                                          PS_CMONTO_RECEP_RECH     IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_CREGISTROS_RECEP_ENV IN OUT NUMBER,
                                          PS_CMONTO_RECEP_ENV     IN OUT NUMBER,
                                          ---------------------------------------
                                          PS_MENSAJE_ERROR IN OUT VARCHAR2
                                          ---------------------------------------
                                          ) RETURN BOOLEAN IS
  BEGIN
    ---------------------------------------
    PS_CREGITROS_CCTE_ACEP := 0;
    PS_CMONTO_CCTE_ACEP    := 0;
    ---------------------------------------
    PS_CREGITROS_CCTE_RECH := 0;
    PS_CMONTO_CCTE_RECH    := 0;
    ---------------------------------------
    PS_CREGITROS_CCTE_ENV := 0;
    PS_CMONTO_CCTE_ENV    := 0;
    ---------------------------------------
    PS_CREGITROS_RECEP_ACEP := 0;
    PS_CMONTO_RECEP_ACEP    := 0;
    ---------------------------------------
    PS_CREGISTROS_RECEP_RECH := 0;
    PS_CMONTO_RECEP_RECH     := 0;
    ---------------------------------------
    PS_CREGISTROS_RECEP_ENV := 0;
    PS_CMONTO_RECEP_ENV     := 0;
    ---------------------------------------
    PS_MENSAJE_ERROR := NULL;
    ---------------------------------------
    ----------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTOS ACEPTADOS EN CUENTA CORRIENTE --
    SELECT COUNT(*),
           SUM(CCTE_MONTO_PESOS)
      INTO PS_CREGITROS_CCTE_ACEP,
           PS_CMONTO_CCTE_ACEP
      FROM SCO_CUENTA_CORRIENTE
     WHERE CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO
       AND CCTE_ID IN
           (SELECT NUMERO_DOCUMENTO
              FROM SCO_PLANO_ENVIO_CARGOS,
                   SCO_DEF_RESPUESTA,
                   SCO_DEF_RESPUESTA_DETALLE
            --SCO_DEF_RESPUESTAS_PROCESOS
             WHERE MEDIO_PAGO = PE_MEDIO_PAGO
               AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
               AND TP_ARCHIVO = PE_TP_ARCHIVO
               AND TP_MOVTO IN (1,
                                2,
                                3)
               AND ENV_ARCH_PAC = 'S'
               AND RESP_ARCH_PAC = 'S'
               AND SCDR_CD_MEDIO_PAGO = MEDIO_PAGO
               AND SCDR_TIPO_ARCHIVO = TP_ARCHIVO
               AND SCDR_TIPO_PROCESO =
                   PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_CARGO
               AND SCDRD_SCDRG_ID_GRUPO = SCDR_SCDRG_ID_GRUPO
               AND SCDRD_COD_RESPUESTA_INTERNO = COD_RESPUESTA_CMR
               AND SCDRD_ESTADO =
                   PKG_PROCESOS_PAC_VISANET.SCDRD_ESTADO_ACTIVO
               AND SCDRD_TIPO_RESPUESTA =
                   PKG_PROCESOS_PAC_VISANET.SCDRD_TIPO_RESPUESTA_ACEPTADO
            -- MMA, 10-01-2012 --
            --AND COD_RESPUESTA_CMR   = SCDCP_CD_RESPUESTA_FPRO
            --AND SCDCP_CD_MEDIO_PAGO = MEDIO_PAGO
            --AND SCDCP_ESTADO        = 'A'
            --AND SCDCP_TP_PROCESO    = 'C'
            --AND SCDCP_TP_RESPUESTA  = 'A'
            );
    ----------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTOS RECHAZADOS EN CUENTA CORRIENTE --
    SELECT COUNT(*),
           SUM(CCTE_MONTO_PESOS)
      INTO PS_CREGITROS_CCTE_RECH,
           PS_CMONTO_CCTE_RECH
      FROM SCO_CUENTA_CORRIENTE
     WHERE CCTE_STATUS != PKG_PROCESOS_PAC_VISANET.CCTE_ST_ACEPTADO
       AND CCTE_ID IN
           (SELECT NUMERO_DOCUMENTO
              FROM SCO_PLANO_ENVIO_CARGOS,
                   SCO_DEF_RESPUESTA,
                   SCO_DEF_RESPUESTA_DETALLE
            --SCO_DEF_RESPUESTAS_PROCESOS
             WHERE MEDIO_PAGO = PE_MEDIO_PAGO
               AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
               AND TP_ARCHIVO = PE_TP_ARCHIVO
               AND TP_MOVTO IN (1,
                                2,
                                3)
               AND ENV_ARCH_PAC = 'S'
               AND RESP_ARCH_PAC = 'S'
               AND SCDR_CD_MEDIO_PAGO = MEDIO_PAGO
               AND SCDR_TIPO_ARCHIVO = TP_ARCHIVO
               AND SCDR_TIPO_PROCESO =
                   PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_CARGO
               AND SCDRD_SCDRG_ID_GRUPO = SCDR_SCDRG_ID_GRUPO
               AND SCDRD_COD_RESPUESTA_INTERNO = COD_RESPUESTA_CMR
               AND SCDRD_ESTADO =
                   PKG_PROCESOS_PAC_VISANET.SCDRD_ESTADO_ACTIVO
               AND SCDRD_TIPO_RESPUESTA =
                   PKG_PROCESOS_PAC_VISANET.SCDRD_TIPO_RESPUESTA_RECHAZADO
            -- MMA, 10-01-2012 --
            --AND COD_RESPUESTA_CMR   = SCDCP_CD_RESPUESTA_FPRO
            --AND SCDCP_CD_MEDIO_PAGO = MEDIO_PAGO
            --AND SCDCP_ESTADO        = 'A'
            --AND SCDCP_TP_PROCESO    = 'C'
            --AND SCDCP_TP_RESPUESTA  = 'R'
            );
    ----------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTOS ENVIADOS EN CUENTA CORRIENTE --
    SELECT COUNT(*),
           SUM(CCTE_MONTO_PESOS)
      INTO PS_CREGITROS_CCTE_ENV,
           PS_CMONTO_CCTE_ENV
      FROM SCO_CUENTA_CORRIENTE
     WHERE CCTE_ID IN (SELECT NUMERO_DOCUMENTO
                         FROM SCO_PLANO_ENVIO_CARGOS
                        WHERE MEDIO_PAGO = PE_MEDIO_PAGO
                          AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                          AND TP_ARCHIVO = PE_TP_ARCHIVO
                          AND TP_MOVTO IN (1,
                                           2,
                                           3)
                          AND ENV_ARCH_PAC = 'S');
    ----------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTO ACEPTADOS EN ENVIO DE CARGOS --
    SELECT NVL(COUNT(*),
               0),
           NVL(SUM(VALOR_CUOTA / 100),
               0)
      INTO PS_CREGITROS_RECEP_ACEP,
           PS_CMONTO_RECEP_ACEP
      FROM SCO_PLANO_ENVIO_CARGOS,
           SCO_DEF_RESPUESTA,
           SCO_DEF_RESPUESTA_DETALLE
    --SCO_DEF_RESPUESTAS_PROCESOS
     WHERE MEDIO_PAGO = PE_MEDIO_PAGO
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND TP_MOVTO IN (1,
                        2,
                        3)
       AND ENV_ARCH_PAC = 'S'
       AND RESP_ARCH_PAC = 'S'
       AND SCDR_CD_MEDIO_PAGO = MEDIO_PAGO
       AND SCDR_TIPO_ARCHIVO = TP_ARCHIVO
       AND SCDR_TIPO_PROCESO =
           PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_CARGO
       AND SCDRD_SCDRG_ID_GRUPO = SCDR_SCDRG_ID_GRUPO
       AND SCDRD_COD_RESPUESTA_INTERNO = COD_RESPUESTA_CMR
       AND SCDRD_ESTADO = PKG_PROCESOS_PAC_VISANET.SCDRD_ESTADO_ACTIVO
       AND SCDRD_TIPO_RESPUESTA =
           PKG_PROCESOS_PAC_VISANET.SCDRD_TIPO_RESPUESTA_ACEPTADO;
    -- MMA, 10-01-2012 --
    --AND COD_RESPUESTA_CMR   = SCDCP_CD_RESPUESTA_FPRO
    --AND SCDCP_CD_MEDIO_PAGO = MEDIO_PAGO
    --AND SCDCP_ESTADO        = 'A'
    --AND SCDCP_TP_PROCESO    = 'C'
    --AND SCDCP_TP_RESPUESTA  = 'A';
    ----------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTO RECHAZADOS  TABLA ENVIO CARGOS
    SELECT NVL(COUNT(1),
               0),
           NVL(SUM(VALOR_CUOTA / 100),
               0)
      INTO PS_CREGISTROS_RECEP_RECH,
           PS_CMONTO_RECEP_RECH
      FROM SCO_PLANO_ENVIO_CARGOS,
           SCO_DEF_RESPUESTA,
           SCO_DEF_RESPUESTA_DETALLE
    --SCO_DEF_RESPUESTAS_PROCESOS
     WHERE MEDIO_PAGO = PE_MEDIO_PAGO
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND TP_MOVTO IN (1,
                        2,
                        3)
       AND ENV_ARCH_PAC = 'S'
       AND RESP_ARCH_PAC = 'S'
       AND SCDR_CD_MEDIO_PAGO = MEDIO_PAGO
       AND SCDR_TIPO_ARCHIVO = TP_ARCHIVO
       AND SCDR_TIPO_PROCESO =
           PKG_PROCESOS_PAC_VISANET.SCDR_TIPO_PROCESO_CARGO
       AND SCDRD_SCDRG_ID_GRUPO = SCDR_SCDRG_ID_GRUPO
       AND SCDRD_COD_RESPUESTA_INTERNO = COD_RESPUESTA_CMR
       AND SCDRD_ESTADO = PKG_PROCESOS_PAC_VISANET.SCDRD_ESTADO_ACTIVO
       AND SCDRD_TIPO_RESPUESTA =
           PKG_PROCESOS_PAC_VISANET.SCDRD_TIPO_RESPUESTA_RECHAZADO;
    -- MMA, 10-01-2012 --
    --AND COD_RESPUESTA_CMR   = SCDCP_CD_RESPUESTA_FPRO
    --AND SCDCP_CD_MEDIO_PAGO = MEDIO_PAGO
    --AND SCDCP_ESTADO        = 'A'
    --AND SCDCP_TP_PROCESO    = 'C'
    --AND SCDCP_TP_RESPUESTA  = 'R';
    ----------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS Y MONTO ENVIADOS ENVIO DE CARGOS --
    SELECT COUNT(1),
           SUM(VALOR_CUOTA / 100)
      INTO PS_CREGISTROS_RECEP_ENV,
           PS_CMONTO_RECEP_ENV
      FROM SCO_PLANO_ENVIO_CARGOS
     WHERE MEDIO_PAGO = PE_MEDIO_PAGO
       AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
       AND TP_ARCHIVO = PE_TP_ARCHIVO
       AND TP_MOVTO IN (1,
                        2,
                        3)
       AND ENV_ARCH_PAC = 'S';
    ------------------------------------------------------------------------------------------
    RETURN TRUE;
    ------------------------------------------------------------------------------------------
  EXCEPTION
    WHEN OTHERS THEN
      ---------------------------------------
      ROLLBACK;
      ---------------------------------------
      PS_MENSAJE_ERROR := SQLERRM;
      ---------------------------------------
      PS_CREGITROS_CCTE_ACEP := 0;
      PS_CMONTO_CCTE_ACEP    := 0;
      ---------------------------------------
      PS_CREGITROS_CCTE_RECH := 0;
      PS_CMONTO_CCTE_RECH    := 0;
      ---------------------------------------
      PS_CREGITROS_CCTE_ENV := 0;
      PS_CMONTO_CCTE_ENV    := 0;
      ---------------------------------------
      PS_CREGITROS_RECEP_ACEP := 0;
      PS_CMONTO_RECEP_ACEP    := 0;
      ---------------------------------------
      PS_CREGISTROS_RECEP_RECH := 0;
      PS_CMONTO_RECEP_RECH     := 0;
      ---------------------------------------
      PS_CREGISTROS_RECEP_ENV := 0;
      PS_CMONTO_RECEP_ENV     := 0;
      ---------------------------------------
      RETURN FALSE;
      ---------------------------------------
  END CUADRATURA_ARCHIVO_RESP_CARGOS;

  ---------------------------------------------------------------------------------------------------
  ---------------------------------------------------------------------------------------------------
  -- SMB INICIO 18-05-2010 NUEVA FUNCTION PARA PROCESAR LOS CAMBIOS DE CUENTA EXCLUSIVOS DE TRANSBANK
  /*
  FUNCTION CAMBIO_CUENTA (PE_NU_PROPUESTA  IN     NUMBER,
                          PE_NU_RUT        IN     VARCHAR2,
                          PE_NU_CUENTA     IN     VARCHAR2,
                          PE_NU_CUENTA_ANT IN     VARCHAR2,
                          PE_CD_MEDIO_PAGO IN        NUMBER,
                          PE_NU_ENDOSO     IN        NUMBER,
                          PS_MENSAJE_ERROR IN OUT VARCHAR2
                               ) RETURN BOOLEAN
  IS
    -------------------------------------------------------------------------------------------------
    -- V_RESULTADO                 VARCHAR2(500);
    V_CUENTA_TRANSBANK_CLIENTE        VARCHAR2(20);
    V_NOMBRE_CLIENTE            VARCHAR2(200);
    V_DIRECCION_CLIENTE         VARCHAR2(200);
    V_RUT_CLIENTE               NUMBER;
    V_DV_CLIENTE                VARCHAR2(1);
    V_MENSAJE_ERROR             VARCHAR2(200);
    -------------------------------------------------------------------------------------------------
    V_SCCD_STATUS_AUX           SCO_CUADRATURA_DIARIA.SCCD_STATUS%TYPE;
    V_REG_SCO_INSCRIPCION       SCO_INSCRIPCION%ROWTYPE;
    V_REG_SCOH_INSCRIPCION      SCOH_INSCRIPCION%ROWTYPE;
    V_REG_SCOH_BIT_CAMB_MED_PAG   SCOH_BITACORA_CAMB_MEDIO_PAGO%ROWTYPE;
    -------------------------------------------------------------------------------------------------
  
    V_ENVIO                      SCO_CUENTA_CORRIENTE.CCTE_TRASP_ENV_CARGOS%TYPE;
  
    EXC_ERR_ST_CUADRATURA       EXCEPTION;
    EXC_ERR_NO_EXIST_CUADRATURA EXCEPTION;
    EXC_ERR_ST_INSCRIPCION      EXCEPTION;
    EXC_ERR_NO_EXIST_INSC          EXCEPTION;
    EXC_ERR_GRABAR_SCOH_INSC    EXCEPTION;
    EXC_ERR_MODIFICAR_SCO_INSC  EXCEPTION;
    EXC_ERR_GENERAR_INSC          EXCEPTION;
    EXC_ERR_MEDIO_PAGO           EXCEPTION;
    EXC_ERR_SEMAFORO            EXCEPTION;
    EXC_ERR_GRAB_BIT_CAMB_MED_PAG     EXCEPTION;
  
    V_SIN_INSC                  NUMBER := 0;
    V_CUOTAS_N                  NUMBER;
  
    V_STAT NUMBER;
    -------------------------------------------------------------------------------------------------
  BEGIN
  
  -- SMB INICIO CONTROL DE CAMBIO CUENTA SOE
  -- SE VALIDA QUE EL MEDIO DE PAGO DE LA PROPUESTA QUE SE ESTA PROCESANDO SEA TRANSBANK
      IF PE_CD_MEDIO_PAGO != CODIGO_MEDIO_PAGO_VISANET THEN
         RAISE EXC_ERR_MEDIO_PAGO;
      END IF;
       -------------------------------------------------------------------------------------------------
         -- SE OBTIENE EL STATUS DE LA CUADRATURA DIARIA --
         BEGIN
               SELECT SCCD_STATUS
              INTO      V_SCCD_STATUS_AUX
            FROM      SCO_CUADRATURA_DIARIA
             WHERE  SCCD_PROPUESTA = PE_NU_PROPUESTA;
            IF (V_SCCD_STATUS_AUX NOT IN (PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE,
                                          PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_DESEST_CARGOS,
  --SMB INICIO CONTROL DE CAMBIO DE CUENTA SE AGRAGAN NUEVOS ESTATUS    SOE
                                          PKG_PROCESOS_PAC_VISANET.SCCD_ST_CERT_ACEP ,
                                          PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_RECH,
                                          PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_SIN_CTACTE,
                                          PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_PENDIENTE_ENV)) THEN
                    RAISE EXC_ERR_ST_CUADRATURA;
               END IF;
         EXCEPTION
             WHEN NO_DATA_FOUND THEN
              RAISE EXC_ERR_NO_EXIST_CUADRATURA;
         END;
         -------------------------------------------------------------------------------------------------
         -- SE OBTIENE EL REGISTRO DE INSCRIPCION TRANSBANK --
         BEGIN
               SELECT *
            INTO      V_REG_SCO_INSCRIPCION
            FROM      SCO_INSCRIPCION
            WHERE  SCOI_CONTRATO_PROPUESTA = PE_NU_PROPUESTA;
            IF (V_REG_SCO_INSCRIPCION.SCOI_STATUS NOT IN (PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP,
                                                          PKG_PROCESOS_PAC_VISANET.SCOI_ST_DESEST_CARGOS,
  -- SMB INICIO CONTROL DE CAMBIO DE CUENTA SE AGREGAN NUEVOS STATUS     SOE
                                                          PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO,
                                                          PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_RECH)) THEN
                  RAISE EXC_ERR_ST_INSCRIPCION;
               END IF;
       EXCEPTION
                 WHEN NO_DATA_FOUND THEN
  -- SMB CONTROL DE CAMBIO CUENTA SOE SI NO EXISTE LA INSCRIPCION INSERTARLA
                      -------------------------------------------------------------------------------------------------
                      -- SE GENERA EL REGISTRO DE INSCRIPCION TRANSBANK --
                         IF (GENERA_REG_INSCRIPCION_X_PPTA (PE_NU_PROPUESTA,V_MENSAJE_ERROR) = FALSE) THEN
                            RAISE EXC_ERR_GENERAR_INSC;
                         END IF;
  
                     -------------------------------------------------------------------------------------------------
                     -- SE OBTIENE DE NUEVO LA INSCRIPCION --
                        SELECT *
                        INTO V_REG_SCO_INSCRIPCION
                        FROM SCO_INSCRIPCION
                        WHERE SCOI_CONTRATO_PROPUESTA = PE_NU_PROPUESTA;
  
                        V_SIN_INSC := 1;
             END;
  
         -------------------------------------------------------------------------------------------------
         -- SE OBTIENEN LOS DATOS DE LA NUEVA CUENTA TRANSBANK --
         -- DE LA NUEVA TABLA DE PERFILACION DEL CLIENTE --
         -- GIO - 03-06-2009 INICIO - PRO T2 -- NO LLAMAR AL MIRROR
  
         V_CUENTA_TRANSBANK_CLIENTE        := PE_NU_CUENTA;
       --  MIRROR_RECTOR.ACCESO_MIRROR (V_CUENTA_TRANSBANK_CLIENTE,V_RESULTADO);
       BEGIN
               SELECT CACN_NM_APELLIDO_RAZON,
                      CACN_NU_DOCUMENTO,
                      CACN_CD_VERIFICADOR,
                      CACN_DI_COBRO_P
              INTO   V_NOMBRE_CLIENTE,
                     V_RUT_CLIENTE  ,
                     V_DV_CLIENTE    ,
                     V_DIRECCION_CLIENTE
              FROM    CART_CLIENTES
              WHERE  CACN_TP_DOCUMENTO = 'RUT'
              AND    CACN_NU_DOCUMENTO = PE_NU_RUT;
  
      EXCEPTION WHEN OTHERS THEN
                PS_MENSAJE_ERROR := 'Error: Al buscar datos de cliente '||SQLERRM;
                 RETURN FALSE;
      END;
        -- GIO - 03-06-2009 FIN- PRO T2 -- NO LLAMAR AL MIRROR
        V_STAT := 2;
        -------------------------------------------------------------------------------------------------
        -- MODIFICA EL REGISTRO DE INSCRIPCION E INSERTA EL NUEVO REGISTRO DE BAJA DE INSCRIPCION EN LA TABLA SCOH_INSCRIPCION --
        V_REG_SCOH_INSCRIPCION.SCOIH_ID                   := PKG_PROCESOS_PAC.OBTENER_ID_SCOH_INSCRIPCION;
      V_REG_SCOH_INSCRIPCION.SCOIH_SCOI_ID             := V_REG_SCO_INSCRIPCION.SCOI_ID;
        V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_PROCESO       := TO_DATE(SYSDATE,'DD-MM-RRRR');
        V_REG_SCOH_INSCRIPCION.SCOIH_STATUS                := PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO;
        V_REG_SCOH_INSCRIPCION.SCOIH_NUMERO_COMERCIO     := V_REG_SCO_INSCRIPCION.SCOI_NUMERO_COMERCIO;
      V_REG_SCOH_INSCRIPCION.SCOIH_RUT                   := V_REG_SCO_INSCRIPCION.SCOI_RUT;
      V_REG_SCOH_INSCRIPCION.SCOIH_DV                   := V_REG_SCO_INSCRIPCION.SCOI_DV;
      V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE              := SUBSTR(V_REG_SCO_INSCRIPCION.SCOI_NOMBRE,1,36);
       V_STAT := 21;
      -------------------------------------------------------------------------------------------------
      -- SE ENVIA LA CUENTA QUE DEVOLVIO TRANSBANK EN EL PROCESO DE INSCRIPCION --
        V_REG_SCOH_INSCRIPCION.SCOIH_CUENTA_TARJETA      := V_REG_SCO_INSCRIPCION.SCOI_TARJETA;
      V_STAT := 211;
        -------------------------------------------------------------------------------------------------
        V_REG_SCOH_INSCRIPCION.SCOIH_TARJETA              := V_REG_SCO_INSCRIPCION.SCOI_TARJETA;
      V_STAT := 212;
      V_REG_SCOH_INSCRIPCION.SCOIH_PRODUCTO            := V_REG_SCO_INSCRIPCION.SCOI_PRODUCTO;
      V_STAT := 213;
      V_REG_SCOH_INSCRIPCION.SCOIH_MANDATO             := V_REG_SCO_INSCRIPCION.SCOI_MANDATO;
      V_STAT := 214;
      V_REG_SCOH_INSCRIPCION.SCOIH_ORIGEN                := V_REG_SCO_INSCRIPCION.SCOI_ORIGEN;
      V_STAT := 215;
      V_REG_SCOH_INSCRIPCION.SCOIH_DIRECCION             := SUBSTR(V_REG_SCO_INSCRIPCION.SCOI_DIRECCION,1,36);
      V_STAT := 22;
      V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_INICIO        := V_REG_SCO_INSCRIPCION.SCOI_FECHA_INICIO;
      V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_TERMINO       := V_REG_SCO_INSCRIPCION.SCOI_FECHA_TERMINO;
      V_REG_SCOH_INSCRIPCION.SCOIH_MONTO                  := V_REG_SCO_INSCRIPCION.SCOI_MONTO;
      V_REG_SCOH_INSCRIPCION.SCOIH_MONEDA              := V_REG_SCO_INSCRIPCION.SCOI_MONEDA;
        V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_REGISTRO         := PKG_PROCESOS_PAC_VISANET.CODIGO_BAJA_INSC_CMR;
      V_STAT := 23;
      V_REG_SCOH_INSCRIPCION.SCOIH_CONTRATO_PROPUESTA     := V_REG_SCO_INSCRIPCION.SCOI_CONTRATO_PROPUESTA;
      V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAGO              := NULL;
      V_REG_SCOH_INSCRIPCION.SCOIH_RESPUESTA_CMR       := NULL;
      V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_NACIMIENTO    := V_REG_SCO_INSCRIPCION.SCOI_FECHA_NACIMIENTO;
      V_REG_SCOH_INSCRIPCION.SCOIH_SEXO                := V_REG_SCO_INSCRIPCION.SCOI_SEXO;
      V_REG_SCOH_INSCRIPCION.SCOIH_NACIONALIDAD        := V_REG_SCO_INSCRIPCION.SCOI_NACIONALIDAD;
      V_STAT := 24;
      V_REG_SCOH_INSCRIPCION.SCOIH_CALLE_NUMERO        := V_REG_SCO_INSCRIPCION.SCOI_CALLE_NUMERO;
      V_REG_SCOH_INSCRIPCION.SCOIH_DEPARTAMENTO        := V_REG_SCO_INSCRIPCION.SCOI_DEPARTAMENTO;
      V_REG_SCOH_INSCRIPCION.SCOIH_VILLA                  := V_REG_SCO_INSCRIPCION.SCOI_VILLA;
      V_REG_SCOH_INSCRIPCION.SCOIH_CIUDAD               := V_REG_SCO_INSCRIPCION.SCOI_CIUDAD;
      V_REG_SCOH_INSCRIPCION.SCOIH_COMUNA              := V_REG_SCO_INSCRIPCION.SCOI_COMUNA;
      V_STAT := 25;
      V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_DIRECCION      := V_REG_SCO_INSCRIPCION.SCOI_TIPO_DIRECCION;
      V_REG_SCOH_INSCRIPCION.SCOIH_ZONA                  := V_REG_SCO_INSCRIPCION.SCOI_ZONA;
      V_REG_SCOH_INSCRIPCION.SCOIH_CENTRAL              := V_REG_SCO_INSCRIPCION.SCOI_CENTRAL;
      V_REG_SCOH_INSCRIPCION.SCOIH_FONO                  := V_REG_SCO_INSCRIPCION.SCOI_FONO;
      V_REG_SCOH_INSCRIPCION.SCOIH_CICLO                  := V_REG_SCO_INSCRIPCION.SCOI_CICLO;
      V_STAT := 26;
      V_REG_SCOH_INSCRIPCION.SCOIH_CODIGO_ENTIDAD        := V_REG_SCO_INSCRIPCION.SCOI_CODIGO_ENTIDAD;
      V_REG_SCOH_INSCRIPCION.SCOIH_CODIGO_PRODUCTO        := V_REG_SCO_INSCRIPCION.SCOI_CODIGO_PRODUCTO;
      V_REG_SCOH_INSCRIPCION.SCOIH_FILLER              := V_REG_SCO_INSCRIPCION.SCOI_FILLER;
      V_REG_SCOH_INSCRIPCION.SCOIH_STRING_ENVIADO        := V_REG_SCO_INSCRIPCION.SCOI_STRING_ENVIADO;
      V_REG_SCOH_INSCRIPCION.SCOIH_STRING_DEVUELTO        := V_REG_SCO_INSCRIPCION.SCOI_STRING_DEVUELTO;
      V_STAT := 27;
      V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAC              := NULL;
      V_REG_SCOH_INSCRIPCION.SCOIH_DIA_FAC              := NULL;
      V_REG_SCOH_INSCRIPCION.SCOIH_MEDIO_PAGO            := V_REG_SCO_INSCRIPCION.SCOI_MEDIO_PAGO;
      V_REG_SCOH_INSCRIPCION.SCOIH_SCIN_ID_INSCRIPCION := V_REG_SCO_INSCRIPCION.SCOI_SCIN_ID_INSCRIPCION;
      V_REG_SCOH_INSCRIPCION.SCOIH_USUARIO              := PKG_PROCESOS_PAC.OBTENER_USUARIO;
      V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE_ARCHIVO        := NULL;
      V_STAT := 28;
      V_REG_SCOH_INSCRIPCION.SCOIH_CAME_TP_TRANSAC        := PKG_PROCESOS_PAC_VISANET.CAME_TP_TRANSAC_END_CUALIT;
      V_REG_SCOH_INSCRIPCION.SCOIH_CAME_CD_MOTIVO        := PKG_PROCESOS_PAC_VISANET.CAME_CD_MOTIVO_CAMB_CUENTA_CMR;
      V_REG_SCOH_INSCRIPCION.SCOIH_TP_INSCRIPCION        := V_REG_SCO_INSCRIPCION.SCOI_TP_INSCRIPCION;
      V_STAT := 29;
  
  -- SMB INICIO CONTROL DE CAMBIO CUENTA SOE
  -- PARA TRANSBANK EL STATUS DE LA BAJA SIEMPRE ES 13 PORQUE NUNCA SE INFORMO LA CUENTA
      V_REG_SCOH_INSCRIPCION.SCOIH_STATUS := PKG_PROCESOS_PAC_VISANET.SCOI_ST_SIN_INSCRIPCION;
  
      -------------------------------------------------------------------------------------------------
      -- GRABA REGISTRO DE BAJA EN LA TABLA SCOH_INSCRIPCION --
      IF (PKG_PROCESOS_PAC.GRABAR_SCOH_INSCRIPCION (V_REG_SCOH_INSCRIPCION,V_MENSAJE_ERROR) = FALSE) THEN
                    RAISE EXC_ERR_GRABAR_SCOH_INSC;
        END IF;
      V_STAT := 3;
      -------------------------------------------------------------------------------------------------
      -- INSERTA EL NUEVO REGISTRO DE ALTA DE INSCRIPCION EN LA TABLA SCOH_INSCRIPCION --
        V_REG_SCOH_INSCRIPCION.SCOIH_ID                  := PKG_PROCESOS_PAC.OBTENER_ID_SCOH_INSCRIPCION;
      V_REG_SCOH_INSCRIPCION.SCOIH_RUT                  := V_RUT_CLIENTE;
        V_REG_SCOH_INSCRIPCION.SCOIH_DV                  := V_DV_CLIENTE;
        V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE              := SUBSTR(V_NOMBRE_CLIENTE,1,36);
        V_REG_SCOH_INSCRIPCION.SCOIH_CUENTA_TARJETA         := V_CUENTA_TRANSBANK_CLIENTE;
      V_STAT := 31;
        V_REG_SCOH_INSCRIPCION.SCOIH_TARJETA              := NULL;
        V_REG_SCOH_INSCRIPCION.SCOIH_MANDATO              := NULL;
        V_REG_SCOH_INSCRIPCION.SCOIH_DIRECCION              := SUBSTR(V_DIRECCION_CLIENTE,1,36);
        V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_REGISTRO          := PKG_PROCESOS_PAC_VISANET.CODIGO_ALTA_INSC_CMR;
        V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAGO              := NULL;
        V_REG_SCOH_INSCRIPCION.SCOIH_RESPUESTA_CMR          := NULL;
      V_STAT := 32;
        V_REG_SCOH_INSCRIPCION.SCOIH_FONO                  := V_REG_SCO_INSCRIPCION.SCOI_ID;
        V_REG_SCOH_INSCRIPCION.SCOIH_STRING_DEVUELTO        := NULL;
        V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAC              := NULL;
        V_REG_SCOH_INSCRIPCION.SCOIH_DIA_FAC              := NULL;
      V_STAT := 33;
  -- SMB INICIO CONTROL DE CAMBIO CUENTA SOE
  --PARA LOS REGISTROS DE ALTA EL STATUS ES CERO
        V_REG_SCOH_INSCRIPCION.SCOIH_STATUS                := PKG_PROCESOS_PAC_VISANET.SCOI_ST_PENDIENTE_ENVIO;
  
  -- SMB CONTROL DE CAMBIO CUENTA SOE
  --SI LA INSCRIPCION ESTA ACEPTADA EL MOVIMIENTO ES DE MODIFICACION
      IF V_REG_SCO_INSCRIPCION.SCOI_STATUS = PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP THEN
         V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_REGISTRO  := PKG_PROCESOS_PAC_VISANET.CODIGO_MODIFICA_INSC_CMR;
      END IF;
  
      -------------------------------------------------------------------------------------------------
      -- GRABA REGISTRO DE ALTA/MODIFICACION EN LA TABLA SCOH_INSCRIPCION --
        IF (PKG_PROCESOS_PAC.GRABAR_SCOH_INSCRIPCION (V_REG_SCOH_INSCRIPCION,V_MENSAJE_ERROR) = FALSE) THEN
               RAISE EXC_ERR_GRABAR_SCOH_INSC;
        END IF;
      V_STAT := 34;
      -------------------------------------------------------------------------------------------------
      -- ACTUALIZA EL REGISTRO DE INSCRIPCION A STATUS 4 --
        V_REG_SCO_INSCRIPCION.SCOI_STATUS                  := PKG_PROCESOS_PAC_VISANET.SCOI_ST_CAMBIO_CUENTA_CMR;
        V_REG_SCO_INSCRIPCION.SCOI_FECHA_PROCESO          := TO_DATE(SYSDATE,'DD-MM-RRRR');
      -- SE MODIFICA REGISTRO EN LA TABLA SCO_INSCRIPCION --
        IF (PKG_PROCESOS_PAC.MODIFICAR_SCO_INSCRIPCION(V_REG_SCO_INSCRIPCION,V_MENSAJE_ERROR) = FALSE) THEN
               RAISE EXC_ERR_MODIFICAR_SCO_INSC;
        END IF;
      V_STAT := 35;
      -------------------------------------------------------------------------------------------------
        UPDATE SCO_CUADRATURA_DIARIA
      SET    SCCD_STATUS        = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_CMR
      WHERE  SCCD_OPERACION     IN (1001,9999)
        AND    SCCD_SUB_OPERACION = 0
      AND    SCCD_PROPUESTA     = PE_NU_PROPUESTA;
      V_STAT := 36;
  
      -- INICIO SMB 27-10-2010 SE COLOCA ANTES DE ACTUALIZAR
      SELECT COUNT(1)
      INTO V_CUOTAS_N
      FROM SCO_CUENTA_CORRIENTE
      WHERE  CCTE_PROPUESTA           = PE_NU_PROPUESTA
      AND    CCTE_TP_MOVTO             = 1
      AND    CCTE_MOVTO                = 'D'
      AND    CCTE_STATUS               = 0  --PENDIENTE
      AND    CCTE_TRASP_ENV_CARGOS     = 'N'--NO TRASPASADA A ENVIO CARGOS
      AND    CCTE_FINICIAL < TRUNC(SYSDATE);
      -- FIN SMB 27-10-2010
  
    -------------------------------------------------------------------------------------------------
    -- SE ACTUALIZA EL ESTADO Y MEDIO DE PAGO DE LOS RECIBOS EN LA TABLA SCO_CUENTA_CORRIENTE --
  -- SMB - INICO CONTROL DE CAMBIO CUENTA SOE ANULAR RECIBOS PENDIENTE Y REZAGADOS Y ELIMINAR ENVIO
      FOR R IN (SELECT *
                FROM SCO_CUENTA_CORRIENTE
                WHERE CCTE_PROPUESTA   = PE_NU_PROPUESTA
                AND   CCTE_TP_MOVTO    = 1
                AND   CCTE_MOVTO       = 'D'
                AND   CCTE_STATUS      IN (0,4)) LOOP
  
       V_STAT := 21;
  
  -- VALIDAR EL SEMAFORO PARA CADA RECIBO
        -- MMA, 17-04-2012 --
        --IF PKG_PROCESOS_PAC.ESTADO_SEMAFORO(PE_CD_MEDIO_PAGO,
        IF PKG_SEMAFORO_PAC.ESTADO_SEMAFORO(PE_CD_MEDIO_PAGO,
                                            R.CCTE_FECHA_ENVIO_PAC) THEN
           RAISE EXC_ERR_SEMAFORO;
        END IF;
  
       IF R.CCTE_TRASP_ENV_CARGOS = 'S' THEN
          DELETE SCO_PLANO_ENVIO_CARGOS
          WHERE  FECHA_CARGA_ARCHIVO = R.CCTE_FECHA_ENVIO_PAC
          AND NUMERO_DOCUMENTO = R.CCTE_ID
          AND MEDIO_PAGO = R.CCTE_MEDIO_PAGO;
  
          V_ENVIO := 'N';
       ELSE
          V_ENVIO := R.CCTE_TRASP_ENV_CARGOS;
       END IF;
  
       V_STAT := 22;
       UPDATE SCO_CUENTA_CORRIENTE
       SET CCTE_STATUS           = PKG_PROCESOS_PAC.CCTE_ST_SIN_MEDIO_PAGO,
           CCTE_MEDIO_PAGO       = PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_SIN_MED_PAG,
           CCTE_TRASP_ENV_CARGOS = V_ENVIO
       WHERE CCTE_ID = R.CCTE_ID;
     END LOOP;
  
   -- INICIO SMB 27-10-2010 SE COLOCA ANTES DE ACTUALIZAR
  
   --SELECT COUNT(1)
   --  INTO V_CUOTAS_N
   --  FROM SCO_CUENTA_CORRIENTE
   --  WHERE  CCTE_PROPUESTA           = PE_NU_PROPUESTA
   --  AND    CCTE_TP_MOVTO             = 1
   --  AND    CCTE_MOVTO                = 'D'
   --  AND    CCTE_STATUS               = 0  --PENDIENTE
   --  AND    CCTE_TRASP_ENV_CARGOS     = 'N'--NO TRASPASADA A ENVIO CARGOS
   --  AND    CCTE_FINICIAL < TRUNC(SYSDATE);
   -- FIN SMB 27-10-2010
  
  
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL ENDOSO DE CAMBIO DE CUENTA PARA INSERTAR EN TABLA SCOH_BITACORA_CAMB_MEDIO_PAGO --
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_ID                 := PKG_PROCESOS_PAC.OBTENER_ID_BIT_CAMB_MEDIO_PAGO;
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_SCCMP_ID           := PKG_PROCESOS_PAC.SCCMP_ID_MANDATO_BANC_CMR;
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_NU_PROPUESTA       := PE_NU_PROPUESTA;
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_NU_ENDOSO          := PE_NU_ENDOSO;
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_FECHA_ENDOSO       := SYSDATE;
  --  V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_CAFR_CD_FRAGMENT   := PE_CAFR_CD_FRAGMENT;
  --  V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_NU_CUOTAS_PACTADAS := PE_NU_CUOTAS_PACTADAS;
    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_ESTADO             := 'VIGENTE';
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_FECHA_ESTADO       := SYSDATE;
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_USER      := PKG_PROCESOS_PAC.OBTENER_USUARIO;
  --    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_CAPJ_CD_SUCURSAL := PE_CD_PUNTO_VENTA;
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_CD_MEDIO_ORI := PE_CD_MEDIO_PAGO;
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_CA_CUOTAS_N_ORI := V_CUOTAS_N;
  --    V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_CA_CUOTAS_S_ORI := 0;
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_TP_REGISTRO   := PKG_PROCESOS_PAC.SCHCMP_TP_REGISTRO_CMR; --PARA IDENTIFICAR EL REGISTRO COMO DE CAMBIO DE CUENTA
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_NU_CUENTA_ANT   := PE_NU_CUENTA_ANT;
      V_REG_SCOH_BIT_CAMB_MED_PAG.SCHCMP_NU_CUENTA_NUEVO   := PE_NU_CUENTA;
  
       V_STAT := 28;
    -------------------------------------------------------------------------------------------------
    V_MENSAJE_ERROR := NULL;
    IF (PKG_PROCESOS_PAC.GRABAR_SCOH_BIT_CAMB_MED_PAGO(V_REG_SCOH_BIT_CAMB_MED_PAG,V_MENSAJE_ERROR) = FALSE) THEN
       RAISE EXC_ERR_GRAB_BIT_CAMB_MED_PAG;
       END IF;
    -------------------------------------------------------------------------------------------------
   -- SMB -  FIN  23-05-2010 - CONTROL DE CAMBIO DE CUENTA SOE
      -------------------------------------------------------------------------------------------------
  
      -------------------------------------------------------------------------------------------------
        PS_MENSAJE_ERROR := NULL;
        -------------------------------------------------------------------------------------------------
        RETURN TRUE;
        -------------------------------------------------------------------------------------------------
  EXCEPTION
     WHEN EXC_ERR_ST_CUADRATURA THEN
          PS_MENSAJE_ERROR := 'El estado de la Cuadratura Diaria no es v�lido para el Endoso de Cambio de Cuenta Transbank';
           RETURN FALSE;
     WHEN EXC_ERR_NO_EXIST_CUADRATURA THEN
          PS_MENSAJE_ERROR := 'No existe Cuadratura Diaria asociada a la propuesta '||PE_NU_PROPUESTA;
           RETURN FALSE;
     WHEN EXC_ERR_ST_INSCRIPCION THEN
          PS_MENSAJE_ERROR := 'El estado de la Inscripci�n Transbank no es v�lido para el Endoso de Cambio de Cuenta Transbank';
           RETURN FALSE;
     WHEN EXC_ERR_NO_EXIST_INSC THEN
          PS_MENSAJE_ERROR := 'No existe Inscripci�n Transbank asociada a la propuesta '||PE_NU_PROPUESTA;
           RETURN FALSE;
     WHEN EXC_ERR_GRABAR_SCOH_INSC THEN
            PS_MENSAJE_ERROR := 'Error al grabar el registro de Endoso de Cambio de Cuenta Transbank en tabla de inscripci�n hist�rica, '||V_MENSAJE_ERROR;
           RETURN FALSE;
     WHEN EXC_ERR_MODIFICAR_SCO_INSC THEN
            PS_MENSAJE_ERROR := 'Error al modificar el registro de inscripci�n asociado a la propuesta '||PE_NU_PROPUESTA;
           RETURN FALSE;
     WHEN EXC_ERR_GENERAR_INSC THEN
            PS_MENSAJE_ERROR := 'Error al generar Inscripci�n Transbank, '||V_MENSAJE_ERROR;
           RETURN FALSE;
     WHEN EXC_ERR_MEDIO_PAGO THEN
            PS_MENSAJE_ERROR := 'Error el medio de pago de la propuesta no es Transbank ';
           RETURN FALSE;
  -- SMB EXCEPTION PARA CUANDO EL SEMAFORO SEA VERDADERO
     WHEN EXC_ERR_SEMAFORO THEN
        PS_MENSAJE_ERROR := 'Endoso de cambio de cuenta no puede ser cursado moment�neamente, por favor generar endoso manual';
        RETURN FALSE;
     WHEN EXC_ERR_GRAB_BIT_CAMB_MED_PAG THEN
          PS_MENSAJE_ERROR := 'Error al grabar en bitacora Cambio Medio de Pago, '||V_MENSAJE_ERROR;
           RETURN FALSE;
     WHEN OTHERS THEN
            PS_MENSAJE_ERROR := SQLERRM||' en '||V_STAT;
           RETURN FALSE;
  END CAMBIO_CUENTA;
  --FIN SMB CONTROL DE CAMBIOS CAMBIO DE CUENTA SOE
  */
  ---------------------------------------------------------------------------------------------------------------------
  -- ELIMINA DUPLICADOS DE ENVIO CARGOS
  /*FUNCTION ELIM_DUPLICADOS_ENVIO_CARGOS (
                                         PE_FECHA_PROCESO_PAC     IN DATE,
                                         PS_CANT_CARG_NORM_PEND   OUT NUMBER,
                                         PS_CANT_CARG_NORM_REZAG  OUT NUMBER,
                                         PS_CANT_NORMALIZACIONES  OUT NUMBER,
                                         PS_CANT_ERRONEOS         OUT NUMBER,
                                         PS_MENSAJE_ERROR         OUT VARCHAR2
                                         )
  RETURN BOOLEAN
  IS
     -----------------------------------------------------------------------------
     --V_MIN_CCTE_FECHA_ENVIO_PAC  SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
     V_ARCH_ELIMINADOS           UTL_FILE.FILE_TYPE;
     V_ARCH_ERRORES              UTL_FILE.FILE_TYPE;
     V_NOM_DIR                   VARCHAR2( 1000 ) := NULL;
     V_LINEA                     VARCHAR2( 1000 ) := NULL;
     V_CANTIDAD                  NUMBER;
     V_SCHPP_ID_PROCESO_PAC      SCOH_BITACORA_PROCESOS_PAC.SCHPP_ID_PROCESO_PAC%TYPE;
     V_SCHPP_ESTADO              SCOH_BITACORA_PROCESOS_PAC.SCHPP_ESTADO%TYPE;
     V_SCHPPA_ID_PROCESO_PAC     SCOH_BITACORA_PROC_PAC_ARCH.SCHPPA_ID_PROCESO_PAC%TYPE;
     V_CCTE_ID                   SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
     V_CCTE_PROPUESTA            SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
     V_MENSAJE_ERROR             VARCHAR2(200);
     EXC_ERR_PROC_ELIMINACION    EXCEPTION;
     EXC_ERR_PROCESO             EXCEPTION;
     -----------------------------------------------------------------------------
     CURSOR PROPUESTAS
         IS
     SELECT NUMERO_CONTRATO PROPUESTA
       FROM SCO_PLANO_ENVIO_CARGOS
      WHERE FECHA_CARGA  = PE_FECHA_PROCESO_PAC
        AND MEDIO_PAGO   = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
        AND TP_ARCHIVO   = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
        AND TP_MOVTO    IN (CCTE_TP_MOVTO_CARGO_NOR,
                            CCTE_TP_MOVTO_CARGO_DIR)
      GROUP BY NUMERO_CONTRATO
     HAVING COUNT(*) > 1;
     -----------------------------------------------------------------------------
     CURSOR ELIMINA
         IS
     SELECT CCTE_ID,
            CCTE_FECHA_ENVIO_PAC,
            CCTE_PLAN,
            CCTE_TP_MOVTO,
            CCTE_STATUS,
            CCTE_CUOTA,
            CCTE_RECIBO
       FROM SCO_CUENTA_CORRIENTE
      WHERE CCTE_FECHA_ENVIO_PAC IN (SELECT MIN(CCTE_FECHA_ENVIO_PAC)
                                         FROM SCO_PLANO_ENVIO_CARGOS,
                                            SCO_CUENTA_CORRIENTE
                                      WHERE MEDIO_PAGO          = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                                        AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC--TO_DATE('27-06-2011','DD-MM-YYYY')
                                        AND TP_ARCHIVO          = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
                                        AND NUMERO_DOCUMENTO    = CCTE_ID
                                        AND CCTE_PROPUESTA      = V_CCTE_PROPUESTA)
        AND CCTE_PROPUESTA = V_CCTE_PROPUESTA
        AND CCTE_ID       != V_CCTE_ID;
     -----------------------------------------------------------------------------
  BEGIN
     -------------------------------------------------------
     PS_CANT_CARG_NORM_PEND   := 0;
     PS_CANT_CARG_NORM_REZAG  := 0;
     PS_CANT_NORMALIZACIONES  := 0;
     --------------------------------------------------------------------------------------
     -- SE VALIDA EXISTENCIA DE PROCESO PAC --
     BEGIN
          SELECT SCHPP_ID_PROCESO_PAC,
                  SCHPP_ESTADO
            INTO V_SCHPPA_ID_PROCESO_PAC,
                  V_SCHPP_ESTADO
            FROM SCOH_BITACORA_PROCESOS_PAC
           WHERE SCHPP_CD_MEDIO_PAGO     = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
             AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
  
          IF (V_SCHPP_ESTADO != 'INICIADO') THEN
             PS_MENSAJE_ERROR := 'EL PROCESO PAC NO SE ENCUENTRA INICIADO';
             RAISE EXC_ERR_PROCESO;
          END IF;
     EXCEPTION
              WHEN NO_DATA_FOUND THEN
                   PS_MENSAJE_ERROR := 'NO EXISTE PROCESO PAC PARA LA FECHA ESPECIFICADA';
                   RAISE EXC_ERR_PROCESO;
     END;
     -----------------------------------------------------------------------------------------
     -- SE VALIDA ENV�O DE ARCHVIVOS A COBRANZA --
     SELECT COUNT(1)
       INTO V_CANTIDAD
       FROM SCOH_BITACORA_PROC_PAC_ARCH
      WHERE SCHPPA_ID_PROCESO_PAC = V_SCHPPA_ID_PROCESO_PAC
        AND SCHPPA_ESTADO = 'PENDIENTE';
     -----------------------------------------------------------------------------------------
     IF V_CANTIDAD > 0 THEN
        PS_MENSAJE_ERROR := 'EXISTE UN ARCHIVO PAC PENDIENTE PARA EL PROCESO ESPECIFICADO';
        RAISE EXC_ERR_PROCESO;
     END IF;
     -----------------------------------------------------------------------------------------
     -- SE OBTIENE LA RUTA DEL ARCHIVO DE DUPLICADOS ELIMINADOS --
     SELECT CACH_DIR_INTERFACE
       INTO V_NOM_DIR
       FROM CART_CONFIG_BATCH;
     -------------------------------------------------------
     -- ABRO ARCHIVO DE CARGOS ELIMINADOS..
     BEGIN
        V_ARCH_ELIMINADOS := UTL_FILE.FOPEN( V_NOM_DIR,
                                             'ELIM_'||TO_CHAR(PE_FECHA_PROCESO_PAC,'DDMMRRRR')||'.TXT',
                                             'W');
     EXCEPTION WHEN OTHERS THEN
        PS_MENSAJE_ERROR := 'Error al crear archivo de Duplicados Eliminados, '||sqlerrm;
        RAISE EXC_ERR_PROCESO;
     END;
     -------------------------------------------------------
     -- ABRO ARCHIVO DE CARGOS ELIMINADOS..
     BEGIN
        V_ARCH_ERRORES := UTL_FILE.FOPEN( V_NOM_DIR,
                                          'ELIM_'||TO_CHAR(PE_FECHA_PROCESO_PAC,'DDMMRRRR')||'_ERROR.TXT',
                                          'W');
     EXCEPTION WHEN OTHERS THEN
        PS_MENSAJE_ERROR := 'Error al crear archivo de Errores de Duplicados Eliminados, '||sqlerrm;
        RAISE EXC_ERR_PROCESO;
     END;
     -------------------------------------------------------
     -- TITULOS!
     UTL_FILE.PUT_LINE (V_ARCH_ELIMINADOS, 'Propuesta;Id Recibo;Fecha Pac;Producto;Tipo Movimiento;Cuota');
     -----------------------------------------------------------------------------
     FOR P IN PROPUESTAS
     LOOP
        -----------------------------------------------------------
        BEGIN
          -----------------------------------------------------------
          V_CCTE_PROPUESTA := P.PROPUESTA;
          -----------------------------------------------------------
          BEGIN
             SELECT CCTE_ID
               INTO V_CCTE_ID
               FROM SCO_CUENTA_CORRIENTE
              WHERE CCTE_FECHA_ENVIO_PAC IN (SELECT MIN(CCTE_FECHA_ENVIO_PAC)
                                               FROM SCO_PLANO_ENVIO_CARGOS,
                                                    SCO_CUENTA_CORRIENTE
                                              WHERE MEDIO_PAGO          = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                                                AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC--TO_DATE('27-06-2011','DD-MM-YYYY')
                                                AND TP_ARCHIVO          = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
                                                AND NUMERO_DOCUMENTO    = CCTE_ID
                                                AND CCTE_PROPUESTA      = V_CCTE_PROPUESTA)
                AND ROWNUM = 1;
          EXCEPTION WHEN OTHERS THEN
            V_MENSAJE_ERROR := 'Error al buscar primer id: '||SQLERRM;
            RAISE EXC_ERR_PROC_ELIMINACION;
          END;
          -----------------------------------------------------------
          -- ELIMINA DUPLICADOS --
          FOR E IN ELIMINA LOOP
             --DBMS_OUTPUT.PUT_LINE (V_CCTE_PROPUESTA||'|'||E.CCTE_ID);
             BEGIN
                   -- SE MODIFCIA REZAGA MOVIMIENTO EN CUENTA CORRIENTE --
                UPDATE SCO_CUENTA_CORRIENTE
                   SET CCTE_STATUS           = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                       CCTE_TRASP_ENV_CARGOS = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO
                 WHERE CCTE_ID = E.CCTE_ID; --NUMERO_DOCUMENTO;
                ------------------------------------------------------------------
                -- SCV 01-08-2011 --
                -- SE COMENTA HASTA QUE SE INTEGRE COBRANZA PAC CON ENDOSOS RECTOR
                -- DESCOMENTAR EN LA VUELTA DE ENDOSOS --
                ------------------------------------------------------------------
  
                -----------------------------------------------------------------------------
                -- SE BUSCA RECIBO RECTOR ASOCIADO A LA CUOTA
                -- SE MODFICA ESTADO DE RECIBO EN RECTOR --
                --IF PKG_PROCESOS_PAC.FN_ACTUALIZA_ESTADO_RECIBO(E.CCTE_RECIBO,
                --                                               PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                  --                                             PS_MENSAJE_ERROR
              --                                                   ) = FALSE THEN
            --         RAISE EXC_ERR_PROC_ELIMINACION;
      --          END IF;
  
                -----------------------------------------------------------------------------
                -- SE ELIMINA REGISTRO DE LA CUENTA PLANO ENVIO CARGOS
                DELETE SCO_PLANO_ENVIO_CARGOS
                    WHERE MEDIO_PAGO          = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                   AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                   AND TP_ARCHIVO          = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
                   AND NUMERO_DOCUMENTO    = E.CCTE_ID; --NUMERO_DOCUMENTO
                -----------------------------------------------------------------------------
                V_LINEA := V_CCTE_PROPUESTA||';'||
                           E.CCTE_ID||';'||
                           TO_CHAR(E.CCTE_FECHA_ENVIO_PAC, 'DDMMRRRR')||';'||
                           E.CCTE_PLAN||';'||
                           E.CCTE_TP_MOVTO||';'||
                           E.CCTE_CUOTA;
                -----------------------------------------------------------------------------
                UTL_FILE.PUT_LINE (V_ARCH_ELIMINADOS, V_LINEA);
                -----------------------------------------------------------------------------
                -- CUENTA SEGUN TIPO DE MOVIMIENTO..
                IF E.CCTE_TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
                     AND E.CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE THEN
                     PS_CANT_CARG_NORM_PEND   := PS_CANT_CARG_NORM_PEND + 1;
                ELSIF E.CCTE_TP_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
                   AND E.CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO THEN
                   PS_CANT_CARG_NORM_REZAG  := PS_CANT_CARG_NORM_REZAG + 1;
                ELSE
                   PS_CANT_NORMALIZACIONES  := PS_CANT_NORMALIZACIONES + 1;
                END IF;
                -----------------------------------------------------------------------------
             EXCEPTION WHEN OTHERS THEN
                -----------------------------------------------------------
                V_LINEA := V_CCTE_PROPUESTA||';'||
                           'Error al eliminar duplicado Id: '||E.CCTE_ID||', '||sqlerrm;
                -----------------------------------------------------------
                UTL_FILE.PUT_LINE (V_ARCH_ERRORES, V_LINEA);
                -----------------------------------------------------------
             END;
             -----------------------------------------------------------
          END LOOP;
          -----------------------------------------------------------
        EXCEPTION WHEN EXC_ERR_PROC_ELIMINACION THEN
          -----------------------------------------------------------
          V_LINEA := V_CCTE_PROPUESTA||';'||
                     'Error al eliminar duplicado, '||sqlerrm;
          -----------------------------------------------------------
          UTL_FILE.PUT_LINE (V_ARCH_ERRORES, V_LINEA);
          -----------------------------------------------------------
        END;
        -----------------------------------------------------------
      END LOOP;
      -------------------------------------------------------
      IF PS_CANT_CARG_NORM_PEND > 0 OR PS_CANT_CARG_NORM_REZAG > 0 OR PS_CANT_NORMALIZACIONES > 0 THEN
         UPDATE SCOH_BITACORA_PROCESOS_PAC
            SET SCHPP_CARGOS_TRASP_ENV_CARGOS  = SCHPP_CARGOS_TRASP_ENV_CARGOS  - PS_CANT_CARG_NORM_PEND,
                SCHPP_REZAGOS_TRASP_ENV_CARGOS = SCHPP_REZAGOS_TRASP_ENV_CARGOS - PS_CANT_CARG_NORM_REZAG,
                SCHPP_ENDOSOS_TRASP_ENV_CARGOS = SCHPP_ENDOSOS_TRASP_ENV_CARGOS - PS_CANT_NORMALIZACIONES
          WHERE SCHPP_ID_PROCESO_PAC           = SCHPP_ID_PROCESO_PAC;
      END IF;
      -------------------------------------------------------
      PS_MENSAJE_ERROR := NULL;
      -------------------------------------------------------
      -- CIERRO ARCHIVOS..
      UTL_FILE.FCLOSE (V_ARCH_ELIMINADOS);
      UTL_FILE.FCLOSE (V_ARCH_ERRORES);
      -----------------------------------------------------------
      RETURN TRUE;
      -----------------------------------------------------------
  EXCEPTION
       WHEN EXC_ERR_PROCESO THEN
            RETURN FALSE;
       WHEN OTHERS THEN
            PS_MENSAJE_ERROR := SQLERRM;
            RETURN FALSE;
  END ELIM_DUPLICADOS_ENVIO_CARGOS;
  */
  FUNCTION ELIM_DUPLICADOS_ENVIO_CARGOS(PE_FECHA_PROCESO_PAC    IN DATE,
                                        PS_CANT_CARG_NORM_PEND  OUT NUMBER,
                                        PS_CANT_CARG_NORM_REZAG OUT NUMBER,
                                        PS_CANT_NORMALIZACIONES OUT NUMBER,
                                        PS_CANT_ERRONEOS        OUT NUMBER,
                                        PS_MENSAJE_ERROR        OUT VARCHAR2)
    RETURN BOOLEAN IS
    -----------------------------------------------------------------------------
    --V_MIN_CCTE_FECHA_ENVIO_PAC  SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE;
    V_ARCH_ELIMINADOS UTL_FILE.FILE_TYPE;
    V_ARCH_ERRORES    UTL_FILE.FILE_TYPE;
    V_NOM_DIR         VARCHAR2(1000) := NULL;
    V_LINEA           VARCHAR2(1000) := NULL;
    V_CANTIDAD        NUMBER;
    --V_SCHPP_ID_PROCESO_PAC  SCOH_BITACORA_PROCESOS_PAC.SCHPP_ID_PROCESO_PAC%TYPE;
    V_SCHPP_ESTADO          SCOH_BITACORA_PROCESOS_PAC.SCHPP_ESTADO%TYPE;
    V_SCHPPA_ID_PROCESO_PAC SCOH_BITACORA_PROC_PAC_ARCH.SCHPPA_ID_PROCESO_PAC%TYPE;
    V_CCTE_ID               SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_PROPUESTA        SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_MENSAJE_ERROR         VARCHAR2(200);
    EXC_ERR_PROC_ELIMINACION EXCEPTION;
    EXC_ERR_PROCESO          EXCEPTION;
    -----------------------------------------------------------------------------
    CURSOR PROPUESTAS IS
      SELECT NUMERO_CONTRATO PROPUESTA
        FROM SCO_PLANO_ENVIO_CARGOS
       WHERE FECHA_CARGA = PE_FECHA_PROCESO_PAC
         AND MEDIO_PAGO = PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND TP_ARCHIVO = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
         AND TP_MOVTO IN
             (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR,
              PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR)
       GROUP BY NUMERO_CONTRATO
      HAVING COUNT(*) > 1;
    -----------------------------------------------------------------------------
    CURSOR ELIMINA IS
      SELECT CCTE_ID,
             CCTE_FECHA_ENVIO_PAC,
             CCTE_PLAN,
             CCTE_TP_MOVTO,
             CCTE_STATUS,
             CCTE_CUOTA,
             CCTE_RECIBO,
             (SELECT COUNT(1)
                FROM SCO_PLANO_ENVIO_CARGOS
               WHERE MEDIO_PAGO =
                     PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                 AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                 AND TP_ARCHIVO = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
                 AND NUMERO_DOCUMENTO = CCTE_ID) EN_ENVIO_CARGOS
        FROM SCO_CUENTA_CORRIENTE
       WHERE CCTE_PROPUESTA = V_CCTE_PROPUESTA
         AND CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC
         AND CCTE_TRASP_ENV_CARGOS =
             PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_SI
         AND CCTE_ID != V_CCTE_ID;
    -----------------------------------------------------------------------------
  BEGIN
    -------------------------------------------------------
    PS_CANT_CARG_NORM_PEND  := 0;
    PS_CANT_CARG_NORM_REZAG := 0;
    PS_CANT_NORMALIZACIONES := 0;
    --------------------------------------------------------------------------------------
    -- SE VALIDA EXISTENCIA DE PROCESO PAC --
    BEGIN
      SELECT SCHPP_ID_PROCESO_PAC,
             SCHPP_ESTADO
        INTO V_SCHPPA_ID_PROCESO_PAC,
             V_SCHPP_ESTADO
        FROM SCOH_BITACORA_PROCESOS_PAC
       WHERE SCHPP_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
      IF (V_SCHPP_ESTADO != 'INICIADO')
      THEN
        PS_MENSAJE_ERROR := 'EL PROCESO PAC NO SE ENCUENTRA INICIADO';
        RAISE EXC_ERR_PROCESO;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        PS_MENSAJE_ERROR := 'NO EXISTE PROCESO PAC PARA LA FECHA ESPECIFICADA';
        RAISE EXC_ERR_PROCESO;
    END;
    -----------------------------------------------------------------------------------------
    -- SE VALIDA ENV�O DE ARCHVIVOS A COBRANZA --
    SELECT COUNT(1)
      INTO V_CANTIDAD
      FROM SCOH_BITACORA_PROC_PAC_ARCH
     WHERE SCHPPA_ID_PROCESO_PAC = V_SCHPPA_ID_PROCESO_PAC
       AND SCHPPA_ESTADO = 'PENDIENTE';
    -----------------------------------------------------------------------------------------
    IF V_CANTIDAD > 0
    THEN
      PS_MENSAJE_ERROR := 'EXISTE UN ARCHIVO PAC PENDIENTE PARA EL PROCESO ESPECIFICADO';
      RAISE EXC_ERR_PROCESO;
    END IF;
    -----------------------------------------------------------------------------------------
    -- SE OBTIENE LA RUTA DEL ARCHIVO DE DUPLICADOS ELIMINADOS --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOM_DIR
      FROM CART_CONFIG_BATCH;
    -------------------------------------------------------
    -- ABRO ARCHIVO DE CARGOS ELIMINADOS..
    BEGIN
      V_ARCH_ELIMINADOS := UTL_FILE.FOPEN(V_NOM_DIR,
                                          'ELIM_' ||
                                          TO_CHAR(PE_FECHA_PROCESO_PAC,
                                                  'DDMMRRRR') || '.TXT',
                                          'W');
    EXCEPTION
      WHEN OTHERS THEN
        PS_MENSAJE_ERROR := 'Error al crear archivo de Duplicados Eliminados, ' ||
                            sqlerrm;
        RAISE EXC_ERR_PROCESO;
    END;
    -------------------------------------------------------
    -- ABRO ARCHIVO DE CARGOS ELIMINADOS..
    BEGIN
      V_ARCH_ERRORES := UTL_FILE.FOPEN(V_NOM_DIR,
                                       'ELIM_' ||
                                       TO_CHAR(PE_FECHA_PROCESO_PAC,
                                               'DDMMRRRR') || '_ERROR.TXT',
                                       'W');
    EXCEPTION
      WHEN OTHERS THEN
        PS_MENSAJE_ERROR := 'Error al crear archivo de Errores de Duplicados Eliminados, ' ||
                            sqlerrm;
        RAISE EXC_ERR_PROCESO;
    END;
    -------------------------------------------------------
    -- TITULOS!
    UTL_FILE.PUT_LINE(V_ARCH_ELIMINADOS,
                      'Propuesta;Id Recibo;Fecha Pac;Producto;Tipo Movimiento;Cuota');
    -----------------------------------------------------------------------------
    FOR P IN PROPUESTAS
    LOOP
      -----------------------------------------------------------
      BEGIN
        -----------------------------------------------------------
        V_CCTE_PROPUESTA := P.PROPUESTA;
        -----------------------------------------------------------
        BEGIN
          -- SE DETERMINA LA CUOTA M�S ANTIGUA TRASPASADA A ENVIO CARGOS POR PROPUESTA
          SELECT CCTE_ID
            INTO V_CCTE_ID
            FROM SCO_CUENTA_CORRIENTE
           WHERE CCTE_PROPUESTA = V_CCTE_PROPUESTA
             AND CCTE_TRASP_ENV_CARGOS =
                 PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_SI
             AND CCTE_TP_MOVTO IN
                 (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR,
                  PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR)
             AND CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC
             AND CCTE_FECHA_ENVIO_PAC IN
                 (SELECT MIN(CCTE_FECHA_ENVIO_PAC)
                    FROM SCO_CUENTA_CORRIENTE
                   WHERE CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC
                     AND CCTE_PROPUESTA = V_CCTE_PROPUESTA
                     AND CCTE_TRASP_ENV_CARGOS =
                         PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_SI
                     AND CCTE_TP_MOVTO IN
                         (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR,
                          PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR))
             AND EXISTS
           (SELECT NUMERO_DOCUMENTO
                    FROM SCO_PLANO_ENVIO_CARGOS
                   WHERE MEDIO_PAGO =
                         PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
                     AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
                     AND TP_ARCHIVO =
                         PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
                     AND NUMERO_DOCUMENTO = CCTE_ID)
             AND ROWNUM = 1;
        EXCEPTION
          WHEN OTHERS THEN
            V_MENSAJE_ERROR := 'Error al buscar primer id: ' || SQLERRM;
            RAISE EXC_ERR_PROC_ELIMINACION;
        END;
        -----------------------------------------------------------
        -- ELIMINA DUPLICADOS --
        FOR E IN ELIMINA
        LOOP
          --DBMS_OUTPUT.PUT_LINE (V_CCTE_PROPUESTA||'|'||E.CCTE_ID);
          BEGIN
            -- SE VALIDA EXISTENCIA EN ENVIO CARGOS
            IF E.EN_ENVIO_CARGOS = 0
            THEN
              PS_MENSAJE_ERROR := 'Movimiento no se encuentra en envio cargos';
              RAISE EXC_ERR_PROC_ELIMINACION;
            END IF;
            -- SE MODIFCIA REZAGA MOVIMIENTO EN CUENTA CORRIENTE --
            UPDATE SCO_CUENTA_CORRIENTE
               SET CCTE_STATUS           = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                   CCTE_TRASP_ENV_CARGOS = PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO
             WHERE CCTE_ID = E.CCTE_ID; --NUMERO_DOCUMENTO;
            -----------------------------------------------------------------------------
            -- SE BUSCA RECIBO RECTOR ASOCIADO A LA CUOTA
            -- SE MODFICA ESTADO DE RECIBO EN RECTOR --
            IF PKG_PROCESOS_PAC.FN_ACTUALIZA_ESTADO_RECIBO(E.CCTE_RECIBO,
                                                           PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO,
                                                           PS_MENSAJE_ERROR) =
               FALSE
            THEN
              RAISE EXC_ERR_PROC_ELIMINACION;
            END IF;
            -----------------------------------------------------------------------------
            -- SE ELIMINA REGISTRO DE LA CUENTA PLANO ENVIO CARGOS
            DELETE SCO_PLANO_ENVIO_CARGOS
             WHERE MEDIO_PAGO =
                   PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
               AND FECHA_CARGA_ARCHIVO = PE_FECHA_PROCESO_PAC
               AND TP_ARCHIVO = PKG_PROCESOS_PAC_VISANET.TP_ARCH_GENERAL
               AND NUMERO_DOCUMENTO = E.CCTE_ID; --NUMERO_DOCUMENTO
            -----------------------------------------------------------------------------
            V_LINEA := V_CCTE_PROPUESTA || ';' || E.CCTE_ID || ';' ||
                       TO_CHAR(E.CCTE_FECHA_ENVIO_PAC,
                               'DDMMRRRR') || ';' || E.CCTE_PLAN || ';' ||
                       E.CCTE_TP_MOVTO || ';' || E.CCTE_CUOTA;
            -----------------------------------------------------------------------------
            UTL_FILE.PUT_LINE(V_ARCH_ELIMINADOS,
                              V_LINEA);
            -----------------------------------------------------------------------------
            -- CUENTA SEGUN TIPO DE MOVIMIENTO..
            IF E.CCTE_TP_MOVTO =
               PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
               AND
               E.CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
            THEN
              PS_CANT_CARG_NORM_PEND := PS_CANT_CARG_NORM_PEND + 1;
            ELSIF E.CCTE_TP_MOVTO =
                  PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
                  AND
                  E.CCTE_STATUS = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO
            THEN
              PS_CANT_CARG_NORM_REZAG := PS_CANT_CARG_NORM_REZAG + 1;
            ELSE
              PS_CANT_NORMALIZACIONES := PS_CANT_NORMALIZACIONES + 1;
            END IF;
            -----------------------------------------------------------------------------
          EXCEPTION
            WHEN OTHERS THEN
              -----------------------------------------------------------
              V_LINEA := V_CCTE_PROPUESTA || ';' ||
                         'Error al eliminar duplicado Id: ' || E.CCTE_ID || ', ' ||
                         sqlerrm;
              -----------------------------------------------------------
              UTL_FILE.PUT_LINE(V_ARCH_ERRORES,
                                V_LINEA);
              -----------------------------------------------------------
          END;
          -----------------------------------------------------------
        END LOOP;
        -----------------------------------------------------------
      EXCEPTION
        WHEN EXC_ERR_PROC_ELIMINACION THEN
          -----------------------------------------------------------
          V_LINEA := V_CCTE_PROPUESTA || ';' ||
                     'Error al eliminar duplicado, ' || PS_MENSAJE_ERROR; --sqlerrm;
          -----------------------------------------------------------
          UTL_FILE.PUT_LINE(V_ARCH_ERRORES,
                            V_LINEA);
          -----------------------------------------------------------
      END;
      -----------------------------------------------------------
    END LOOP;
    -------------------------------------------------------
    IF PS_CANT_CARG_NORM_PEND > 0
       OR PS_CANT_CARG_NORM_REZAG > 0
       OR PS_CANT_NORMALIZACIONES > 0
    THEN
      UPDATE SCOH_BITACORA_PROCESOS_PAC
         SET SCHPP_CARGOS_TRASP_ENV_CARGOS  = SCHPP_CARGOS_TRASP_ENV_CARGOS -
                                              PS_CANT_CARG_NORM_PEND,
             SCHPP_REZAGOS_TRASP_ENV_CARGOS = SCHPP_REZAGOS_TRASP_ENV_CARGOS -
                                              PS_CANT_CARG_NORM_REZAG,
             SCHPP_ENDOSOS_TRASP_ENV_CARGOS = SCHPP_ENDOSOS_TRASP_ENV_CARGOS -
                                              PS_CANT_NORMALIZACIONES
       WHERE SCHPP_ID_PROCESO_PAC = SCHPP_ID_PROCESO_PAC;
    END IF;
    -------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    -------------------------------------------------------
    -- CIERRO ARCHIVOS..
    UTL_FILE.FCLOSE(V_ARCH_ELIMINADOS);
    UTL_FILE.FCLOSE(V_ARCH_ERRORES);
    -----------------------------------------------------------
    RETURN TRUE;
    -----------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_PROCESO THEN
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := SQLERRM;
      RETURN FALSE;
  END ELIM_DUPLICADOS_ENVIO_CARGOS;

  -- -----------------------------------
  FUNCTION TRASP_MOVTOS_ENVIO_CARGOS(PE_FECHA_PROCESO_PAC    IN DATE,
                                     PE_TIPO_MOVTO           IN SCO_CUENTA_CORRIENTE.CCTE_TP_MOVTO%TYPE,
                                     PE_ESTADO               IN SCO_CUENTA_CORRIENTE.CCTE_STATUS%TYPE,
                                     PS_REGISTROS_PROCESADOS OUT NUMBER,
                                     PS_REGISTROS_TRASP      OUT NUMBER,
                                     PS_REGISTROS_ERRONEOS   OUT NUMBER,
                                     PS_MENSAJE_ERROR        OUT VARCHAR2)
    RETURN BOOLEAN IS
    -------------------------------------------------------------------------------------------------
    -- DEFINICION DE VARIABLES DE PASO --
    V_NOMBRE_DIRECTORIO       VARCHAR2(255);
    V_NOMBRE_ARCHIVO_NO_TRASP VARCHAR2(255);
    V_ARCHIVO_NO_TRASP        UTL_FILE.FILE_TYPE;
    --V_FECHA_EFECTIVA          DATE;
    V_CODIGO_TX         NUMBER(3);
    V_GLOSA_CMR_ARCHIVO VARCHAR2(100);
    -------------------------------------------------------------------------------------------------
    -- CANTIDAD DE REGISTROS A GRABAR POR TRANSACCION --
    V_CANT_REG_TRANSAC NUMBER := 1000;
    -------------------------------------------------------------------------------------------------
    -- FECHA BASE PARA TOMAR TODOS LOS REZAGOS DESDE LA FECHA PAC HASTA UN A�O ATRAS --
    --V_FECHA_DESDE             DATE := ADD_MONTHS(PE_FECHA_PROCESO_PAC, -12);
    --V_CAPB_NU_SEGURO_CMR      CART_PRODPLANES.CAPB_NU_SEGURO_CMR%TYPE;
    V_CACE_CAMD_CD_MEDIO_PAGO CART_CERTIFICADOS.CACE_CAMD_CD_MEDIO_PAGO%TYPE;
    V_CACE_ST_CERTIFICADO     CART_CERTIFICADOS.CACE_ST_CERTIFICADO%TYPE;
    V_CACE_CARP_CD_RAMO       CART_CERTIFICADOS.CACE_CARP_CD_RAMO%TYPE;
    V_CACE_CAPU_CD_PRODUCTO   CART_CERTIFICADOS.CACE_CAPU_CD_PRODUCTO%TYPE;
    V_CACE_CAPB_CD_PLAN       CART_CERTIFICADOS.CACE_CAPB_CD_PLAN%TYPE;
    V_CACE_FE_DESDE           CART_CERTIFICADOS.CACE_FE_DESDE%TYPE;
    V_CACE_CACN_CD_NAC_DEUDOR CART_CERTIFICADOS.CACE_CACN_CD_NAC_DEUDOR%TYPE;
    V_CACE_CACN_NU_CED_DEUDOR CART_CERTIFICADOS.CACE_CACN_NU_CED_DEUDOR%TYPE;
    --V_CACN_NU_DOCUMENTO       CART_CLIENTES.CACN_NU_DOCUMENTO%TYPE;
    --V_CACN_CD_VERIFICADOR     CART_CLIENTES.CACN_CD_VERIFICADOR%TYPE;
    --V_CACN_NOMBRES            CART_CLIENTES.CACN_NOMBRES%TYPE;
    V_SCOI_STATUS           SCO_INSCRIPCION.SCOI_STATUS%TYPE;
    V_SCOI_DIA_FAC          SCO_INSCRIPCION.SCOI_DIA_FAC%TYPE;
    V_SCOI_TARJETA          SCO_INSCRIPCION.SCOI_TARJETA%TYPE;
    V_SCOI_NUMERO_COMERCIO  SCO_INSCRIPCION.SCOI_NUMERO_COMERCIO%TYPE;
    V_SCOI_PRODUCTO         SCO_INSCRIPCION.SCOI_PRODUCTO%TYPE;
    V_SCOI_TP_INSCRIPCION   SCO_INSCRIPCION.SCOI_TP_INSCRIPCION%TYPE;
    V_SCOI_RUT              SCO_INSCRIPCION.SCOI_RUT%TYPE;
    V_SCOI_DV               SCO_INSCRIPCION.SCOI_DV%TYPE;
    V_SCOI_NOMBRE           SCO_INSCRIPCION.SCOI_NOMBRE%TYPE;
    V_CCTE_ID               SCO_CUENTA_CORRIENTE.CCTE_ID%TYPE;
    V_CCTE_PROPUESTA        SCO_CUENTA_CORRIENTE.CCTE_PROPUESTA%TYPE;
    V_REG_SCOH_BIT_PROC_PAC SCOH_BITACORA_PROCESOS_PAC%ROWTYPE;
    V_SCCP_GLOSA_CMR        SCO_PRODUCTOS.SCCP_GLOSA_CMR%TYPE;
    V_SCCD_FRAGMENT         SCO_CUADRATURA_DIARIA.SCCD_FRAGMENT%TYPE;
    --
    V_TASA_CAMBIO      CART_TASAS_CAMBIOS.CATA_TA_CAMBIO_COMPRA%TYPE;
    V_CCTE_MONTO_PESOS SCO_CUENTA_CORRIENTE.CCTE_MONTO_PESOS%TYPE;
    -------------------------------------------------------------------------------------------------
    -- DEFINICION DE EXCEPCIONES --
    EXC_ERR_NO_EXISTE_PPTA_RECTOR  EXCEPTION;
    EXC_ERR_MEDIO_DE_PAGO_NO_CMR   EXCEPTION;
    EXC_ERR_PPTA_RECTOR_NO_VIGENTE EXCEPTION;
    EXC_ERR_ST_INSC_NO_VALIDO      EXCEPTION;
    EXC_ERR_INSC_NO_EXISTE         EXCEPTION;
    EXC_ERR_CLIENTE_NO_EXISTE      EXCEPTION;
    EXC_ERR_NO_EXISTE_PROD_PAC     EXCEPTION;
    EXC_ERR_ST_PROC_PAC            EXCEPTION;
    EXC_ERR_PROC_PAC_NO_EXISTE     EXCEPTION;
    EXC_ERR_ABRIR_ARCH_NO_TRASP    EXCEPTION;
    EXC_ERR_BUSCAR_GLOSA           EXCEPTION;
    --
    EXC_ERR_TIPO_ARCH_NO_EXISTE EXCEPTION;
    EXC_ERR_CALCULO_TASA_CAMBIO EXCEPTION;
    -------------------------------------------------------------------------------------------------
    -- CURSOR DE REGISTROS A TRASPASAR--
    CURSOR REGISTRO IS
      SELECT CCTE_ID,
             CCTE_PROPUESTA,
             CCTE_PLAN,
             CCTE_MEDIO_PAGO,
             CCTE_TP_MOVTO,
             CCTE_STATUS,
             CCTE_CUOTA,
             CCTE_MONEDA,
             CCTE_MONTO_MONEDA,
             CCTE_INTENTOS,
             CCTE_FECHA_ENVIO_PAC
        FROM SCO_CUENTA_CORRIENTE,
             SCO_DEF_MOVTO_ST_TRASP
       WHERE CCTE_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND CCTE_TRASP_ENV_CARGOS =
             PKG_PROCESOS_PAC_VISANET.CCTE_TRASP_ENV_CARGOS_NO
         AND CCTE_TP_MOVTO = PE_TIPO_MOVTO
         AND CCTE_STATUS = PE_ESTADO
         AND CCTE_TP_MOVTO = SDMST_CCTE_TP_MOVTO
         AND CCTE_STATUS = SDMST_CCTE_STATUS
         AND ((SDMST_TP_FECHA = 1 AND
             CCTE_FECHA_ENVIO_PAC = PE_FECHA_PROCESO_PAC) OR
             (SDMST_TP_FECHA = 2 AND CCTE_FECHA_ENVIO_PAC >= SDMST_FECHA AND
             CCTE_FECHA_ENVIO_PAC <= PE_FECHA_PROCESO_PAC));
  BEGIN
    -- --------------------------
    -- INICIALIZA CONTADORES..
    PS_REGISTROS_PROCESADOS := 0;
    PS_REGISTROS_TRASP      := 0;
    PS_REGISTROS_ERRONEOS   := 0;
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENEN LOS DATOS DEL PROCESO PAC --
    V_REG_SCOH_BIT_PROC_PAC := NULL;
    BEGIN
      SELECT *
        INTO V_REG_SCOH_BIT_PROC_PAC
        FROM SCOH_BITACORA_PROCESOS_PAC
       WHERE SCHPP_CD_MEDIO_PAGO =
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN
         AND SCHPP_FECHA_PROCESO_PAC = PE_FECHA_PROCESO_PAC;
      --
      IF (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ESTADO != 'INICIADO')
      THEN
        RAISE EXC_ERR_ST_PROC_PAC;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_PROC_PAC_NO_EXISTE;
    END;
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENE EL DIRECTORIO DE DESTINO --
    SELECT CACH_DIR_INTERFACE
      INTO V_NOMBRE_DIRECTORIO
      FROM CART_CONFIG_BATCH;
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENE EL NOMBRE DEL ARCHIVO DE NO TRASPASADOS --
    V_NOMBRE_ARCHIVO_NO_TRASP := 'NO_TRASP_ENV_CARGOS_VISANET' ||
                                 TO_CHAR(PE_FECHA_PROCESO_PAC,
                                         'YYYYMMDD') || '.TXT';
    BEGIN
      V_ARCHIVO_NO_TRASP := UTL_FILE.FOPEN(V_NOMBRE_DIRECTORIO,
                                           V_NOMBRE_ARCHIVO_NO_TRASP,
                                           'W');
    EXCEPTION
      WHEN UTL_FILE.INVALID_OPERATION THEN
        RAISE EXC_ERR_ABRIR_ARCH_NO_TRASP;
      WHEN OTHERS THEN
        RAISE EXC_ERR_ABRIR_ARCH_NO_TRASP;
    END;
    -------------------------------------------------------------------------------------------------
    FOR C IN REGISTRO
    LOOP
      BEGIN
        -- VARIABLES DE PASO --
        V_CCTE_ID        := C.CCTE_ID;
        V_CCTE_PROPUESTA := C.CCTE_PROPUESTA;
        -- SE OBTIENEN LOS DATOS DE LA PROPUESTA RECTOR --
        V_CACE_CARP_CD_RAMO       := NULL;
        V_CACE_CAMD_CD_MEDIO_PAGO := NULL;
        V_CACE_CAPU_CD_PRODUCTO   := NULL;
        V_CACE_CAPB_CD_PLAN       := NULL;
        V_CACE_ST_CERTIFICADO     := NULL;
        V_CACE_FE_DESDE           := NULL;
        V_CACE_CACN_CD_NAC_DEUDOR := NULL;
        V_CACE_CACN_NU_CED_DEUDOR := NULL;
        BEGIN
          SELECT CACE_CARP_CD_RAMO,
                 CACE_CAMD_CD_MEDIO_PAGO,
                 CACE_CAPU_CD_PRODUCTO,
                 CACE_CAPB_CD_PLAN,
                 CACE_ST_CERTIFICADO,
                 CACE_FE_DESDE,
                 CACE_CACN_CD_NAC_DEUDOR,
                 CACE_CACN_NU_CED_DEUDOR
            INTO V_CACE_CARP_CD_RAMO,
                 V_CACE_CAMD_CD_MEDIO_PAGO,
                 V_CACE_CAPU_CD_PRODUCTO,
                 V_CACE_CAPB_CD_PLAN,
                 V_CACE_ST_CERTIFICADO,
                 V_CACE_FE_DESDE,
                 V_CACE_CACN_CD_NAC_DEUDOR,
                 V_CACE_CACN_NU_CED_DEUDOR
            FROM CART_CERTIFICADOS
           WHERE CACE_NU_PROPUESTA = C.CCTE_PROPUESTA;
          IF (V_CACE_CAMD_CD_MEDIO_PAGO !=
             PKG_PROCESOS_PAC_VISANET.CODIGO_MEDIO_PAGO_VN)
          THEN
            RAISE EXC_ERR_MEDIO_DE_PAGO_NO_CMR;
          END IF;
          ------------------------------------
          -- SCV  01-08-2011 --
          -- SE VALIDA ESTADO RECTOR DE PROPUESTA SOLO PARA CARGOS NORMALES PENDIENTES --
          IF (PE_TIPO_MOVTO = CCTE_TP_MOVTO_CARGO_NOR)
             AND
             (PE_ESTADO = CCTE_ST_PENDIENTE OR PE_ESTADO = CCTE_ST_REZAGADO)
             AND (V_CACE_ST_CERTIFICADO NOT IN (1,
                                                14,
                                                80))
          THEN
            ------------------------------------
            RAISE EXC_ERR_PPTA_RECTOR_NO_VIGENTE;
            ------------------------------------
          END IF;
          ------------------------------------
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE EXC_ERR_NO_EXISTE_PPTA_RECTOR;
        END;
        -- SE OBTIENEN LOS DATOS DE LA INSCRIPCION --
        V_SCOI_STATUS          := NULL;
        V_SCOI_DIA_FAC         := NULL;
        V_SCOI_TARJETA         := NULL;
        V_SCOI_NUMERO_COMERCIO := NULL;
        V_SCOI_PRODUCTO        := NULL;
        -- JOHN ORTEGA 31/07/2007
        -- SE EXTRAE EL CODIGO DE PRODUCTO DE LA TABLA SCO_INSCRIPCION
        -- PARA ENVIAR EN LOS COBROS EL MISMO CODIGO DE PRODUCTO CON CUAL SE INSCRIBIO
        BEGIN
          SELECT SCOI_STATUS,
                 SCOI_DIA_FAC,
                 LTRIM(RTRIM(SCOI_TARJETA)),
                 SCOI_NUMERO_COMERCIO,
                 SCOI_PRODUCTO,
                 SCOI_RUT,
                 SCOI_DV,
                 SCOI_NOMBRE
            INTO V_SCOI_STATUS,
                 V_SCOI_DIA_FAC,
                 V_SCOI_TARJETA,
                 V_SCOI_NUMERO_COMERCIO,
                 V_SCOI_PRODUCTO,
                 V_SCOI_RUT,
                 V_SCOI_DV,
                 V_SCOI_NOMBRE
            FROM SCO_INSCRIPCION
           WHERE SCOI_CONTRATO_PROPUESTA = C.CCTE_PROPUESTA;
          IF (V_SCOI_STATUS != PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP)
          THEN
            RAISE EXC_ERR_ST_INSC_NO_VALIDO;
          END IF;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE EXC_ERR_INSC_NO_EXISTE;
        END;
        ------------------------------------------------------------------------------------
        IF (V_SCOI_RUT IS NULL)
        THEN
          V_SCOI_RUT := 0;
        END IF;
        ------------------------------------------------------------------------------------
        IF (V_SCOI_DV IS NULL)
        THEN
          V_SCOI_DV := '0';
        END IF;
        /*
        -- SE OBTIENEN LOS DATOS DEL CLIENTE --
        V_CACN_NU_DOCUMENTO   := NULL;
        V_CACN_CD_VERIFICADOR := NULL;
        V_CACN_NOMBRES        := NULL;
        BEGIN
             SELECT CACN_NU_DOCUMENTO,
                    CACN_CD_VERIFICADOR,
                    NVL(RPAD(SUBSTR(RTRIM(LTRIM(CACN_APELLIDO_PATERNO))||' '|| RTRIM(LTRIM(CACN_APELLIDO_MATERNO)),1,23),24,' ')||RPAD(SUBSTR(RTRIM(LTRIM(CACN_NOMBRES)),1,12),12,' '), SUBSTR(LTRIM(RTRIM(CACN_NM_APELLIDO_RAZON)),1,36))
               INTO V_CACN_NU_DOCUMENTO,
                    V_CACN_CD_VERIFICADOR,
                    V_CACN_NOMBRES
               FROM CART_CLIENTES
              WHERE CACN_CD_NACIONALIDAD = V_CACE_CACN_CD_NAC_DEUDOR
                AND CACN_NU_CEDULA_RIF   = V_CACE_CACN_NU_CED_DEUDOR;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
           RAISE EXC_ERR_CLIENTE_NO_EXISTE;
        END;
        
        IF (V_CACN_NU_DOCUMENTO IS NULL) THEN
           V_CACN_NU_DOCUMENTO := 0;
        END IF;
        IF (V_CACN_CD_VERIFICADOR IS NULL) THEN
           V_CACN_CD_VERIFICADOR := '0';
        END IF;
        
        */
        -- JOHN ORTEGA 31/07/2007
        -- SE COMENTA LA OBTENCION DEL CODIGO DE PRODUCTO DE LA TABLA
        -- CART_PRODPLANES, YA QUE SE OBTENDRA DE LA TABLA SCO_INSCRIPCION
        -- SE OBTINEN LOS DATOS DEL PRODUCTO --
        --SELECT CAPB_NU_SEGURO_CMR
        --  INTO CAPB_NU_SEGURO_CMR_AUX
        --  FROM CART_PRODPLANES
        -- WHERE CAPB_CARP_CD_RAMO     = V_CACE_CARP_CD_RAMO
        --   AND CAPB_CAPU_CD_PRODUCTO = V_CACE_CAPU_CD_PRODUCTO
        --   AND CAPB_CD_PLAN          = V_CACE_CAPB_CD_PLAN;
        --
        -- VALIDO TIPO DE ARCHIVO (REGIONALIZACION)!
        --
        BEGIN
          SELECT SCDEC_TP_ARCHIVO
            INTO V_SCOI_TP_INSCRIPCION
            FROM SCO_DEF_ENVIO_CARGOS
           WHERE SCDEC_CD_PLAN = C.CCTE_PLAN
             AND SCDEC_CD_MEDIO_PAGO = C.CCTE_MEDIO_PAGO
             AND SCDEC_TP_MOVTO = C.CCTE_TP_MOVTO;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE EXC_ERR_TIPO_ARCH_NO_EXISTE;
        END;
        -------------------------------------------------------------------------------------------------
        -- LUIS MORA DIAZ / 05-11-2008 --
        -- SE MODIFICA LA FORMA DE CALCULO DE LA TASA DE CAMBIO SEGUN MONEDA DEL PRODUCTO --
        -- SE OBTIENE LA TASA DE CAMBIO Y EL MONTO DE LA MONEDA --
        --V_MONTO_TASA := 1;
        --IF (C.CCTE_MONEDA = PKG_PROCESOS_PAC_VISANET.CODIGO_MONEDA_UF) THEN
        --   PKG_PROCESOS_PAC.TASA_CAMBIO_GENERAL (
        --                                         TO_DATE(PE_FECHA_PROCESO_PAC,'DD-MM-RRRR'),
        --                                         PKG_PROCESOS_PAC_VISANET.CODIGO_MONEDA_UF,
        --                                         V_FECHA_EFECTIVA,
        --                                         V_MONTO_TASA
        --                                         );
        --   V_MONTO_CUOTA := ROUND((C.CCTE_MONTO_MONEDA * V_MONTO_TASA),0) * 100;
        --ELSIF (C.CCTE_MONEDA = PKG_PROCESOS_PAC_VISANET.CODIGO_MONEDA_DOLAR) THEN
        --   PKG_PROCESOS_PAC.TASA_CAMBIO_GENERAL (
        --                                         TO_DATE(SYSDATE,'DD-MM-RRRR'),
        --                                         PKG_PROCESOS_PAC_VISANET.CODIGO_MONEDA_DOLAR,
        --                                         V_FECHA_EFECTIVA,
        --                                         V_MONTO_TASA
        --                                         );
        --   V_MONTO_CUOTA := ROUND((C.CCTE_MONTO_MONEDA * V_MONTO_TASA),0) * 100;
        --ELSE
        --   V_MONTO_CUOTA := (ROUND(C.CCTE_MONTO_MONEDA,0) * 100);
        --END IF;
        -- POR REGIONALIZACION..
        ------------------------------------------------------------------------------------
        -- LUIS MORA DIAZ --
        -- 08/05/2011 --
        -- SE MODIFICA LA FORMA DE CALCULO DE LA TASA DE CAMBIO SEGUN MONEDA DEL PRODUCTO REGIONAL --
        --V_TASA_CAMBIO      := 1;
        --V_CCTE_MONTO_PESOS := NULL;
        --IF (PKG_PROCESOS_PAC.FN_CALCULO_TASA_CAMBIO_PAC (
        --                                            TO_DATE(PE_FECHA_PROCESO_PAC,'DD-MM-RRRR'),
        --                                            C.CCTE_MONEDA,
        --                                            C.CCTE_MONTO_MONEDA,
        --                                            V_CCTE_MONTO_PESOS,
        --                                            V_TASA_CAMBIO,
        --                                            PS_MENSAJE_ERROR
        --                                            )  = FALSE) THEN
        --   RAISE EXC_ERR_CALCULO_TASA_CAMBIO;
        --END IF;
        -----------------------------------------------------------------------------------
        -- SCV 02-08-2011 --
        -- SE REDONDEA SIN DECIMALES --
        --V_CCTE_MONTO_PESOS := ROUND(V_CCTE_MONTO_PESOS,0);
        V_TASA_CAMBIO      := 1;
        V_CCTE_MONTO_PESOS := ROUND(C.CCTE_MONTO_MONEDA,
                                    2);
        -------------------------------------------------------------------------------------------------
        -- JOHN ORTEGA 19/12/2007
        -- BUSCA LA GLOSA DEL PRODUCTO Y EL FRAGMENT DE LA PROPUESTA
        BEGIN
          V_SCCP_GLOSA_CMR := NULL;
          V_SCCD_FRAGMENT  := NULL;
          SELECT RPAD(NVL(SCCP_GLOSA_CMR,
                          'SEG FPRO'),
                      15,
                      ' '),
                 SCCD_FRAGMENT
            INTO V_SCCP_GLOSA_CMR,
                 V_SCCD_FRAGMENT
            FROM SCO_PRODUCTOS,
                 SCO_CUADRATURA_DIARIA,
                 CART_CERTIFICADOS
           WHERE CACE_NU_PROPUESTA = C.CCTE_PROPUESTA
             AND SCCD_PROPUESTA = CACE_NU_PROPUESTA
             AND SCCP_CAPB_CD_PLAN = CACE_CAPB_CD_PLAN;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            RAISE EXC_ERR_BUSCAR_GLOSA;
        END;
        IF (C.CCTE_TP_MOVTO IN
           (PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR,
             PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_DIR))
        THEN
          V_CODIGO_TX := '200';
          IF (C.CCTE_TP_MOVTO = 1)
          THEN
            V_GLOSA_CMR_ARCHIVO := (LPAD(C.CCTE_CUOTA,
                                         2,
                                         '0') || '-' ||
                                   LPAD(V_SCCD_FRAGMENT,
                                         2,
                                         '0') || ' ' ||
                                   RPAD(SUBSTR(V_SCCP_GLOSA_CMR,
                                                1,
                                                15),
                                         15,
                                         ' ') || ' ' ||
                                   LPAD(C.CCTE_PROPUESTA,
                                         8,
                                         '0'));
          ELSE
            V_GLOSA_CMR_ARCHIVO := ('CARGO' || ' ' ||
                                   RPAD(SUBSTR(V_SCCP_GLOSA_CMR,
                                                1,
                                                15),
                                         15,
                                         ' ') || ' ' ||
                                   LPAD(C.CCTE_PROPUESTA,
                                         8,
                                         '0'));
          END IF;
        ELSIF (C.CCTE_TP_MOVTO =
              PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_ABONO_DIR)
        THEN
          V_CODIGO_TX         := '400';
          V_GLOSA_CMR_ARCHIVO := ('ABONO' || ' ' ||
                                 RPAD(SUBSTR(V_SCCP_GLOSA_CMR,
                                              1,
                                              15),
                                       15,
                                       ' ') || ' ' ||
                                 LPAD(C.CCTE_PROPUESTA,
                                       8,
                                       '0'));
        END IF;
        -- SE INSERTA EN TABLA SCO_PLANO_ENVIO_CARGOS --
        INSERT INTO SCO_PLANO_ENVIO_CARGOS
          (NUMERO_COMERCIO,
           CICLO_CARGA,
           MES_COBERTURA,
           RUT,
           DV,
           NOMBRE,
           TARJETA_CMR,
           MARCA1,
           VALOR_CUOTA,
           MARCA2,
           FECHA_CARGA,
           NUMERO_CONTRATO,
           NUMERO_DOCUMENTO,
           NOMBRE_FANTASIA,
           COD_RESPUESTA_CMR,
           FECHA_FACTURACION_CMR,
           CICLO_NUEVO,
           CODIGO_TX,
           FECHA_INICIO,
           CODIGO_PRODUCTO,
           PROPUESTA,
           CUOTA,
           MODALIDAD_DE_CARGO,
           RAMO_CODIGO_ENTIDAD,
           PLAN,
           CODIGO_AREA,
           TELEFONO,
           CICLO,
           EMPRESA,
           LIBRE,
           NOMBRE_ARCHIVO,
           FECHA_CARGA_ARCHIVO,
           MEDIO_PAGO,
           STATUS,
           TP_MOVTO,
           ENV_ARCH_PAC,
           RESP_ARCH_PAC,
           TP_ARCHIVO)
        VALUES
          (V_SCOI_TP_INSCRIPCION,
           LPAD(TO_CHAR(PE_FECHA_PROCESO_PAC,
                        'DD'),
                2,
                '0'),
           TO_NUMBER(TO_CHAR(PE_FECHA_PROCESO_PAC,
                             'MMRRRR')),
           LPAD(V_SCOI_RUT,
                9,
                '0'),
           RPAD(V_SCOI_DV,
                1,
                ' '),
           RPAD(V_SCOI_NOMBRE,
                36,
                ' '),
           V_SCOI_TARJETA,
           ' ',
           V_CCTE_MONTO_PESOS * 100, --LPAD(V_MONTO_CUOTA,9,'0'),
           ' ',
           C.CCTE_FECHA_ENVIO_PAC,
           LPAD(C.CCTE_PROPUESTA,
                13,
                '0'),
           LPAD(C.CCTE_ID,
                12,
                '0'),
           RPAD(NVL(V_GLOSA_CMR_ARCHIVO,
                    ' '),
                32,
                ' '),
           '000',
           LPAD(V_SCOI_DIA_FAC,
                2,
                '0'),
           '00',
           LPAD(V_CODIGO_TX,
                3,
                '0'),
           V_CACE_FE_DESDE,
           LPAD(V_SCOI_NUMERO_COMERCIO,
                3,
                '0'),
           LPAD(TO_CHAR(C.CCTE_PROPUESTA),
                12,
                '0'),
           LPAD(TO_CHAR(C.CCTE_CUOTA),
                3,
                '0'),
           '000',
           LPAD(V_CACE_CARP_CD_RAMO,
                3,
                '0'),
           V_CACE_CAPB_CD_PLAN,
           '0000',
           C.CCTE_ID,
           ' ',
           '  ',
           '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
           NULL,
           PE_FECHA_PROCESO_PAC,
           C.CCTE_MEDIO_PAGO,
           C.CCTE_STATUS,
           C.CCTE_TP_MOVTO,
           'S',
           'N',
           V_SCOI_TP_INSCRIPCION);
        -- SE ACTUALIZA EL REGISTRO EN LA TABLA SCO_CUENTA_CORRIENTE --
        UPDATE SCO_CUENTA_CORRIENTE
           SET CCTE_MONTO_PESOS       = V_CCTE_MONTO_PESOS, --(V_MONTO_CUOTA/100),
               CCTE_DIA_PAC_PROCESADO = PE_FECHA_PROCESO_PAC,
               --CCTE_INTENTOS          = (C.CCTE_INTENTOS + 1),
               CCTE_TIPO_CAMBIO      = V_TASA_CAMBIO, --V_MONTO_TASA,
               CCTE_CAMPO7           = 2,
               CCTE_TRASP_ENV_CARGOS = 'S'
         WHERE CCTE_ID = C.CCTE_ID;
        -- SE CUENTAN LOS REGISTROS TRASPASADOS --
        PS_REGISTROS_TRASP := (PS_REGISTROS_TRASP + 1);
        IF (MOD(PS_REGISTROS_TRASP,
                V_CANT_REG_TRANSAC) = 0)
        THEN
          -------------------------------------------------------------------------------------------------
          --  SE ACTUALIZA LA CANTIDAD DE REGISTROS TRASPASADOS
          IF PE_TIPO_MOVTO =
             PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
             AND PE_ESTADO = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
          THEN
            -----------------------------------------------------------------------------------------
            -- SE ACTUALIZA EL TOTAL DE CARGOS TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
            UPDATE SCOH_BITACORA_PROCESOS_PAC
               SET SCHPP_CARGOS_TRASP_ENV_CARGOS =
                   (V_REG_SCOH_BIT_PROC_PAC.SCHPP_CARGOS_TRASP_ENV_CARGOS +
                   PS_REGISTROS_TRASP)
             WHERE SCHPP_ID_PROCESO_PAC =
                   V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
            -----------------------------------------------------------------------------------------
          ELSIF PE_TIPO_MOVTO =
                PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
                AND PE_ESTADO = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO
          THEN
            -----------------------------------------------------------------------------------------
            -- SE ACTUALIZA EL TOTAL DE REZAGOS TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
            UPDATE SCOH_BITACORA_PROCESOS_PAC
               SET SCHPP_REZAGOS_TRASP_ENV_CARGOS =
                   (V_REG_SCOH_BIT_PROC_PAC.SCHPP_REZAGOS_TRASP_ENV_CARGOS +
                   PS_REGISTROS_TRASP)
             WHERE SCHPP_ID_PROCESO_PAC =
                   V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
            -----------------------------------------------------------------------------------------
          ELSE
            -----------------------------------------------------------------------------------------
            -- SE ACTUALIZA EL TOTAL DE NORMALIZACIONES TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
            UPDATE SCOH_BITACORA_PROCESOS_PAC
               SET SCHPP_ENDOSOS_TRASP_ENV_CARGOS =
                   (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ENDOSOS_TRASP_ENV_CARGOS +
                   PS_REGISTROS_TRASP)
             WHERE SCHPP_ID_PROCESO_PAC =
                   V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
            -----------------------------------------------------------------------------------------
          END IF;
          COMMIT;
        END IF;
      EXCEPTION
        WHEN EXC_ERR_NO_EXISTE_PPTA_RECTOR THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID || ', PROPUESTA ' ||
                                   V_CCTE_PROPUESTA ||
                                   ' NO EXISTE EN RECTOR';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_MEDIO_DE_PAGO_NO_CMR THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', EL MEDIO DE PAGO DE LA PROPUESTA ' ||
                                   V_CCTE_PROPUESTA || ' NO ES CMR';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_PPTA_RECTOR_NO_VIGENTE THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID || ', PROPUESTA ' ||
                                   V_CCTE_PROPUESTA ||
                                   ' NO ESTA VIGENTE EN RECTOR';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_ST_INSC_NO_VALIDO THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', LA INSCRIPCI�N DE LA PROPUESTA ' ||
                                   V_CCTE_PROPUESTA || ' NO ES V�LIDA';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_INSC_NO_EXISTE THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', EL REGISTRO DE INSCRIPCI�N DE LA PROPUESTA ' ||
                                   V_CCTE_PROPUESTA || ' NO EXISTE';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_CLIENTE_NO_EXISTE THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', EL CLIENTE ASOCIADO A LA PROPUESTA ' ||
                                   V_CCTE_PROPUESTA || ' NO EXISTE';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_NO_EXISTE_PROD_PAC THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID || ', EL PRODUCTO ' ||
                                   V_CACE_CAPB_CD_PLAN ||
                                   ' NO EXISTE EN EL SISTEMA PAC';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_BUSCAR_GLOSA THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID || ', LA GLOSA DEL PRODUCTO ' ||
                                   V_CACE_CAPB_CD_PLAN ||
                                   ' NO SE ENCUENTRA';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_CALCULO_TASA_CAMBIO THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', EL CALCULO DE TASA DE CAMBIO FALLO' ||
                                   V_CCTE_ID;
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
        WHEN EXC_ERR_TIPO_ARCH_NO_EXISTE THEN
          PS_MENSAJE_ERROR      := 'ERROR AL TRASPASAR MOVIMIENTO ID ' ||
                                   V_CCTE_ID ||
                                   ', EL TIPO DE ARCHIVO NO EXISTE';
          PS_REGISTROS_ERRONEOS := (PS_REGISTROS_ERRONEOS + 1);
          UTL_FILE.PUT_LINE(V_ARCHIVO_NO_TRASP,
                            RPAD(PS_MENSAJE_ERROR,
                                 200,
                                 ' '));
      END;
      --V_REGISTROS_TRASP := V_REGISTROS_TRASP + 1;
      -- SE CUENTAN LOS REGISTROS PROCESADOS --
      PS_REGISTROS_PROCESADOS := (PS_REGISTROS_PROCESADOS + 1);
    END LOOP;
    -- -------------------------------------------------------------------------------------
    -- SE ACTUALIZA EL TOTAL DE CARGOS TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
    IF PE_TIPO_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
       AND PE_ESTADO = PKG_PROCESOS_PAC_VISANET.CCTE_ST_PENDIENTE
    THEN
      -----------------------------------------------------------------------------------------
      -- SE ACTUALIZA EL TOTAL DE CARGOS TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
      UPDATE SCOH_BITACORA_PROCESOS_PAC
         SET SCHPP_CARGOS_TRASP_ENV_CARGOS =
             (V_REG_SCOH_BIT_PROC_PAC.SCHPP_CARGOS_TRASP_ENV_CARGOS +
             PS_REGISTROS_TRASP)
       WHERE SCHPP_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
      -----------------------------------------------------------------------------------------
    ELSIF PE_TIPO_MOVTO = PKG_PROCESOS_PAC_VISANET.CCTE_TP_MOVTO_CARGO_NOR
          AND PE_ESTADO = PKG_PROCESOS_PAC_VISANET.CCTE_ST_REZAGADO
    THEN
      -----------------------------------------------------------------------------------------
      -- SE ACTUALIZA EL TOTAL DE REZAGOS TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
      UPDATE SCOH_BITACORA_PROCESOS_PAC
         SET SCHPP_REZAGOS_TRASP_ENV_CARGOS =
             (V_REG_SCOH_BIT_PROC_PAC.SCHPP_REZAGOS_TRASP_ENV_CARGOS +
             PS_REGISTROS_TRASP)
       WHERE SCHPP_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
      -----------------------------------------------------------------------------------------
    ELSE
      -----------------------------------------------------------------------------------------
      -- SE ACTUALIZA EL TOTAL DE NORMALIZACIONES TRASPASADOS EN LA TABLA SCOH_BITACORA_PROCESOS_PAC --
      UPDATE SCOH_BITACORA_PROCESOS_PAC
         SET SCHPP_ENDOSOS_TRASP_ENV_CARGOS =
             (V_REG_SCOH_BIT_PROC_PAC.SCHPP_ENDOSOS_TRASP_ENV_CARGOS +
             PS_REGISTROS_TRASP)
       WHERE SCHPP_ID_PROCESO_PAC =
             V_REG_SCOH_BIT_PROC_PAC.SCHPP_ID_PROCESO_PAC;
      -----------------------------------------------------------------------------------------
    END IF;
    -------------------------------------------------------------------------------------------------
    COMMIT;
    -------------------------------------------------------------------------------------------------
    UTL_FILE.FCLOSE(V_ARCHIVO_NO_TRASP);
    -------------------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    ------------------------------------------------------------------------------------------------
    RETURN TRUE;
    -------------------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_ST_PROC_PAC THEN
      PS_MENSAJE_ERROR := 'ERROR, EL ESTADO DEL PROCESO PAC NO ES V�LIDO';
      ROLLBACK;
      RETURN FALSE;
    WHEN EXC_ERR_PROC_PAC_NO_EXISTE THEN
      PS_MENSAJE_ERROR := 'ERROR, EL PROCESO PAC NO EXISTE';
      ROLLBACK;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'ERROR AL TRASPASAR MOVIMIENTO ID ' || V_CCTE_ID ||
                          ', PROPUESTA ' || V_CCTE_PROPUESTA || ', ' ||
                          SQLERRM;
      ROLLBACK;
      RETURN FALSE;
  END TRASP_MOVTOS_ENVIO_CARGOS;

  -------------------------------------------------------------------------------------------------
  FUNCTION FN_VALIDA_DATOS_CUENTA(PE_PROPUESTA  IN NUMBER,
                                  PE_MEDIO_PAGO IN NUMBER,
                                  PS_COD_ERROR  IN OUT NUMBER,
                                  PS_MSG_ERROR  IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    REG_SCO_CUENTA_BANCO SCO_CUENTA_BANCO%ROWTYPE;
  BEGIN
    ------------------------------------------------------------------------
    SELECT *
      INTO REG_SCO_CUENTA_BANCO
      FROM SCO_CUENTA_BANCO
     WHERE SOCB_PROPUESTA = PE_PROPUESTA
       AND SOCB_CD_MEDIO_PAGO = PE_MEDIO_PAGO;
    ------------------------------------------------------------------------ 
    IF (REG_SCO_CUENTA_BANCO.SOCB_CUENTA_CORRIENTE IS NULL)
    THEN
      PS_MSG_ERROR := 'NUMERO DE CUENTA NO INGRESADO';
      PS_COD_ERROR := 32;
      RETURN FALSE;
    ELSIF (REG_SCO_CUENTA_BANCO.SOCB_MES_VENCIMIENTO) IS NULL
    THEN
      PS_MSG_ERROR := 'MES DE VENCIMIENTO DE TARJETA NO INGRESADO';
      PS_COD_ERROR := 32;
      RETURN FALSE;
    ELSIF (REG_SCO_CUENTA_BANCO.SOCB_ANIO_VENCIMIENTO) IS NULL
    THEN
      PS_MSG_ERROR := 'A�O DE VENCIMIENTO DE TARJETA NO INGRESADO';
      PS_COD_ERROR := 32;
      RETURN FALSE;
    END IF;
    RETURN TRUE;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      PS_MSG_ERROR := 'REGISTRO EN CUENTA BANCO NO EXISTE';
      PS_COD_ERROR := 31;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MSG_ERROR := SQLERRM;
      PS_COD_ERROR := 32;
      RETURN FALSE;
  END FN_VALIDA_DATOS_CUENTA;

  --
  FUNCTION FN_END_CAMBIO_CUENTA(PE_NU_PROPUESTA   IN NUMBER,
                                PE_TP_DOCUMENTO   IN CART_CLIENTES.CACN_TP_DOCUMENTO%TYPE,
                                PE_NU_DOCUMENTO   IN CART_CLIENTES.CACN_NU_DOCUMENTO%TYPE,
                                PE_NU_ENDOSO      IN NUMBER,
                                PE_NU_CUENTA_ANT  IN VARCHAR2,
                                PE_NU_CUENTA_NEW  IN VARCHAR2,
                                PE_CD_PUNTO_VENTA IN NUMBER,
                                PS_MENSAJE_ERROR  IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    -------------------------------------------------------------------------------------------------
    V_NOMBRE_CLIENTE           VARCHAR2(200) := NULL;
    V_DIRECCION_CLIENTE        VARCHAR2(200) := NULL;
    V_NUMERO_DOCUMENTO_CLIENTE VARCHAR2(20) := NULL;
    V_TIPO_DOCUMENTO_CLIENTE   VARCHAR2(1) := NULL;
    V_MENSAJE_ERROR            VARCHAR2(200) := NULL;
    V_COD_ERROR                NUMBER := 0;
    V_IMPLICITO                BOOLEAN;
    -------------------------------------------------------------------------------------------------
    --V_SCCD_STATUS               SCO_CUADRATURA_DIARIA.SCCD_STATUS%TYPE := NULL;
    V_SCCD_FRAGMENT             SCO_CUADRATURA_DIARIA.SCCD_FRAGMENT%TYPE := NULL;
    V_REG_SCO_INSCRIPCION       SCO_INSCRIPCION%ROWTYPE;
    V_REG_SCOH_INSCRIPCION      SCOH_INSCRIPCION%ROWTYPE;
    V_REG_SCO_CUADRATURA_DIARIA SCO_CUADRATURA_DIARIA%ROWTYPE;
    ------------------------------------------------------------------------------------------------- 
    V_SIG_FECHA_PAC           SCO_CUENTA_CORRIENTE.CCTE_FECHA_ENVIO_PAC%TYPE := NULL;
    V_SCOI_DIA_PAC            SCO_INSCRIPCION.SCOI_DIA_PAC%TYPE := NULL;
    V_SCOI_DIA_PAGO           SCO_INSCRIPCION.SCOI_DIA_PAGO%TYPE := NULL;
    V_ESTADO_INSCRIPCION_ALTA SCO_INSCRIPCION.SCOI_TIPO_REGISTRO%TYPE := NULL;
    V_ESTADO_INSCRIPCION_BAJA SCO_INSCRIPCION.SCOI_TIPO_REGISTRO%TYPE := NULL;
    -------------------------------------------------------------------------------------------------
    EXC_ERR_ST_CUADRATURA       EXCEPTION;
    EXC_ERR_NO_EXIST_CUADRATURA EXCEPTION;
    EXC_ERR_ST_INSCRIPCION      EXCEPTION;
    EXC_ERR_NO_EXIST_INSC       EXCEPTION;
    EXC_ERR_GRABAR_SCOH_INSC    EXCEPTION;
    EXC_ERR_MODIFICAR_SCO_INSC  EXCEPTION;
    EXC_ERR_OBTENER_CLIENTES    EXCEPTION;
    EXC_ERR_GRAB_BIT_CAMB_CTA   EXCEPTION;
    EXC_ERR_SIGUIENTE_FECPAC    EXCEPTION;
    EXC_ERR_DATOS_CUENTA        EXCEPTION;
    EXC_ERR_DIA_PAGO            EXCEPTION;
    EXC_ERR_IMPLICITO           EXCEPTION;
    -------------------------------------------------------------------------------------------------
    --JORTEGA 28-06-2012
    -- SE COMENTA OBTENCION DE CUOTAS PENDIENTE Y REZAGADAS 
    /*
    -- SE OBTIENEN LAS CUOTAS PENDIENTES Y REZAGADAS ENVIADAS A COBRO --
    CURSOR CUOTAS_PEND_REZAG_ENVIADAS IS
       SELECT CCTE_ID,
              CCTE_MEDIO_PAGO,
              CCTE_FECHA_ENVIO_PAC,
              CCTE_DIA_PAC_PROCESADO
         FROM SCO_CUENTA_CORRIENTE
        WHERE CCTE_PROPUESTA        = PE_NU_PROPUESTA
          AND CCTE_TP_MOVTO         = PKG_PROCESOS_PAC_CMR.CCTE_TP_MOVTO_CARGO_NOR
          AND CCTE_STATUS           IN (PKG_PROCESOS_PAC_CMR.CCTE_ST_PENDIENTE,PKG_PROCESOS_PAC_CMR.CCTE_ST_REZAGADO)
          AND CCTE_TRASP_ENV_CARGOS = PKG_PROCESOS_PAC_CMR.CCTE_TRASP_ENV_CARGOS_SI;
    */
    -------------------------------------------------------------------------------------------------
  BEGIN
    -------------------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENE EL STATUS DE LA CUADRATURA DIARIA --
    BEGIN
      SELECT *
        INTO V_REG_SCO_CUADRATURA_DIARIA
        FROM SCO_CUADRATURA_DIARIA
       WHERE SCCD_PROPUESTA = PE_NU_PROPUESTA;
      IF (V_REG_SCO_CUADRATURA_DIARIA.SCCD_STATUS NOT IN
         (PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_ACEP_CON_CTACTE,
           PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_DESEST_CARGOS))
      THEN
        RAISE EXC_ERR_ST_CUADRATURA;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_NO_EXIST_CUADRATURA;
    END;
    -------------------------------------------------------------------------------------------------
    -- SE OBTIENE EL REGISTRO DE INSCRIPCION CMR --
    BEGIN
      SELECT *
        INTO V_REG_SCO_INSCRIPCION
        FROM SCO_INSCRIPCION
       WHERE SCOI_CONTRATO_PROPUESTA = PE_NU_PROPUESTA;
      IF (V_REG_SCO_INSCRIPCION.SCOI_STATUS NOT IN
         (PKG_PROCESOS_PAC_VISANET.SCOI_ST_INSC_ACEP,
           PKG_PROCESOS_PAC_VISANET.SCOI_ST_DESEST_CARGOS))
      THEN
        RAISE EXC_ERR_ST_INSCRIPCION;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE EXC_ERR_NO_EXIST_INSC;
    END;
    -------------------------------------------------------------------------------------------------
    -- SE OBTINEN LOS DATOS DEL NUEVO CONTRATANTE --
    BEGIN
      SELECT CACN_NOMBRES || ' ' || CACN_APELLIDO_PATERNO || ' ' ||
             CACN_APELLIDO_MATERNO,
             CACN_NU_DOCUMENTO,
             CACN_TP_DOCUMENTO,
             CACN_DI_COBRO_P
        INTO V_NOMBRE_CLIENTE,
             V_NUMERO_DOCUMENTO_CLIENTE,
             V_TIPO_DOCUMENTO_CLIENTE,
             V_DIRECCION_CLIENTE
        FROM CART_CERTIFICADOS,
             CART_CERTIFICADOS_ENDOSOS,
             CART_CLIENTES
       WHERE CACE_NU_PROPUESTA = PE_NU_PROPUESTA
         AND CACE_CASU_CD_SUCURSAL = CACW_CASU_CD_SUCURSAL
         AND CACE_CARP_CD_RAMO = CACW_CARP_CD_RAMO
         AND CACE_CAPO_NU_POLIZA = CACW_CAPO_NU_POLIZA
         AND CACE_NU_CERTIFICADO = CACW_CACE_NU_CERTIFICADO
         AND CACW_NU_ENDOSO =
             NVL((SELECT MAX(CACW_NU_ENDOSO)
                   FROM CART_CERTIFICADOS_ENDOSOS
                  WHERE CACW_CASU_CD_SUCURSAL = CACE_CASU_CD_SUCURSAL
                    AND CACW_CARP_CD_RAMO = CACE_CARP_CD_RAMO
                    AND CACW_CAPO_NU_POLIZA = CACE_CAPO_NU_POLIZA
                    AND CACW_CACE_NU_CERTIFICADO = CACE_NU_CERTIFICADO
                    AND CACW_CAME_TP_TRANSAC =
                        PKG_PROCESOS_PAC.CAME_TP_TRANSAC_END_CUALIT
                    AND CACW_CAME_CD_MOTIVO =
                        PKG_PROCESOS_PAC.CAME_CD_MOTIVO_CAMB_CUENTA_CMR),
                 0) -- SOLO ENDOSO DE CAMBIO DE CUENTA --
         AND CACW_CAPO_CD_NACIONALIDAD = CACN_CD_NACIONALIDAD
         AND CACW_CAPO_NU_CEDULA = CACN_NU_CEDULA_RIF;
    EXCEPTION
      WHEN OTHERS THEN
        BEGIN
          SELECT CACN_NOMBRES || ' ' || CACN_APELLIDO_PATERNO || ' ' ||
                 CACN_APELLIDO_MATERNO,
                 CACN_NU_DOCUMENTO,
                 CACN_TP_DOCUMENTO,
                 CACN_DI_COBRO_P
            INTO V_NOMBRE_CLIENTE,
                 V_NUMERO_DOCUMENTO_CLIENTE,
                 V_TIPO_DOCUMENTO_CLIENTE,
                 V_DIRECCION_CLIENTE
            FROM CART_CERTIFICADOS,
                 CART_CERTIFICADOS_ENDOSOS,
                 CART_CLIENTES
           WHERE CACE_NU_PROPUESTA = PE_NU_PROPUESTA
             AND CACE_CASU_CD_SUCURSAL = CACW_CASU_CD_SUCURSAL
             AND CACE_CARP_CD_RAMO = CACW_CARP_CD_RAMO
             AND CACE_CAPO_NU_POLIZA = CACW_CAPO_NU_POLIZA
             AND CACE_NU_CERTIFICADO = CACW_CACE_NU_CERTIFICADO
             AND CACW_NU_ENDOSO = 0 -- SOLO ENDODO DE CAMBIO DE CUENTA --
             AND CACW_CAPO_CD_NACIONALIDAD = CACN_CD_NACIONALIDAD
             AND CACW_CAPO_NU_CEDULA = CACN_NU_CEDULA_RIF;
        EXCEPTION
          WHEN OTHERS THEN
            RAISE EXC_ERR_OBTENER_CLIENTES;
        END;
    END;
    IF NOT
        PKG_PROCESOS_PAC.FN_CALCULA_SIGUIENTE_FECHA_PAC(PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                                        SYSDATE,
                                                        V_SIG_FECHA_PAC,
                                                        PS_MENSAJE_ERROR)
    THEN
      RAISE EXC_ERR_SIGUIENTE_FECPAC;
    END IF;
    --
    V_SCOI_DIA_PAC := TO_NUMBER(TO_CHAR(V_SIG_FECHA_PAC,
                                        'DD'));
    --
    ---------------------------------------------------------------------------------
    -- VALIDA DATOS DE CUENTA 
    IF NOT
        PKG_PROCESOS_PAC_VISANET.FN_VALIDA_DATOS_CUENTA(PE_NU_PROPUESTA,
                                                        PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                                        V_COD_ERROR,
                                                        PS_MENSAJE_ERROR)
    THEN
      RAISE EXC_ERR_DATOS_CUENTA;
    END IF;
    --------------------------------------------------------------------------------
    -- OBTIENE DIA PAGO
    --IF NOT PKG_PROCESOS_PAC.FN_DIA_PAGO(PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,  
    IF NOT FN_DIA_PAGO_VNET(PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                            V_SCOI_DIA_PAC,
                            V_SCOI_DIA_PAGO,
                            PS_MENSAJE_ERROR)
    THEN
      RAISE EXC_ERR_DIA_PAGO;
    END IF;
    --JORTEGA 
    --28-06-2012 
    -- SE AGREGA CONSULTA TIPO DE INSCRIOCION IMPLICITA O EXPLICITA 
    --DETERMINA INSCRIPCION EXPLICITO O IMPLICITA PARA EL REGISTROS DE BAJA O DESAFILIACION--
    IF NOT PKG_PROCESOS_PAC.FN_IMPLICITO(V_REG_SCO_CUADRATURA_DIARIA.SCCD_PLAN,
                                         PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                         PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                         V_IMPLICITO,
                                         PS_MENSAJE_ERROR)
    THEN
      RAISE EXC_ERR_IMPLICITO;
    END IF;
    IF V_IMPLICITO
    THEN
      V_ESTADO_INSCRIPCION_BAJA := PKG_PROCESOS_PAC.SCOI_ST_INSC_ACEP;
    ELSE
      V_ESTADO_INSCRIPCION_BAJA := PKG_PROCESOS_PAC.SCOI_ST_PENDIENTE_ENVIO;
    END IF;
    -------------------------------------------------------------------------------------------------
    -- MODIFICA EL REGISTRO DE INSCRIPCION E INSERTA EL NUEVO REGISTRO DE BAJA DE INSCRIPCION EN LA TABLA SCOH_INSCRIPCION --
    V_REG_SCOH_INSCRIPCION.SCOIH_ID            := PKG_PROCESOS_PAC.OBTENER_ID_SCOH_INSCRIPCION;
    V_REG_SCOH_INSCRIPCION.SCOIH_SCOI_ID       := V_REG_SCO_INSCRIPCION.SCOI_ID;
    V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_PROCESO := TO_DATE(SYSDATE,
                                                          'DD-MM-RRRR');
    -- LUIS MORA DIAZ --
    -- 08/05/2011 --
    -- EL REGISTRO QUEDA AUTOMATICAMENTE ACEPTADO --
    V_REG_SCOH_INSCRIPCION.SCOIH_STATUS          := V_ESTADO_INSCRIPCION_BAJA;
    V_REG_SCOH_INSCRIPCION.SCOIH_NUMERO_COMERCIO := V_REG_SCO_INSCRIPCION.SCOI_NUMERO_COMERCIO;
    V_REG_SCOH_INSCRIPCION.SCOIH_RUT             := V_REG_SCO_INSCRIPCION.SCOI_RUT;
    V_REG_SCOH_INSCRIPCION.SCOIH_DV              := V_REG_SCO_INSCRIPCION.SCOI_DV;
    V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE          := V_REG_SCO_INSCRIPCION.SCOI_NOMBRE;
    -- SE DA DE BAJA LA CUENTA QUE DEVOLVIO CMR EN EL PROCESO DE INSCRIPCION --
    V_REG_SCOH_INSCRIPCION.SCOIH_CUENTA_TARJETA := V_REG_SCO_INSCRIPCION.SCOI_TARJETA;
    V_REG_SCOH_INSCRIPCION.SCOIH_TARJETA        := V_REG_SCO_INSCRIPCION.SCOI_TARJETA;
    V_REG_SCOH_INSCRIPCION.SCOIH_PRODUCTO       := V_REG_SCO_INSCRIPCION.SCOI_PRODUCTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_MANDATO        := V_REG_SCO_INSCRIPCION.SCOI_MANDATO;
    V_REG_SCOH_INSCRIPCION.SCOIH_ORIGEN         := V_REG_SCO_INSCRIPCION.SCOI_ORIGEN;
    V_REG_SCOH_INSCRIPCION.SCOIH_DIRECCION      := V_REG_SCO_INSCRIPCION.SCOI_DIRECCION;
    V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_INICIO   := V_REG_SCO_INSCRIPCION.SCOI_FECHA_INICIO;
    V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_TERMINO  := V_REG_SCO_INSCRIPCION.SCOI_FECHA_TERMINO;
    V_REG_SCOH_INSCRIPCION.SCOIH_MONTO          := V_REG_SCO_INSCRIPCION.SCOI_MONTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_MONEDA         := V_REG_SCO_INSCRIPCION.SCOI_MONEDA;
    -- MOVIMIENTO DE BAJA DE INSCRIPCION --
    V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_REGISTRO       := PKG_PROCESOS_PAC_VISANET.CODIGO_BAJA_INSC_VN;
    V_REG_SCOH_INSCRIPCION.SCOIH_CONTRATO_PROPUESTA  := V_REG_SCO_INSCRIPCION.SCOI_CONTRATO_PROPUESTA;
    V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAGO            := V_REG_SCO_INSCRIPCION.SCOI_DIA_PAGO;
    V_REG_SCOH_INSCRIPCION.SCOIH_RESPUESTA_CMR       := 0; -- CODIGO ACEPTACION CMR --
    V_REG_SCOH_INSCRIPCION.SCOIH_FECHA_NACIMIENTO    := V_REG_SCO_INSCRIPCION.SCOI_FECHA_NACIMIENTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_SEXO                := V_REG_SCO_INSCRIPCION.SCOI_SEXO;
    V_REG_SCOH_INSCRIPCION.SCOIH_NACIONALIDAD        := V_REG_SCO_INSCRIPCION.SCOI_NACIONALIDAD;
    V_REG_SCOH_INSCRIPCION.SCOIH_CALLE_NUMERO        := V_REG_SCO_INSCRIPCION.SCOI_CALLE_NUMERO;
    V_REG_SCOH_INSCRIPCION.SCOIH_DEPARTAMENTO        := V_REG_SCO_INSCRIPCION.SCOI_DEPARTAMENTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_VILLA               := V_REG_SCO_INSCRIPCION.SCOI_VILLA;
    V_REG_SCOH_INSCRIPCION.SCOIH_CIUDAD              := V_REG_SCO_INSCRIPCION.SCOI_CIUDAD;
    V_REG_SCOH_INSCRIPCION.SCOIH_COMUNA              := V_REG_SCO_INSCRIPCION.SCOI_COMUNA;
    V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_DIRECCION      := V_REG_SCO_INSCRIPCION.SCOI_TIPO_DIRECCION;
    V_REG_SCOH_INSCRIPCION.SCOIH_ZONA                := V_REG_SCO_INSCRIPCION.SCOI_ZONA;
    V_REG_SCOH_INSCRIPCION.SCOIH_CENTRAL             := V_REG_SCO_INSCRIPCION.SCOI_CENTRAL;
    V_REG_SCOH_INSCRIPCION.SCOIH_FONO                := V_REG_SCO_INSCRIPCION.SCOI_FONO;
    V_REG_SCOH_INSCRIPCION.SCOIH_CICLO               := V_REG_SCO_INSCRIPCION.SCOI_CICLO;
    V_REG_SCOH_INSCRIPCION.SCOIH_CODIGO_ENTIDAD      := V_REG_SCO_INSCRIPCION.SCOI_CODIGO_ENTIDAD;
    V_REG_SCOH_INSCRIPCION.SCOIH_CODIGO_PRODUCTO     := V_REG_SCO_INSCRIPCION.SCOI_CODIGO_PRODUCTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_FILLER              := V_REG_SCO_INSCRIPCION.SCOI_FILLER;
    V_REG_SCOH_INSCRIPCION.SCOIH_STRING_ENVIADO      := V_REG_SCO_INSCRIPCION.SCOI_STRING_ENVIADO;
    V_REG_SCOH_INSCRIPCION.SCOIH_STRING_DEVUELTO     := V_REG_SCO_INSCRIPCION.SCOI_STRING_DEVUELTO;
    V_REG_SCOH_INSCRIPCION.SCOIH_DIA_PAC             := V_REG_SCO_INSCRIPCION.SCOI_DIA_PAC;
    V_REG_SCOH_INSCRIPCION.SCOIH_DIA_FAC             := V_REG_SCO_INSCRIPCION.SCOI_DIA_FAC;
    V_REG_SCOH_INSCRIPCION.SCOIH_MEDIO_PAGO          := V_REG_SCO_INSCRIPCION.SCOI_MEDIO_PAGO;
    V_REG_SCOH_INSCRIPCION.SCOIH_SCIN_ID_INSCRIPCION := V_REG_SCO_INSCRIPCION.SCOI_SCIN_ID_INSCRIPCION;
    V_REG_SCOH_INSCRIPCION.SCOIH_USUARIO             := PKG_PROCESOS_PAC.OBTENER_USUARIO;
    V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE_ARCHIVO      := NULL;
    V_REG_SCOH_INSCRIPCION.SCOIH_CAME_TP_TRANSAC     := PKG_PROCESOS_PAC.CAME_TP_TRANSAC_END_CUALIT;
    V_REG_SCOH_INSCRIPCION.SCOIH_CAME_CD_MOTIVO      := PKG_PROCESOS_PAC.CAME_CD_MOTIVO_CAMB_CUENTA_CMR;
    V_REG_SCOH_INSCRIPCION.SCOIH_TP_INSCRIPCION      := V_REG_SCO_INSCRIPCION.SCOI_TP_INSCRIPCION;
    -------------------------------------------------------------------------------------------------
    -- GRABA REGISTRO DE BAJA EN LA TABLA SCOH_INSCRIPCION --
    IF (PKG_PROCESOS_PAC.GRABAR_SCOH_INSCRIPCION(V_REG_SCOH_INSCRIPCION,
                                                 V_MENSAJE_ERROR) = FALSE)
    THEN
      RAISE EXC_ERR_GRABAR_SCOH_INSC;
    END IF;
    -- SE GRABA LA NUEVA TARJETA EN LA INSCRIPCION --
    V_REG_SCO_INSCRIPCION.SCOI_CUENTA_TARJETA := PE_NU_CUENTA_NEW;
    V_REG_SCO_INSCRIPCION.SCOI_TARJETA        := PE_NU_CUENTA_NEW;
    -------------------------------------------------------------------------------------------------
    --JORTEGA 
    --28-06-2012 
    -- SE AGREGA CONSULTA TIPO DE INSCRIOCION IMPLICITA O EXPLICITA 
    --DETERMINA INSCRIPCION EXPLICITO O IMPLICITA PARA EL REGISTROS DE BAJA O DESAFILIACION--
    IF NOT PKG_PROCESOS_PAC.FN_IMPLICITO(V_REG_SCO_CUADRATURA_DIARIA.SCCD_PLAN,
                                         PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                         PKG_PROCESOS_PAC_VISANET.CODIGO_ALTA_INSC_VN,
                                         V_IMPLICITO,
                                         PS_MENSAJE_ERROR)
    THEN
      RAISE EXC_ERR_IMPLICITO;
    END IF;
    IF V_IMPLICITO
    THEN
      V_ESTADO_INSCRIPCION_ALTA := PKG_PROCESOS_PAC.SCOI_ST_INSC_ACEP;
    ELSE
      V_ESTADO_INSCRIPCION_ALTA := PKG_PROCESOS_PAC.SCOI_ST_PENDIENTE_ENVIO;
    END IF;
    -- INSERTA EL NUEVO REGISTRO DE ALTA DE INSCRIPCION EN LA TABLA SCOH_INSCRIPCION -- 
    V_REG_SCOH_INSCRIPCION.SCOIH_ID             := V_ESTADO_INSCRIPCION_ALTA;
    V_REG_SCOH_INSCRIPCION.SCOIH_ID             := PKG_PROCESOS_PAC.OBTENER_ID_SCOH_INSCRIPCION;
    V_REG_SCOH_INSCRIPCION.SCOIH_RUT            := V_NUMERO_DOCUMENTO_CLIENTE;
    V_REG_SCOH_INSCRIPCION.SCOIH_DV             := V_TIPO_DOCUMENTO_CLIENTE;
    V_REG_SCOH_INSCRIPCION.SCOIH_NOMBRE         := SUBSTR(V_NOMBRE_CLIENTE,
                                                          1,
                                                          36);
    V_REG_SCOH_INSCRIPCION.SCOIH_CUENTA_TARJETA := V_REG_SCO_INSCRIPCION.SCOI_CUENTA_TARJETA;
    V_REG_SCOH_INSCRIPCION.SCOIH_TARJETA        := V_REG_SCO_INSCRIPCION.SCOI_TARJETA;
    V_REG_SCOH_INSCRIPCION.SCOIH_DIRECCION      := SUBSTR(V_DIRECCION_CLIENTE,
                                                          1,
                                                          36);
    V_REG_SCOH_INSCRIPCION.SCOIH_TIPO_REGISTRO  := PKG_PROCESOS_PAC_VISANET.CODIGO_ALTA_INSC_VN;
    V_REG_SCOH_INSCRIPCION.SCOIH_FONO           := V_REG_SCO_INSCRIPCION.SCOI_ID; -- SE GUARDA EL ID DE LA INSCRIPCION --
    -------------------------------------------------------------------------------------------------
    -- GRABA REGISTRO DE ALTA EN LA TABLA SCOH_INSCRIPCION --
    IF (PKG_PROCESOS_PAC.GRABAR_SCOH_INSCRIPCION(V_REG_SCOH_INSCRIPCION,
                                                 V_MENSAJE_ERROR) = FALSE)
    THEN
      RAISE EXC_ERR_GRABAR_SCOH_INSC;
    END IF;
    -------------------------------------------------------------------------------------------------
    V_REG_SCO_INSCRIPCION.SCOI_STATUS         := PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_VN;
    V_REG_SCO_INSCRIPCION.SCOI_RESPUESTA_CMR  := 0; -- CODIGO ACEPTACION CMR --
    V_REG_SCO_INSCRIPCION.SCOI_FECHA_PROCESO  := TO_DATE(SYSDATE,
                                                         'DD-MM-RRRR');
    V_REG_SCO_INSCRIPCION.SCOI_CUENTA_TARJETA := PE_NU_CUENTA_NEW;
    V_REG_SCO_INSCRIPCION.SCOI_TARJETA        := PE_NU_CUENTA_NEW;
    V_REG_SCO_INSCRIPCION.SCOI_CLIENTE_NUEVO  := NULL;
    -- SE MODIFICA REGISTRO EN LA TABLA SCO_INSCRIPCION --
    IF (PKG_PROCESOS_PAC.MODIFICAR_SCO_INSCRIPCION(V_REG_SCO_INSCRIPCION,
                                                   V_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_MODIFICAR_SCO_INSC;
    END IF;
    -------------------------------------------------------------------------------------------------
    --JORTEGA 28-05-2012 
    -- SE COMENTA ACTUALIZACION DE REGISTROS TRASPASADOS
    /*
    -- SE MODIFICA CUENTA DE CUOTAS ENVIADAS A COBRANZA --
    FOR C IN CUOTAS_PEND_REZAG_ENVIADAS LOOP
        -- SI SEMAFORO NO ESTA ACTIVO SE MODIFICA L ACUENTA DE ENVIO --
        IF NOT PKG_SEMAFORO_PAC.ESTADO_SEMAFORO (C.CCTE_MEDIO_PAGO,C.CCTE_FECHA_ENVIO_PAC) THEN
           ------------------------------------------------------------------------------------------
           -- SE MODIFICAN DATOS DE LA CUENTA CORRIENTE --
           UPDATE SCO_PLANO_ENVIO_CARGOS
              SET TARJETA_CMR         = PE_NU_CUENTA_NEW  -- SOLO PARA TARJETAS CMR --
            WHERE NUMERO_DOCUMENTO    = C.CCTE_ID
              AND MEDIO_PAGO          = C.CCTE_MEDIO_PAGO
              AND FECHA_CARGA_ARCHIVO = C.CCTE_DIA_PAC_PROCESADO;
           ------------------------------------------------------------------------------------------
        END IF;
    END LOOP;
    */
    -------------------------------------------------------------------------------------------------
    -- SE ACTUALIZA ESTADO DE CUADRATURA DIARIA --
    UPDATE SCO_CUADRATURA_DIARIA
       SET SCCD_STATUS = PKG_PROCESOS_PAC_VISANET.SCCD_ST_INSC_CAMBIO_CTA_VN
     WHERE SCCD_OPERACION IN (1001,
                              9999)
       AND SCCD_SUB_OPERACION = 0
       AND SCCD_PROPUESTA = PE_NU_PROPUESTA;
    -------------------------------------------------------------------------------------------------
    -- LUIS MORA DIAZ  19-01-2012 --
    -- SE GRABA BITACORA DE CAMBIO DE MEDIO DE PAGO --
    IF (PKG_PROCESOS_PAC.FN_END_GRABAR_BIT_CAMB_CUENTA(PE_NU_PROPUESTA,
                                                       PE_NU_ENDOSO,
                                                       PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                                       PKG_PROCESOS_PAC.CODIGO_MEDIO_PAGO_VISANET,
                                                       PE_NU_CUENTA_ANT,
                                                       PE_NU_CUENTA_NEW,
                                                       V_SCCD_FRAGMENT,
                                                       V_SCCD_FRAGMENT,
                                                       PE_CD_PUNTO_VENTA,
                                                       PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_GRAB_BIT_CAMB_CTA;
    END IF;
    -------------------------------------------------------------------------------------------------
    PS_MENSAJE_ERROR := NULL;
    -------------------------------------------------------------------------------------------------
    RETURN TRUE;
    -------------------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_ST_CUADRATURA THEN
      PS_MENSAJE_ERROR := 'EL ESTADO DE LA CUADRATURA DIARIA NO ES VALIDO PARA EL ENDOSO DE CAMBIO DE CUENTA CMR';
      RETURN FALSE;
    WHEN EXC_ERR_NO_EXIST_CUADRATURA THEN
      PS_MENSAJE_ERROR := 'NO EXISTE CUADRATURA DIARIA ASOCIADA A LA PROPUESTA ' ||
                          PE_NU_PROPUESTA;
      RETURN FALSE;
    WHEN EXC_ERR_ST_INSCRIPCION THEN
      PS_MENSAJE_ERROR := 'EL ESTADO DE LA INSCRIPCION CMR NO ES VALIDO PARA EL ENDOSO DE CAMBIO DE CUENTA CMR';
      RETURN FALSE;
    WHEN EXC_ERR_NO_EXIST_INSC THEN
      PS_MENSAJE_ERROR := 'NO EXISTE INSCRIPCION CMR ASOCIADA A LA PROPUESTA ' ||
                          PE_NU_PROPUESTA;
      RETURN FALSE;
    WHEN EXC_ERR_SIGUIENTE_FECPAC THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER FECHA PAC DE INSCRIPCION. ' ||
                          PS_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_DATOS_CUENTA THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER DATOS CUENTA BANCO. ' ||
                          PS_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_DIA_PAGO THEN
      PS_MENSAJE_ERROR := 'ERROR AL OBTENER DIA PAGO DE INSCRIPCION. ' ||
                          PS_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_IMPLICITO THEN
      PS_MENSAJE_ERROR := 'ERROR AL TIPO DE INSCRIPCION DEL PRODUCTO NO CONFIGURADO. ' ||
                          PS_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_GRABAR_SCOH_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL GRABAR EL REGISTRO DE ENDOSO DE CAMBIO DE CUENTA CMR EN TABLA DE INSCRIPCION HISTORICA. ' ||
                          V_MENSAJE_ERROR;
      RETURN FALSE;
    WHEN EXC_ERR_MODIFICAR_SCO_INSC THEN
      PS_MENSAJE_ERROR := 'ERROR AL MODIFICAR EL REGISTRO DE INSCRIPCION ASOCIADO A LA PROPUESTA ' ||
                          PE_NU_PROPUESTA;
      RETURN FALSE;
    WHEN EXC_ERR_OBTENER_CLIENTES THEN
      PS_MENSAJE_ERROR := 'ERROR AL BUSCAR DATOS DE CLIENTES. ' || SQLERRM;
      RETURN FALSE;
    WHEN EXC_ERR_GRAB_BIT_CAMB_CTA THEN
      PS_MENSAJE_ERROR := 'ERROR AL GRABAR BITACORA CAMBIO DE CUENTA. ' ||
                          SQLERRM;
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := SQLERRM;
      RETURN FALSE;
  END FN_END_CAMBIO_CUENTA;

  -------------------------------------------------------------------------------------------------
  FUNCTION FN_REZAGAR_CUOTA_RECTOR(PE_CCTE_PROPUESTA       IN NUMBER,
                                   PE_CCTE_ID              IN NUMBER,
                                   PE_CCTE_RECIBO          IN NUMBER,
                                   PE_CCTE_TARJETA         IN VARCHAR2,
                                   PE_FECHA_PROCESO_PAC    IN DATE,
                                   PE_CCTE_MONEDA          IN VARCHAR2,
                                   PE_CCTE_MONTO_MONEDA    IN NUMBER,
                                   PE_CCTE_NU_ENDOSO       IN NUMBER,
                                   PE_CCTE_CUOTA           IN NUMBER,
                                   PE_CCTE_FECHA_ENVIO_PAC IN DATE,
                                   PS_MENSAJE_ERROR        IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    V_CCTE_MONTO_PESOS       NUMBER := 0;
    V_TASA_CAMBIO            NUMBER := 1;
    V_CD_PAIS                NUMBER := 0;
    V_EXISTE_CUOTA_FACTURADA NUMBER := 0;
    EXC_ERR_CALCULO_TASA_CAMBIO EXCEPTION;
    EXC_ERR_OBT_CODIGO_PAIS     EXCEPTION;
  BEGIN
    -------------------------------------------------------------------------------------
    -- SE OBTIENE LA TASA DE CAMBIO DE LA MONEDA DE LA CUOTA --
    V_CCTE_MONTO_PESOS := NULL;
    IF (PKG_PROCESOS_PAC.FN_CALCULO_TASA_CAMBIO_PAC(TO_DATE(PE_FECHA_PROCESO_PAC,
                                                            'DD-MM-RRRR'),
                                                    PE_CCTE_MONEDA,
                                                    PE_CCTE_MONTO_MONEDA,
                                                    V_CCTE_MONTO_PESOS,
                                                    V_TASA_CAMBIO,
                                                    PS_MENSAJE_ERROR) =
       FALSE)
    THEN
      RAISE EXC_ERR_CALCULO_TASA_CAMBIO;
    END IF;
    -------------------------------------------------------------------------------------
    -- SE OBTIENE EL CODIGO DEL PAIS --
    V_CD_PAIS := NULL;
    IF (PKG_PROCESOS_PAC.FN_OBTENER_CODIGO_PAIS(V_CD_PAIS,
                                                PS_MENSAJE_ERROR) = FALSE)
    THEN
      RAISE EXC_ERR_OBT_CODIGO_PAIS;
    END IF;
    -------------------------------------------------------------------------------------
    -- SOLO PARA PERU SE REDONDEA A 2 DECIMALES EN MONTO PESOS --
    IF (V_CD_PAIS = PKG_PROCESOS_PAC_VISANET.CODIGO_PAIS_PERU)
    THEN
      V_CCTE_MONTO_PESOS := ROUND(V_CCTE_MONTO_PESOS,
                                  3);
    ELSE
      V_CCTE_MONTO_PESOS := ROUND(V_CCTE_MONTO_PESOS,
                                  0);
    END IF;
    -------------------------------------------------------------------------------------
    INSERT INTO BATB_CUOTAS_REZAGADAS
      (BATB_FE_PAC,
       BATB_TP_REGISTRO,
       BATB_CACE_NU_PROPUESTA,
       BATB_NU_ENDOSO,
       BATB_CARE_NU_CUOTA,
       BATB_CARE_SUB_CUOTA,
       BATB_CD_ORIGEN,
       BATB_CAMO_CD_MONEDA,
       BATB_CATA_TA_CAMBIO,
       BATB_MT_CUOTA,
       BATB_MT_PESOS,
       BATB_ST_PAC,
       BATB_CADM_NU_CUENTA,
       BATB_NU_ENVIOS,
       BATB_FE_ULTIMO_ENVIO,
       BATB_IN_REPACTACION)
    VALUES
      (PE_CCTE_FECHA_ENVIO_PAC,
       'C',
       PE_CCTE_PROPUESTA,
       PE_CCTE_NU_ENDOSO,
       PE_CCTE_CUOTA,
       0,
       0,
       PE_CCTE_MONEDA,
       V_TASA_CAMBIO,
       PE_CCTE_MONTO_MONEDA,
       V_CCTE_MONTO_PESOS,
       1,
       PE_CCTE_TARJETA,
       1,
       SYSDATE,
       'N');
    SELECT COUNT(1)
      INTO V_EXISTE_CUOTA_FACTURADA
      FROM BATB_CUOTAS_FACTURADAS
     WHERE BATB_FE_PAC >= PE_CCTE_FECHA_ENVIO_PAC
          --AND BATB_TP_REGISTRO       = 'C'
       AND BATB_CACE_NU_PROPUESTA = PE_CCTE_PROPUESTA
       AND BATB_NU_ENDOSO = PE_CCTE_NU_ENDOSO
       AND BATB_CARE_NU_CUOTA = PE_CCTE_CUOTA
       AND BATB_CARE_SUB_CUOTA = 0;
    IF (V_EXISTE_CUOTA_FACTURADA = 0)
    THEN
      INSERT INTO BATB_CUOTAS_FACTURADAS
        (BATB_FE_PAC,
         BATB_CACE_NU_PROPUESTA,
         BATB_NU_ENDOSO,
         BATB_CARE_NU_CUOTA,
         BATB_CARE_SUB_CUOTA,
         BATB_CD_ORIGEN,
         BATB_CAMO_CD_MONEDA,
         BATB_CATA_TA_CAMBIO,
         BATB_MT_CUOTA,
         BATB_MT_PESOS,
         BATB_ST_PAC,
         BATB_CADM_NU_CUENTA)
      VALUES
        (PE_CCTE_FECHA_ENVIO_PAC,
         PE_CCTE_PROPUESTA,
         PE_CCTE_NU_ENDOSO,
         PE_CCTE_CUOTA,
         0,
         0,
         PE_CCTE_MONEDA,
         V_TASA_CAMBIO,
         PE_CCTE_MONTO_MONEDA,
         V_CCTE_MONTO_PESOS,
         1,
         PE_CCTE_TARJETA);
    ELSE
      -- ACTUALIZA BATB_CUOTAS_FACTURADAS --
      UPDATE BATB_CUOTAS_FACTURADAS
         SET BATB_ST_PAC       = PKG_PROCESOS_PAC_VISANET.BATB_ST_PAC_EN_PROCESO,
             BATB_FE_CARGO_CMR = PE_FECHA_PROCESO_PAC
       WHERE BATB_FE_PAC >= PE_CCTE_FECHA_ENVIO_PAC
            --AND BATB_TP_REGISTRO       = 'C'
         AND BATB_CACE_NU_PROPUESTA = PE_CCTE_PROPUESTA
         AND BATB_NU_ENDOSO = PE_CCTE_NU_ENDOSO
         AND BATB_CARE_NU_CUOTA = PE_CCTE_CUOTA
         AND BATB_CARE_SUB_CUOTA = 0;
    END IF;
    -------------------------------------------------------------------------------------
    RETURN TRUE;
    -------------------------------------------------------------------------------------
  EXCEPTION
    WHEN EXC_ERR_CALCULO_TASA_CAMBIO THEN
      RETURN FALSE;
    WHEN OTHERS THEN
      PS_MENSAJE_ERROR := 'ERROR AL REZAGAR CUOTA ID ' || PE_CCTE_ID ||
                          ' EN RECTOR. ' || SQLERRM;
      RETURN FALSE;
  END FN_REZAGAR_CUOTA_RECTOR;

  -------------------------------------------------------------------------------------------------
  FUNCTION GENERA_SOLICITUD_ANULACION(PE_NU_PROPUESTA IN CART_CERTIFICADOS.CACE_NU_PROPUESTA%TYPE,
                                      PE_CD_MOTIVO    IN CART_OPERACIONES_DIARIAS.CAOD_CAME_CD_MOTIVO%TYPE,
                                      PE_MSJ          IN OUT VARCHAR2)
    RETURN BOOLEAN IS
    CD_CLASE VARCHAR2(2);
    NEXISTE  NUMBER(1);
    --NEXISTE1     NUMBER;
    ST_SOL       VARCHAR2(1);
    NACIONALIDAD VARCHAR2(1);
    CEDULA_RIF   VARCHAR2(9);
    ESTADO       NUMBER;
    FECHA_HASTA  DATE;
    --BOTON        NUMBER;
    --PERMISO4     NUMBER(3);
    --CONTROLTIPO4 NUMBER(1);
    --CONTROLCANAL NUMBER(1);
    --
    V_CSAP_CASU_CD_SUCURSAL    CART_SOLICITUDES_TRANSACCIONES.CSAP_CASU_CD_SUCURSAL%TYPE;
    V_CSAP_CAPO_NU_POLIZA      CART_SOLICITUDES_TRANSACCIONES.CSAP_CAPO_NU_POLIZA%TYPE;
    V_CSAP_CACE_NU_CERTIFICADO CART_SOLICITUDES_TRANSACCIONES.CSAP_CACE_NU_CERTIFICADO%TYPE;
    V_CSAP_CARP_CD_RAMO        CART_SOLICITUDES_TRANSACCIONES.CSAP_CARP_CD_RAMO%TYPE;
    V_CSAP_CAPJ_CD_SUCURSAL    CART_SOLICITUDES_TRANSACCIONES.CSAP_CAPJ_CD_SUCURSAL%TYPE;
    V_CSAP_CAME_TP_TRANSAC     CART_SOLICITUDES_TRANSACCIONES.CSAP_CAME_TP_TRANSAC%TYPE;
    V_CSAP_NU_SOLICITUD        CART_SOLICITUDES_TRANSACCIONES.CSAP_NU_SOLICITUD%TYPE;
    --
    V_C_PRODUCTO CART_PRODUCTOS.CAPU_CD_PRODUCTO%TYPE;
    V_C_PLAN     CART_CERTIFICADOS.CACE_CAPB_CD_PLAN%TYPE;
    V_RUT        CART_CLIENTES.CACN_NU_DOCUMENTO%TYPE;
    --
    EX_ERR_PROPUESTA           EXCEPTION;
    EX_ERR_ANULACION_PENDIENTE EXCEPTION;
    EX_ERR_ANULACION_PROCESADA EXCEPTION;
    EX_ERR_ELIMINADA           EXCEPTION;
    EXC_ERR_OP_DIARIAS         EXCEPTION;
  BEGIN
    BEGIN
      SELECT CACE_CASU_CD_SUCURSAL,
             CACE_CAPO_NU_POLIZA,
             CACE_NU_CERTIFICADO,
             CAPU_CACY_CD_CLASE,
             CACE_CAPU_CD_PRODUCTO,
             CACE_CAPB_CD_PLAN,
             CACE_CACN_CD_NACIONALIDAD,
             CACE_CACN_NU_CEDULA_RIF,
             CACE_CARP_CD_RAMO,
             CACE_ST_CERTIFICADO,
             CACE_FE_HASTA,
             CACN_NU_DOCUMENTO
        INTO V_CSAP_CASU_CD_SUCURSAL,
             V_CSAP_CAPO_NU_POLIZA,
             V_CSAP_CACE_NU_CERTIFICADO,
             CD_CLASE,
             V_C_PRODUCTO,
             V_C_PLAN,
             NACIONALIDAD,
             CEDULA_RIF,
             V_CSAP_CARP_CD_RAMO,
             ESTADO,
             FECHA_HASTA,
             V_RUT
        FROM CART_CERTIFICADOS,
             CART_PRODUCTOS,
             CART_CLIENTES
       WHERE CAPU_CARP_CD_RAMO = CACE_CARP_CD_RAMO
         AND CAPU_CD_PRODUCTO = CACE_CAPU_CD_PRODUCTO
         AND CACE_NU_PROPUESTA = PE_NU_PROPUESTA
         AND CACE_ST_CERTIFICADO IN (1,
                                     11,
                                     95)
            -- CLIENTE.
         AND CACE_CACN_CD_NAC_DEUDOR = CACN_CD_NACIONALIDAD
         AND CACE_CACN_NU_CED_DEUDOR = CACN_NU_CEDULA_RIF;
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EX_ERR_PROPUESTA;
    END;
    --
    IF ESTADO = 11
    THEN
      RAISE EX_ERR_ELIMINADA;
    END IF;
    BEGIN
      SELECT 1,
             CSAP_ST_SOLICITUD
        INTO NEXISTE,
             ST_SOL
        FROM CART_SOLICITUDES_TRANSACCIONES
       WHERE CSAP_CACE_NU_PROPUESTA = PE_NU_PROPUESTA
         AND CSAP_ST_SOLICITUD = '1';
      RAISE EX_ERR_ANULACION_PENDIENTE;
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        RAISE EX_ERR_ANULACION_PENDIENTE;
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    BEGIN
      SELECT 1,
             CSAP_ST_SOLICITUD
        INTO NEXISTE,
             ST_SOL
        FROM CART_SOLICITUDES_TRANSACCIONES
       WHERE CSAP_CACE_NU_PROPUESTA = PE_NU_PROPUESTA
         AND CSAP_ST_SOLICITUD = '2';
      RAISE EX_ERR_ANULACION_PROCESADA;
    EXCEPTION
      WHEN TOO_MANY_ROWS THEN
        RAISE EX_ERR_ANULACION_PROCESADA;
      WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    --
    --
    V_CSAP_CAME_TP_TRANSAC := 'A';
    -- CREA OPERACION DIARIA..
    BEGIN
      INSERT INTO CART_OPERACIONES_DIARIAS
        (CAOD_CAOP_CD_OPERACION,
         CAOD_CD_SUB_OPERACION,
         CAOD_FE_OPERACION,
         CAOD_HR_OPERACION,
         CAOD_CD_USUARIO,
         CAOD_CASU_CD_SUCURSAL,
         CAOD_CARP_CD_RAMO,
         CAOD_CAPO_NU_POLIZA,
         CAOD_CACE_NU_CERTIFICADO,
         CAOD_CASO_NU_SOLICITUD,
         CAOD_NU_DOCUMENTO,
         CAOD_CAPJ_CD_SUCURSAL_BANCO,
         CAOD_CAME_CD_MOTIVO)
      VALUES
        (1017,
         0,
         SYSDATE,
         SYSDATE,
         SUBSTR(USER,
                1,
                10),
         V_CSAP_CASU_CD_SUCURSAL,
         V_CSAP_CARP_CD_RAMO,
         V_CSAP_CAPO_NU_POLIZA,
         V_CSAP_CACE_NU_CERTIFICADO,
         PE_NU_PROPUESTA,
         V_RUT, -- DEDDE CART_CLIENTE..
         V_CSAP_CAPJ_CD_SUCURSAL,
         PE_CD_MOTIVO -- DECODE(TO_NUMBER( V_COD_RESPUESTA_JOURNAL ),1,91, 2, 25, 10, 18, NULL)
         );
    EXCEPTION
      WHEN OTHERS THEN
        RAISE EXC_ERR_OP_DIARIAS;
    END;
    -- OBTENGO SECUENCIA DE NUMERO DE SOLICITUD..
    SELECT SEQ_SOLICITUD_ANULACION.NEXTVAL
      INTO V_CSAP_NU_SOLICITUD
      FROM DUAL;
    ------------------------------------------
    -- SCV  08-04-2011  --  SE COMENTA CAMPO  CSAP_CAPJ_CD_SUC_ENDOSO PARA COMPILAR  EN TEST !!
    ------------------------------------------
    -- CREA SOLICITUD DE ANULACION..
    INSERT INTO CART_SOLICITUDES_TRANSACCIONES
      (CSAP_NU_SOLICITUD,
       CSAP_CASU_CD_SUCURSAL,
       CSAP_CARP_CD_RAMO,
       CSAP_CAPO_NU_POLIZA,
       CSAP_CACE_NU_CERTIFICADO,
       CSAP_CAME_TP_TRANSAC,
       CSAP_CAME_CD_MOTIVO,
       CSAP_CAPJ_CD_SUCURSAL,
       CSAP_CAUS_CD_USUARIO,
       CSAP_FE_REGISTRO,
       CSAP_FE_SOLICITUD,
       CSAP_ST_SOLICITUD,
       CSAP_FE_STATUS,
       CSAP_DE_OBSERVACIONES,
       CSAP_CACE_NU_PROPUESTA,
       CSAP_CAPD_CD_PRODUCTOR
       --,CSAP_CAZB_NU_COTIZACION
       --,CSAP_CAZB_NU_CONSECUTIVO
       --,CSAP_NU_ENDOSO
       --,CSAP_CAPJ_CD_SUC_ENDOSO
       )
    VALUES
      (V_CSAP_NU_SOLICITUD,
       V_CSAP_CASU_CD_SUCURSAL,
       V_CSAP_CARP_CD_RAMO,
       V_CSAP_CAPO_NU_POLIZA,
       V_CSAP_CACE_NU_CERTIFICADO,
       V_CSAP_CAME_TP_TRANSAC,
       PE_CD_MOTIVO,
       V_CSAP_CAPJ_CD_SUCURSAL,
       SUBSTR(USER,
              1,
              10),
       SYSDATE,
       TRUNC(SYSDATE),
       '1',
       SYSDATE,
       'ANULACION POR CARGA DE RESPUESTA DE PROCESO DE COBRANZA PAC, RESPUESTA INDICA QUE SE DEBE ANULAR LA PROPUESTA', -- OBSERVACIONES
       PE_NU_PROPUESTA,
       NULL
       --,NULL
       --,NULL
       --,NULL
       --,NULL
       );
    --
    RETURN(TRUE);
    --
  EXCEPTION
    WHEN EX_ERR_PROPUESTA THEN
      PE_MSJ := 'ERROR AL RECUPERAR PROPUESTA :' || SQLERRM;
      RETURN(FALSE);
    WHEN EX_ERR_ANULACION_PENDIENTE THEN
      PE_MSJ := 'ERROR ANULACION PENDIENTE';
      RETURN(TRUE);
    WHEN EX_ERR_ANULACION_PROCESADA THEN
      PE_MSJ := 'ERROR ANULACION PROCESADA';
      RETURN(TRUE);
    WHEN EX_ERR_ELIMINADA THEN
      PE_MSJ := 'ERROR ANULACION ELIMINADA';
      RETURN(TRUE);
    WHEN EXC_ERR_OP_DIARIAS THEN
      PE_MSJ := 'ERROR AL CREAR OPERACION DIARIA';
      RETURN(FALSE);
    WHEN OTHERS THEN
      PE_MSJ := SQLERRM;
      RETURN(FALSE);
  END GENERA_SOLICITUD_ANULACION;

-------------------------------------------------------------------------------------------------
END PKG_PROCESOS_PAC_VISANET;
/
